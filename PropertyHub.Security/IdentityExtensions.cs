﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using PropertyHub.Data;

namespace PropertyHub.Security
{
   public static  class IdentityExtensions
    {
       public static string GetUserNames(this IIdentity itentity)
       {
           using (var context = new ApplicationDbContext())
           {
               var user = context.Users.FirstOrDefault(a => a.UserName == itentity.Name);
               if (user != null)
               {
                   return user.FirstName + " " + user.LastName;
               }
               else
               {
                   return itentity.Name;
               }
           }
       }

      
    }
}
