﻿//Tsing
//Created on 


// modified by adam 2015-08-06
// add customOptions param to customarize the feature of kendoGrid
// i.e. UIGrid.Init(tableId, url, columns, model, {selectable: "multiple"});

var UIGrid = function () {

    var self = this;
    self.tableId = "";
    self.url = "";
    self.columns = [];
    self.model = {};
    self.sort = {};


    function onDataBound(arg) {
        $("a.ajax").ajaxLink();
    }


    var handleInit = function (customOptions) {
        var options = {
            dataSource: {
                type: "odata-v4",
                transport: {
                    read: self.url//"/odata/SubscriptionTests"
                },
                schema: {
                    model: self.model
                },

                change: function (data) {
                    //  $("a.ajax").ajaxLink();
                },

                pageSize: 15,
                serverPaging: true,
                serverFiltering: true,
                sort: self.sort,
                serverSorting: true
                // scrollable: false
            },
            selectable: "single",
            height: 540,
          //  sort:self.sort,
            dataBound: onDataBound,
            filterable: true,
            sortable: true,
            // pageable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns:
                self.columns
        };

        //merge two options
        if (customOptions != undefined) {
            for (prop in customOptions) {
                options[prop] = customOptions[prop];
            }
        }

        $(self.tableId).kendoGrid(options);
    }

    return {
        Init: function (_tableId, _url, _columns, _model, _custom_options,_sort) {
            self.url = _url;
            self.columns = _columns;
            self.tableId = _tableId;
            self.model = _model;
            self.sort = _sort;

            handleInit(_custom_options);
        }
    }
}();