$(document).ready(function () {
    $("#side-menu li[class!='nav-header'] a").ajaxLink();


    //Tsing
    //added by Tsing on 8/24/2015
    //put a timer to retrieve the data from the server side.

    $('#divId').timer({
        duration: '5m30s',
        callback: function () {
            alert('Time up!');
        }
    });


    //Tsing 
    //get the modules from the database.
    

    //$(options.target).empty();
    $("#side-menu").append("<div id='menuLoading' class='sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>");


    $.ajax({
        url: "/Module",
        type: 'GET',
        async: true,
        cache: false,
        success: function(data) {
            //  alert(data);
            // $("#side-menu").remove("<i class='icon-spinner icon-spin icon-2x pull-left'></i>Loading...");

            $("#menuLoading").remove();
            $("#side-menu").append(data);

            $("#side-menu li[class!='nav-header'] a").ajaxLink();

            $('#side-menu').metisMenu();
        },
        error: function(e) {
            alert(e);
        }
    });
    
    //show the dashboard
    $.ajax({
        url: "/dashboard",
        type: 'GET',
        async: true,
        cache: false,
        success: function (data) {
            $("#mainContent").append(data);
        },
        error: function (e) {
            alert(e);
        }
    });



});
(function ($) {
    $.fn.extend({
        ajaxLink: function (options) {

            var defaults = {
                target: '#mainContent'
            };

            var options = $.extend(defaults, options);

            var clickHandler = function () {
                var url = $(this).attr('href');
                if (url != "" && url != "#") {
                    $(options.target).empty();
                    $(options.target).append("<div id='menuLoading' class=' sk-spinner sk-spinner-three-bounce'><div class='sk-bounce1'></div><div class='sk-bounce2'></div><div class='sk-bounce3'></div></div>");

                    $.ajax({
                        url: url,
                        type: 'GET',
                        async: true,
                        cache: false,
                        success: function (data) {
                            
                            $(options.target).empty();
                            $(options.target).append(data);
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });
                    return false;
                }
            };

            return this.each(function () {
                $(this).unbind('click');
                $(this).bind('click', clickHandler);
            });
        }
    });
})(jQuery);