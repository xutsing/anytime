﻿var databodylabel = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridbodylabel";

            var columns = [
           {
               field: "data_body_label_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_body_label_text",
                title: "评分标准值"
            },
           {
               template: "<a href='/bodylabel/Edit/#:data.data_body_label_id#' class='ajax'>编辑</> | <a href='javascript:databodylabel.deleteRow(#:data_body_label_id#)'>删除</>",
               field: "data_body_label_id",
               title: "操作",
               width: 150
           }];

            var url = "/odata/BodyLabelDTO";


            var model = {
                fields: {
                    data_body_label_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/bodylabel/Delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



