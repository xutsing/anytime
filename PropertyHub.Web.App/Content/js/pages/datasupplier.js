﻿var datasupplier = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridsupplier";

            var columns = [
           {
               field: "data_supplier_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_supplier_name",
                title: "公司名称",
            },
            {
                field: "data_supplier_address",
                title: "公司地址",
            },
              {
                  field: "data_supplier_phone",
                  title: "公司电话",
              },
              {
                  field: "data_suplier_contracts",
                  title: "联系人",
              },
           //{
           //    template: "<a href='/supplier/Edit/#:data.data_supplier_id#' class='ajax'>编辑</> | <a href='javascript:datasupplier.deleteRow(#:data.data_supplier_id#)'>删除</>",
           //    field: "data_supplier_id",
           //    title: "操作",
           //    width: 150
           //}
            ];

            var url = "/odata/SupplierDTO";


            var model = {
                fields: {
                    data_supplier_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            // $("#data_service_validate_months").kendoNumericTextBox({ format: "n0" });
            // $("#data_service_price").kendoNumericTextBox({
            //    format: "c",
            //    decimals: 3
            //});
            //$("#data_service_status").kendoComboBox();
        },

        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridsupplier").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_supplier_id;

            $.ajax({
                url: "/supplier/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#gridsupplier").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_supplier_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/supplier/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }


        //deleteRow: function (Id) {
        //    if (confirm('Are you true to delete the row?')) {
        //        $.ajax({
        //            url: "/Supplier/Delete",
        //            data: {
        //                id: Id
        //            },
        //            type: "POST",
        //            success: function (data) {
        //                $("#mainContent").html(data);
        //            }
        //        });
        //    }
        //}
    };
}();



