﻿var datacodes = function () {
    return {
        init: function (data_code_record_id) {
            $("a.ajax").ajaxLink();


            //    alert(data_code_record_id);
            //Table ID
            var tableId = "#grdcodes";

            //define columns
            var columns = [
           {
               field: "data_code_text",
               title: "激活码"
            
           },
           
                {
                    field: "datecreated",
                    title: "创建日期",
                    template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                },

            {
                field: "data_code_status",
                title: "状态",
                //template: "#=data_code_status==0?'<i class=\"fa fa-check-circle text-navy\"></i>':''#",
               // template: "#=data_code_status==true?'<i class=\"fa fa-check-circle text-navy\"></i>':'<i class=\"fa fa-comment text-navy\"></i>'#",
                template: "# if (data_code_status == 14) {# <i class=\"fa fa-circle-o text-navy\"></i>#} else if (data_code_status == 15) {# <i class=\"fa fa-circle text-navy\"></i> #} else if (data_code_status == 16) {# <i class=\"fa fa-check text-navy\"></i> #} #",
                //filterable: { multi: true }
            }


            ];
            var url = "/odata/CodeDTO";

            if (data_code_record_id != 0) {
                url = url + "?$filter=data_code_record_id eq " + data_code_record_id + "&";
            }


            // alert(url);

            //define the data type
            var model = {
                fields: {
                  //  data_member_value: { type: "number" },
                    data_code_text: { type: "string" }
                }


            };

            UIGrid.Init(tableId, url, columns, model);


        },



        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },


    };
}();



