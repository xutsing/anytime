﻿var datacodesrecords = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridcode";

            var columns = [
           {
               field: "data_code_record_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_supplier_id",
                title: "供应商"
            },
            {
                field: "data_service_id",
                title: "所带课程"
            },
              {
                  field: "data_code_record_money",
                  title: "价格",
                  format: "{0:c}",
                  width: 200
               
              },
               {
                   field: "data_code_record_count",
                   title: "数量",
                   width: 120
               },

                 {
                     field: "datecreated",
                     title: "创建时间",
                     template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                 },

               {
                   field: "data_code_record_status",
                   title: "状态",
                   width: 100
               },



            {
                template: " <a href='javascript:datacodesrecords.viewRow(#:data.data_code_record_id#)'>明细</> | <a href='/codes/edit/#:data.data_code_record_id#' class='ajax'>编辑</> | <a href='javascript:dataservice.deleteRow(#:data.data_code_record_id#)'>删除</>",
                field: "data_code_record_id",
                title: "操作",
                width: 150
            }];

            var url = "/odata/CodeRecordDTO";

            var model = {
                fields: {
                    data_code_record_id: { type: "number" },
                    data_code_record_count: { type: "number" }
                    //  datecreated: {type:""}
                }
            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");


            $("#data_code_record_count").kendoNumericTextBox({ format: "n0" });
            $("#data_code_record_money").kendoNumericTextBox({
                format: "c",
                decimals: 3
            });
            $("#data_supplier_id").kendoComboBox();
            $("#data_service_id").kendoComboBox();
            $("#data_code_record_status").kendoComboBox();
            $("#data_member_service_id").kendoComboBox();
            
        },

        viewRow: function (Id) {

            $.ajax({
                url: "/Codes/ViewDetail",
                data: {
                    data_record_id: Id
                },
                //type: "POST",
                success: function (data) {
                    // $("#mainContent").html(data);
                    //#window
                    // alert(data);
                    var window = $("#window");

                    window.html(data);
                    window.kendoWindow({
                        width: "500",
                        height: "750",
                        title: "激活码明细",
                        modal: true,
                        visible: false

                    });

                    var win = $("#window").data("kendoWindow");       
            
                    win.center();
                    win.open();
                }
            });
        },

        
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/CodeRecordDTO/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },

        exportfile: function (){
            var grid = $("#gridcode").data("kendoGrid");
            //当前选中行
        var selectedItem = grid.dataItem(grid.select());

        if (selectedItem == null) {
            alert("请选中需要查看的一行");
            return;
        }
        var Id = selectedItem.data_code_record_id;


        $.ajax({
            url: "/Codes/SaveCSV",
            data: {
                Id: Id
            },
            success: function (data) {
             //   alert(data);

                window.open(data);

             //   alert("文件保存成功");
            }
        });

    }
    };
}();



