﻿var databasetask = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
           {
               field: "data_basetask_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_basetask_name",
                title: "名称"
            },
            {
                field: "data_basetask_type",
                title: "类别"
            },
              {
                  field: "data_basetask_url",
                  title: "链接",
              },

               {
                   field: "data_basetask_remark",
                   title: "备注"
               },
            //{
            //    template: "<a href='/basetask/edit/#:data.data_basetask_id#' class='ajax'>编辑</> | <a href='javascript:databasetask.deleteRow(#:data.data_basetask_id#)'>删除</>",
            //    field: "data_basetask_id",
            //    title: "操作",
            //    width: 150
            //}
            ];

            var url = "/odata/BaseTask";

            var model = {
                fields: {
                    data_basetask_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_basetask_type").kendoComboBox();
        },

        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_basetask_id;

            $.ajax({
                url: "/basetask/edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },

        deleteRow: function () {
            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_basetask_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/basetask/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



