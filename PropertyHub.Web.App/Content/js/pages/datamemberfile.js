﻿var datamemberfile = function () {
    return {
        init: function (data_member_id) {
            $("a.ajax").ajaxLink();
            var tableId = "#grid_member_file";
         
            var columns = [
                {
                    field: "data_member_information_Id",
                    title: "ID",
                    width: 60
                },
                {
                    field: "data_member_name",
                    title: "姓名"
                },
                {
                    field: "data_member_identity",
                    title: "身份证"
                },
                {
                    field: "data_member_country",
                    title: "国家"
                },
                //dateupdated
                
                {
                    field: "dateupdated",
                    title: "更新时间",
                    template: "#= kendo.toString(kendo.parseDate(dateupdated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                }


            ]
            ;


            var url = "/odata/DataMemberFileDTO";

            if (data_member_id != 0) {
                url = url + "?$filter=data_member_id eq " + data_member_id + "&";
            }

            //define the data type
            var model = {
                fields: {
                    data_member_information_Id: { type: "number" },
                    data_member_name: { type: "string" },
                    dateupdated: {type:"date"}
                }
            };
            var sort = { field: "dateupdated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model, { selectable: "signle" },sort);
        },
        //编辑一份会员家庭健康档案
        edit: function() {
            var grid = $("#grid_member_file").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_member_id;//会员编号
            var taskId = 0;//当前没有任务。
            
            $.ajax({
                url: "/memberfile/new",
                data: {
                    member_id: Id,
                    taskId:0
                },
               // type: "POST",
                success: function (data) {
                    $("#mainContent").html(data);
                }
            });

        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#selected_secratry").kendoComboBox();
        },


        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/demo/delete",
                    data: {
                        id: data_member_id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



