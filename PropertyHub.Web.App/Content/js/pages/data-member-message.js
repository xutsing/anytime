﻿/*
    added by adam 2015-08-01
*/

var data_member_message = function () {
    var gridName = "grid_member_message";
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#" + gridName;

            var columns = [
                {
                   field: "data_member_message_id",
                   title: "ID",
                   width: 60
                },
                {
                    field: "data_member_names",
                    title: "推送至"
                },
                {
                    field: "data_member_message_title",
                    title: "消息标题"
                },
                {
                    field: "record_status",
                    title: "状态",
                    template: "#= record_status == '0' ? '草稿' : '已审核' #"
                },
                {
                    field: "datecreated",
                    title: "创建日期",
                    template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                }];

            var url = "/odata/membermessagedto";

            var model = {
                fields: {
                    data_member_message_id: { type: "number" }
                }
            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/membermessage/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },
        view: function (a) {
            var grid = $("#" + gridName).data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_message_id;
                
                $(a).attr("href", "/membermessage/detail/" + id);
            }           
        },
        edit: function (a) {
            var grid = $("#" + gridName).data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_message_id;

                $(a).attr("href", "/membermessage/edit/" + id);
            }
        },
        deleteIt: function () {
            var grid = $("#" + gridName).data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_message_id;

                this.deleteRow(id);
            }
        },
    };
}();



