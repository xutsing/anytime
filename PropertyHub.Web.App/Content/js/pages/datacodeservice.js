﻿var datacodesservice = function () {
    return {
        init: function (data_code_record_id) {
            $("a.ajax").ajaxLink();



            //Table ID
            var tableId = "#gridcodeservices";

            //define columns
            var columns = [
           {
               field: "data_code_id",
               title: "激活码"
           },
            {
                field: "data_member_id",
                title: "会员"
            },
             {
                 field: "data_service_id",
                 title: "激活课程"
             },

               {
                   field: "dateActualStarted",
                   title: "生效日期",
                   template: "#= kendo.toString(kendo.parseDate(dateActualStarted, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
               },

            {
                field: "dateActualEnd",
                title: "结束日期",
                template: "#= kendo.toString(kendo.parseDate(dateActualEnd, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
         },



                {
                    field: "datecreated",
                    title: "激活日期",
                    template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                }




            ];
            var url = "/odata/CodeServiceDTO";

           

            //define the data type
            var model = {
                fields: {
                    //  data_member_value: { type: "number" },
                    dateActualStarted: { type: "date" },
                    dateActualEnd: { type: "date" },
                    datecreated: { type: "date" }
                }


            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);


        },



        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        }
    };
}();



