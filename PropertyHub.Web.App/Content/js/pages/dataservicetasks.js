﻿var dataservicetasks = function () {

    var currentselectedIndex = 0;


    return {
        init: function (serviceId) {
            $("a.ajax").ajaxLink();

            var crudServiceBaseUrl = "/odata/ServiceTask?serviceId=" + serviceId,
            dataSource = new kendo.data.DataSource({
                type: "odata-v4",
                transport: {
                    read: {
                        url: crudServiceBaseUrl
                    }
                },
                batch: true,
                pageSize: 8,
                serverPaging: true,
                schema: {
                    model: {
                        id: "data_service_tasks_id"
                    }
                }
            });

            $("#grid").kendoGrid({
                dataSource: dataSource,
                navigatable: true,
              //  pageable: false,
                selectable: "single row",
                scrollable: false,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
              //  height: 300,
                dataBound: dataservicetasks.onDataBound,
                columns: [
                    { field: "data_service_tasks_id", title: "序号", width: 120 },
                    { field: "data_basetask_name", title: "任务", width: 220 },
                    { field: "data_nexttask_interval", title: "时间间隔", width: 120 },
                    {
                        field: "Iscycle", title: "是否循环", width: 50,
                        template: "#= Iscycle ? '是' : '' #"
                    },
                    {
                        field: "cycleDays", title: "循环间隔（天）", width: 120,
                        template: "#= Iscycle ? cycleDays : '' #"
                    },
                    {
                        field: "cyclemax", title: "最大循环次数", width: 120,
                        template: "#= Iscycle ? cyclemax : '' #"
                    }
                    //disable by tsing .dont need the last column any more
                    //8/7/2015
                    //,
                    //{
                    //    template: "<a href='javascript:dataservicetasks.deleteRow(#:data.data_service_tasks_id#)'>删除</> | <a href='javascript:dataservicetasks.changeOrder(#:data.data_service_tasks_id#,1)'>向上</> | <a href='javascript:dataservicetasks.changeOrder(#:data.data_service_tasks_id#,0)'>向下</>",
                    //    field: "data_service_tasks_id",
                    //    title: "操作",
                    //    width: 150
                    //}
                ]
            });

        },

         onDataBound:function(args) {
                   
             // alert("2222");
             grid = $("#grid").data("kendoGrid");
             var row = grid.tbody.find(">tr:not(.k-grouping-row)").eq(currentselectedIndex);
             // alert(row);
             grid.select(row);

        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            var window1 = $("#taskWindow");
            window1.kendoWindow({
                width: "400",
                height: "400",
                title: "修改任务",
                modal: true,
                visible: false
            });

            var window2 = $("#task2Window");
            window2.kendoWindow({
                width: "400",
                height: "400",
                title: "修改任务",
                modal: true,
                visible: false
            });


        },
        
        closeNewTask: function() {
            var win = $("#taskWindow").data("kendoWindow");
            win.center().close();
        },

        openForm: function (data_service_id) {
            
            var window = $("#taskWindow");
            window.kendoWindow({
                width: "400",
                height: "400",
                title: "新增任务",
                modal: true,
                content: {
                    url: "/Service/NewTask",
                    data: {
                        data_service_id: data_service_id
                    }
                },
                visible: false
            });


            $("#taskWindow").data("kendoWindow").center().open();

        },

        update : function() {

            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            //alert(selectedItem);
            if (selectedItem == null) {
                return;
            }

            Id = selectedItem.data_service_tasks_id;
            //alert(Id);

            var window2 = $("#task2Window");
            window2.kendoWindow({
                width: "400",
                height: "300",
                title: "修改任务",
                modal: true,
                visible: false
            });

            $.ajax({
                url: "/service/edittask",
                data: {
                    id: Id
                },
                success: function (data) {

                   $("#task2Window").html(data);
                   $("#task2Window").data("kendoWindow").center().open();

                }
            });
        
        },

        updatereply: function (result) {

            if(result == 0)
                return;//执行没有成功.
            

          

            var win = $("#task2Window").data("kendoWindow");
            win.center().close();

            var grid = $("#grid").data("kendoGrid");
              //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            Id = selectedItem.data_service_tasks_id;
            
            $.ajax({
                url: "/service/QueryTask",
                data: {
                    id: Id
                },
                success: function (data) {
                    // alert(data);
                    var grid = $("#grid").data("kendoGrid");  
                    //当前选中行
                    var selectedItem = grid.dataItem(grid.select());           
                    //selectedItem.message_Isreply = result==1?true:false;
                    selectedItem.set("data_basetask_name", data.Data.basetask);
                    //alert(data.Data.data_basetask);
                    selectedItem.set("data_nexttask_interval", data.Data.data_nexttask_interval);
                    selectedItem.set("Iscycle", data.Data.Iscycle);
                    //alert(data.Data.data_basetask);
                    selectedItem.set("cycleDays", data.Data.cycleDays);
                    selectedItem.set("cyclemax", data.Data.cyclemax);
                    //alert(data.Data.data_basetask);

                }
            });

            },


        deleteRow: function (Id) {

            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            // alert(selectedItem);
            if (selectedItem == null) {
                return;
            }

            Id = selectedItem.data_service_tasks_id;


            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/service/deletetask",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {

                        $("#mainContent").html(data);
                    }
                });
            }
        },
        changeOrder: function (Id, Direction) {
            //alert("clicked");
            //Tsing 8/7/2015
            //首先判断当前grid是否有行选中，如果没有的话，return
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            // alert(selectedItem);
            if (selectedItem == null) {
                return;
            }

            Id = selectedItem.data_service_tasks_id;

            //alert(selectedItem);
            var dataRows = grid.items();
            //alert(dataRows.length);
            var rowIndex = dataRows.index(grid.select());
            // alert(rowIndex);
            if (Direction == 1 && rowIndex == 0) {
                return;
            }
            if (Direction == 0 && rowIndex == dataRows.length - 1) {
                return;
            }


            // return;
            //direction 1 朝上 
            //direction 0 朝下

            //调用调整顺序的方法
            $.ajax({
                url: "/service/TaskOrder",
                data: {
                    id: Id,
                    direction: Direction
                },
                type: "POST",
                success: function (data) {
                    if (Direction == 1) {
                        currentselectedIndex = rowIndex - 1;
                    }
                    if (Direction == 0) {
                        currentselectedIndex = rowIndex + 1;
                    }
                     //  alert(data);
                    // 刷新列表
                    $("#mainContent").html(data);
                    //重新将选择的哪行设置为选中状态

                    //var newindex = 0;
                   
                   // alert(newindex);

                }
            });
        },
        IsCycled: function (ckBox) {
            var checked = $(ckBox).prop("checked");
            $(ckBox).parent().parent().parent().find("#cycleDays").enable(checked);
            $(ckBox).parent().parent().parent().find("#cyclemax").enable(checked);
        }
    };
}();

