﻿var datamemberadvice = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grdadvice";

            var columns = [
           {
               field: "data_member_advice_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_member_name",
                title: "会员"
            },  
             {
                field: "datecreated",
                title: "创建日期",
                template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
              }


            ];

            var url = "/odata/DataMemberAdviceDTO";


            var model = {
                fields: {
                    data_member_advice_id: { type: "number" }
                }
            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        edit: function () {
        
            var grid = $("#grdadvice").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_member_advice_id;//会员编号
            var taskId = 0;//当前没有任务。

            $.ajax({
                url: "/MemberAdvice/Edit",
                data: {
                    data_member_advice_id: Id
                },
                // type: "POST",
                success: function (data) {
                    $("#mainContent").html(data);
                }
            });

        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/bodylabel/Delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



