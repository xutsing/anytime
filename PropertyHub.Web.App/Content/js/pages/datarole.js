﻿var datarole = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();
            //Table ID
            var tableId = "#grdrole";
            //define columns
            var columns = [
             {
                 field: "Name",
                 title: "角色名称"
             },
            {
                template: "<a href='/role/edit/#:data.Id#' class='ajax'>编辑</> | <a href='/role/editusers/#:data.Id#' class='ajax'>设置成员</> | <a href='/role/editpermission/#:data.Id#' class='ajax'>权限</> | <a href='javascript:datarole.delete(\"#:data.Id#\");'>删除</>",
                field: "Id",
                title: "操作",
                width: 300
            }
            ];
            var url = "/odata/RoleDTO";
            //define the data type
            var model = {
                fields: {
                    Name: { type: "string" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);


        },

        delete: function (id) {
            if (confirm('确认删除角色?')) {
                $.ajax({
                    url: "/role/delete",
                    data: {
                        id: id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        }
    };
}();



