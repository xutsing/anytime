﻿var dataservice = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
           {
               field: "data_service_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_service_name",
                title: "名称"
            },
            {
                field: "data_service_validate_months",
                title: "有效期"
            },
              {
                  field: "data_service_price",
                  title: "价格",
                  format: "{0:c}"
              },

               {
                   field: "data_service_status",
                   title: "状态"
               },
            //{
            //    template: "<a href='/service/detail/#:data.data_service_id#' class='ajax'>查看</> | <a href='/service/edit/#:data.data_service_id#' class='ajax'>编辑</> | <a href='javascript:dataservice.deleteRow(#:data.data_service_id#)'>删除</>",
            //    field: "data_service_id",
            //    title: "操作",
            //    width: 150
            //}
            ];

            var url = "/odata/Service";

            var model = {
                fields: {
                    data_service_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
            $("#data_machine_type_id").kendoComboBox();
            $("#data_machine_status").kendoComboBox();
        },

        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_service_id;

            $.ajax({
                url: "/Service/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });
        },

        detail: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_service_id;

            $.ajax({
                url: "/Service/detail",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });
        },

        deleteRow: function () {

            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_service_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/service/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



