﻿/*
    added by adam 2015-08-04
*/

var data_machine_action = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
                {
                   field: "data_machine_action_id",
                   title: "ID",
                   width: 60
                },
                {
                    field: "data_machine_action_title",
                    title: "名称"
                },
                {
                    field: "data_machine_name",
                    title: "对应器械"
                },
                {
                    field: "data_machine_action_remark",
                    title: "备注"
                },
                {
                    field: "data_machine_action_status_text",
                    title: "状态"
                },
                //{
                //    template: "<a href='/machineaction/detail/#:data.data_machine_action_id#' class='ajax'>查看</> | <a href='/machineaction/edit/#:data.data_machine_action_id#' class='ajax'>编辑</> | <a href='javascript:data_machine_action.deleteRow(#:data.data_machine_action_id#)'>删除</>",
                //    field: "data_machine_action_id",
                //    title: "操作",
                //    width: 150
                //}
            ];

            var url = "/odata/machineactiondto";

            var model = {
                fields: {
                    data_machine_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_machine_action_status").kendoComboBox();
            $("#data_machine_id").kendoComboBox();


         //   $("#data_machine_action_path_upload").kendoUpload();
        },

        view: function () {
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要查看的一行");
                return;
            }
            var Id = selectedItem.data_machine_action_id;

            $.ajax({
                url: "/machineaction/detail",
                data: {
                    Id: Id
                },
                success: function (data) {
                    $("#mainContent").html("");
                    $("#mainContent").append(data);
                }
            });
        },

        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_machine_action_id;

            $.ajax({
                url: "/machineaction/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },

        deleteRow: function () {

            var grid = $("#grid").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_machine_action_id;
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/machineaction/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



