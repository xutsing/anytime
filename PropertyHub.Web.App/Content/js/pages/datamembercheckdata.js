﻿var datamembercheckdata = function () {
    return {
        init: function (data_member_checkrecord_id) {
            $("a.ajax").ajaxLink();


            // alert("loaded");
            //Table ID
            var tableId = "#grdcheckdata";

            //define columns
            var columns = [
           {
               field: "data_base_index_id",
               title: "参数",
               width: 150,
               filterable: false,
           },
            {
                field: "data_member_value",
                title: "数值",
                width:150
            },
                {
                    field: "datecreated",
                    title: "上传日期",
                    template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #",
                    width: 150
                }




            ];
            var url = "/odata/MemberCheckDataDTO";

           // alert(data_member_checkrecord_id);

            if (data_member_checkrecord_id != 0) {
                url = url + "?$filter=data_member_checkrecord_id eq " + data_member_checkrecord_id + "&";
            }


             // alert(url);

            //define the data type
            var model = {
                fields: {
                    data_member_value: { type: "number" },
                    data_base_index: { type: "string" }
                }


            };

            UIGrid.Init(tableId, url, columns, model);


        },



        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        }
    };
}();



