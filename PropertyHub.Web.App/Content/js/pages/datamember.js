﻿var datamember = function () {
    return {
        init: function (userId) {
            $("a.ajax").ajaxLink();
            var tableId = "#grid_member";
            //define columns
            var columns = [
                {
                    field: "data_member_id",
                    title: "ID",
                    width: 60
                },
                {
                    field: "data_member_name",
                    title: "姓名"
                },
                {
                    field: "data_member_loginname",
                    title: "登录名"
                },
                {
                    field: "data_member_mobile",
                    title: "移动电话"
                },
                {
                    field: "data_member_sex",
                    title: "性别"
                },
                {
                    field: "data_member_birthday",
                    title: "出生日期",
                    template: "#= kendo.toString(kendo.parseDate(data_member_birthday, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"

                },
                //datecreated
                 {
                     field: "datecreated",
                     title: "注册日期",
                     template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"

                 },


                //modified by adam 2015-08-05, add column
                {
                    field: "data_member_service_name",
                    title: "健康秘书"
                }


            ];
            var url = "/odata/Members";

            if (userId != "") {
                //data_member_service_id          
                url = url + "?$filter=data_member_service_id eq '" + userId + "'&";
            }

            //define the data type
            var model = {
                fields: {
                    data_member_id: { type: "number" },
                    data_member_name: { type: "string" },
                    data_member_birthday: { type: "date" }
                }
            };


            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model, { selectable: "multiple" },sort);
        },
        //added by tsing 2-15/08-19 
        viewrow:function()
        {
            var grid = $("#grid_member").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem == null) {
                alert("请选中需要查看的一行");
                return;
            }
            var Id = selectedItem.data_member_id;//会员编号

            $.ajax({
                url: "/member/view",
                data:{data_member_id:Id},
                success: function (data) {
                    $("#mainContent").html(data);
                    //var window = $("#window")
                    //     .kendoWindow({
                    //         height: "600",
                    //         width: "1000",
                    //         title: "会员详细资料",
                    //         modal: true,
                    //         visible: false
                    //     })
                    //     .css({ width: "100%", margin: "0" })
                    //     .html(data)
                    //     .data("kendoWindow")
                    //     .center().open();
                }
            });

        },

        //added by adam 2015-08-15, pop-up window for assigning secratry
        assign_secratry: function () {
            var grid = $("#grid_member").data("kendoGrid");
            var selectedRow = grid.select();
            if (selectedRow.length == 0) {
                return;
            }
            var ids = [];

            for (var i = 0; i < selectedRow.length; i++) {
                var selectedItem = grid.dataItem(selectedRow[i]);
                ids.push(selectedItem.data_member_id);
            }

            
            $.ajax({
                url: "/Member/ViewAssignSecratry?IDs=," + ids.join(",") + ",",
                success: function (data) {
                   var window = $("#window")
                        .kendoWindow({
                            height: "300",
                            width: "650",
                            title: "分配秘书",
                            modal: true,
                            visible: false
                        })
                        .css({ width: "100%", margin: "0"})
                        .html(data)
                        .data("kendoWindow")
                        .center().open();
                }
            });
        },
        //added by adam 2015-08-06, execute when assign success
        assign_secratry_success: function (secratry) {
            var grid = $("#grid_member").data("kendoGrid");
            var selectedRow = grid.select();
            for (var i = 0; i < selectedRow.length; i++) {
                $(selectedRow[i]).find("td:eq(7)").text(secratry);
            }
        },
        //added by adam 2015-08-07
        unassign_secratry: function () {
            if (!confirm("您确定要解除所选会员的秘书吗？"))
                return;

            var grid = $("#grid_member").data("kendoGrid");
            var selectedRow = grid.select();
            if (selectedRow.length == 0) {
                return;
            }
            var ids = [];

            for (var i = 0; i < selectedRow.length; i++) {
                var selectedItem = grid.dataItem(selectedRow[i]);
                ids.push(selectedItem.data_member_id);
            }

            $.ajax({
                url: "/Member/UnAssignSecratry",
                method: "POST",
                data: "selected_members=," + ids.join(",") + ",",
                success: function (data) {
                    var grid = $("#grid_member").data("kendoGrid");
                    var selectedRow = grid.select();
                    for (var i = 0; i < selectedRow.length; i++) {
                        $(selectedRow[i]).find("td:eq(7)").text("");
                    }
                }
            });
        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

        
        },

        viewRow: function (Id) {
            $.ajax({
                url: "/Member/View",
                data: {
                    data_member_id: Id
                },
                //type: "POST",
                success: function (data) {
                    // $("#mainContent").html(data);
                    //#window
                    // alert(data);
                    var window = $("#window");
                    window.html(data);
                    window.kendoWindow({
                        width: "1000",
                        height: "600",
                        title: "会员详细资料",
                        modal: true,
                        visible: false

                    });


                    var win = $("#window").data("kendoWindow");
                    win.center();
                    win.open();



                }
            });
        },
        viewbodyreport: function ()
        {
            var grid = $("#grid_member").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem == null) {
                alert("请选中需要查询的一行");
                return;
            }
            var Id = selectedItem.data_member_id;//会员编号

            $.ajax({
                url: "/member/ViewBodyReport",
                data: { data_member_id: Id },
                success: function (data) {
                    $("#window").html(data);
                    var window = $("#window")
                         .kendoWindow({
                             height: "600",
                             width: "1000",
                             title: "会员体检报告",
                             modal: true,
                             visible: false
                         })
                         .css({ width: "100%", margin: "0" })
                         .html(data)
                         .data("kendoWindow")
                         .center().open();
                }
            });

        },

        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/demo/delete",
                    data: {
                        id: data_member_id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



