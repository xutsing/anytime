﻿var datauser = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();


            //Table ID
            var tableId = "#grduser";

            //define columns
            var columns = [
       
             {
                 field: "Email",
                 title: "登录名"
             },
               {
                   field: "FirstName",
                   title: "FirstName"
               },
                 {
                     field: "LastName",
                     title: "LastName"
                 },
            {
                template: "<a href='/user/Edit/#:data.Id#' class='ajax'>编辑</> | <a href='/user/Updatepassword/#:data.Id#' class='ajax'>更改密码</> | <a href='javascript:datauser.lockuser(\"#:data.Id#\");'>删除</>",
                field: "Id",
                title: "操作",
                width: 300
            }




            ];
            var url = "/odata/UserDTO";


            //define the data type
            var model = {
                fields: {
                    UserName: { type: "string" }
                }


            };

            UIGrid.Init(tableId, url, columns, model);


        },

        lockuser: function (id) {
            if (confirm('确认删除用户?')) {
                $.ajax({
                    url: "/user/lockuser",
                    data: {
                        id: id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        }
    };
}();



