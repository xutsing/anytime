﻿var datamembertesting = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
           {
               field: "data_member_testing_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_member_name",
                title: "会员名称",
                width: 120
            },
            {
                field: "data_questions_testing_title",
                title: "调查问卷",
                width: 150
            },
              {
                  field: "data_questions_testing_point",
                  title: "得分",
                  width: 80
              },

                //dateupdated
                  {
                      field: "dateupdated",
                      title: "更新时间",
                      width: 150,
                      template: "#= kendo.toString(kendo.parseDate(dateupdated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"

                  },

               {
                   field: "data_questions_testing_remark",
                   title: "备注"
               },
            {
                template: "<a href='/membertesting/edit/#:data.data_member_testing_id#' class='ajax'>编辑</> | <a href='javascript:datamembertesting.deleteRow(#:data.data_member_testing_id#)'>删除</>",
                field: "data_member_testing_id",
                title: "操作",
                width: 150
            }];

            var url = "/odata/MemberTesting";

            var model = {
                fields: {
                    data_member_testing_id: { type: "number" },
                    dateupdated: { type: "date" }
                }
            };
            var sort = { field: "dateupdated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/membertesting/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



