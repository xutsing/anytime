﻿var dataindexgrade = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridindexgrade";

            var columns = [
           {
               field: "data_index_grade_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_index_grade1_title",
                title: "成绩"
            },
            {
                field: "data_index_grade_age_min",
                title: "最低年龄"
            },
              {
                  field: "data_index_grade_age_max",
                  title: "最高年龄",
              },

               {
                   field: "data_index_grade_point_min",
                   title: "最低成绩"
               },
               {
                   field: "data_index_grade_point_max",
                   title: "最高成绩"
               },
               {
                   field: "data_index_grade_comments",
                   title: "备注"
               }
              //{
              //    template: "<a href='/indexgrade/Edit/#:data_index_grade_id#' class='ajax'>编辑</> | <a href='javascript:dataindexgrade.deleteRow(#:data.data_index_grade_id#)'>删除</>",
              //    field: "data_index_grade_id",
              //    title: "操作",
              //    width: 150
              //}
            ];

            var url = "/odata/IndexGradeDTO";


            var model = {
                fields: {
                    data_index_grade_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
            $("#data_index_grade1").kendoComboBox();

        },
        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridindexgrade").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_index_grade_id;

            $.ajax({
                url: "/IndexGrade/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#gridindexgrade").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_index_grade_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/indexgrade/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



