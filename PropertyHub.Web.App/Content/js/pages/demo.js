﻿var demo = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            //Table ID
            var tableId = "#grid";

            //define columns
            var columns = [
           {
                field: "Id",
                title: "ID",
                width: 60
           },
            {
                field: "Name",
                title: "Name"
            },
            {
                field: "CompanyName",
                title: "Company Name"
            },
            {
                template: "<a href='javascript:demo.deleteRow(#:data.Id#)'>Delete</>",
                field: "Id",
                title: "Actions",
                width: 100
            }];
            var url = "/odata/SubscriptionTests";


            //define the data type
            var model = {
                fields: {
                    Id: { type: "number" },
                    Name: { type: "string" }
                }


            };

            UIGrid.Init(tableId, url, columns, model);


        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/demo/delete",
                    data: {
                        id: id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();