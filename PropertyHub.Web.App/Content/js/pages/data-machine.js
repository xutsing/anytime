﻿/*
    added by adam 2015-08-01
*/

var data_machine = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
                {
                   field: "data_machine_id",
                   title: "ID",
                   width: 60
                },
                {
                    field: "data_machine_name",
                    title: "名称"
                },
                {
                    field: "data_machine_type_title",
                    title: "类型"
                },
                {
                    field: "data_machine_status_text",
                    title: "状态"
                }
            ];

            var url = "/odata/machinedto";

            var model = {
                fields: {
                    data_machine_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_machine_status").kendoComboBox();
            $("#data_machine_type").kendoComboBox();
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/machine/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },
        view: function (a) {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_machine_id;
                
                $(a).attr("href", "/machine/detail/" + id);
            }           
        },
        edit: function (a) {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_machine_id;

                $(a).attr("href", "/machine/edit/" + id);
            }
        },
        deleteIt: function () {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_machine_id;

                this.deleteRow(id);
            }
        },
    };
}();



