﻿var datamemberrecords = function () {
    return {
        init: function (data_member_id) {
            $("a.ajax").ajaxLink();


            //Table ID
            var tableId = "#grdservice";

            //define columns
            var columns = [
           {
               field: "data_member_checkrecord_id",
               title: "ID",
               width: 60,
               filterable: false,
           },

            {
                field: "data_member_name",
                title: "会员"
            }
            ,
            {
                field: "datecreated",
                title: "数据上传日期",
            template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
            },
            {
                field: "data_Isfinished",
                title: "是否完成健身建议",
                template: "#=data_Isfinished==true?'<i class=\"fa fa-check-circle text-navy\"></i>':''#",
            },
             //{
             //  field: "data_create_date",
             //  title: "上传时间",
             //  template: "#= kendo.toString(kendo.parseDate(data_create_date, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
             //    },

            {
                field: "data_member_checkrecord_remark",
                title: "备注"
            },

    {
        template: "<a href='javascript:datamemberrecords.viewRow(#:data.data_member_checkrecord_id#)' ><i class='fa fa-eye'></i>查看</> | <a href='javascript:datamemberrecords.viewChart(#:data.data_member_checkrecord_id#)' ><i class='fa fa-pie-chart'></i>体测报告</>",
     
        field: "data_member_id",
        title: " ",
        width: 200,
        filterable: false,
    }
        
];
    var url = "/odata/MemberCheckDTO";

    if (data_member_id != 0)
    {
        url = url + "?$filter=data_member_id eq " + data_member_id+"&";
    }


    //  alert(url);

    //define the data type
    var model = {
        fields: {
            data_member_checkrecord_id: { type: "number" }
            //  data_member_name: { type: "string" }
        }


    };

    var sort = { field: "datecreated", dir: "desc" };
    UIGrid.Init(tableId, url, columns, model,null,sort);


},

        viewChart: function (Id) {
            $.ajax({
                url: "/Member/ViewCheckDataReport",
                data: {
                    data_member_checkrecord_id: Id
                },
                //type: "POST",
                success: function(data) {
                    // $("#mainContent").html(data);
                    //#window
                    // alert(data);
                    var window = $("#window");
                    window.html(data);
                    window.kendoWindow({
                        width: "600",
                        height: "600",
                        title: "体测详细资料",
                        modal: true,
                        visible: false
                    });


                    var win = $("#window").data("kendoWindow");
                    win.center();
                    win.open();
                }
            });
        },
        //
        createreport: function () {
            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#grdservice").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_member_checkrecord_id;
            
            $.ajax({
                url: "/Member/EditCheckReport",
                data: {
                    check_Id: Id
                },
                success: function (data) {
                    var window = $("#checkwindow");
                    window.html(data);
                    window.kendoWindow({
                        width: "800",
                        title: "生成健身建议",
                        modal: true,
                        visible: false

                    });
                    var win = $("#checkwindow").data("kendoWindow");
                    win.center();
                    win.open();
                }
            });


        },
        //关闭当前体侧健身建议
        closereport: function () {
            //关闭当前窗口.
            var win = $("#checkwindow").data("kendoWindow");
            win.close();
        },

        viewRow: function (Id) {      
        $.ajax({
            url: "/Member/ViewCheckdata",
            data: {
                data_member_checkrecord_id: Id
            },
            //type: "POST",
            success: function (data) {
                // $("#mainContent").html(data);
                //#window
                // alert(data);
                var window = $("#window");
                window.html(data);
                window.kendoWindow({
                    width: "500",
                    height: "600",
                    title: "体测详细资料",
                    modal: true,
                    visible: false

                });


                var win = $("#window").data("kendoWindow");
                win.center();
                win.open();
            }
        });
        },
        



initForm: function () {
    $("a.ajax").ajaxLink();
    $.validator.unobtrusive.parse("form");
}
};
}();



