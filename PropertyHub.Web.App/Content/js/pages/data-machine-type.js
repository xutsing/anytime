﻿/*
    added by adam 2015-08-01
*/

var data_machine_type = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#grid";

            var columns = [
                {
                   field: "data_machine_type_id",
                   title: "ID",
                   width: 60
                },
                {
                    field: "data_machine_type_title",
                    title: "名称"
                },
                {
                    template: "<a href='/machinetype/detail/#:data.data_machine_type_id#' class='ajax'>查看</> | <a href='/machinetype/edit/#:data.data_machine_type_id#' class='ajax'>编辑</> | <a href='javascript:data_machine_type.deleteRow(#:data.data_machine_type_id#)'>删除</>",
                    field: "data_machine_type_id",
                    title: "操作",
                    width: 150
                }
            ];

            var url = "/odata/machinetypedto";

            var model = {
                fields: {
                    data_machine_type_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/machinetype/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



