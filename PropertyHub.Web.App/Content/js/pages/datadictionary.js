﻿var datadictionary = function () {
    return {
        init: function (type) {
            $("a.ajax").ajaxLink();

            var tableId = "#griddictionary";

            var columns = [
           {
               field: "data_dictionary_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_dictionary_type",
                title: "数据类型"
            },
            {
                field: "data_dictionary_text",
                title: "数据文本"
            },
              {
                  field: "data_dictionary_value",
                  title: "数据值",
              },

            //{
            //    template: "<a href='/baseindex/Edit/#:data.data_base_index_id#' class='ajax'>编辑</> | <a href='javascript:databaseindex.deleteRow(#:data.data_base_index_id#)'>删除</>",
            //    field: "data_base_index_id",
            //    title: "操作",
            //    width: 150
            //}
            ];

            var url = "/odata/DataDictionaryDTO";
            url = url + "?$filter=data_dictionary_type eq '" + type + "'&";


            var model = {
                fields: {
                    data_dictionary_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            // $("#data_service_validate_months").kendoNumericTextBox({ format: "n0" });
            // $("#data_service_price").kendoNumericTextBox({
            //    format: "c",
            //    decimals: 3
            //});
            //$("#data_service_status").kendoComboBox();
        },
        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#griddictionary").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_dictionary_id;

            $.ajax({
                url: "/DataDictionary/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#griddictionary").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_dictionary_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/DataDictionary/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



