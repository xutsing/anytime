﻿var dataindexdata = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridindexdata";

            var columns = [
           {
               field: "data_index_data_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_base_index_title",
                title: "基本指标"//,
              //  filterable: { ui: indexFilter }
            },
            {
                field: "data_body_label_title",
                title: "分值"
            },
              {
                  field: "data_sex_title",
                  title: "性别",
              },

               {
                   field: "data_age_range_title",
                   title: "年龄范围"
               },
               {
                   field: "data_index_data_min",
                   title: "最小值"
               },
               {
                   field: "data_index_data_max",
                   title: "最大值"
               }
            ];

            var url = "/odata/IndexDataDTO";


            var model = {
                fields: {
                    data_index_data_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_base_index_id").kendoComboBox();
            $("#data_body_label_id").kendoComboBox();
            $("#data_sex_id").kendoComboBox();
            $("#data_age_range_id").kendoComboBox();

        },
        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridindexdata").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_index_data_id;

            $.ajax({
                url: "/indexdata/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#gridindexdata").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_index_data_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/indexdata/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();


//function indexFilter(element) {
//    element.kendoDropDownList({
//        dataSource: indexes,
//        optionLabel: "--Select Value--"
//    });
//}