﻿/*
    added by adam 2015-08-10
*/

var data_prescription = function () {
    return {
        init: function (tableId, data_member_id) {
            $("a.ajax").ajaxLink();

            var tid;
            if (!tableId)
                tid = "#grid";
            else
                tid = "#" + tableId;

            var columns = [
                {
                    field: "data_member_prescription_id",
                    title: "ID",
                    width: 60
                },
                {
                    field: "data_member_prescription_title",
                    title: "标题"
                },
                {
                    field: "data_member_name",
                    title: "会员"
                },
                {
                    template: "<a href='javascript:void(0)' onclick='window.open(\"#:data.data_member_prescription_path#\")'>PDF文件</a>",
                    field: "data_member_prescription_path",
                    title: "处方文件"
                },
                {
                    field: "data_member_prescription_status_text",
                    title: "状态"
                },
                {
                    field: "datecreated",
                    title: "创建时间",
                    template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
                }
            ];

            var url = "/odata/PrescriptionDTO";
            if (data_member_id && data_member_id > 0) {
                url = url + "?$filter=data_member_id eq " + data_member_id + "&";
            }

            var model = {
                fields: {
                    data_member_prescription_id: { type: "number" }
                }
            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tid, url, columns, model, { dataBinding: this.initFormDataBound },sort);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_machine_action_id").kendoComboBox();
            $("#data_member_prescription_status").kendoComboBox();
            $("#data_machine_action_number").kendoNumericTextBox({ format: "n0" });
            $("#data_machine_action_times").kendoNumericTextBox({ format: "n0" });
        },
        detailForm: function (style) {
            var columns = [
                {
                    field: "detail_id",
                    title: "ID",
                    width: 60
                },
                {
                    field: "machine_action_text",
                    title: "器械动作"
                },
                {
                    field: "data_machine_action_times",
                    title: "运动时长"
                },                
                {
                    field: "data_machine_action_number",
                    title: "运动次数"
                },
                {
                    field: "data_machine_action_remark",
                    title: "备注"
                }
            ];

            if (style == "operate") {
                columns.push(
                    {
                        template: "<a href='javascript:data_prescription.delete_detail(#:data.detail_id#)'>删除</a>",
                        field: "detail_id",
                        title: "操作",
                        width: 150
                    });
            }

            $("#grid_detail").kendoGrid({
                dataSource: detail_datasource,
                columns: columns
            });
        },
        add_detail: function () {
            var machine_action = $("#data_machine_action_id").val();
            var machine_action_text = $("#data_machine_action_id option:selected").text();
            var machine_action_number = $("#data_machine_action_number").val();
            var machine_action_times = $("#data_machine_action_times").val();
            var machine_action_remark = $("#data_machine_action_remark").val();
            var detail_id = detail_datasource.data().length + 1;

            var data = '{"detail_id": ' + detail_id + ', "data_machine_action_id": '
                + machine_action + ', "machine_action_text": "'
                + machine_action_text + '", "data_machine_action_times": '
                + machine_action_times + ', "data_machine_action_number": '
                + machine_action_number + ', "data_machine_action_remark": "'
                + machine_action_remark + '"}';

            detail_datasource.add($.parseJSON(data));
        },
        delete_detail: function (id) {
            var data = detail_datasource.data();
            var index = -1;
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                if (item.detail_id < id)
                    continue;

                if (item.detail_id == id) {
                    index = i;
                    continue;
                }

                item.detail_id--;
            }

            detail_datasource.remove(data.at(index));
        },
        dataSourceChange: function (e) {
            $("#machine_action_detail_list").val(JSON.stringify(detail_datasource.data()));
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/prescription/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        },
        initFormDataBound: function (e) {
            for (var i in e.items) {
                e.items[i].datecreated = e.items[i].datecreated == null ? ""
                    : e.items[i].datecreated.replace("T", " ").replace("+08:00", "").substr(0, 19);
            }
        },
        view: function (a) {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_prescription_id;

                $(a).attr("href", "/prescription/detail/" + id);
            }
        },
        edit: function (a) {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_prescription_id;

                $(a).attr("href", "/prescription/edit/" + id);
            }
        },
        deleteIt: function () {
            var grid = $("#grid").data("kendoGrid");
            var selectRow = grid.select();

            if (selectRow.length > 0) {
                var dataRow = grid.dataSource.getByUid(selectRow.data("uid"));
                var id = dataRow.data_member_prescription_id;

                this.deleteRow(id);
            }
        }
    };
}();



