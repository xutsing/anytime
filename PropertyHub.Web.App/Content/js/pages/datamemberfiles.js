﻿var datamemberfiles = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();
            var tableId = "#gridmemberfiles";
         
            var columns = [
                {
                    field: "dat_member_file_id",
                    title: "ID",
                    width: 60
                },
                {
                    field: "data_member_id_text",
                    title: "所有人"
                },
                {
                    field: "data_member_file_path",
                    title: "路径"
                },
 
             ];

            var url = "/odata/DataMemberFilesDTO";


            var model = {
                fields: {
                    dat_member_file_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
            $("#data_member_id").kendoComboBox();
        },
        edit: function () {
            var grid = $("#gridmemberfiles").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem == null) {
                alert("请选中需要查看的一行");
                return;
            }
            var Id = selectedItem.dat_member_file_id;//会员编号

            $.ajax({
                url: "/DataMemberFiles/Edit",
                data: { Id: Id },
                success: function (data) {
                    $("#mainContent").html(data);
                    //var window = $("#window")
                    //     .kendoWindow({
                    //         height: "600",
                    //         width: "1000",
                    //         title: "会员详细资料",
                    //         modal: true,
                    //         visible: false
                    //     })
                    //     .css({ width: "100%", margin: "0" })
                    //     .html(data)
                    //     .data("kendoWindow")
                    //     .center().open();
                }
            });

        },
        deleteRow: function () {

            var grid = $("#gridmemberfiles").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.dat_member_file_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/DataMemberFiles/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();