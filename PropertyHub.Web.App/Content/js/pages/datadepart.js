﻿var datadepart = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#griddepart";

            var columns = [
           {
               field: "Id",
               title: "ID",
               width: 60
           },
            {
                field: "DepartName",
                title: "名称"
            },
            {
                field: "Reamrk",
                title: "备注"
            },
            {
                template: "<a href='/depart/edit/#:data.Id#' class='ajax'>编辑</> | <a href='javascript:datadepart.deleteRow(#:data.Id#)'>删除</>",
                field: "Id",
                title: "操作",
                width: 150
            }];

            var url = "/odata/ODepartDTO";

            var model = {
                fields: {
                    Id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            $("#data_service_validate_months").kendoNumericTextBox({ format: "n0" });
            $("#data_service_price").kendoNumericTextBox({
                format: "c",
                decimals: 3
            });
            $("#data_service_status").kendoComboBox();
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/depart/Delete",
                    data: {
                        Id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



