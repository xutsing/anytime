﻿var datamessage = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridmessage";

            var columns = [
            {
                field: "data_message_id",
                title: "ID",
                width:60

            },
              {
                  field: "message_type_title",
                  title: "分类",

              },

            {
                field: "message_member_name",
                title: "会员"
            },
            {
                field: "message_date",
                title: "留言时间",
                template: "#= kendo.toString(kendo.parseDate(message_date, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
            },
            {
                field: "message_Isreply",
                title: "是否回复",
                template: "#=message_Isreply==true?'<i class=\"fa fa-check-circle text-navy\"></i>':'<i class=\"fa fa-comment text-navy\"></i>'#",
                filterable: { multi: true }
               
            }

            ];
            var url = "/odata/MessageDTO";
            var model = {
                fields: {
                    data_message_id: { type: "number" },
                    message_date: { type: "date" }
                 //   message_Isreply: { type: "bit" }
                }
            };


            var sort = { field: "message_date", dir: "desc" };
            
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        updatereply: function (result) {
            
            if (result == 0)
                return;//执行没有成功.

            var win = $("#messagewindow").data("kendoWindow");
            win.center().close();

            var grid = $("#gridmessage").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            //  selectedItem.message_Isreply = result==1?true:false;
            selectedItem.set("message_Isreply", result == 1 ? true : false);

        },

        reply: function () {


            //首先判断当前grid是否有选中的行
            var grid = $("#gridmessage").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            // alert(selectedItem);

            if (selectedItem == null) {

                return;
            }

            var window = $("#messagewindow");
            window.kendoWindow({
                height: "700",
                width: "680",
                //    resizable: false,
                title: "会员提问信息",
                modal: true,
                visible: false

            });


            var Id = selectedItem.data_message_id;
            $.ajax({
                url: "/Message/Reply",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //  alert(data);
                    window.html(data);
                    //  window.html("<div>Hello world</div>");
                    var win = $("#messagewindow").data("kendoWindow");
                    win.center().open();
                    //window.open();
                }
            });


        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },
        deleteRow: function (Id) {
            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/service/delete",
                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



