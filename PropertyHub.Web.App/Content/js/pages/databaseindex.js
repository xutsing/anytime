﻿var databaseindex = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridindex";

            var columns = [
           {
               field: "data_base_index_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_base_index_title",
                title: "中文名称"
            },
            {
                field: "data_base_index_title_en",
                title: "英文名称"
            },
              {
                  field: "data_base_key",
                  title: "KEY",
              },

               {
                   field: "data_base_index_unit",
                   title: "基本单位"
               },
            //{
            //    template: "<a href='/baseindex/Edit/#:data.data_base_index_id#' class='ajax'>编辑</> | <a href='javascript:databaseindex.deleteRow(#:data.data_base_index_id#)'>删除</>",
            //    field: "data_base_index_id",
            //    title: "操作",
            //    width: 150
            //}
            ];

            var url = "/odata/BaseIndexDTO";


            var model = {
                fields: {
                    data_base_index_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

           // $("#data_service_validate_months").kendoNumericTextBox({ format: "n0" });
           // $("#data_service_price").kendoNumericTextBox({
            //    format: "c",
            //    decimals: 3
            //});
            //$("#data_service_status").kendoComboBox();
        },
        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridindex").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_base_index_id;

            $.ajax({
                url: "/baseindex/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#gridindex").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_base_index_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/baseindex/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



