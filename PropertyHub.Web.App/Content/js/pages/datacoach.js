﻿var datacoach = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridcoach";

            var columns = [
           {
               field: "data_anytime_friend_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_name",
                title: "姓名"
            },
           {
               field: "data_sex_title",
               title: "性别",
           },
            {
                field: "data_age",
                title: "年龄",
            },

            {
                field: "ProvinceName",
                title: "省份"
            },
            {
                field: "CityName",
                title: "城市"
            },
            {
                field: "AreaName",
                title: "区域"
            },
            {
                field: "data_profession",
                title: "职业"
            },
            {
                field: "data_Ip",
                title: "联系方式"
            },
              {
                  field: "datecreated",
                  title: "创建时间",
                  template: "#= kendo.toString(kendo.parseDate(datecreated, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
              }

           //{
           //    template: "<a href='/age/Edit/#:data.data_age_range_id#' class='ajax'>编辑</> | <a href='javascript:dataage.deleteRow(#:data.data_age_range_id#)'>删除</>",
           //     field: "data_age_range_id",
           //     title: "操作",
           //     width: 150
           //}
            ];

            var url = "/odata/DataCoachDTO";


            var model = {
                fields: {
                    data_anytime_friend_id: { type: "number" }
                }
            };
            var sort = { field: "datecreated", dir: "desc" };
            UIGrid.Init(tableId, url, columns, model,null,sort);
        },
        edit: function () {
            // alert("123");
            var grid = $("#gridcoach").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要查看的一行");
                return;
            }
            var Id = selectedItem.data_anytime_friend_id;


            var window = $("#newwindow");
            window.kendoWindow({
                width: "700",
                title: "教练资料",
                modal: true,
                visible: false
            });

            $.ajax({
                url: "/DataCoach/Detail",
                data: {
                    Id: Id
                },
                success: function (data) {
                    // alert(data);
                    window.html(data);
                    var win = $("#newwindow").data("kendoWindow");
                    win.center().open();
                }
            });
        },


        close: function () {
            var win = $("#newwindow").data("kendoWindow");
            win.center().close();
        }

        //close: function () {
        //    var win = $("#newwindow").data("kendoWindow");
        //    win.center().close();
        //},


        //deleteRow: function () {



        //    if (confirm('Are you true to delete the row?')) {
        //        $.ajax({
        //            url: "/age/delete",

        //            data: {
        //                id: Id
        //            },
        //            type: "POST",
        //            success: function (data) {
        //                $("#mainContent").html(data);
        //            }
        //        });
        //    }
        //}
    };
}();



