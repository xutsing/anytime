﻿var dataage = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridage";

            var columns = [
           {
               field: "data_age_range_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_age_min",
                title: "年龄最小值"
            },
            {
                field: "data_age_max",
                title: "年龄最大值"
            },
              {
                  field: "data_age_title",
                  title: "年龄简称",
              },
           //{
           //    template: "<a href='/age/Edit/#:data.data_age_range_id#' class='ajax'>编辑</> | <a href='javascript:dataage.deleteRow(#:data.data_age_range_id#)'>删除</>",
           //     field: "data_age_range_id",
           //     title: "操作",
           //     width: 150
           //}
            ];

            var url = "/odata/AgeDTO";


            var model = {
                fields: {
                    data_age_range_id: { type: "number" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");

            // $("#data_service_validate_months").kendoNumericTextBox({ format: "n0" });
            // $("#data_service_price").kendoNumericTextBox({
            //    format: "c",
            //    decimals: 3
            //});
            //$("#data_service_status").kendoComboBox();
        },
        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridage").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }
            var Id = selectedItem.data_age_range_id;

            $.ajax({
                url: "/age/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        deleteRow: function () {

            var grid = $("#gridage").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要删除的一行");
                return;
            }
            var Id = selectedItem.data_age_range_id;

            if (confirm('Are you true to delete the row?')) {
                $.ajax({
                    url: "/age/delete",

                    data: {
                        id: Id
                    },
                    type: "POST",
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



