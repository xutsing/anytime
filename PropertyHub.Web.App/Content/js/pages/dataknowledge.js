﻿var dataknowledge = function () {
    return {
        init: function () {
            $("a.ajax").ajaxLink();

            var tableId = "#gridknowledge";
            //var radio = new document.createElement("input");
            //    radio.setAttribute("type", "radio");
            var columns = [
           {
               field: "data_knowledge_id",
               title: "ID",
               width: 60
           },
            {
                field: "data_knowledge_type_title",
                title: "类型"
            },
            {
                field: "data_knowledge_title",
                title: "题目"
            },
              {
                  field: "data_knowledge_keyword",
                  title: "关键字",
              },

               {
                   field: "data_knowledge_status",
                   title: "状态",
                   template: "#=data_knowledge_status==35?'<i class=\"fa fa-toggle-on text-navy\"></i>':'<i class=\"fa fa-toggle-off text-navy\"></i>'#"
               }
            ];

            var url = "/odata/DataKnowledgeDTO";


            var model = {
                fields: {
                    data_knowledge_id: { type: "number" }
                }
            };
            //modified by adam, allow row multiple selected
            UIGrid.Init(tableId, url, columns, model, {selectable: "multiple"});
        },

        edit: function () {

            //alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridknowledge").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());

            if (selectedItem == null) {
                alert("请选中需要修改的一行");
                return;
            }

            if (grid.select().length > 1) {
                alert("每次只能编辑一行数据");
                return;
            }

            var Id = selectedItem.data_knowledge_id;

            $.ajax({
                url: "/DataKnowledge/Edit",
                data: {
                    Id: Id
                },
                success: function (data) {

                    //alert(data);
                    $("#mainContent").html("");
                    $("#mainContent").append(data);

                }
            });


        },
        check: function () {
            
           // alert("test");
            //首先判断当前grid是否有选中的行
            var grid = $("#gridknowledge").data("kendoGrid");           
            var selectedRow = grid.select();

            if (selectedRow.length == 0) {
                alert("请选中需要审核的一行");
                return;
            } else if (selectedRow.length > 1) {
                if (!confirm("您确认要把这些知识条目全部审核通过吗？")) {
                    return;
                }

                var ids = [];

                for (var i = 0; i < selectedRow.length; i++) {
                    var selectedItem = grid.dataItem(selectedRow[i]);
                    ids.push(selectedItem.data_knowledge_id);
                }

                $.ajax({
                    type: "post",
                    url: "/DataKnowledge/Checks",
                    data: {
                        ids: ids
                    },
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });

            } else {
                var selectedItem = grid.dataItem(selectedRow);
                var Id = selectedItem.data_knowledge_id;

                $.ajax({
                    url: "/DataKnowledge/Check",
                    data: {
                        Id: Id
                    },
                    success: function (data) {
                        $("#mainContent").html("");
                        $("#mainContent").append(data);

                    }
                });
            }   
        },
        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
            $("#data_knowledge_type").kendoComboBox();
            

        //    $("#data_knowledge_path_upload").kendoUpload();
        },

        deleteRow: function (Id) {
            var grid = $("#gridknowledge").data("kendoGrid");
            //当前选中行
            var selectedRow = grid.select();

            var ids = [];

            for (var i = 0; i < selectedRow.length; i++) {
                var selectedItem = grid.dataItem(selectedRow[i]);
                ids.push(selectedItem.data_knowledge_id);
            }

            if (confirm('您确认删除选定的数据吗？')) {
                $.ajax({
                    type: "post",
                    url: "/DataKnowledge/Delete",
                    data: {
                        ids: ids
                    },
                    success: function (data) {
                        $("#mainContent").html(data);
                    }
                });
            }
        }
    };
}();



