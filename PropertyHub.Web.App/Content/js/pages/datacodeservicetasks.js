﻿var datacodeservicetasks = function () {
    return {
        init: function (data_code_record_id) {
            $("a.ajax").ajaxLink();

            //Table ID
            var tableId = "#griddatacodeservicetasks";

            //define columns
            var columns = [
            {
                field: "data_code_services_tasks_id",
                title: "ID",
                width: 60
            },
           {
               field: "data_member_name",
               title: "会员名称"
           },
            {
                field: "data_basetask_name",
                title: "基本任务"
            },
             {
                 field: "data_basetask_url",
                 title: "链接"
             },

               {
                   field: "data_code_service_date",
                   title: "任务时间",
                   template: "#= kendo.toString(kendo.parseDate(data_code_service_date, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
               },

            {
                field: "data_dictionary_text",
                title: "执行情况",
                template: function (dataItem) {
                    switch (dataItem.data_dictionary_text) {
                        case "执行中":
                            return '<i class=\"fa fa-flag-o text-navy\"></i>';
                        case "完成":
                            return '<i class=\"fa fa-flag text-navy\"></i>';
                        default:
                            return '';
                    }
                }
            },
            ];
            var url = "/odata/DataCodeServicesTasksDTO";

            //define the data type
            var model = {
                fields: {
                    data_code_services_tasks_id: { type: "number" },
                    data_code_service_date: { type: "date" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);


        },

        init_1: function (data_code_record_id) {
            $("a.ajax").ajaxLink();

            //Table ID
            var tableId = "#griddatacodeservicetasks";

            //define columns
            var columns = [
            {
                field: "data_code_services_tasks_id",
                title: "ID",
                width: 60
            },
           {
               field: "data_member_name",
               title: "会员名称"
           },
            {
                field: "data_basetask_name",
                title: "基本任务"
            },
             {
                 field: "data_basetask_url",
                 title: "链接"
             },

               {
                   field: "data_code_service_date",
                   title: "任务时间",
                   template: "#= kendo.toString(kendo.parseDate(data_code_service_date, 'yyyy-MM-dd'), 'MM/dd/yyyy') #"
               },

            {
                field: "data_dictionary_text",
                title: "执行情况",
                template: function (dataItem) {
                    switch (dataItem.data_dictionary_text) {
                        case "执行中":
                            return '<i class=\"fa fa-flag-o text-navy\"></i>';
                        case "完成":
                            return '<i class=\"fa fa-flag text-navy\"></i>';
                        default:
                            return '';
                    }
                }
            },
            ];
            var url = "/odata/DataCodeServicesTasksTodayDTO";



            //define the data type
            var model = {
                fields: {
                    data_code_services_tasks_id: { type: "number" },
                    data_code_service_date: { type: "date" }
                }
            };

            UIGrid.Init(tableId, url, columns, model);


        },

        updatetask: function (result) {


            var win = $("#taskwindow").data("kendoWindow");
            win.center().close();

            var grid = $("#griddatacodeservicetasks").data("kendoGrid");
            //当前选中行
            var selectedItem = grid.dataItem(grid.select());
            //  selectedItem.message_Isreply = result==1?true:false;
            selectedItem.set("data_dictionary_text", result);

        },

        initForm: function () {
            $("a.ajax").ajaxLink();
            $.validator.unobtrusive.parse("form");
        },

        execute: function (kind) {

            //首先判断当前grid是否有选中的行
       var grid = $("#griddatacodeservicetasks").data("kendoGrid");
            //当前选中行
       var selectedItem = grid.dataItem(grid.select());

        if (selectedItem == null) {
            alert("请选中一行");
            return;
        }
        var Id = selectedItem.data_code_services_tasks_id;

            //这个地方需要判断一下当前任务是个普通任务，还是一个转到另外一个链接的任务，
            //通过链接来进行判断

        //modified by adam 2016-03-18 所有任务执行时跳转到消息推送新增
        //var url = selectedItem.data_basetask_url;
        var url = "/MemberMessage/New";
        
        if (url != null)
        {
            //转入到task自己的链接，提交的参数有taskId,memberId.
            //modified by adam 2015-09-09 add backKind parameter to indicate which url will jump back to

            var taskId = Id;
            var memberId = selectedItem.data_member_id;
            
            $.ajax({
                url: url,
                data: {
                    taskId: Id,
                    member_id: memberId,
                    backKind: kind
                },
                success: function (data) {
                    //  alert(data);
                  //  window.html(data);
                   // var win = $("#window").data("kendoWindow");
                    //  win.center().open();
                    $("#mainContent").html(data);

                }
            });


            return;
        }



        var window = $("#taskwindow");
        window.kendoWindow({
            height: "550",
            width: "600",
            //    resizable: false,
            title: "当前任务",
            modal: true,
            visible: false

        });

        $.ajax({
            url: "/DataCodeServicesTasks/Reply",
            data: {
                id: Id
            },
            success: function (data) {
              //  alert(data);
                window.html(data);
                var win = $("#taskwindow").data("kendoWindow");
                win.center().open();

            }
        });
    }
    };
}();



