﻿using System.Web.Http;
using AutoMapper.Mappers;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Services.MemberFile;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft;

namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// created by tsing 
    /// </summary>
    public class MemberFileController : Controller
    {

        private readonly MemberFileService service;

        private readonly CodeServices codeservice;

        public MemberFileController(MemberFileService _service, CodeServices _codeservice)
        {
            service = _service;
            codeservice = _codeservice;

        }

        //
        // GET: /MemberFile/
        public ActionResult Index()
        {
            return PartialView();
        }

        [System.Web.Mvc.HttpPost]
        //  [ValidateAntiForgeryToken]
        public ActionResult New(MemberFileModel model, int? backKind)
        {
            if (ModelState.IsValid)
            {
                var info = new data_member_information();
                AutoMapper.Mapper.Map(model.member_inforamtion, info);
                service.UpdateMemberInfo(info);

                //更新当前任务状态。
                codeservice.UpdateTaskStatus(model.TaskId, model.IsFinished, model.TaskComments);

                if (model.TaskId != 0)
                {
                    //说明当前这个是从我的任务过来的，这时候回到我的任务即可

                    if (backKind == 1)
                    {
                        return RedirectToAction("Index", "DataCodeServicesTasks");
                    }
                    else if (backKind == 2)
                    {
                        return RedirectToAction("TasksToday", "DataCodeServicesTasks");
                    }

                }


                //任务状态
                ViewBag.TaskStatus = service.ListOptions("data_code_services_tasks");

                ViewBag.Bloods = service.ListBloods();
                //RH阴性
                ViewBag.RHs = service.ListRH();
                //职业
                ViewBag.Profession = service.ListProfession();
                //疾病
                ViewBag.Disease = service.ListDisease();
                //体育锻炼频率

                ViewBag.Physical = service.ListPhysicalRate();
                ViewBag.PhysicalPlace = service.ListPhysicalPlace();
                ViewBag.PhysicalSchedule = service.ListPhysicalSchedule();
                ViewBag.Smoke = service.ListSmoke();
                ViewBag.Drink = service.ListDrink();
                ViewBag.DrinkOptions = service.ListOptions("data_drink_wine_options");

                //脑血管疾病
                ViewBag.BrainDisease = service.ListOptions("data_member_brain");
                //肾脏疾病
                ViewBag.Kidney = service.ListOptions("data_member_kidney");
                ViewBag.HeartDisease = service.ListOptions("data_member_heart");
                ViewBag.BloodPipe = service.ListOptions("data_member_artery");
                ViewBag.EyeDisease = service.ListOptions("data_member_eye");

                return RedirectToAction("Index");
            }

            return PartialView(model);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="informationId"></param>
        /// <param name="member_id"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public ActionResult New(int member_id, int taskId, int? backKind)
        {

            //member_id = 1;

            var member = service.GetMember(member_id);
            var member_information = service.GetMemberInfor(member_id);



            var Model = new MemberFileModel();
            var task = service.GetTask(taskId);
            if (task != null)
            {
                //如果当前是个任务 
                Model.TaskId = taskId;
                Model.IsFinished = task.data_code_services_tasks_status.Value;
                Model.TaskComments = task.data_code_service_remark;
            }


            //将data_member-->memberdto
            AutoMapper.Mapper.Map(member, Model.member);
            AutoMapper.Mapper.Map(member_information, Model.member_inforamtion);
            AutoMapper.Mapper.Map(task, Model.task);

            Model.TaskId = taskId;//


            ViewBag.TaskStatus = service.ListOptions("data_code_services_tasks");
            //List Bloods
            //血型
            ViewBag.Bloods = service.ListBloods();
            //RH阴性
            ViewBag.RHs = service.ListRH();
            //职业
            ViewBag.Profession = service.ListProfession();
            //疾病
            ViewBag.Disease = service.ListDisease();
            //体育锻炼频率

            ViewBag.Physical = service.ListPhysicalRate();
            ViewBag.PhysicalPlace = service.ListPhysicalPlace();
            ViewBag.PhysicalSchedule = service.ListPhysicalSchedule();
            ViewBag.Smoke = service.ListSmoke();
            ViewBag.Drink = service.ListDrink();
            ViewBag.DrinkOptions = service.ListOptions("data_drink_wine_options");

            //脑血管疾病
            ViewBag.BrainDisease = service.ListOptions("data_member_brain");
            //肾脏疾病
            ViewBag.Kidney = service.ListOptions("data_member_kidney");
            ViewBag.HeartDisease = service.ListOptions("data_member_heart");
            ViewBag.BloodPipe = service.ListOptions("data_member_artery");
            ViewBag.EyeDisease = service.ListOptions("data_member_eye");
            ViewBag.backKind = backKind;

            Model.TaskId = taskId;

            return PartialView(Model);
        }




        //tsing
        //added on 9/6 接口程序

        /// <summary>
        /// 获得运动频率
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSportRate()
        {
            var items = service.ListOptions("data_member_physical");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获得运动时间段
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSportSchedule()
        {
            var items = service.ListOptions("data_physical_schedule");
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetSportLocation()
        {
            var items = service.ListOptions("data_physical_place");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获得某个会员的运动习惯调查
        /// </summary>
        /// <returns></returns>
        public string GetSporthabbit(int member_id)
        {
            var file = service.GetMemberInfor(member_id);

            var model = new SportHabbit
            {
                data_member_id = member_id,
                totaltime = file.data_physical_total_time.HasValue ? file.data_physical_total_time.Value : 0,
                eachtime = file.data_physical_time.HasValue ? file.data_physical_time.Value : 0,
                sportrate = file.data_physical_rate.HasValue
                    ? file.data_physical_rate.Value
                    : 0,
                sportlocation = file.data_physical_place.HasValue ? file.data_physical_place.Value : 0,
                sportschedule = file.data_physical_schedule.HasValue
                    ? file.data_physical_schedule.Value
                    : 0,
                favoritesport = file.data_physical_item
            };


            //  return Json(model,JsonRequestBehavior.AllowGet);

            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.Indented, timeFormat);


        }
        /// <summary>
        /// 获得某个会员的生活方式
        /// </summary>
        /// <param name="member_id"></param>
        /// <returns></returns>
        public string GetLiveHabbit(int member_id)
        {
            var file = service.GetMemberInfor(member_id);
            var model = new LiveHabbit
            {
                data_member_id = member_id,
                //  food=file.data_food,//
                smoke = file.data_smoke.HasValue ? file.data_smoke.Value : 0,//吸烟情况
                smokeage = file.data_smoke_age.HasValue ? file.data_smoke_age.Value : 0,//吸烟年
                smokeday = file.data_smoke_day.HasValue ? file.data_smoke_day.Value : 0,
                drinkrate = file.data_drink.HasValue ? file.data_drink.Value : 0,// 饮酒频率
                average1 = file.data_drink_average1.HasValue ? file.data_drink_average1.Value : 0,//平均每次
                average2 = file.data_drink_average2.HasValue ? file.data_drink_average2.Value : 0,//平均每次多少瓶
                isdry = file.data_drink_Isdry,//是否戒酒
                drinkdrydate = file.data_drink_Isdry_date.HasValue ? file.data_drink_Isdry_date.Value : DateTime.Now,
                drinkoptions = file.data_drink_wine_options,
                drinkoptionscomments = file.data_drink_wine_options_others,
                food =file.data_food
            };
            // return Json(model, JsonRequestBehavior.AllowGet);

            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.Indented, timeFormat);

        }

        /// <summary>
        /// 获得一个会员的健康状况
        /// </summary>
        /// <param name="member_id"></param>
        /// <returns></returns>
        public string GetMemberHealthy(int member_id)
        {
            var file = service.GetMemberInfor(member_id);
            var model = new MemberInfo();
            model.data_member_id = file.data_member_id.Value;
            model.identifytext = file.data_member_identity;
            model.nationality = file.data_member_country;
            //model.bloodType = file.data_member_blood.HasValue;
            if (file.data_member_blood.HasValue)
                model.bloodtype = file.data_member_blood.Value;
            if (file.data_member_rh.HasValue)
                model.rhproperty = file.data_member_rh.Value;
            if (file.data_member_profession.HasValue)
                model.job = file.data_member_profession.Value;//职业
            model.diseases = file.data_member_disease;//疾病
            model.cancer = file.data_member_disease_c;//恶性肿瘤
            model.otherdiseases = file.data_member_other_disease_others;//其他疾病

            //model.disease1 = file.data_member_disease_1;
            //if (file.data_member_disease_1_date.HasValue)
            //    model.disease1_date = file.data_member_disease_1_date.Value;

            //model.disease2 = file.data_member_disease_2;
            //if (file.data_member_disease_2_date.HasValue)
            //    model.disease2_date = file.data_member_disease_2_date.Value;

            //model.disease3 = file.data_member_disease_3;
            //if (file.data_member_disease_3_date.HasValue)
            //    model.disease3_date = file.data_member_disease_3_date.Value;

            //model.disease4 = file.data_member_disease_4;
            //if (file.data_member_disease_4_date.HasValue)
            //    model.disease4_date = file.data_member_disease_4_date.Value;

            //model.disease5 = file.data_member_disease_5;
            //if (file.data_member_disease_5_date.HasValue)
            //    model.disease5_date = file.data_member_disease_5_date.Value;

            //model.disease6 = file.data_member_disease_6;
            //if (file.data_member_disease_6_date.HasValue)
            //    model.disease6_date = file.data_member_disease_6_date.Value;

            //model.issurgery = file.data_member_surgery;//是否做过手术

            //model.surgery1 = file.data_member_surgery_1;
            //if (file.data_member_surgery_1_date.HasValue)
            //    model.surgery1_date = file.data_member_surgery_1_date.Value;

            //model.surgery2 = file.data_member_surgery_2;
            //if (file.data_member_surgery_2_date.HasValue)
            //    model.surgery2_date = file.data_member_surgery_2_date.Value;

            //model.iswound = file.data_member_injury;//是否受过外伤

            //model.wound1 = file.data_member_injury_1;
            //if (file.data_member_injury_1_date.HasValue)
            //    model.wound1_date = file.data_member_injury_1_date.Value;

            //model.wound2 = file.data_member_injury_2;
            //if (file.data_member_injury_2_date.HasValue)
            //    model.wound2_date = file.data_member_injury_2_date.Value;

            //model.isblood = file.data_member_blood_transfer;//是否输血

            //model.blood1 = file.data_member_blood_transfer_1_reason;
            //if (file.data_member_blood_transfer_1_date.HasValue)
            //    model.blood1_date = file.data_member_blood_transfer_1_date.Value;

            //model.blood2 = file.data_member_blood_transfer_2_reason;
            //if (file.data_member_blood_transfer_2_date.HasValue)
            //    model.blood2_date = file.data_member_blood_transfer_2_date.Value;//第二次输血

            //model.father_disease = file.data_member_family_father;
            //model.mother_disease = file.data_member_family_mother;
            //model.sister_disease = file.data_member_family_brothers;
            //model.children_disease = file.data_member_family_children;

            //model.isinherited = file.data_member_inherited;//是否有遗传疾病
            //model.inherited_disease = file.data_member_inherited_disease;//其他的遗传疾病

            //  return Json(model, JsonRequestBehavior.AllowGet);

            //Tsing 2015/10/12
            //更新
            model.data_member_ethnic = file.data_member_ethnic;

            model.blood_comments = file.data_member_blood_comments;
            model.diseases_comments = file.data_member_diseases_comments;
            model.operstions_comments = file.data_member_operations_comments;
            model.wound_comments = file.data_member_wound_comments;

            model.disease_1 = file.data_member_diseases_1;
           // if (model.disease_1 == true)
           // {
                model.disease_1_relationship = file.data_member_diseases_1_relationship;
                model.disease_1_text = file.data_member_diseases_1_text;
          //  }

            model.disease_2 = file.data_member_diseases_2;//.HasValue && file.data_member_diseases_2.Value;
          //  if (model.disease_2 == true)
          //  {
                model.disease_2_relationship = file.data_member_diseases_2_relationship;
                model.disease_2_text = file.data_member_diseases_2_text;
           // }

            model.disease_3 = file.data_member_diseases_3;//.HasValue && file.data_member_diseases_3.Value;
          //  if (model.disease_3 == true)
          //  {
                model.disease_3_relationship = file.data_member_diseases_3_relationship;
                model.disease_3_text = file.data_member_diseases_3_text;
           // }



            model.data_member_blood_bit = file.data_member_blood_bit;//.HasValue?file.data_member_blood_bit.Value:false;
            model.data_member_diseases_bit = file.data_member_diseases_bit;//.HasValue?file.data_member_diseases_bit.Value:false;
            model.data_member_operations_bit = file.data_member_operations_bit;//.HasValue
              //  ? file.data_member_operations_bit;//.Value: false;
            model.data_member_wound_bit = file.data_member_wound_bit;//.HasValue ? file.data_member_wound_bit.Value : false;
            


            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.Indented, timeFormat);

            //  return JsonConvert.SerializeObject(model);
        }

        public JsonResult UpdateMemberHealthy([FromUri] MemberInfo model)
        {
            var res = new Result<bool>();
            res.IsSuccessful = false;
            if (model == null)
            {
                res.Message = "参数不正确";
                res.IsSuccessful = false;

                return Json(res, JsonRequestBehavior.AllowGet);
            }

            res=service.UpdateMemberInformation(model);
           // res.IsSuccessful = true;
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateLiveHabbit([FromUri] LiveHabbit model)
        {
            var res = new Result<bool>();
            res.IsSuccessful = false;
            if (model == null)
            {
                res.Message = "参数不正确";
                res.IsSuccessful = false;

                return Json(res, JsonRequestBehavior.AllowGet);
            }




            res = service.UpdateMemberLiveHabbit(model);
            // res.IsSuccessful = true;
            return Json(res, JsonRequestBehavior.AllowGet);


        }



        /// <summary>
        /// 提交某个会员的运动习惯调查
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateSporthabbit([FromUri] SportHabbit model)
        {
            var res = new Result<bool>();
            if (model == null)
            {
                res.IsSuccessful = false;
                res.Message = "参数无效";
                return Json(res, JsonRequestBehavior.AllowGet);
            }

            try
            {
              res=  service.UpdateMemberPhysicalInfo(model);
              //  res.IsSuccessful = true;
              //  res.Parameter = true;
            }
            catch (Exception exception)
            {
                res.Message = exception.Message;
                res.IsSuccessful = false;

            }
            return Json(res, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 获得饮食习惯
        /// </summary>
        /// <returns></returns>
        public JsonResult GetFoodHabbit()
        {
            var items = service.ListOptions("data_member_food");
            return Json(items, JsonRequestBehavior.AllowGet);
        }





        /// <summary>
        /// 获得吸烟情况
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSmoke()
        {
            var items = service.ListOptions("data_smoke");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 喝酒频率
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDrinkRate()
        {
            var items = service.ListOptions("data_drink");
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 饮酒种类
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDrinkTypes()
        {
            var items = service.ListOptions("data_drink_wine_options");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获得血型
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBloods()
        {
            var items = service.ListOptions("data_member_blood");
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获得RH
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRHProperties()
        {
            var items = service.ListOptions("data_member_rh");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获得职业列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetJobs()
        {
            var items = service.ListOptions("data_member_profession");
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获得疾病
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDiseases()
        {
            var items = service.ListOptions("data_member_disease");
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }
}