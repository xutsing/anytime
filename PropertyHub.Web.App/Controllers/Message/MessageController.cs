﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNet.Identity;
using PropertyHub.Services;
using Newtonsoft.Json;
using Mapper = AutoMapper.Mapper;


namespace PropertyHub.Web.App.Controllers
{

    /// <summary>
    /// Created by Tsing on 8/5/2015
    /// 这个controller主要用于手机App的调用
    /// </summary>
    public class MessageController : Controller
    {
        private readonly MessageService service;
        private CodeServices cservice;

        public MessageController(MessageService _service,CodeServices _cservice)
        {
            service = _service;
            cservice = _cservice;
        }

        public ActionResult Index()
        {

            return PartialView();
        }


        public ActionResult Reply(int Id)
        {
            var message = service.GetMessageById(Id);
            var model = new MessageDTO();
            Mapper.Map(message, model);


            ViewBag.Knowledges = cservice.ListKnowledge(0);
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JavaScriptResult Reply(MessageDTO model)
        {
          //  var userId = HttpContext.User.Identity.GetUserName();

            model.message_comments = Server.HtmlDecode(model.message_comments);
           var updateresult= service.UpdateMessage(model.data_message_id, model.message_Isreply, model.message_comments,
                HttpContext.User.Identity.GetUserName());

            var result = updateresult ? (model.message_Isreply ? 1 : 2) : 0;

            var js = string.Format("close({0});", result);
            return JavaScript(js);

        }



        /// <summary>
        /// 获得Message的类别，留言的时候用.
        /// </summary>
        /// <returns></returns>
        public string GetMessageTypes()
        {
            var types = service.ListMessageTypes();
            return JsonConvert.SerializeObject(types);
        }

        /// <summary>
        /// 留言 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public string RequestMessage(int member_id,int message_type,string message_text)
        {




            var result = service.CreateMessage(member_id, message_type, message_text, true);
            return JsonConvert.SerializeObject(result);
        }

      
      


        /// <summary>
        /// 获得当前用户的message list
        /// </summary>
        /// <param name="member_id"></param>
        /// <returns></returns>
        public string GetMessages(int member_id,int pagesize,int pagenumber,int? type)
        {
            var messages = service.GetMessages(member_id, type, pagesize, pagenumber);

            var res = new Result<string>();
            res.IsSuccessful = messages.IsSuccessful;
            res.Message = messages.Message;
            res.recordcount = messages.recordcount;

            var readIds = service.GetReadMessages(member_id);


            var items = (messages.Parameter.Select(item => new
            {
                Id = item.data_message_id,
                // type=200,  //对于会员留言的消息都是100
                Isfinished = item.message_Isreply,
                message = item.message_date.ToString(), //Tsing 会员留言时间
                date = item.message_comment_date.ToString(),
                text = item.message_text,
                isread = readIds.Contains(item.data_message_id) ? true : false,
                comments = (item.message_Isreply ? item.message_comments : "健身秘书正在回复中")
            })
                ).ToList();






            res.Parameter = JsonConvert.SerializeObject(items);

            res.pagenumber = pagenumber;
            res.pagesize = pagesize;
            return JsonConvert.SerializeObject(res); 
        }



        public string GetAMessages(int data_message_id,int data_member_id)
        {
            var message = service.GetMessageInformationById(data_message_id, data_member_id);


            var item = new
            {
                Id=message.Parameter.data_message_id,
                Isfinished=message.Parameter.message_Isreply,
                message=message.Parameter.message_date.ToString(),//留言时间
                date=message.Parameter.message_comment_date.ToString(),
                text=message.Parameter.message_text,
                comments = (message.Parameter.message_Isreply ? message.Parameter.message_comments : "健身秘书正在回复中")
            };

            return JsonConvert.SerializeObject(item);


        }


        public string GetUnReadCount(int member_id,int? type)
        {
            var result = service.GetUnReadCount(member_id, type);





            return JsonConvert.SerializeObject(result);
        }





    }
}