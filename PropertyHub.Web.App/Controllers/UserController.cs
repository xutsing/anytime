﻿using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{
    public class UserController : Controller
    {

        private readonly MembershipService _membershipService;
     //   private readonly 

        public UserController(MembershipService membershipservice)
        {
            _membershipService = membershipservice;
        }

        public ActionResult Index()
        {
            return PartialView();
        }


        public ActionResult Edit(string Id)
        {
            var user = _membershipService.GetUserbyId(Id);

            if (user == null)
            {
                //jump to the new view
                return PartialView("New");
            }
            else
            {
                var model = new RegisterAsSubscriptionModel();
                model.User = user;
                return PartialView(model);

            }      
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(RegisterAsSubscriptionModel model)
        {
            model.User.UserName = model.User.Email;
            model.Subscription = new Subscription();
            model.Subscription.CompanyName = "Anytime";
            model.Subscription.Subdomain = "Anytime";


           await _membershipService.UpdateAsSubscription(model.User, model.Subscription);


            if (ModelState.IsValid)
            {
                //   ModelState.AddModelError("", "Success!");
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }



        public ActionResult New()
        {
            return PartialView();
        }

        public ActionResult Updatepassword(string Id)
        {
            RegisterAsSubscriptionModel model = new RegisterAsSubscriptionModel();
            model.User = new ApplicationUser();

            model.User.Id = Id;

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Updatepassword(RegisterAsSubscriptionModel model)
        {
            await _membershipService.ChangePassword(model.User.Id, model.Password);
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            return PartialView(model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(RegisterAsSubscriptionModel model)
        {
            model.User.UserName = model.User.Email;
            model.Subscription = new Subscription();
            model.Subscription.CompanyName = "Anytime";
            model.Subscription.Subdomain = "Anytime";

        
            await _membershipService.RegisterAsSubscription(model.User, model.Subscription, model.Password, model.ConfirmPassword);


            
            if (ModelState.IsValid)
            {
             //   ModelState.AddModelError("", "Success!");
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        public async Task<ActionResult> lockuser(string id)
        {
            await _membershipService.LockUser(id);
            return RedirectToAction("Index"); 
        }

    }
}