﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class IndexDataController : Controller
    {
        private readonly IndexDataService _service;

        public IndexDataController(IndexDataService service)
        {
            _service =service;
        }
        // GET: IndexData
       [CheckPermission]
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            
            return PartialView();
        }

        public ActionResult New()
        {
            var model = new IndexDataDTO();

            ViewBag.Status = _service.ListStatus();
            ViewBag.BaseIndex = _service.ListBaseIndex();
            ViewBag.BodyLabel = _service.ListBodyLabel();
            ViewBag.SexTitle = _service.ListSex();
            ViewBag.AgeRange = _service.ListAgeRange();

            return PartialView(model);
        }


        public ActionResult Edit(int Id)
        {
            var model = new IndexDataDTO();
            model.data_index_data_id = Id;
            var depart = _service.GetIndexData(Id);


            model.data_index_data_id = depart.data_index_data_id;
            model.data_base_index_id = depart.data_base_index_id;
            model.data_body_label_id = depart.data_body_label_id;
            model.data_sex_id = depart.data_sex_id;
            model.data_age_range_id = depart.data_age_range_id;
            model.data_index_data_min = depart.data_index_data_min;
            model.data_index_data_max = depart.data_index_data_max;
            model.data_index_data_point = depart.data_index_data_point;
            model.data_index_data_label = depart.data_index_data_label;

            ViewBag.Status = _service.ListStatus();
            ViewBag.Status = _service.ListStatus();
            ViewBag.BaseIndex = _service.ListBaseIndex();
            ViewBag.BodyLabel = _service.ListBodyLabel();
            ViewBag.SexTitle = _service.ListSex();
            ViewBag.AgeRange = _service.ListAgeRange();

            return PartialView("New", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(IndexDataDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_base_index_id, model.data_body_label_id, model.data_sex_id, model.data_age_range_id,
                    model.data_index_data_min, model.data_index_data_max, model.data_index_data_point, model.data_index_data_label);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(IndexDataDTO model)
        {
            ViewBag.Status = _service.ListStatus();
            ViewBag.Status = _service.ListStatus();
            ViewBag.BaseIndex = _service.ListBaseIndex();
            ViewBag.BodyLabel = _service.ListBodyLabel();
            ViewBag.SexTitle = _service.ListSex();
            ViewBag.AgeRange = _service.ListAgeRange();

            if (ModelState.IsValid)
            {
                var machine = new data_index_data();
                Mapper.Map(model, machine);
                await _service.Update(machine);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}