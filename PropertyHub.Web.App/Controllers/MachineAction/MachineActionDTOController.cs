﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;


namespace PropertyHub.Web.App.Controllers.OData
{
    /// <summary>
    /// added by adam, dto controller for machine 2015-08-01
    /// </summary>
    public class MachineActionDTOController : ODataControllerBase
    {
         private readonly MachineActionService _service;

         public MachineActionDTOController(MachineActionService service)
        {
            _service = service;
        }

        [EnableQuery]
         public IQueryable<MachineActionDTO> Get()
        {
            return _service.ListMachineActions().Project().To<MachineActionDTO>();
        }
    }
}