﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;


namespace PropertyHub.Web.App.Controllers
{
    public class DepartController : Controller
    {
        private readonly SubscriptionService _service;
        private readonly MembershipService _membership;

        public DepartController(SubscriptionService service,MembershipService membership)
        {
            _service = service;
            _membership = membership;
        }

        public ActionResult Index()
        {
            return PartialView();
        }


        public ActionResult EditUsers(int Id)
        {
            var depart = new DepartDTO();
            depart.Id = Id;

            var Leaders = _service.GetDepartMembers(Id).Where(a => a.IsLeader);
            var memebers = _service.GetDepartMembers(Id).Where(a => a.IsLeader == false);

            string leadervalues = string.Empty;
            string membervalues = string.Empty;

            if (Leaders.Count() > 0)
            {
                int i = 0;
                foreach (var leader in Leaders)
                {
                    if (i == 0)
                    {
                        leadervalues = leader.UserId;
                    }
                    else
                    {
                        leadervalues = "," + leader.UserId;
                    }

                    i++;
                }
            }

            ViewBag.leaders = Leaders;


            if (memebers.Count() > 0)
            {
                int i = 0;
                foreach (var member in memebers)
                {
                    if (i == 0)
                    {
                        membervalues = member.UserId;
                    }
                    else
                    {
                        membervalues = "," + member.UserId;
                    }
                    i++;
                }
 
            }
            ViewBag.members = membervalues;

            return PartialView(depart);
 
        }


        public ActionResult New()
        {
            var model = new DepartDTOModel();
            var users = _membership.GetUserItems();
            ViewBag.Users = users;
            return PartialView(model);

        }

        public ActionResult Edit(int Id)
        {
            var model = new DepartDTOModel();
            model.Id = Id;
            var depart = _service.GetDepartById(Id);


            model.DepartName = depart.DepartName;
            model.Reamrk = depart.Reamrk;

            var leaders = depart.Members.Where(a => a.IsLeader);
            var members=depart.Members.Where(a=>a.IsLeader ==false);
            if (leaders.Count() > 0)
            {
                int i = 0;
                foreach (var leader in leaders)
                {
                    if (i == 0)
                    {
                        model.leaders = leader.UserId;
                    }
                    else
                    {
                        model.leaders = model.leaders + "," + leader.UserId;
                    }

                    i++;
                }
            }

            if (members.Count() > 0)
            {
                int i = 0;
                foreach (var member in members)
                {

                    if (i == 0)
                    {
                        model.members = member.UserId;
                    }
                    else
                    {
                        model.members = model.members + "," + member.UserId;
                    }


                    i++;
                }
            }

            var users = _membership.GetUserItems();
            ViewBag.Users = users;
           // return PartialView(model);
            return PartialView("New", model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DepartDTOModel model)
        {
            _service.UpdateDepart(model.Id,model.DepartName, model.Reamrk, model.leaders, model.members);

            if (ModelState.IsValid)
            {
                //   ModelState.AddModelError("", "Success!");
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(DepartDTOModel model)
        {


            _service.AddDepart(model.DepartName, model.Reamrk,model.leaders,model.members);

            if (ModelState.IsValid)
            {
                //   ModelState.AddModelError("", "Success!");
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        public ActionResult Delete(string Id)
        {
            _service.DeletePart(int.Parse(Id));
            return RedirectToAction("Index"); 
        }
	}
}