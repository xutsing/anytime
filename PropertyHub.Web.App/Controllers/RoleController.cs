﻿using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{
    public class RoleController : Controller
    {
        private readonly MembershipService _membershipService;
        public RoleController(MembershipService membershipservice)
        {
            _membershipService = membershipservice;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult New()
        {
            return PartialView();

        }

        public ActionResult Edit(string Id)
        {
            var entity = _membershipService.GetRoleById(Id);
            if (entity != null)
            {
                var role = new RoleDTO();
                role.Id = entity.Id;
                role.Name = entity.Name;
                return PartialView("New", role);
            }

            return PartialView("Index");
        }

        public  ActionResult delete(string Id)
        {
             _membershipService.DeleteRoleById(Id);
            return RedirectToAction("Index");  
           
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(RoleDTO role)
        {
            await _membershipService.UpdateRoe(role.Id ,role.Name);

            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return PartialView(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(RoleDTO role)
        {
            await _membershipService.CreateRole(role.Name);
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return PartialView(role);
        }




        public ActionResult EditPermission(string Id)
        {

            var model = new RolePersmissionDTO();
            model.RoleId = Id;//当前角色ID
            model.PermissionIds = _membershipService.GetRolePermissionids(Id);//获得角色的权限IDs
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPermission(RolePersmissionDTO model)
        {
            _membershipService.UpdateRolePermissions(model.RoleId,model.PermissionIds);
            return RedirectToAction("Index");
 
        }


        public string  GetRolePermission(string roleId,int? Id)
        {
            var permissions = _membershipService.GetRolePermission(roleId, Id);
            var data=Newtonsoft.Json.JsonConvert.SerializeObject(permissions);
            return data;

        
        }



        public ActionResult EditUsers(string Id)
        {

            ViewBag.Users = _membershipService.GetUserItems();
            var users = _membershipService.GetUsersByRole(Id);

            var userIds = string.Empty;

            int i = 0;
            foreach (var id in users)
            {
                if (i == 0)
                {
                    userIds = id;
                }
                else
                {
                    userIds = userIds + "," + id;
                }
                i++;
            }



            var model = new RoleViewModel();
            model.Id = Id;
            model.UserIds = userIds;
            
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(RoleViewModel model)
        {
            
            //if()

            var reslt= _membershipService.UpdateRoleUsers(model.Id,model.UserIds);

            return RedirectToAction("Index");
        }


    }
}