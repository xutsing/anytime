﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System.Web.OData;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// added by adam 2015-08-01
    /// </summary>
    public class MachineTypeController : Controller
    {
        private readonly MachineTypeService _service;

        public MachineTypeController(MachineTypeService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult New()
        {
            var entity = new MachineTypeDTO();
            Mapper.Map(new data_machine_type(), entity);

            return PartialView(entity);
        }

        public ActionResult Edit(int id)
        {
            var entity = new MachineTypeDTO();
            Mapper.Map(_service.Get(id), entity);

            return PartialView("New", entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(MachineTypeDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine_type = new data_machine_type();
                Mapper.Map(model, machine_type);

                await _service.Add(machine_type);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(MachineTypeDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine_type = new data_machine_type();
                Mapper.Map(model, machine_type);

                await _service.Update(machine_type);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

        [EnableQuery]
        public ActionResult Detail(int id)
        {
            var entity = new MachineTypeDTO();
            Mapper.Map(_service.Get(id), entity);

            return PartialView(entity);
        }
    }
}