﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using PropertyHub.Services;


namespace PropertyHub.Web.App.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {

        public readonly DashboardService service;
        public DashboardController(DashboardService _service)
        {
            service = _service;
        }

        public ActionResult Index()
        {
            return PartialView();
        }
        
        public int GetMembersCount()
        {
            //return 0;
            return service.GetMemberCount();
        }
        public int GetTodayMembersCount()
        {
            return service.GetTodayMemberCount();
            //return 0;
        }

        public int GetServicesCount()
        {
            return service.GetServicesCount() ;
        }
        public int GetTodayServicesCount()
        {
            return service.GetTodayServicesCount();
        }

        public int GetCoursesCount()
        {
            return service.GetCoursesCount();
        }

        public int GetTodayCoursesCount()
        {
            return service.GetTodayServicesCount();
        }
        public int GetTasks()
        {
            return service.GetTasks();
        }

        public int GetTodayTasks()
        {
            return service.GetTodayTasks();
        }

        public int GetMessages()
        {
            //System.Web.HttpContext.Current.User.Identity.GetUserId();
            return service.GetMessages();
        }
        public int GetTodayMessages()
        {
            return service.GetTodayMessages();
        }

        public int GetMyChecksCount()
        {
            return service.GetMyChecksCount();
        }

        public int GetTodayMyChecksCount()
        {
            return service.GetTodayMyChecksCount();
        }


        public int GetUnProcessedTask()
        {

            //System.Web.HttpContext
           // 
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return 0;
            var task = service.GetUnProcessedTasks();
            return task;

        }

        public int GetUnReplyMessage()
        {
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                return 0;

            var count = service.GetUnProcessedMessages();
            return count;


        //    return 0;
        }

    }
}