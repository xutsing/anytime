﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class DataDictionaryController : Controller
    {
        // GET: DataDictionary
        private readonly DataDictionaryService _service;

        public DataDictionaryController(DataDictionaryService service)
        {
            _service = service;

        }

        [CheckPermission]
        // GET: DataKnowledge
        public ActionResult Index(string type)
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;
            ViewBag.canCheck = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion

            ViewBag.type = type;

            return PartialView();
        }

        public ActionResult New()
        {
            var model = new DataDictionaryDTO();
            model.data_dictionary_type = "data_knowledge";
            return PartialView(model);
        }


        public ActionResult Edit(int Id)
        {
            var model = new DataDictionaryDTO();
            model.data_dictionary_id = Id;
            var dictionary = _service.GetDataDictionary(Id);

            model.data_dictionary_type = "data_knowledge";
            model.data_dictionary_text = dictionary.data_dictionary_text;
            model.data_dictionary_value = dictionary.data_dictionary_value;

            return PartialView("New", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(DataDictionaryDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_dictionary_type, model.data_dictionary_text, model.data_dictionary_value);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DataDictionaryDTO model)
        {
            if (ModelState.IsValid)
            {
                var dictionary = new data_dictionary();
                Mapper.Map(model, dictionary);
                await _service.Update(dictionary);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}