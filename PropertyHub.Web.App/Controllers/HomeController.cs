﻿using PropertyHub.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly SubscriptionService _subscriptionService;
        public HomeController(SubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }

        public ActionResult Index()
        {
            ViewBag.Test = this.ControllerContext.RouteData.Values["Subscription"];

            //var entity = _subscriptionService.AddTest();
            //var list = _subscriptionService.ListTest().ToList();
            //var t = list.LastOrDefault();
            //ViewBag.ListCount = string.Format("{0} - {1}", list.Count(), t == null ? 

            //    null : (t.Subscription == null ? "(null)" : t.Subscription.CompanyName));

            return View();
        }

        public async Task<ActionResult> Add()
        {
            await _subscriptionService.AddTest();

            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            //this.ModelState.AddModelError
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}