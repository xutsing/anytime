﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web;


//added by adam 2015-08-04
//for upload file with jquery.form.js
namespace PropertyHub.Web.App.Controllers
{
    public class FileUploadController : Controller
    {
        [HttpPost]
        public JsonResult Index()
        {
            var result = new JsonResult();

            try
            {
                string prefix = Request.Form["prefix"];
                var file = Request.Files[0];
                var uploadPath = Server.MapPath(string.Format("~/Uploads/{0}/",prefix));
                System.IO.Directory.CreateDirectory(uploadPath);
                var path = prefix + "_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + "_" + file.FileName;
                file.SaveAs(uploadPath + path);

                result.Data = new { filesaved = true, filepath = string.Format("/Uploads/{0}/",prefix) + path};
            }
            catch (Exception e)
            {
                result.Data = new { filesaved = false, filepath = "" };
            }

            return result;
        }
    }
}