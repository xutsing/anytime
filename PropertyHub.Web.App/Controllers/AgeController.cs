﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;
//author: wxw
//date: 15.08.03

namespace PropertyHub.Web.App.Controllers
{
    public class AgeController : Controller
    {
        private readonly AgeService _service;
        // GET: Age
        [CheckPermission]
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            return PartialView();
        }

        public AgeController(AgeService service)
        {
            _service = service;

        }
        public ActionResult New()
        {
            var model = new AgeDTO();

            ViewBag.Status = _service.ListStatus();

            return PartialView(model);
        }

        public ActionResult Edit(int Id)
        {
            var model = new AgeDTO();
            model.data_age_range_id = Id;
            var depart = _service.GetAge(Id);


            model.data_age_min = depart.data_age_min;
            model.data_age_max = depart.data_age_max;
            model.data_age_title = depart.data_age_title;

            ViewBag.Status = _service.ListStatus();

            return PartialView("New", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(AgeDTO model)
        {

            if (model.data_age_min >= model.data_age_max)
            {
                ModelState.AddModelError("data_age_min","年龄最小值比年龄最大值小");
            }


            if (ModelState.IsValid)
            {
                await _service.Add(model.data_age_min, model.data_age_max, model.data_age_title);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AgeDTO model)
        {
            if (model.data_age_min >= model.data_age_max)
            {
                ModelState.AddModelError("data_age_min", "年龄最小值比年龄最大值小");
            }

      
            if (ModelState.IsValid)
            {
                var machine = new data_age_range();
                Mapper.Map(model, machine);

                await _service.Update(machine);
                return RedirectToAction("index");
            }
            return PartialView("New",model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}