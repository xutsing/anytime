﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Services.WorkerHelper;
using PropertyHub.Web.App.Models;
using System.Web.OData;
using Mapper = AutoMapper.Mapper;
using Newtonsoft.Json;
using System.Web.Mvc;

using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NotesFor.HtmlToOpenXml;



namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// //added by adam 2015-10-05 体质检测报告
    /// </summary>  
    public class CheckReportController : Controller
    {
        private readonly MachineService _service;
        private readonly MemberService _mservice;
        private readonly MemberFileService _fservice;
        private readonly DataDictionaryService _dservice;
        private readonly MemberTestingService _tservice;
        private readonly MessageService _meservice;
        private readonly CodeServices _cservice;
        private readonly AgeService _aservice;
        private readonly IndexDataService _iservice;
        private readonly BodyLabelService _bservice;
        private readonly BaseIndexService _biservice;
        private readonly MemberFilesService _mfservice;



        public CheckReportController(MachineService service, MemberService mservice, 
            MemberFileService fservice, DataDictionaryService dservice, MemberTestingService tserice, 
            MessageService meservice, CodeServices cservice, AgeService aservice, IndexDataService iservice, 
            BodyLabelService bservice, BaseIndexService biservice, MemberFilesService mfservice)
        {
            _service = service;
            _mservice = mservice;
            _fservice = fservice;
            _dservice = dservice;
            _tservice = tserice;
            _meservice = meservice;
            _cservice = cservice;
            _aservice = aservice;
            _iservice = iservice;
            _bservice = bservice;
            _biservice = biservice;
            _mfservice = mfservice;
           
        }

        public ActionResult New(int member_id)
        {
            //DocumentHelper.GenerateDocumentUsingSampleDocGenerator();

            ViewBag.KnowledgeTypes = _meservice.ListKnowledgeType();
            ViewBag.Knowledges = _cservice.ListKnowledge(0);
            ViewBag.SmokingType = _dservice.ListSmokingType();
            ViewBag.DrinkingType = _dservice.ListDrinkingType();
            ViewBag.ExerciseFrequency = _dservice.ListExerciseFrequency();
            ViewBag.ExerciseTime = _dservice.ListExerciseTime();

             var entity = new CheckReportModel();
             entity.memberId = member_id;
             var s_member_id = member_id.ToString();


             //基础信息
             // entity.baseInfor = new CheckReportModel.BaseInfor();
             entity.Name = _mservice.Get(member_id).data_member_name;
             entity.Sex = _mservice.Get(member_id).data_sex.data_sex_title;
             entity.Age = (DateTime.Now.Year - _mservice.Get(member_id).data_member_birthday.Value.Year).ToString();
             entity.Phone = _mservice.Get(member_id).data_member_mobile;
             entity.Nation = _fservice.GetMemberInfor(member_id).data_member_ethnic;
             int? JobID = _fservice.GetMemberInfor(member_id).data_member_profession;
             entity.Job = _dservice.GetDataDictionary(JobID).data_dictionary_text;
             entity.Height = _mservice.Get(member_id).data_member_height;
             entity.Weight = _fservice.GetMemberInfor(member_id).data_member_weight.ToString();
             entity.LoginName = _mservice.Get(member_id).data_member_loginname;
            

             //既往史
             //entity.illHistory = new CheckReportModel.IllHistory();
             //疾病
             entity.isIll = _fservice.GetMemberInfor(member_id).data_member_diseases_bit;
             if (entity.isIll == true)
             {
                 entity.illName = _fservice.GetMemberInfor(member_id).data_member_diseases_comments;
             }//entity.illDate = _fservice.GetMemberInfor(member_id). 

             //手术
             entity.isSurgery = _fservice.GetMemberInfor(member_id).data_member_operations_bit;
             if (entity.isSurgery == true)
             {
                 entity.surgeryName = _fservice.GetMemberInfor(member_id).data_member_operations_comments;
             }
             //entity.surgeryDate = _fservice.GetMemberInfor(member_id).

             //外伤
             entity.isWound = _fservice.GetMemberInfor(member_id).data_member_wound_bit;
             if (entity.isWound == true)
             {
                 entity.woundName = _fservice.GetMemberInfor(member_id).data_member_wound_comments;
             }
             //entity.woundDate = _fservice.GetMemberInfor(member_id).

             //输血
             entity.isBlood = _fservice.GetMemberInfor(member_id).data_member_blood_bit;
             if (entity.isBlood == true)
             {
                 entity.bloodName = _fservice.GetMemberInfor(member_id).data_member_blood_comments;
             }
             //entity.bloodDate = _fservice.GetMemberInfor(member_id).

             //家族史
             //疾病1
             entity.isIll_H1 = _fservice.GetMemberInfor(member_id).data_member_diseases_1;
             entity.ill_H1Name = _fservice.GetMemberInfor(member_id).data_member_diseases_1_text;
             entity.ill_H1Relation = _fservice.GetMemberInfor(member_id).data_member_diseases_1_relationship;

             //疾病2
             entity.isIll_H2 = _fservice.GetMemberInfor(member_id).data_member_diseases_2;
             entity.ill_H2Name = _fservice.GetMemberInfor(member_id).data_member_diseases_2_text;
             entity.ill_H2Relation = _fservice.GetMemberInfor(member_id).data_member_diseases_2_relationship;

             //疾病3
             entity.isIll_H3 = _fservice.GetMemberInfor(member_id).data_member_diseases_3;
             entity.ill_H3Name = _fservice.GetMemberInfor(member_id).data_member_diseases_3_text;
             entity.ill_H3Relation = _fservice.GetMemberInfor(member_id).data_member_diseases_3_relationship;

             //?????????????????????????????????????????????????????????????????????????????????????????????????????????
             //生活方式
             //entity.habit = new CheckReportModel.LivingHabit();
             //饮食习惯
             //entity.LivingisDiet1
             //entity.LivingisDiet2
             //entity.LivingisDiet3
             //entity.LivingisDiet4
             //entity.LivingisDiet5
             //entity.LivingisDiet6

             //吸烟情况
             if (_fservice.GetMemberInfor(member_id).data_smoke == 1)
             {
                 entity.isSmoking1 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_smoke == 2)
             {
                 entity.isSmoking2 = true;
             }
             else
             {
                 entity.isSmoking3 = true;
             }
             entity.smokeAge = _fservice.GetMemberInfor(member_id).data_smoke_age.ToString();
             entity.smokeAmount = _fservice.GetMemberInfor(member_id).data_smoke_day.ToString();

             //饮酒情况
             if (_fservice.GetMemberInfor(member_id).data_drink == 92)
             {
                 entity.isDrinking1 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_drink == 93)
             {
                 entity.isDrinking2 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_drink == 94)
             {
                 entity.isDrinking3 = true;
             }
             else
             {
                 entity.isDrinking4 = true;
             }
             //几两
             entity.drinkingAmount1 = _fservice.GetMemberInfor(member_id).data_drink_average1.ToString();
             //几瓶
             entity.drinkingAmount2 = _fservice.GetMemberInfor(member_id).data_drink_average2.ToString();

             //????????????????????????????????????????????????????????????????????????????????????????????????
             //戒酒情况
             if (_fservice.GetMemberInfor(member_id).data_drink_Isdry)
             {
                 entity.isDryOut1 = false;
                 entity.isDryOut2 = true;
             }
             else
             {
                 entity.isDryOut1 = true;
                 entity.isDryOut2 = false;
             }
             entity.dryOutDate = _fservice.GetMemberInfor(member_id).data_drink_Isdry_age.ToString();


             //饮酒种类
             entity.isWine1 = false;
             entity.isWine2 = false;
             entity.isWine3 = false;
             entity.isWine4 = false;

             if (_fservice.GetMemberInfor(member_id).data_drink_wine_options == "白酒")
             {
                 entity.isWine1 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_drink_wine_options == "啤酒")
             {
                 entity.isWine2 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_drink_wine_options == "红酒")
             {
                 entity.isWine3 = true;
             }
             else if (_fservice.GetMemberInfor(member_id).data_drink_wine_options == "黄酒")
             {
                 entity.isWine4 = true;
             }

             entity.otherWine = _fservice.GetMemberInfor(member_id).data_drink_wine_options_others;

             //运动风险
             //entity.exeRisk = new CheckReportModel.ExeciseRisk();
             var testingModel = new TestingModel();
             var exeRisk = _tservice.GetTesting(member_id, 1);
             string results = exeRisk.data_questions_testing_result_json;
             testingModel.Questions = JsonConvert.DeserializeObject<IList<TestingItemModel>>(results);

             int counts = testingModel.Questions.Count;

             if (counts >= 1)
             {
                 if (testingModel.Questions[0].Answer == "True")
                 {
                     entity.isQ1 = true;
                     entity.isQ1_text = "是";
                 }
                 else
                 {

                     entity.isQ1 = true;
                     entity.isQ1_text = "否";
                 }
             }

             if (counts >= 2)
             {
                 if (testingModel.Questions[1].Answer == "True")
                 {
                     entity.isQ2 = true;
                     entity.isQ2_text = "是";
                 }
                 else
                 {
                     entity.isQ2 = false;
                     entity.isQ2_text = "否";
                 }
             }

             if (counts >= 3)
             {
                 if (testingModel.Questions[2].Answer == "True")
                 {
                     entity.isQ3 = true;
                     entity.isQ3_text = "是";
                 }
                 else
                 {
                     entity.isQ3 = false;
                     entity.isQ3_text = "否";
                 }
             }

             if (counts >= 4)
             {
                 if (testingModel.Questions[3].Answer == "True")
                 {
                     entity.isQ4 = true;
                     entity.isQ4_text = "是";
                 }
                 else
                 {
                     entity.isQ4 = false;
                     entity.isQ4_text = "否";
                 }
             }

             if (counts >= 5)
             {
                 if (testingModel.Questions[4].Answer == "True")
                 {
                     entity.isQ5 = true;
                     entity.isQ5_text = "是";
                 }
                 else
                 {
                     entity.isQ5 = true;
                     entity.isQ5_text = "否";
                 }
             }

             if (counts >= 6)
             {
                 if (testingModel.Questions[5].Answer == "True")
                 {
                     entity.isQ6 = true;
                     entity.isQ6_text = "是";
                 }
                 else
                 {
                     entity.isQ6 = false;
                     entity.isQ6_text = "否";
                 }
             }

             if (counts >= 7)
             {
                 if (testingModel.Questions[6].Answer == "True")
                 {
                     entity.isQ7 = true;
                     entity.isQ7_text = "是";
                 }
                 else
                 {
                     entity.isQ7 = false;
                     entity.isQ7_text = "否";
                 }
             }

             if (counts >= 8)
             {
                 if (testingModel.Questions[7].Answer == "True")
                 {
                     entity.isQ8 = true;
                     entity.isQ8_text = "是";
                 }
                 else
                 {
                     entity.isQ8 = false;
                     entity.isQ8_text = "否";
                 }
             }

             //entity.riskEstimate

             //运动习惯
             //entity.exeHabit = new CheckReportModel.ExeciseHabit();

             //运动历程
             entity.exeYears = _fservice.GetMemberInfor(member_id).data_physical_total_time.ToString();
             entity.exeMinites = _fservice.GetMemberInfor(member_id).data_physical_time.ToString();

             //运动频率
             //entity.isRate1 = false;
             //entity.isRate2 = false;
             //entity.isRate3 = false;
             //entity.isRate4 = false;
            entity.isRate = _fservice.GetMemberInfor(member_id).data_physical_rate;


             //运动时间段
             entity.isTime1 = false;
             entity.isTime2 = false;
             entity.isTime3 = false;
             entity.isTime4 = false;
             entity.isTime5 = false;

            
             if (_fservice.GetMemberInfor(member_id).data_physical_schedule == 84)
             {
                 entity.isTime1 = true;
                 entity.isTime_text = "早晨";
             }
             else if (_fservice.GetMemberInfor(member_id).data_physical_schedule == 85)
             {
                 entity.isTime2 = true;
                 entity.isTime_text = "中午" + " " + entity.isTime_text; 
             }
             else if (_fservice.GetMemberInfor(member_id).data_physical_schedule == 86)
             {
                 entity.isTime3 = true;
                 entity.isTime_text = "下午" + " " + entity.isTime_text; 
             }
             else if (_fservice.GetMemberInfor(member_id).data_physical_schedule == 87)
             {
                 entity.isTime4 = true;
             }
             else
             {
                 entity.isTime5 = true;
                 entity.isTime_text = "不固定";
             }
             //运动场所
             entity.isPlace1 = false;
             entity.isPlace2 = false;
             entity.isPlace3 = false;
             entity.isPlace4 = false;

             if (_fservice.GetMemberInfor(member_id).data_physical_place == 80)
             {
                 entity.isPlace1 = true;
                 entity.isPlace_text = "健身房";
             }
             else if (_fservice.GetMemberInfor(member_id).data_physical_place == 81)
             {
                 entity.isPlace2 = true;
                 entity.isPlace_text = "体育馆";
             }
             else if (_fservice.GetMemberInfor(member_id).data_physical_place == 82)
             {
                 entity.isPlace3 = true;
                 entity.isPlace_text = "户外";
             }
             else
             {
                 entity.isPlace4 = true;
                 entity.isPlace_text = "室内";
             }
   
             entity.favSports = _fservice.GetMemberInfor(member_id).data_physical_item;
             //体质测试报告
             var bodyreportResult = _cservice.GetBodyReport(member_id);

             var tmp = bodyreportResult.Parameter;

             foreach (var result in tmp)
             {

                 var num = int.Parse(result.data_body_label_text);

                 switch (result.data_base_index_title)
                 {
                     case "体重":
                         {
                             entity.Score_1 = result.data_member_value;
                             entity.Weight = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_1 = "";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_1 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_1 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_1 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_1 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_1 = "运动健将";
                                 }
                             }
                             break;
                         }
                     case "握力":
                         {
                             entity.Score_2 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_2 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_2 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_2 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_2 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_2 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_2 = "运动健将";
                                 }
                             }
                             break;

                         }

                     case "肺活量":
                         {
                             entity.Score_3 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_3 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_3 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_3 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_3 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_3 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_3 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "俯卧撑":
                         {
                             entity.Score_4 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_4 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_4 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_4 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_4 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_4 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_4 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "仰卧起坐":
                         {
                             entity.Score_5 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_5 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_5 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_5 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_5 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_5 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_5 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "台阶试验":
                         {
                             entity.Score_6 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_6 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_6 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_6 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_6 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_6 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_6 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "坐位体前屈":
                         {
                             entity.Score_7 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_7 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_7 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_7 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_7 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_7 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_7 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "闭眼单脚站立":
                         {
                             entity.Score_8 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_8 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_8 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_8 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_8 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_8 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_8 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "纵跳":
                         {
                             entity.Score_9 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_9 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_9 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_9 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_9 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_9 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_9 = "运动健将";
                                 }
                             }
                             break;
                         }

                     case "选择反应时":
                         {
                             entity.Score_10 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_10 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_10 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_10 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_10 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_10 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_10 = "运动健将";
                                 }
                             }
                             break;
                         }
                     //base index没有这个指标?????????????????????????????????????????????????????????????????????????????????
                     case "心率":
                         {
                             entity.Score_11 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_11 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_11 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_11 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_11 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_11 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_11 = "运动健将";
                                 }
                             }
                             break;
                         }
                     case "身体脂肪":
                         {
                             entity.Score_12 = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.Level_12 = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.Level_12 = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.Level_12 = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.Level_12 = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.Level_12 = "运动能手";
                                 }
                                 else
                                 {
                                     entity.Level_12 = "运动健将";
                                 }
                             }
                             break;
                         }
                     case "体质指数(BMI)":
                         {
                             entity.BMI = result.data_member_value;
                             if (result.data_body_label_text == null)
                             {
                                 entity.BMI_Score = "马上行动";
                             }
                             else
                             {
                                 if (num >= 0 && num < 20)
                                 {
                                     entity.BMI_Score = "急需改善";
                                 }
                                 else if (num >= 20 && num < 40)
                                 {
                                     entity.BMI_Score = "马上行动";
                                 }
                                 else if (num >= 40 && num < 60)
                                 {
                                     entity.BMI_Score = "保持运动";
                                 }
                                 else if (num >= 60 && num < 80)
                                 {
                                     entity.BMI_Score = "运动能手";
                                 }
                                 else
                                 {
                                     entity.BMI_Score = "运动健将";
                                 }
                             }
                             break;
                         }
                     case "肌肉含量":
                         {
                             entity.Muscle = result.data_member_value;
                             break;
                         }
                     case "体脂含量":
                        {
                            entity.BodyFat = result.data_member_value;
                            break;
                        }
                     case "身高":
                        {
                            entity.Height = result.data_member_value;
                            break;
                        }
                 }
             }

            //运动处方       
             
             var age_id = _aservice.GetAgeId(int.Parse(entity.Age));
             var sex_id = _mservice.Get(member_id).data_sex.data_sex_id;
             var bodylabel_id = _bservice.GetBodyLabel(3);

             var baseindex_id = _biservice.GetBaseIndexIdByName("BMI");
             if (age_id != null && sex_id != null && bodylabel_id != null && baseindex_id != null)
             {
                 entity.BMI_Min = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
                 entity.BMI_Max = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
             }

             baseindex_id = _biservice.GetBaseIndexIdByName("体重");
             if (age_id != null && sex_id != null && bodylabel_id != null && baseindex_id != null)
             {
                 entity.Weight_Min = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
                 entity.Weight_Max = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
             }

             baseindex_id = _biservice.GetBaseIndexIdByName("肌肉含量");
             if (age_id != null && sex_id != null && bodylabel_id != null && baseindex_id != null)
             {
                 entity.Muscle_Min = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
                 entity.Muscle_Max = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
             }

             baseindex_id = _biservice.GetBaseIndexIdByName("体脂含量");
             if (age_id != null && sex_id != null && bodylabel_id != null && baseindex_id != null)
             {
                 entity.BodyFat_Min = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
                 entity.BodyFat_Max = _iservice.GetIndexDataMin(baseindex_id, bodylabel_id, sex_id, age_id);
             }    

            return PartialView(entity);
        }


        //public ActionResult Edit(int memberId)
        //{
        //    var cmodel = new CheckReportModel();
        //    cmodel.memberId = memberId;


        //    ViewBag.Status = _service.ListStatus();

        //    return PartialView("New", cmodel);
        //}


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(CheckReportModel model)
        {

            if (ModelState.IsValid)
            {
                //if (model.isIll != null)
                //{
                    if (model.isIll == false)
                    {
                        model.isIll_text = "有";
                    }
                    else
                    {
                        model.isIll_text = "无";
                    }//entity.illDate = _fservice.GetMemberInfor(member_id). 
                //}

                //if (model.isSurgery != null)
                //{
                    if (model.isSurgery == false)
                    {
                        model.isSurgery_text = "有";
                    }
                    else
                    {
                        model.isSurgery_text = "无";
                    }
                //}

                if (model.isWound == false)
                {
                    model.isWound_text = "有";
                }
                else
                {
                    model.isWound_text = "无";
                }
                if (model.isBlood == false)
                {
                    model.isBlood_text = "有";
                }
                else
                {
                    model.isBlood_text = "无";
                }
                //饮食情况
                if (model.isDiet1 == true)
                {
                    model.isDiet_text = "荤素均衡";
                }
                if (model.isDiet2 == true)
                {
                    model.isDiet_text = "荤食为主";
                }
                if (model.isDiet3 == true)
                {
                    model.isDiet_text = "素食为主";
                }
                if (model.isDiet4 == true)
                {
                    model.isDiet_text = "嗜盐" + " " + model.isDiet_text;
                }
                if (model.isDiet5 == true)
                {
                    model.isDiet_text = "嗜油" + " " + model.isDiet_text;
                }
                if (model.isDiet6 == true)
                {
                    model.isDiet_text = "嗜糖" + " " + model.isDiet_text;
                }
                //吸烟情况
                if (model.isSmoking1 == true)
                {
                    model.isSmoking_text = "从不吸烟";
                }

                if (model.isSmoking2 == true)
                {
                    model.isSmoking_text = "已戒烟";
                }

                if (model.isSmoking3 == true)
                {
                    model.isSmoking_text = "吸烟";
                }
                //戒酒
                if (model.isDryOut1 == true)
                {
                    model.isDryOut_text = "是";
                }

                if (model.isDryOut2 == true)
                {
                    model.isDryOut_text = "否";
                }
                //饮酒种类
                if (model.isWine1 == true)
                {
                    model.isWine_text = "白酒";
                }
                if (model.isWine2 == true)
                {
                    model.isWine_text = "啤酒" + " " + model.isWine_text;
                }
                if (model.isWine3 == true)
                {
                    model.isWine_text = "红酒" + " " + model.isWine_text;
                }
                if (model.isWine4 == true)
                {
                    model.isWine_text = "黄酒" + " " + model.isWine_text;
                }

                model.isWine_text = model.isWine_text + " " + model.otherWine;
                //运动状况调查表
                //if (model.isQ1 != null)
                //{
                    if (model.isQ1 == true)
                    {
                        model.isQ1_text = "是";
                    }
                    else
                    {
                        model.isQ1_text = "否";
                    }
                //}
                //if (model.isQ2 != null)
                //{
                    if (model.isQ2 == true)
                    {
                        model.isQ2_text = "是";
                    }
                    else
                    {
                        model.isQ2_text = "否";
                    }
                //}
                //if (model.isQ3 != null)
                //{
                    if (model.isQ3 == true)
                    {
                        model.isQ3_text = "是";
                    }
                    else
                    {
                        model.isQ3_text = "否";
                    }
                //}
                //if (model.isQ4 != null)
                //{
                    if (model.isQ4 == true)
                    {
                        model.isQ4_text = "是";
                    }
                    else
                    {
                        model.isQ4_text = "否";
                    }
                //}
                //if (model.isQ5 != null)
                //{
                    if (model.isQ5 == true)
                    {
                        model.isQ5_text = "是";
                    }
                    else
                    {
                        model.isQ5_text = "否";
                    }
                //}
                //if (model.isQ6 != null)
                //{
                    if (model.isQ6 == true)
                    {
                        model.isQ6_text = "是";
                    }
                    else
                    {
                        model.isQ6_text = "否";
                    }
                //}
                //if (model.isQ7 != null)
                //{
                    if (model.isQ7 == true)
                    {
                        model.isQ7_text = "是";
                    }
                    else
                    {
                        model.isQ7_text = "否";
                    }
                //}
                //if (model.isQ8 != null)
                //{
                    if (model.isQ8 == true)
                    {
                        model.isQ8_text = "是";
                    }
                    else
                    {
                        model.isQ8_text = "否";
                    }
                //}
                //运动频率
                if (model.isRate == 76)
                {
                    model.isRate_text = "3次以上/周";
                }
                else if (model.isRate == 77)
                {
                    model.isRate_text = "1-2次/周";
                }
                else if (model.isRate == 78)
                {
                    model.isRate_text = "偶尔";
                }
                else
                {
                    model.isRate_text = "不锻炼";
                }
                //运动时间段
                if (model.isTime1 == true)
                {
                    model.isTime_text = "早晨";
                }
                if (model.isTime2 == true)
                {
                    model.isTime_text = "中午" + " " + model.isTime_text; 
                }
                if (model.isTime3 == true)
                {
                    model.isTime_text = "下午" + " " + model.isTime_text; 
                }
                if (model.isTime4 == true)
                {
                    model.isTime_text = "晚上" + " " + model.isTime_text; 
                }
                if (model.isTime5 == true)
                {                              
                    model.isTime_text = "不固定";
                }
                //运动场所
                if (model.isPlace1 == true)
                {
                    model.isPlace_text = "健身房";
                }
                if (model.isPlace2 == true)
                {
                    model.isPlace_text = "体育馆";
                }
                if (model.isPlace3 == true)
                {
                    model.isPlace_text = "户外";
                }
                if (model.isPlace4 == true)
                {
                    model.isPlace_text = "室内";
                }
    
                //Tsing
                //updated on the 10/26
                try
                {
                    DocumentHelper.GenerateDocumentUsingSampleDocGenerator(model, _mfservice);

                }
                catch (Exception ex)
                { 
                                        
                }

                //return RedirectToAction("Index");
                return RedirectToAction("Index", "DataMemberFiles");
            }

            return PartialView(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit(CheckReportModel model)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        var memberfiles = new data_check_report();
        //        Mapper.Map(model, memberfiles);
        //        await service.Update(memberfiles);
        //        return RedirectToAction("index");
        //    }
        //    return PartialView(model);
        //}


    }
}