﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System.Web.OData;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// added by adam 2015-08-01
    /// </summary>  
    public class MachineController : Controller
    {
        private readonly MachineService _service;

        public MachineController(MachineService service)
        {
            _service = service;
        }

        [CheckPermission]
        public ActionResult Index()
        {
            #region button access control
            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canView = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            if (actionList == null)
                return PartialView();

            foreach (var action in actionList) 
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "查看")
                {
                    ViewBag.canView = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion

            return PartialView();
        }

        public ActionResult New()
        {
            var entity = new MachineDTO();
            Mapper.Map(new data_machine(), entity);

            ViewBag.Status = _service.ListStatus();
            ViewBag.MachineTypes = _service.ListMachineTypes();

            return PartialView(entity);
        }

        public ActionResult Edit(int id)
        {
            var entity = new MachineDTO();
            Mapper.Map(_service.Get(id), entity);
            
            ViewBag.Status = _service.ListStatus();
            ViewBag.MachineTypes = _service.ListMachineTypes();

            return PartialView("New", entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(MachineDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine = new data_machine();
                Mapper.Map(model, machine);

                await _service.Add(machine);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(MachineDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine = new data_machine();
                Mapper.Map(model, machine);

                await _service.Update(machine);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

        [EnableQuery]
        public ActionResult Detail(int id)
        {
            var entity = new MachineDTO();
            Mapper.Map(_service.Get(id), entity);

            return PartialView(entity);
        }
    }
}