﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text.pdf.qrcode;
using iTextSharp.tool.xml.html.head;
using PropertyHub.Web.App.Models;

namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// Created by Tsing
    /// this controller is used ot createa a html.to print.
    /// </summary>
    public class PrintController : Controller
    {

        /// <summary>
        /// created by Tsing
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Print(PrintModel model)
        {
            model.Queryhtml = model.begindate + "~" + model.enddate;
            return View(model);
        }
    }
}