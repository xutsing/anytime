﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class CodeStatusController : Controller
    {
        // GET: CodeStatus
        private readonly CodeServices _service;

        public CodeStatusController(CodeServices service)
        {
            _service = service;

        }
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Wrong()
        {
            return PartialView();
        }
        public ActionResult New()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Index(CodeStatusDTO model)
        {
            if (model.data_code_text != null)
            {
               var result = _service.GetCodeStatus(model.data_code_text);

               if (result != null)
               { 
                    var codestatus = new CodeStatusDTO();
                     Mapper.Map(result, codestatus);

                    if (result.data_code_status_text == "激活")
                    {
                         codestatus.data_member_actived_text = _service.GetMemberName(codestatus.data_member_actived);
                    }

                    return PartialView("New", codestatus);
               }
               else
               {
                   return PartialView("Wrong");
               }    
            }          
            return PartialView(model);  
        }


        [HttpPost]
        public ActionResult Query(string text)
        {
            if (text != null)
            {
                var result = _service.GetCodeStatus(text);

                if (result != null)
                {
                    var codestatus = new CodeStatusDTO();
                    Mapper.Map(result, codestatus);

                    if (result.data_code_status_text == "激活")
                    {
                        codestatus.data_member_actived_text = _service.GetMemberName(codestatus.data_member_actived);
                    }

                    return PartialView("New", codestatus);
                }
                else
                {
                    return PartialView("Wrong");
                }
            }
            return PartialView("Index");
        }
    }
}