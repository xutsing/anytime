﻿using Microsoft.AspNet.Identity.Owin;
using PropertyHub.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{
    public class LoginController : ControllerBase
    {
        private readonly MembershipService _membershipService;

        public LoginController(MembershipService membershipService)
        {
            _membershipService = membershipService;
            ViewBag.BodyClass = "gray-bg";
        }

        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(string username, string password, string returnUrl)
        {
            var result = await _membershipService.Login(username, password, false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Redirect(string.IsNullOrWhiteSpace(returnUrl) ? "/" : returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("error", "The email and the password is invalid.");
                    break;
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            _membershipService.SignOut();
            return Redirect("/");
        }
    }
}