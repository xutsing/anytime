﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class DataKnowledgeController : Controller
    {
        private readonly DataKnowledgeService _service;

        public DataKnowledgeController(DataKnowledgeService service)
        {
            _service = service;

        }
        [CheckPermission]
        // GET: DataKnowledge
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;
            ViewBag.canCheck = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }


                if (action.PermissionName == "审核")
                {
                    ViewBag.canCheck = true;
                }


                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            return PartialView();
        }

        public ActionResult New()
        {
            var model = new DataKnowledgeDTO();

            ViewBag.Status = _service.ListStatus();
            ViewBag.DataKnowledgeType = _service.ListDataKnowledgeType();

            return PartialView(model);
        }



        public ActionResult Edit(int Id)
        {
            var model = new DataKnowledgeDTO();
            model.data_knowledge_id= Id;
            var knowledge = _service.GetDataKnowledge(Id);

            model.data_knowledge_type = knowledge.data_knowledge_type;
            model.data_knowledge_title = knowledge.data_knowledge_title;
            model.data_knowledge_summary = knowledge.data_knowledge_summary;
            model.data_knowledge_remark = knowledge.data_knowledge_remark;
            model.data_knowledge_path = knowledge.data_knowledge_path;
            model.data_knowledge_author_type = knowledge.data_knowledge_author_type;
            model.data_knowledge_author_id = knowledge.data_knowledge_author_id;
            model.data_knowledge_status = knowledge.data_knowledge_status;
            model.data_knowledge_keyword = knowledge.data_knowledge_keyword;
            model.createdby_id = knowledge.createdby_id;
            model.createdby_name = knowledge.createdby_name;


        
          

            ViewBag.Status = _service.ListStatus();
            ViewBag.DataKnowledgeType = _service.ListDataKnowledgeType();

            return PartialView("New", model);
        }

        public ActionResult Check(int Id)
        {
            var model = new DataKnowledgeDTO();
            model.data_knowledge_id = Id;
            var knowledge = _service.GetDataKnowledge(Id);

            Mapper.Map(knowledge, model);


            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> Checks(int[] ids)
        {
            await _service.Checks(ids);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(DataKnowledgeDTO model)
        {
            if (ModelState.IsValid)
            {

                model.data_knowledge_summary = Server.HtmlDecode(model.data_knowledge_summary);
                await _service.Add(model.data_knowledge_type, model.data_knowledge_title,
                    model.data_knowledge_summary, model.data_knowledge_remark, model.data_knowledge_path, model.data_knowledge_author_type,
                    model.data_knowledge_author_id, model.data_knowledge_keyword, model.createdby_id, model.createdby_name);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DataKnowledgeDTO model)
        {
            if (ModelState.IsValid)
            {

                model.data_knowledge_summary = Server.HtmlDecode(model.data_knowledge_summary);
                var knowledge = new data_knowledge();
                Mapper.Map(model, knowledge);
                await _service.Update(knowledge);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int[] ids)
        {
            await _service.Delete(ids);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Check(DataKnowledgeDTO model)
        {
            if (ModelState.IsValid)
            {
                var knowledge = new data_knowledge();
                Mapper.Map(model, knowledge);
                await _service.UpdateState(knowledge);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        [HttpGet]
        public JsonResult GetKnowledgeListByCondi(int? data_knowledge_type, string data_knowledge_keyword)
        {
            IList<data_knowledge> list = _service.ListKnowledge(data_knowledge_type, data_knowledge_keyword).ToList();
 
            var result = new object[list.Count()];

            for (var i=0; i<list.Count(); i++)
            {
                result[i] = (new {
                    value = list.ElementAt(i).data_knowledge_id,
                    text = list.ElementAt(i).data_knowledge_keyword + " " + list.ElementAt(i).data_knowledge_title
                });
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}