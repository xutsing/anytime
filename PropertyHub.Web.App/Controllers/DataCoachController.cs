﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class DataCoachController : Controller
    {

         private readonly DataCoachService _service;

        public DataCoachController(DataCoachService service)
        {
            _service = service;

        }
        // GET: DataCoach
        [CheckPermission]
        // GET: DataKnowledge
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;
            ViewBag.canCheck = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
               if (action.PermissionName == "查看")
               {
                   ViewBag.canAdd = true;
               }

            //    if (action.PermissionName == "编辑")
            //    {
            //        ViewBag.canEdit = true;
            //    }


            //    if (action.PermissionName == "审核")
            //    {
            //        ViewBag.canCheck = true;
            //    }


            //    if (action.PermissionName == "删除")
            //    {
            //        ViewBag.canDelete = true;
            //    }
            }
            #endregion
            return PartialView();
        }
   

          public ActionResult Detail(int Id)
        {
            var model = new DataCoachDTO();

            var coach = _service.Get(Id);

            model.AreaName = coach.AreaName;
            model.CityName = coach.CityName;
            model.ProvinceName = coach.ProvinceName;
            model.data_age = coach.data_age;
            model.data_anytime_friend_id = coach.data_anytime_friend_id;
            model.data_area_Id = coach.data_anytime_friend_id;
            model.data_city_Id = coach.data_city_Id;
            model.data_comments = coach.data_comments;
            model.data_Ip = coach.data_Ip;
            model.data_name = coach.data_name;
            model.data_profession = coach.data_profession;
            model.data_province_Id = coach.data_province_Id;
            model.data_sex_Id = coach.data_sex_Id;
            model.data_sex_title = coach.data_sex_title;
            model.datecreated = coach.datecreated;


            return PartialView(model);
        }
    }
}