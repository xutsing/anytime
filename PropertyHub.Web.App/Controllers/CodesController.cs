﻿using iTextSharp.text.pdf.qrcode;
using Microsoft.Ajax.Utilities;
using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft;
using System.Data;
using System.IO;
using System.Collections;
using System.ComponentModel;

namespace PropertyHub.Web.App.Controllers
{
    public class CodesController : Controller
    {
        //
        // GET: /Codes/
        private readonly CodeServices service;

        private readonly MembershipService mservice;

        public CodesController(CodeServices _service,MembershipService _mservice)
        {
            service = _service;
            mservice = _mservice;
        }
        
        public ActionResult Index()
        {
            return PartialView();
        }

        /// <summary>
        /// 激活码激活
        /// </summary>
        /// <param name="code"></param>
        /// <param name="member_id"></param>
        /// <returns></returns>
        public JsonResult ActiveCode(string code, int member_id)
        {
            var result = service.RegisterCode(code,member_id);
            return Json(result, JsonRequestBehavior.AllowGet);
         
        }

        public ActionResult New()
        {
            var entity = new data_code_record();
            ViewBag.Status = service.ListStatus();
            ViewBag.Supplier = service.ListSuppliers();
            ViewBag.Services = service.ListClasses();
            ViewBag.Secretary = mservice.GetUserItems();
            return PartialView(entity);
        }

        public ActionResult ViewDetail(int data_record_id)
        {
            var datacode = new CodeDTO();
            datacode.data_code_record_id = data_record_id;
            return PartialView(datacode);
        }

        public ActionResult ViewCodeServices()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(data_code_record model)
        {
            var user=mservice.GetUserbyId(model.data_member_service_id);

            model.data_member_service_name = user.FirstName + user.LastName;

            if (ModelState.IsValid)
            {
                try
                {
                    await service.CreateCodes(model.data_supplier_id.Value, model.data_service_id.Value, 1, 1, model.data_code_record_money.Value, 1, model.data_code_record_status, model.data_code_record_count.Value, model.data_member_service_id, model.data_member_service_name);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    
                }

                   
            }
            ViewBag.Status = service.ListStatus();
            ViewBag.Supplier = service.ListSuppliers();
            ViewBag.Services = service.ListClasses();
            ViewBag.Secretary = mservice.GetUserItems();
            return PartialView(model);
        }

        public ActionResult Edit(int id)
        {
            var entity = service.Get(id);
            ViewBag.Status = service.ListStatus();
            ViewBag.Supplier = service.ListSuppliers();
            ViewBag.Services = service.ListClasses();
            ViewBag.Secretary = mservice.GetUserItems();
            return PartialView("New", entity);
 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(data_code_record model)
        {
            var user = mservice.GetUserbyId(model.data_member_service_id);
            model.data_member_service_name = user.FirstName + user.LastName;
            if (ModelState.IsValid)
            {
               var res= service.UpdateCodes(model.data_code_record_id, model.data_supplier_id.Value, model.data_service_id.Value,
                   1, 1, model.data_code_record_money.Value, 
                   1, model.data_code_record_count.Value,model.data_code_record_status.Value, model.data_member_service_id, model.data_member_service_name);
                if (res.IsSuccessful)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("data_code_record_status", res.Message);
            }
            ViewBag.Secretary = mservice.GetUserItems();
            ViewBag.Status = service.ListStatus();
            ViewBag.Supplier = service.ListSuppliers();
            ViewBag.Services = service.ListClasses();
            return PartialView("New", model);
        }

        public string SaveCSV(int id)
        {

            var entity = service.ListCodedetails(id);
            var csvFileHelper = new CSVFileHelper();
            string filename = DateTime.Now.ToString("yyyy-MM-dd") + "-" + id.ToString();
            string folder = Server.MapPath("..") + "\\Uploads\\codes\\";
            var filepath = csvFileHelper.CreateFile(folder, filename, "csv");
            bool flag = csvFileHelper.SaveDataToCSVFile(entity, filepath);
            return @"/uploads/codes/"+filename+".csv";
        }
    }
}