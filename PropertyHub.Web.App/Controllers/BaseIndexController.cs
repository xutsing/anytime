﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class BaseIndexController : Controller
    {

        private readonly BaseIndexService _service;

        public BaseIndexController(BaseIndexService service)
        {
            _service = service;

        }
       [CheckPermission]
        // GET: BaseIndex
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            return PartialView();
        }

        public ActionResult New()
        {
            var model = new BaseIndexDTO();

            ViewBag.Status = _service.ListStatus();

            return PartialView(model);
        }

        public ActionResult Edit(int Id)
        {
            var model = new BaseIndexDTO();
            model.data_base_index_id = Id;
            var depart = _service.GetBaseIndex(Id);


            model.data_base_index_title = depart.data_base_index_title;
            model.data_base_index_title_en = depart.data_base_index_title_en;
            model.data_base_index_unit = depart.data_base_index_unit;
            model.data_base_key = depart.data_base_key;

            ViewBag.Status = _service.ListStatus();

            return PartialView("New", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(BaseIndexDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_base_index_title, model.data_base_index_title_en, model.data_base_index_unit, model.data_base_key);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(BaseIndexDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine = new data_base_index();
                Mapper.Map(model, machine);
                await _service.Update(machine);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

    }
}