﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PropertyHub.Services.Report;
using PropertyHub.Web.App.Models;

namespace PropertyHub.Web.App.Controllers
{
    public class ReportController : Controller
    {

        public ReportService _service;
        public ReportController(ReportService service)
        {
            _service = service;
        }


        //
        // GET: /Report/
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 按照时间段来统计每个健康秘书的会员个数
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public ActionResult ReportBySec()
        {

            var model = new ReportBySecModel();
           
            model.begin = DateTime.Now.AddMonths(-1).ToShortDateString();
            model.end = DateTime.Now.ToShortDateString();


            return PartialView(model);
        }

        [HttpPost]
        public ActionResult ReportBySec(ReportBySecModel model)
        {
            model.Results = _service.GetReportMemberBySec(DateTime.Parse(model.begin), DateTime.Parse(model.end));
            return PartialView(model);
        }



        public ActionResult ReportCodes()
        {
            var model = new ReportCodesModel();
            model.begin = DateTime.Now.AddMonths(-1).ToShortDateString();
            model.end = DateTime.Now.ToShortDateString();
            return PartialView(model);
        }

          [HttpPost]
        public ActionResult ReportCodes(ReportCodesModel model)
          {
              model.Results = _service.GetReportCodes(DateTime.Parse(model.begin), DateTime.Parse(model.end));
              return PartialView(model);
          }


          public ActionResult ReportMemberTask()
          {

              var model = new ReportMembertaskModel();

              model.begin = DateTime.Now.AddMonths(-1).ToShortDateString();
              model.end = DateTime.Now.ToShortDateString();

              return PartialView(model);
          }

          public ActionResult ReportMember()
          {
              var model = new ReportMemberModel();

              model.begin = DateTime.Now.AddMonths(-1).ToShortDateString();
              model.end = DateTime.Now.ToShortDateString();

              return PartialView(model);

          }

          [HttpPost]
          public ActionResult ReportMember(ReportMemberModel model)
          {
              model.Results = _service.GetMembers(DateTime.Parse(model.begin), DateTime.Parse(model.end));
              return PartialView(model);
          }

          [HttpPost]
          public ActionResult ReportMemberTask(ReportMembertaskModel model)
          {
              model.Results = _service.GetMemberTasks(DateTime.Parse(model.begin), DateTime.Parse(model.end));
              return PartialView(model);
          }



        /// <summary>
        /// 按照时间段来统计每个厂家，服务产品统计.
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public ActionResult ReportByService(DateTime? begin, DateTime? end)
        {
            return PartialView();
        }
    }
}