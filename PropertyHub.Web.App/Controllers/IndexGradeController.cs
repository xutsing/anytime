﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class IndexGradeController : Controller
    {
        private readonly IndexGradeService _service;

        public IndexGradeController(IndexGradeService service)
        {
            _service = service;

        }
        // GET: IndexGrade
       [CheckPermission]
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList) 
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            
            return PartialView();
        }

        public ActionResult New()
        {
            var model = new IndexGradeDTO();

            ViewBag.Status = _service.ListStatus();

            return PartialView(model);
        }


        public ActionResult Edit(int Id)
        {
            var model = new IndexGradeDTO();
            model.data_index_grade_id = Id;
            var depart = _service.GetIndexGrade(Id);


            model.data_index_grade1 = depart.data_index_grade1;
            model.data_index_grade_age_min = depart.data_index_grade_age_min;
            model.data_index_grade_age_max = depart.data_index_grade_age_max;
            model.data_index_grade_point_min = depart.data_index_grade_point_min;
            model.data_index_grade_point_max = depart.data_index_grade_point_max;
            model.data_index_grade_comments = depart.data_index_grade_comments;
        
            ViewBag.Status = _service.ListStatus();
            return PartialView("New", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(IndexGradeDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_index_grade1, model.data_index_grade_age_min, model.data_index_grade_age_max, 
                    model.data_index_grade_point_min, model.data_index_grade_point_max, model.data_index_grade_comments);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(IndexGradeDTO model)
        {
            if (ModelState.IsValid)
            {
                var grade = new data_index_grade();
                Mapper.Map(model, grade);
                await _service.Update(grade);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}