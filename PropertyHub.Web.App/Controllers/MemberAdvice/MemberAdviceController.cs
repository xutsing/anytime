﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using iTextSharp.text.pdf.qrcode;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;

namespace PropertyHub.Web.App.Controllers
{
    public class MemberAdviceController : Controller
    {

        private MemberService service;

        private CodeServices cservice;

        public MemberAdviceController(MemberService _service,CodeServices _cservice)
        {
            service = _service;
            cservice = _cservice;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        /// <summary>
        /// new 一个健身建议
        /// </summary>
        /// <param name="member_id">会员编号</param>
        /// <param name="taskId">TaskId</param>
        /// <returns></returns>
        public ActionResult New(int member_id, int taskId, int? backKind)
        {
            var model = new DataMemberAdviceDTO();
            model.data_member_id = member_id;

            ViewBag.Knowledges = cservice.ListKnowledge(0);

            model.taskId = taskId;

            //如果Taskid !=0 
            //意味着这个任务是从任务列表传进来的.
            if(model.taskId !=0)
            {
                var advice = service.GetMemberAdvicesByTaskid(model.taskId);
                if (advice.IsSuccessful)
                {
                    Mapper.Map(advice.Parameter, model);
                }

                var task = cservice.GetTaskById(taskId);
                model.IsFinished = task.data_code_services_tasks_status.Value;
                model.TaskComments = task.data_code_service_remark;

            }

            ViewBag.TaskStatus = service.ListOptions("data_code_services_tasks");
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();

            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(DataMemberAdviceDTO modelDto)
        {
            if (ModelState.IsValid)
            {
                modelDto.data_member_advice_summary = Server.HtmlDecode(modelDto.data_member_advice_summary);

              
                    var res = service.CreateMemberAdvice(modelDto.data_member_id.Value,
                        modelDto.data_member_advice_summary, modelDto.data_member_advice_isfinished,
                        modelDto.data_member_advice_id);



                if (modelDto.taskId == 0)
                {
                    //回到index

                    return PartialView("Index");
                }
                else
                {
                    //回到我的任务

                    //update the task information
                    cservice.UpdateTaskStatus(modelDto.taskId, modelDto.IsFinished, modelDto.TaskComments, res.Parameter.data_member_advice_id);

                    return RedirectToAction("TasksToday", "DataCodeServicesTasks");
                }    
                 
            }

            ViewBag.Knowledges = cservice.ListKnowledge(0);
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();
            return PartialView(modelDto);
            
        }


   
        public ActionResult Edit(int data_member_advice_id)
        {
            var res = service.GetMemberAdvice(data_member_advice_id);
            ViewBag.knowledges = cservice.ListKnowledge(0);
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();

            if (res.IsSuccessful)
            {
                var advice = res.Parameter;
                var model = new DataMemberAdviceDTO();
                Mapper.Map(advice, model);//映射过来
                return PartialView("New", model);
            }
            
            return PartialView();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DataMemberAdviceDTO model)
        {
            if (ModelState.IsValid)
            {
                model.data_member_advice_summary = Server.HtmlDecode(model.data_member_advice_summary);
                var res = service.UpdateMemberAdvice(model.data_member_advice_id, model.data_member_id.Value,
                    model.data_member_advice_summary);
            }
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();
            //直接回到index界面即可
            return PartialView("Index");
        }
    }
}