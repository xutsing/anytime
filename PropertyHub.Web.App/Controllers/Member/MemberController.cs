﻿using System.Web.Configuration;
using System.Web.UI.WebControls.WebParts;
using AutoMapper;
using iTextSharp.tool.xml.html.head;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft;
using System.Web.Security;

namespace PropertyHub.Web.App.Controllers
{
    public class MemberController : Controller
    {

        private readonly MemberService service;
        private readonly MembershipService sService;
        private readonly CodeServices cservice;

        public MemberController(MemberService _service, 
            MembershipService _sService,CodeServices _cservice)
        {
            service = _service;
            sService = _sService;
            cservice = _cservice;
        }

        /// <summary>
        /// 会员列表
        /// </summary>
        /// <returns></returns>
        [CheckPermission]
        public ActionResult Index()
        {

            var model = new MemberIndexModel();
            #region 权限模块

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            ViewBag.Only = false;//初始化查看全部
            if (actionList != null)
            {
                foreach (var action in actionList)
                {
                    if (action.PermissionName == "只看自己")
                    {
                        ViewBag.Only = true;
                        
                    }
                }
            }

            #endregion

            if ((bool) ViewBag.Only)
                model.userId = HttpContext.User.Identity.GetUserId();
            else
            {
                model.userId = string.Empty;
            }


            return PartialView(model);
        }

        /// <summary>
        /// 获得用户等级
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUserLevels()
        {
            var res = new List<string>();
            res.Add("业余");
            res.Add("专业");
            res.Add("高级");

            return Json(res, JsonRequestBehavior.AllowGet); 

        }

        /// <summary>
        /// register a user
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loginname"></param>
        /// <param name="pw"></param>
        /// <param name="mobile"></param>
        /// <param name="sex"></param>
        /// <param name="birthday"></param>
        /// <returns></returns>
        public JsonResult RegisterUser(int? member_id, string name, string loginname, string pw, string mobile, string sex, string birthday,string type,string height,string typetext)
        {

            var result = service.RegisterUser(member_id,name, loginname, pw, mobile, sex, birthday, height, typetext, type);
            return Json(result, JsonRequestBehavior.AllowGet); 
        }

        /// <summary>
        /// validate a user information.
        /// </summary>
        /// <param name="loginname"></param>
        /// <param name="pw"></param>
        /// <returns></returns>
        public JsonResult ValidateUser(string loginname, string pw)
        {
            var result = service.ValidateUser(loginname, pw);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //add by wxw
        //2015-11-04
        public JsonResult ConfirmUser(string loginname, string name, string mobile, string birthday, string height)
        {
            var result = service.ConfirmUser(loginname, name, mobile, birthday, height);
             return Json(result, JsonRequestBehavior.AllowGet);
         }
        //add by wxw
        //2015-11-04
        public JsonResult ModifyPassword(int? member_id, string password, string confirm_password)
        {
            var result = service.ModifyPassword(member_id, password, confirm_password);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UploadData(int member_id,string checkdate,string data)
        {
            var result = service.SaveCheckData(member_id, checkdate,data);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult View(int data_member_id)
        {
            var data_member = service.DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);

            var memberInfo = new MemberDTO();
            memberInfo.data_member_id = data_member_id;
            memberInfo.data_member_loginname = data_member.data_member_loginname;
            memberInfo.data_member_mobile = data_member.data_member_mobile;
            memberInfo.data_member_name = data_member.data_member_name;
            memberInfo.data_member_sex = data_member.data_sex.data_sex_title;
            memberInfo.data_member_birthday = data_member.data_member_birthday;

            return PartialView(memberInfo);
 
        }

        public ActionResult viewUploadRecords()
        {
            return PartialView();
        }


        public ActionResult ViewCheckdata(int? data_member_checkrecord_id)
        {
          //  var data_
         //   var data = service.DataContext.data_member_checkdata.Where(a => a.data_member_checkdata_id == ////data_member_checkdata_id).AsQueryable().Project().To<MemberCheckDataDTO>();

            var model = new MemberCheckDataDTO();
            model.data_member_checkrecord_id = data_member_checkrecord_id;
            return PartialView(model);
        }

        public ActionResult ViewCheckDataReport(int? data_member_checkrecord_id)
        {

            var model = new MemberCheckDataReport();

            var checkrecord = cservice.GetCheckrecord(data_member_checkrecord_id.Value);
            model.data_member_id = checkrecord.data_member.data_member_id;
            model.data_member_name = checkrecord.data_member.data_member_name;
            model.data_sex_name = checkrecord.data_member.data_sex.data_sex_title;
            model.age = (DateTime.Now.Year - (checkrecord.data_member.data_member_birthday.HasValue
                ? checkrecord.data_member.data_member_birthday.Value.Year
                : 1980)).ToString();                           
          
            var res = cservice.GetReport(data_member_checkrecord_id.Value);
            //检测结果
            model.Results = res.Parameter;

            model.Data = new Dictionary<string, string>();

            foreach (var item in model.Results)
            {
                model.Data.Add(item.data_base_index_key, item.data_member_value);
            }

            model.sum = res.Parameter.Sum(a => int.Parse(a.data_body_label_text));

            return PartialView(model);
        }

        /// <summary>
        /// 获得一个会员的所有的上传记录
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public string GetCheckDataRecords(int data_member_id)
        {
            var res = service.ListMemberRecords(data_member_id);

            var items = from item in res
                select new
                {
                    Id = item.data_member_checkrecord_id,
                    uploaddate = item.datecreated
                };

            return JsonConvert.SerializeObject(items.ToList());

        }


        public string GetCheckDataDetails(int data_member_id, int?toprows=20)
        {



            var res = cservice.ListCheckData(data_member_id,toprows.HasValue?toprows.Value:20);

            IsoDateTimeConverter timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

            return JsonConvert.SerializeObject(res, Newtonsoft.Json.Formatting.Indented, timeFormat);

            //if (res.IsSuccessful)
            //    return JsonConvert.SerializeObject(res);
            //else
            //{
            //    return string.Empty 
            //}
        }

        public string GetCheckReportById(int? data_member_checkrecord_id)
        {
            var model = new MemberCheckDataReport();

            var checkrecord = cservice.GetCheckrecord(data_member_checkrecord_id.Value);
            model.data_member_id = checkrecord.data_member.data_member_id;
            model.data_member_name = checkrecord.data_member.data_member_name;
            model.data_sex_name = checkrecord.data_member.data_sex.data_sex_title;
            model.age = (DateTime.Now.Year - (checkrecord.data_member.data_member_birthday.HasValue
                ? checkrecord.data_member.data_member_birthday.Value.Year
                : 1980)).ToString();

            var res = cservice.GetReport(data_member_checkrecord_id.Value);
            //检测结果
            model.Results = res.Parameter;
            model.Data = new Dictionary<string, string>();

            foreach (var item in model.Results)
            {
                model.Data.Add(item.data_base_index_key, item.data_member_value);
            }

            model.sum = res.Parameter.Sum(a => int.Parse(a.data_body_label_text));

            return JsonConvert.SerializeObject(model);
        }




        public ActionResult ViewBodyReport(int data_member_id)
        {
            var model = new MemberBodyReport();

            var member = service.Get(data_member_id);
            model.data_member_name = member.data_member_name;
            model.data_sex_name = member.data_sex.data_sex_title;
            model.age = (DateTime.Now.Year - (member.data_member_birthday.HasValue
                ? member.data_member_birthday.Value.Year
                : 1980)).ToString();

           var res = cservice.GetBodyReport(data_member_id);

           model.Results = res.Parameter;
           model.sum = 0;
           //model.sum = res.Parameter.Sum(a => int.Parse(a.data_body_label_text));

           return PartialView(model);

        }




        //added by adam 2015-08-06, for Assign Secratry
        public ActionResult ViewAssignSecratry(string IDs)
        {
            var model = new MemberDTO();
            ViewBag.Secratries = sService.ListSecratries();
            ViewBag.SelectedMembers = service.List().Where(m => IDs.Contains("," + m.data_member_id.ToString() + ",")).ToList<data_member>();
            ViewBag.SelectedMembersID = IDs;

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult AssignSecratry(string selected_secratry_name, string selected_secratry, string selected_members)
        {
            try
            { 
                var model = new MemberDTO();

                //updated by tsing

                if (string.IsNullOrEmpty(selected_secratry_name))
                {
                    var user = sService.GetUserbyId(selected_secratry);

                    selected_secratry_name = user.FirstName + user.LastName;
                }


                var updateList = service.List().Where(u => selected_members.Contains(","+u.data_member_id + ",")).ToList<data_member>();
            
                foreach (var u in updateList)
                {
                    u.data_member_service_id = selected_secratry;
                    u.data_member_service_name = selected_secratry_name;
                    u.data_member_has_service = true;
                    u.data_member_secretary.Add(new data_member_secretary { 
                        data_member_id = u.data_member_id,
                        data_member_service_id = selected_secratry,
                        data_member_service_name = selected_secratry_name,
                        datecreated = DateTime.Now
                    });
                    service.Update(u);
                }
            }
            catch
            {
                return new JsonResult() { Data = new { op = false } };
            }


            return new JsonResult() { Data = new { op = true } };
        }


        public ActionResult SelectMember(int linktype,int? taskId)
        {
            var model = new SelectMemberModel();
            model.TaskId = taskId.HasValue ? taskId.Value : 0;
            model.linktype = linktype;

            ViewBag.Members = service.ListMembers();

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SelectMember(SelectMemberModel model)
        {
              if (ModelState.IsValid)
              {
                  //跳转到其他界面
                  if (model.linktype == 0)
                  {
                      //调差问卷
                      return RedirectToAction("Testing", "MemberTesting", new { memberId = model.data_member_id,testingId=1, taskId = model.TaskId }); 
                  }
                  else if (model.linktype == 1)
                  {
                      //个性化方案 
                      return RedirectToAction("New", "Prescription", new { member_id = model.data_member_id, task_id = model.TaskId });

                  }
                  else if (model.linktype == 2)
                  {
                      //家庭健康档案
                      return RedirectToAction("new", "memberfile", new { member_id = model.data_member_id, taskId = model.TaskId }); 

                  }
                  else if (model.linktype == 3)
                  {
                      //健身建议
                      return RedirectToAction("new", "MemberAdvice", new { member_id = model.data_member_id, taskId = model.TaskId }); 
                  }
                  //added by adam 2015-10-05 体质检测报告
                  else if (model.linktype == 4)
                  {
                      return RedirectToAction("new", "CheckReport", new { member_id = model.data_member_id, taskId = model.TaskId });
                  }
              }

              ViewBag.Members = service.ListMembers();
              return PartialView(model);

        }


   


        [HttpPost]
        public JsonResult UnAssignSecratry(string selected_members)
        {
            try
            {
                var model = new MemberDTO();

                var updateList = service.List().Where(u => selected_members.Contains("," + u.data_member_id + ",")).ToList<data_member>();

                foreach (var u in updateList)
                {
                    u.data_member_service_id = "";
                    u.data_member_service_name = "";
                    u.data_member_has_service = false;
                    service.Update(u);
                }
            }
            catch
            {
                return new JsonResult() { Data = new { op = false } };
            }

            return new JsonResult() { Data = new { op = true } };
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult EditCheckReport(int check_Id)
        {
            var checkrecord = cservice.GetCheckrecord(check_Id);

            var model = new MemberCheckMVCModel();

            model.check_id = check_Id;
            model.IsFinished = checkrecord.data_Isfinished.HasValue && checkrecord.data_Isfinished.Value;
            model.CheckReport = checkrecord.data_checkreport;

            //应该来说只列出来健身建议的知识库.
            ViewBag.Knowledges = cservice.ListKnowledge(0);
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditCheckReport(MemberCheckMVCModel model)
        {

            model.CheckReport = Server.HtmlDecode(model.CheckReport);
            var createres=cservice.CreateCheckReport(model.check_id, model.IsFinished, model.CheckReport);

            if (createres.IsSuccessful)
            {
                return JavaScript("datamemberrecords.closereport(); datamemberrecords.init(0);");
            }

            return PartialView(model);
        }


        /// <summary>
        /// 根据id 获得Comments.
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public string GetKnowledgeComments(string Ids)
        {
            var res = cservice.GetKnowledgeComments(Ids);
            if (res.IsSuccessful)
                return res.Parameter;
            else
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// 根据会员id, 获得一个会员所拥有的service内容，有效期.
        /// </summary>
        /// <returns></returns>
        public string GetMemberServices(int data_member_id)
        {
            var res = cservice.GetMemberServices(data_member_id);

          //  return JsonConvert.SerializeObject(res);
            if (res.IsSuccessful)
                return res.Parameter;
            else
            {
                return string.Empty;
            }

        }
        /// <summary>
        /// 
        /// 获得一个会员的健身建议，一个
        /// </summary>
        /// <param name="date_member_id"></param>
        /// <returns></returns>
        public string GetMemberAdvice(int data_member_id,int? data_member_checkrecord_id)
        {
            var res = cservice.GetMemberAdvice(data_member_id, data_member_checkrecord_id);
            if (res.IsSuccessful)
                return res.Parameter;
            else
            {
                return string.Empty;
            }
        }


        public string GetMemberAdvices(int data_member_id)
        {
            var res = cservice.GetMemberAdvices(data_member_id);
            if (res.IsSuccessful)
                return res.Parameter;
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 获得一个会员的运动健身处方
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public string GetMemberPrescription(int data_member_id,int? toprows=20)
        {
            var res = cservice.GetMemberPrescriptions(data_member_id, toprows.HasValue?toprows.Value:20);
            if (res.IsSuccessful)
                return res.Parameter;
            else
            {
                return string.Empty;
            }
        }


        public string GetMemberInformationById(int data_member_id)
        {

            var member = service.Get(data_member_id);
            var model = new MemberDTO();
            Mapper.Map(member, model);


            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.Indented, timeFormat);

           // return JsonConvert.SerializeObject(model);



        }


    }
}