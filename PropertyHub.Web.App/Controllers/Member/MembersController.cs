﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using System.Web.Http;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class MembersController: ODataControllerBase
    {
        private readonly MemberService _service;
        public MembersController(MemberService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MemberDTO> Get()
        {
            return _service.List().Project().To<MemberDTO>();
        }

        [EnableQuery]
        public IQueryable<MemberCheckDTO> GetMemberCheckDTO([FromODataUri] int key)
        {
            return _service.ListMemberRecords(key).Project().To<MemberCheckDTO>();
        } 
    }
}