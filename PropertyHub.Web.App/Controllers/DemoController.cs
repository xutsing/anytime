﻿using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{
    public class DemoController : ControllerBase
    {
        private readonly SubscriptionService _service;
       // private readonly BusinessService _bservice;

        public DemoController(SubscriptionService service)
        {
            _service = service;
         //   _bservice = bservice;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult New()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<ActionResult> New(SubscriptionTest model)
        {
            if (ModelState.IsValid)
            {
                await _service.AddTest(model);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteTest(id);
            return RedirectToAction("Index");
        }
    }
}