﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using System.Web.Http;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class CodeDTOController : ODataControllerBase
    {

        private readonly CodeServices _services;
        public CodeDTOController(CodeServices service)
        {
            _services = service;
        }

        [EnableQuery]
        public IQueryable<CodeDTO> Get()
        {
            return _services.ListRecordCodes().Project().To<CodeDTO>();
        }



	}
}