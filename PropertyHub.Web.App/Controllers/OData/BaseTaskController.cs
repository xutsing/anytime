﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;


namespace PropertyHub.Web.App.Controllers.OData
{
    public class BaseTaskController : ODataControllerBase
    {
        private readonly BaseTaskService _service;

        public BaseTaskController(BaseTaskService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<BaseTaskDTO> Get()
        {
            return _service.ListBaseTask().Project().To<BaseTaskDTO>();
        }
    }
}