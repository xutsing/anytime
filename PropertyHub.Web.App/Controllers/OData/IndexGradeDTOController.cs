﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class IndexGradeDTOController : ODataControllerBase
    {
        private readonly IndexGradeService _service;
        public IndexGradeDTOController(IndexGradeService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<IndexGradeDTO> Get()
        {
            return _service.ListIndexGrade().Project().To<IndexGradeDTO>();
        }
    }
}