﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;


namespace PropertyHub.Web.App.Controllers.OData
{
    public class CodeServiceDTOController : ODataController
    {

        private readonly CodeServices service;
        public CodeServiceDTOController(CodeServices _service)
        {
            service = _service;
        }


        [EnableQuery]
        public IQueryable<CodeServiceDTO> Get()
        {
            return service.ListCodeServices().Project().To<CodeServiceDTO>();
        }
	}
}