﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class UserDTOController : ODataController
    {
        private readonly SubscriptionService _service;

        public UserDTOController(SubscriptionService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<UserDTO> Get()
        {
            return _service.GetUsers().Project().To<UserDTO>();
        }


	}
}