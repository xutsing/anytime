﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using System.Web.Http;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class DataCoachDTOController : ODataControllerBase
    {
        // GET: DataCoachDTO
       private DataCoachService _service;

       public DataCoachDTOController(DataCoachService service)
        {
            _service = service;
        }

       [EnableQuery]
       public IQueryable<DataCoachDTO> Get()
        {
            return _service.ListDataCoach().Project().To<DataCoachDTO>();
        }
    }
}