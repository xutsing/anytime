﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Controllers.OData
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using PropertyHub.Data;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<data_member>("ODataMember");
    builder.EntitySet<data_code>("data_code"); 
    builder.EntitySet<data_code_services>("data_code_services"); 
    builder.EntitySet<data_comments>("data_comments"); 
    builder.EntitySet<data_member_device>("data_member_device"); 
    builder.EntitySet<data_member_type>("data_member_type"); 
    builder.EntitySet<data_member_secretary>("data_member_secretary"); 
    builder.EntitySet<data_member_prescription>("data_member_prescription"); 
    builder.EntitySet<data_member_checkrecord>("data_member_checkrecord"); 
    builder.EntitySet<data_member_testing>("data_member_testing"); 
    builder.EntitySet<data_sex>("data_sex"); 
    builder.EntitySet<data_service_comments>("data_service_comments"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ODataMemberController : ODataController
    {
        private AnyTimeEntities db = new AnyTimeEntities();

        // GET odata/ODataMember
        [Queryable]
        public IQueryable<data_member> GetODataMember()
        {
            return db.data_member;
        }

        // GET odata/ODataMember(5)
        [Queryable]
        public SingleResult<data_member> Getdata_member([FromODataUri] int key)
        {
            return SingleResult.Create(db.data_member.Where(data_member => data_member.data_member_id == key));
        }

        // PUT odata/ODataMember(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, data_member data_member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != data_member.data_member_id)
            {
                return BadRequest();
            }

            db.Entry(data_member).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!data_memberExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(data_member);
        }

        // POST odata/ODataMember
        public async Task<IHttpActionResult> Post(data_member data_member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.data_member.Add(data_member);
            await db.SaveChangesAsync();

            return Created(data_member);
        }

        // PATCH odata/ODataMember(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<data_member> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            data_member data_member = await db.data_member.FindAsync(key);
            if (data_member == null)
            {
                return NotFound();
            }

            patch.Patch(data_member);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!data_memberExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(data_member);
        }

        // DELETE odata/ODataMember(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            data_member data_member = await db.data_member.FindAsync(key);
            if (data_member == null)
            {
                return NotFound();
            }

            db.data_member.Remove(data_member);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/ODataMember(5)/data_code
        [Queryable]
        public IQueryable<data_code> Getdata_code([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_code);
        }

        // GET odata/ODataMember(5)/data_code_services
        [Queryable]
        public IQueryable<data_code_services> Getdata_code_services([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_code_services);
        }

        // GET odata/ODataMember(5)/data_comments
        [Queryable]
        public IQueryable<data_comments> Getdata_comments([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_comments);
        }

        // GET odata/ODataMember(5)/data_member_device
        [Queryable]
        public IQueryable<data_member_device> Getdata_member_device([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_member_device);
        }

        // GET odata/ODataMember(5)/data_member_type
        [Queryable]
        public SingleResult<data_member_type> Getdata_member_type([FromODataUri] int key)
        {
            return SingleResult.Create(db.data_member.Where(m => m.data_member_id == key).Select(m => m.data_member_type));
        }

        // GET odata/ODataMember(5)/data_member_secretary
        [Queryable]
        public IQueryable<data_member_secretary> Getdata_member_secretary([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_member_secretary);
        }

        // GET odata/ODataMember(5)/data_member_prescription
        [Queryable]
        public IQueryable<data_member_prescription> Getdata_member_prescription([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_member_prescription);
        }

        // GET odata/ODataMember(5)/data_member_checkrecord
        [Queryable]
        public IQueryable<data_member_checkrecord> Getdata_member_checkrecord([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_member_checkrecord);
        }

        // GET odata/ODataMember(5)/data_member_testing
        [Queryable]
        public IQueryable<data_member_testing> Getdata_member_testing([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_member_testing);
        }

        // GET odata/ODataMember(5)/data_sex
        [Queryable]
        public SingleResult<data_sex> Getdata_sex([FromODataUri] int key)
        {
            return SingleResult.Create(db.data_member.Where(m => m.data_member_id == key).Select(m => m.data_sex));
        }

        // GET odata/ODataMember(5)/data_service_comments
        [Queryable]
        public IQueryable<data_service_comments> Getdata_service_comments([FromODataUri] int key)
        {
            return db.data_member.Where(m => m.data_member_id == key).SelectMany(m => m.data_service_comments);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool data_memberExists(int key)
        {
            return db.data_member.Count(e => e.data_member_id == key) > 0;
        }
    }
}
