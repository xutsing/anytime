﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using System.Web.Http;

namespace PropertyHub.Web.App.Controllers.OData
{
    public class CodeRecordDTOController : ODataControllerBase
    {


        private readonly CodeServices _service;
        public CodeRecordDTOController(CodeServices service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<CodeRecordDTO> Get()
        {
            return _service.ListRecords().OrderBy(a=>a.datecreated).Project().To<CodeRecordDTO>();
        }

	}
}