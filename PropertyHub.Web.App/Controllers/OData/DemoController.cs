﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;

namespace PropertyHub.Web.App.Controllers.OData
{
    //[ODataRoutePrefix("SubscriptionTests")]
    public class SubscriptionTestsController : ODataControllerBase
    {
        private readonly SubscriptionService _service;

        public SubscriptionTestsController(SubscriptionService service)
        {
            _service = service;
        }

        //[ODataRoute]
        [EnableQuery]
        public IQueryable<SubscriptionTestDto> Get()
        {

            

            return _service.ListTest().Project().To<SubscriptionTestDto>();
        }


    }
}
