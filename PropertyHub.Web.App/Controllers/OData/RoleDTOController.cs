﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace PropertyHub.Web.App.Controllers.OData
{
    public class RoleDTOController : ODataController
    {
        private readonly SubscriptionService _service;

        public RoleDTOController(SubscriptionService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<RoleDTO> Get()
        {
            return _service.GetRoles().Project().To<RoleDTO>();

        }


	}
}