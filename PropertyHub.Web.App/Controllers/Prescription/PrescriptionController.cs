﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System.Web.OData;
using AutoMapper.QueryableExtensions;
using Mapper = AutoMapper.Mapper;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace PropertyHub.Web.App.Controllers
{
    /// <summary>
    /// added by adam 2015-08-01
    /// </summary>
    public class PrescriptionController : Controller
    {
        private readonly PrescriptionService _service;
        private readonly CodeServices _bservice;
        private readonly MemberService _mservice;
        
        public PrescriptionController(PrescriptionService service, CodeServices bservice, MemberService mservice)
        {
            _service = service;
            _bservice = bservice;
            _mservice = mservice;
        }

        public ActionResult Index()
        {
            #region button access control
            ViewBag.canAdd = true;
            ViewBag.canEdit = true;
            ViewBag.canView = true;
            ViewBag.canDelete = true;

            //IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            //if (actionList == null)
            //    return PartialView();

            //foreach (var action in actionList)
            //{
            //    if (action.PermissionName == "新增")
            //    {
            //        ViewBag.canAdd = true;
            //    }

            //    if (action.PermissionName == "编辑")
            //    {
            //        ViewBag.canEdit = true;
            //    }

            //    if (action.PermissionName == "查看")
            //    {
            //        ViewBag.canView = true;
            //    }

            //    if (action.PermissionName == "删除")
            //    {
            //        ViewBag.canDelete = true;
            //    }
            //}
            #endregion


            return PartialView();
        }

        public ActionResult New(int? taskId, int? member_id, int? backKind)
        {
            var entity = new PrescriptionDTO();

            var model = new data_member_prescription();

            if (member_id != null) 
            {
                model.data_member_id = member_id.Value;
                model.data_member = _mservice.Get(member_id.Value);
            }            

            Mapper.Map(model, entity);

            ViewBag.Status = _service.ListStatus();
            //ViewBag.Members = _service.ListMembers();
            ViewBag.MachineActions = _service.ListMachineActions();
            ViewBag.DetailList = "[]";

            if (taskId != null)
            {
                ViewBag.taskid = taskId.Value;
                
            }
            ViewBag.backKind = backKind;

            return PartialView(entity);
        }

        public ActionResult Edit(int id, int? taskId)
        {
            var modal = _service.Get(id);

            var entity = new PrescriptionDTO();
            Mapper.Map(modal, entity);

            var detailList = modal.data_member_prescription_detail.AsQueryable().Project().To<PrescriptionDetailDTO>()
                .Select((m, index) => new {
                    detail_id = index + 1,
                    data_machine_action_id = m.data_machine_action_id,
                    machine_action_text = m.machine_action_text,
                    data_machine_action_times = m.data_machine_action_times,
                    data_machine_action_number = m.data_machine_action_number,
                    data_machine_action_remark = m.data_machine_action_remark,
                });

            ViewBag.Status = _service.ListStatus();
            //ViewBag.Members = _service.ListMembers();
            ViewBag.MachineActions = _service.ListMachineActions();
            ViewBag.DetailList = JsonConvert.SerializeObject(detailList);

            if (taskId != null)
            {
                ViewBag.taskid = taskId.Value;
            }

            return PartialView("New", entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(PrescriptionDTO model, string machine_action_detail_list, int? backKind)
        {           
            if (ModelState.IsValid)
            {
                var entity = new data_member_prescription();
                Mapper.Map(model, entity);

                IList<PrescriptionDetailDTO> list = JsonConvert.DeserializeObject<IList<PrescriptionDetailDTO>>(machine_action_detail_list);
                entity.data_member_prescription_detail = list.AsQueryable().Project().To<data_member_prescription_detail>().ToList();
                string fileName = "prescription_" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".pdf";
                entity.data_member_prescription_path = @"/uploads/pdfs/" + fileName;

                entity.datecreated = DateTime.Now;

                var result = await _service.Add(entity);
                //create pdf file
                CreatePDFFile(model, list, fileName);

                //complete task
                if (Request.Form["task_id"] != null)
                {
                    var task_id = int.Parse(Request.Form["task_id"]);
                    var complete = Request.Form.GetValues("task_complete")[0] == "true";

                    if (complete) 
                    {
                        _bservice.CompleteServiceTasks(task_id, result.data_member_prescription_id, 30);
                    }
                    else 
                    {
                        _bservice.CompleteServiceTasks(task_id, result.data_member_prescription_id, 29);
                    }

                    //说明当前这个是从我的任务过来的，这时候回到我的任务即可

                    if (backKind == 1)
                    {
                        return RedirectToAction("Index", "DataCodeServicesTasks");
                    }
                    else if (backKind == 2)
                    {
                        return RedirectToAction("TasksToday", "DataCodeServicesTasks");
                    }
                }

                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(PrescriptionDTO model, string machine_action_detail_list)
        {
            if (ModelState.IsValid)
            {
                var entity = new data_member_prescription();
                Mapper.Map(model, entity);
                entity.dateupdated = DateTime.Now;

                IList<PrescriptionDetailDTO> list = JsonConvert.DeserializeObject<IList<PrescriptionDetailDTO>>(machine_action_detail_list);
                entity.data_member_prescription_detail = list.AsQueryable().Project().To<data_member_prescription_detail>().ToList();

                CreatePDFFile(model, list, entity.data_member_prescription_path.Split(new [] {"/"}, StringSplitOptions.RemoveEmptyEntries)[2]);
                await _service.Update(entity);

                //complete task
                if (Request.Form["task_id"] != null)
                {
                    var task_id = int.Parse(Request.Form["task_id"]);
                    var complete = Request.Form.GetValues("task_complete")[0] == "true";

                    if (complete)
                    {
                        _bservice.CompleteServiceTasks(task_id, entity.data_member_prescription_id, 30);
                    }
                    else
                    {
                        _bservice.CompleteServiceTasks(task_id, entity.data_member_prescription_id, 29);
                    }
                }

                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            var modal = _service.Get(id);

            var filePath = Server.MapPath("~" + modal.data_member_prescription_path);
            FileInfo fi = new FileInfo(filePath);
            if (fi.Exists) {
                fi.Delete();
            }

            await _service.Delete(id);
            return RedirectToAction("Index");
        }

        [EnableQuery]
        public ActionResult Detail(int id)
        {
            var modal = _service.Get(id);

            var entity = new PrescriptionDTO();
            Mapper.Map(modal, entity);

            var detailList = modal.data_member_prescription_detail.AsQueryable().Project().To<PrescriptionDetailDTO>()
                .Select((m, index) => new
                {
                    detail_id = index + 1,
                    data_machine_action_id = m.data_machine_action_id,
                    machine_action_text = m.machine_action_text,
                    data_machine_action_times = m.data_machine_action_times,
                    data_machine_action_number = m.data_machine_action_number,
                    data_machine_action_remark = m.data_machine_action_remark,
                });

            ViewBag.DetailList = JsonConvert.SerializeObject(detailList);

            return PartialView(entity);
        }

        private string CreatePDFContentText(PrescriptionDTO model, IList<PrescriptionDetailDTO> list)
        {
            string template1 = @"<table><tr><td></td><td align='center'><h2>{0}</h2></td><td></td></tr><tr><td>&nbsp;</td><td></td><td></td></tr><tr><td>&nbsp;</td><td></td><td></td></tr><tr><td>会员姓名: {1}</td><td></td><td></td></tr><tr><td>&nbsp;</td><td></td><td></td></tr><tr><td>&nbsp;</td><td></td><td></td></tr><tr><td>处方明细</td><td></td><td></td></tr><tr><td colspan='3'><table border='1' cellpadding='20' cellspacing='0' ><tr><td align='center'>序号</td><td align='center'>器械动作</td><td align='center'>运动时间</td><td align='center'>运动次数</td><td width='28%' align='center'>备注</td></tr>{2}</table></td></tr></table>";

            string template2 = @"<tr><td align='center'>{0}</td><td align='center'>{1}</td><td align='center'>{2}</td><td align='center'>{3}</td><td align='center'>{4}</td></tr>";
            var s2 = "";
            var i = 1;
            foreach (var detail in list) {
                s2 += String.Format(template2, new object[5] {
                            i,
                            detail.machine_action_text,
                            detail.data_machine_action_times,
                            detail.data_machine_action_number,
                            detail.data_machine_action_remark
                        });

                i++;
            }
            var s1 = String.Format(template1, new object[3] {
                model.data_member_prescription_title,
                model.data_member_name,
                s2
            });

            return s1;
        }

        //generate pdf from html code
        //refernce to http://www.cnblogs.com/lonelyxmas/p/3619072.html?utm_source=tuicool
        //modified by adam 2015-08-12
        private byte[] ConvertHtmlTextToPDF(string htmlText)
        {
            if (string.IsNullOrEmpty(htmlText))
            {
                return null;
            }

            //避免当htmlText无任何html tag标签的纯文字时，转PDF时会挂掉，所以一律加上<p>标签
            htmlText = "<p>" + htmlText + "</p>";

            MemoryStream outputStream = new MemoryStream();//要把PDF写到哪个串流
            byte[] data = Encoding.UTF8.GetBytes(htmlText);//字串转成byte[]
            MemoryStream msInput = new MemoryStream(data);
            Document doc = new Document();//要写PDF的文件，建构子没填的话预设直式A4
            PdfWriter writer = PdfWriter.GetInstance(doc, outputStream);
            //指定文件预设开档时的缩放为100%
            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
            //开启Document文件 
            doc.Open();
            //使用XMLWorkerHelper把Html parse到PDF档里
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8, new PropertyHub.Web.App.App_Start.UnicodeFontFactory());
            //将pdfDest设定的资料写到PDF档
            PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);
            writer.SetOpenAction(action);
            doc.Close();
            msInput.Close();
            outputStream.Close();
            //回传PDF档案 
            return outputStream.ToArray();
        }

        private string CreatePDFFile(PrescriptionDTO model, IList<PrescriptionDetailDTO> list, string fileName)
        {
            string filePath = Server.MapPath(@"~/uploads/pdfs/") + fileName;
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                byte[] content = ConvertHtmlTextToPDF(CreatePDFContentText(model, list));

                fs.Write(content, 0, content.Length);
                fs.Flush();

                fs.Close();
            }
            return filePath;
        }
    }
}