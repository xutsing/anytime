﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;


namespace PropertyHub.Web.App.Controllers.OData
{
    /// <summary>
    /// added by adam, dto controller for member prescription 2015-08-10
    /// </summary>
    public class PrescriptionDTOController : ODataControllerBase
    {
         private readonly PrescriptionService _service;

         public PrescriptionDTOController(PrescriptionService service)
        {
            _service = service;
        }

        [EnableQuery]
         public IQueryable<PrescriptionDTO> Get()
        {
            var res= _service.ListPrescriptions().Project().To<PrescriptionDTO>();
            return res;
        }
    }
}