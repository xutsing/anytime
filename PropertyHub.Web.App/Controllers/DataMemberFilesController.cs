﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System.Web.OData;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class DataMemberFilesController : Controller
    {
        // GET: DataMemberFiles
        private readonly MemberFilesService service;

        public DataMemberFilesController(MemberFilesService _service)
        {
            service = _service;
        }
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult New()
        {
            ViewBag.Members = service.ListMembers();
            var model = new DataMemberFilesDTO();

            return PartialView(model);
        }


        public ActionResult Edit(int Id)
        {
            var model = new DataMemberFilesDTO();
            model.dat_member_file_id = Id;
            var depart = service.GetDataMemberFiles(Id);


            model.data_member_id = depart.data_member_id;
            model.data_member_file_path = depart.data_member_file_path;
            model.data_member_id_text = service.GetMemberName(Id); ;

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(DataMemberFilesDTO model)
        {
            if (ModelState.IsValid)
            {
                await service.Add(model.data_member_id, model.data_member_file_path);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DataMemberFilesDTO model)
        {

            if (ModelState.IsValid)
            {
                var memberfiles = new data_member_file();
                Mapper.Map(model, memberfiles);
                await service.Update(memberfiles);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}