﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System.Web.Script.Serialization;

namespace PropertyHub.Web.App.Controllers
{
    public class BaseTaskController : Controller
    {
        private readonly BaseTaskService _service;

        public BaseTaskController(BaseTaskService service)
        {
            _service = service;
        }
        [CheckPermission]
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion
            return PartialView();
        }

        public ActionResult New()
        {
            var entity = new data_basetask();

            ViewBag.Types = _service.ListBaseTaskType();

            return PartialView(entity);
        }

        public ActionResult Edit(int id)
        {
            var entity = _service.Get(id);

            ViewBag.Types = _service.ListBaseTaskType();

            return PartialView("New", entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(data_basetask model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(data_basetask model)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(model);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

    }
}