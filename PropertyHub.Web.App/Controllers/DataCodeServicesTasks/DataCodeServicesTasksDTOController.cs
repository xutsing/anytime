﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PropertyHub.Data;
using Microsoft.Data.OData;
using System.Web.OData.Routing;
using PropertyHub.Web.App.Models;
using PropertyHub.Services;
using AutoMapper.QueryableExtensions;
using System.Web.OData;
using System.Web.OData.Query;
using Microsoft.AspNet.Identity;


namespace PropertyHub.Web.App.Controllers.OData
{
    public class DataCodeServicesTasksDTOController : ODataControllerBase
    {
        private readonly CodeServices _service;

        public DataCodeServicesTasksDTOController(CodeServices service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<DataCodeServicesTasksDTO> Get()
        {
            //string userId = System.Web.HttpContext.Current.User.Identity.ToString();
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var res = _service.GetMyTasks(userId).Project().To<DataCodeServicesTasksDTO>();
            return res;
            }
	}
}