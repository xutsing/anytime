﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class DataCodeServicesTasksController : Controller
    {
       private readonly CodeServices _service;

       public DataCodeServicesTasksController(CodeServices service)
        {
            _service = service;

        }
        //
        // GET: /DataCodeServicesTasks/
        public ActionResult Index()
        {
            return PartialView();
        }

        //added by adam 2015-09-09 today's task
        public ActionResult TasksToday()
        {
            return PartialView("TasksToday");
        }

        public async Task<ActionResult> Execute(int id)
        {
            await _service.execute(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// reply a task.对于一个没有链接，仅仅通过回复来执行的
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Reply(int id)
        {
            //
            var task = _service.GetTaskById(id);
            var model=new TaskDTO();
            //Status
            ViewBag.Status = _service.ListTaskStatus();
            Mapper.Map(task, model);
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JavaScriptResult Reply(TaskDTO model)
        {

            model.data_code_service_remark = Server.HtmlDecode(model.data_code_service_remark);

           var res= _service.UpdateTaskStatus(model.data_code_services_tasks_id, model.data_code_services_tasks_status, model.data_code_service_remark);

           var result = model.data_code_services_tasks_status == 28 ?
               "未执行" : (model.data_code_services_tasks_status == 29 ? "执行中" : "完成");

           var js = string.Format("close(\"{0}\");", result);
           return JavaScript(js);
 
        }
	}
}