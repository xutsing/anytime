﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Presentation;
using iTextSharp.text.pdf.qrcode;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using PropertyHub.Data;
using System.Threading.Tasks;

namespace PropertyHub.Web.App.Controllers
{
    public class MemberMessageController : Controller
    {
        private MemberService mservice;
        private MemberMessageService service;
        private CodeServices cservice;

        public MemberMessageController(MemberMessageService _service, MemberService _mservice, CodeServices _cservice)
        {
            service = _service;
            mservice = _mservice;
            cservice = _cservice;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult New(int? taskId, int? member_id, int? backKind)
        {
            var model = new MemberMessageDTO();

            if (member_id != null)
            {
                model.data_member_ids = member_id.Value.ToString();
            }

            if (taskId != null)
            {
                ViewBag.TaskId = taskId.Value;
            }

            if (backKind != null)
            {
                ViewBag.BackKind = backKind.Value;
            }

            ViewBag.Knowledges = service.ListKnowledge(0);
            ViewBag.MemberList = mservice.ListMembers();
            ViewBag.MessageTypeListype = mservice.ListOptions("data_member_message_type");
            ViewBag.Status = service.ListStatus();
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();
            ViewBag.TaskStatus = service.ListOptions("data_code_services_tasks");

            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(MemberMessageDTO model, int? taskId, int? backKind, string IsFinished, string TaskComments)
        {
            if (ModelState.IsValid)
            {
                var message = new data_member_message();
                Mapper.Map(model, message);
                message.data_member_message_text = Server.HtmlDecode(message.data_member_message_text);
                message.datecreated = DateTime.Now;
                message.data_member_names = string.IsNullOrEmpty(message.data_member_names) ? "全体会员" : message.data_member_names;
                if (message.data_member_names != "全体会员")
                {
                    message.data_member_names = message.data_member_names.Remove(message.data_member_names.Length - 1);
                }

                if (message.data_member_ids != null)
                {
                    message.data_member_ids = "," + message.data_member_ids + ",";
                }
                message.data_member_message_type = model.data_member_message_type;

                var result = await service.Add(message);

                if (result != null && taskId != null)
                {
                    cservice.UpdateTaskStatus(taskId.Value, int.Parse(IsFinished), TaskComments, result.data_member_message_id);
                    if (backKind == 1)
                    {
                        return RedirectToAction("Index", "DataCodeServicesTasks");
                    }
                    else if (backKind == 2)
                    {
                        return RedirectToAction("TasksToday", "DataCodeServicesTasks");
                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.Knowledges = service.ListKnowledge(0);
            ViewBag.MemberList = mservice.ListMembers();
            ViewBag.MessageTypeListype = mservice.ListOptions("data_member_message_type");
            ViewBag.Status = service.ListStatus();
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();
            if (taskId != null)
            {
                ViewBag.TaskId = taskId.Value;
            }

            if (backKind != null)
            {
                ViewBag.BackKind = backKind.Value;
            }
            ViewBag.TaskStatus = service.ListOptions("data_code_services_tasks");

            return PartialView(model);

        }

        public ActionResult Edit(int id)
        {
            var message = service.Get(id);
            ViewBag.Knowledges = service.ListKnowledge(0);
            ViewBag.MemberList = mservice.ListMembers();
            ViewBag.MessageTypeListype = mservice.ListOptions("data_member_message_type");
            ViewBag.Status = service.ListStatus();
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();

            var model = new MemberMessageDTO();
            Mapper.Map(message, model);

            if (model.data_member_ids != null)
                model.data_member_ids = model.data_member_ids.Remove(model.data_member_ids.Length - 1).Remove(0, 1);

            return PartialView("New", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(MemberMessageDTO model)
        {
            if (ModelState.IsValid)
            {
                var message = new data_member_message();
                Mapper.Map(model, message);
                message.data_member_message_text = Server.HtmlDecode(message.data_member_message_text);
                message.data_member_names = message.data_member_names == null ? "全体会员" : message.data_member_names;

                if (message.data_member_names != "全体会员" && message.data_member_names[message.data_member_names.Length - 1] == ',')
                {
                    message.data_member_names = message.data_member_names.Remove(message.data_member_names.Length - 1);
                }

                if (message.data_member_ids != null)
                {
                    message.data_member_ids = "," + message.data_member_ids + ",";
                }

                message.dateupdated = DateTime.Now;

                await service.Update(message);
                return RedirectToAction("Index");
            }

            ViewBag.Knowledges = service.ListKnowledge(0);
            ViewBag.MemberList = mservice.ListMembers();
            ViewBag.MessageTypeListype = mservice.ListOptions("data_member_message_type");
            ViewBag.Status = service.ListStatus();
            ViewBag.KnowledgeTypes = service.ListKnowledgeType();

            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            await service.Delete(id);
            return RedirectToAction("Index");
        }

        #region service api

        /// <summary>
        /// 根据data_member_id获得会员自己的推送消息列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPersonalMemberMessageList(int data_member_id)
        {
            var items = service.GetPersonalMemberMessageList(data_member_id);
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 根据data_member_message_id获得推送的消息明细
        /// </summary>
        /// <returns></returns>
        public string GetMemberMessageById(int data_member_message_id)
        {
            var items = service.Get(data_member_message_id);
            var res =
                new
                {
                    id = items.data_member_message_id,
                    title = items.data_member_message_title,
                    content = items.data_member_message_text,
                    datecreated = items.datecreated
                };

            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(res, Newtonsoft.Json.Formatting.Indented, timeFormat);
        }

        #endregion



        #region Api Tsing 2015/10/16
        /// <summary>
        /// 获得某个会员的运动健身建议
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public string GetMemberSportAdvices(int data_member_id, int pagesize, int pagenumber)
        {
            var items = service.GetMemberAdvices(data_member_id, null, pagesize, pagenumber);

            items.pagenumber = pagenumber;
            items.pagesize = pagesize;


            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(items, Newtonsoft.Json.Formatting.Indented, timeFormat);
        }




        public string GetSportAdvice(int data_member_id, int id, int type)
        {
            var res = service.GetAdviceDetails(data_member_id, id, type);
            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(res, Newtonsoft.Json.Formatting.Indented, timeFormat);

        }



        /// <summary>
        /// 获得某个会员的全时消息
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public string GetMemberMessages(int data_member_id, int pagesize,int pagenumber)
        {
            var items = service.GetMessages(data_member_id, null,pagesize,pagenumber);

            items.pagenumber = pagenumber;
            items.pagesize = pagesize;

            var timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(items, Newtonsoft.Json.Formatting.Indented, timeFormat);
        }



        /// <summary>
        /// 获得某个会员未读的全时消息
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public string getunreadmessages(int data_member_id)
        {

            var result = new Result<long>();
            try
            {
                result.IsSuccessful = true;
                result.Parameter = service.GetUnReads(data_member_id).Parameter;


            }
            catch (Exception exception)
            {

            }

            return JsonConvert.SerializeObject(result);
        }


        public string getunreadadvices(int data_member_id)
        {
            var result = new Result<long>();
            try
            {
                result.IsSuccessful = true;
                result.Parameter = service.GetUnReadsAdvices(data_member_id).Parameter;

            }
            catch (Exception exception)
            {

                result.Message = exception.Message;
                result.IsSuccessful = false;
                result.Parameter = 0;
                throw;
            }

            return JsonConvert.SerializeObject(result);

        }




        #endregion



    }
}