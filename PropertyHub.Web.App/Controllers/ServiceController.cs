﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;

namespace PropertyHub.Web.App.Controllers
{
    public class ServiceController : Controller
    {
        private readonly Service _service;

        public ServiceController(Service service)
        {
            _service = service;
        }

        [CheckPermission]
        public ActionResult Index()
        {
            #region button access control
            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canView = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            if (actionList == null)
                return PartialView();

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "查看")
                {
                    ViewBag.canView = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion

            return PartialView();
        }

        public ActionResult New()
        {
            var entity = new data_service();

            ViewBag.Status = _service.ListStatus();

            return PartialView(entity);
        }

        public ActionResult Edit(int id)
        {
            var entity = _service.Get(id);

            ViewBag.Status = _service.ListStatus();

            return PartialView("New", entity);
        }

        public ActionResult EditTask(int id)
        {

            ViewBag.Tasks = _service.ListBaseTasks();

            var entity = _service.GetServiceTask(id);

            return PartialView(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(data_service model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        public ActionResult NewTask(int data_service_id)
        {

            var model = new data_service_tasks();
            model.data_service_id = data_service_id;

            ViewBag.Tasks = _service.ListBaseTasks();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NewTask(data_service_tasks model)
        {
            if (ModelState.IsValid)
            {
                await _service.AddTask(model);

                return RedirectToAction("Detail", new { id = model.data_service_id, tab = 1 });
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(data_service model)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(model);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EditTask(data_service_tasks model)
        //{


        //    if (ModelState.IsValid)
        //    {
        //        await _service.UpdateTask(model);

        //        return RedirectToAction("Detail", new { id = model.data_service_id, tab = 1 });
        //    }
        //    return PartialView(model);
        //}


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JavaScriptResult EditTask(data_service_tasks model)
        {
            if (model.Iscycle != null && model.Iscycle.Value)
            {
                if (model.cycleDays == null || model.cyclemax == null)
                {
                    return JavaScript("alert('必须输入循环间隔和最大次数');");
                }
            }

            var updateresult = _service.UpdateTask(model);

            var result = updateresult ? 1 : 0;

            var js = string.Format("close({0});", result);

            return JavaScript(js);

        }

 
        public JsonResult QueryTask(int id)
        {

            var entity = _service.GetServiceTask(id);
            var basetask = _service.GetServiceTaskBaseIndexName(id);   
         
            JsonResult json = new JsonResult
            {
                Data = new
                {
                    basetask = basetask,
                    data_nexttask_interval = entity.data_nexttask_interval,
                    Iscycle = entity.Iscycle,
                    cycleDays = entity.cycleDays,
                    cyclemax = entity.cyclemax
                }
            };
            return Json(json,JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

        public ActionResult Detail(int id, int tab = 0)
        {
            var entity = _service.Get(id);

            ViewBag.Tasks = _service.ListBaseTasks();

            ViewBag.CurrentTab = tab;

            return PartialView(entity);
        }

        public async Task<ActionResult> DeleteTask(int id)
        {
            var serviceId = _service.GetServiceTask(id).data_service_id;

            await _service.DeleteTask(id);
            return RedirectToAction("Detail", new { id = serviceId, tab = 1 });
        }
        public async Task<ActionResult> TaskOrder(int id, int direction)
        {
            var serviceId = _service.GetServiceTask(id).data_service_id;

            await _service.ChangeOrder(id, direction);

            return RedirectToAction("Detail", new { id = serviceId, tab = 1 });
        }


      


    }
}