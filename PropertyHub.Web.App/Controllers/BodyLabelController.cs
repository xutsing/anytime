﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;

namespace PropertyHub.Web.App.Controllers
{
    public class BodyLabelController : Controller
    {
        private readonly BodyLabelService _service;

        public BodyLabelController(BodyLabelService service)
        {
            _service = service;

        }

        // GET: BodyLabel
        public ActionResult Index()
        {
            return PartialView(); 
        }

        public ActionResult New()
        {
            var model = new BodyLabelDTO();

            ViewBag.Status = _service.ListStatus();

            return PartialView(model);
        }

        public ActionResult Edit(int Id)
        {
            var model = new BodyLabelDTO();
            model.data_body_label_id = Id;
            var depart = _service.GetBaseIndex(Id);

            model.data_body_label_text = depart.data_body_label_text;

            ViewBag.Status = _service.ListStatus();

            return PartialView("New", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        //some properties missed
        //zhege 
        public async Task<ActionResult> New(BodyLabelDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_body_label_text);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(BodyLabelDTO model)
        {
            if (ModelState.IsValid)
            {
                var machine = new data_body_label();
                Mapper.Map(model, machine);
                await _service.Update(machine);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}