﻿using Microsoft.AspNet.Identity.EntityFramework;
using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Controllers
{

    [Authorize]
    //modified by adam 2015-08-15 add role filter for modules
    public class ModuleController:ControllerBase
    {
        private readonly SubscriptionService _service;
        private readonly MembershipService _bservice;

        public ModuleController(SubscriptionService service, MembershipService bservice)
        {
            _service = service;
            _bservice = bservice;
         
        }

        //modified by adam 2015-08-15 add permission filter
        public async Task<ActionResult> Index()
        {
            var cUser = _bservice.GetCurrentLogonUser();

            var roleIDs = "";

            foreach (IdentityUserRole role in cUser.Roles) 
            {
                roleIDs += "," + role.RoleId + ",";
            }

            var modules = _service.GetMenuModulesByRole(roleIDs);

            //modules = _service.GetAllModules();

            return PartialView(modules);
        }
           
    }
}