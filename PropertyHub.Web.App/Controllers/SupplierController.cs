﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Mapper = AutoMapper.Mapper;


namespace PropertyHub.Web.App.Controllers
{
    public class SupplierController : Controller
    {
        private readonly SupplierService _service;

        public SupplierController(SupplierService service)
        {
            _service = service;

        }
        // GET: Supplier
       [CheckPermission]
        public ActionResult Index()
        {
            #region button access control

            ViewBag.canAdd = false;
            ViewBag.canEdit = false;
            ViewBag.canDelete = false;

            IList<MembershipService.PermissonInfo> actionList = (IList<MembershipService.PermissonInfo>)TempData["actionList"];

            foreach (var action in actionList)
            {
                if (action.PermissionName == "新增")
                {
                    ViewBag.canAdd = true;
                }

                if (action.PermissionName == "编辑")
                {
                    ViewBag.canEdit = true;
                }

                if (action.PermissionName == "删除")
                {
                    ViewBag.canDelete = true;
                }
            }
            #endregion

            return PartialView();
        }

        public ActionResult New()
        {
            var model = new SupplierDTO();

            ViewBag.Status = _service.ListStatus();

            return PartialView(model);
        }

        public ActionResult Edit(int Id)
        {
            var model = new SupplierDTO();
            model.data_supplier_id = Id;
            var depart = _service.GetSupplier(Id);


            model.data_supplier_name = depart.data_supplier_name;
            model.data_supplier_address = depart.data_supplier_address;
            model.data_supplier_phone = depart.data_supplier_phone;
            model.data_suplier_contracts = depart.data_suplier_contracts;

            ViewBag.Status = _service.ListStatus();

            return PartialView("New", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> New(SupplierDTO model)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_supplier_name, model.data_supplier_address, model.data_supplier_phone, model.data_suplier_contracts);
                return RedirectToAction("Index");
            }
            return PartialView(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(SupplierDTO model)
        {
            if (ModelState.IsValid)
            {
                var supplier = new data_supplier();
                Mapper.Map(model, supplier);
                await _service.Update(supplier);
                return RedirectToAction("index");
            }
            return PartialView(model);
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}