﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.App.Models;
using Newtonsoft.Json;

namespace PropertyHub.Web.App.Controllers
{
    public class MemberTestingController : Controller
    {
        private readonly MemberTestingService _service;
        private readonly MemberService _memberService;
        private readonly CodeServices _codeServices;

        public MemberTestingController(MemberTestingService service,MemberService memberService,CodeServices codeService)
        {
            _service = service;
            _memberService = memberService;
            _codeServices = codeService;
        }

        public ActionResult Index()
        {
            return PartialView();
        }

        public async Task<ActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var memberTesting = _service.Get(id);

            var testingModel = new TestingModel();

            testingModel.data_member_testing_id = memberTesting.data_member_testing_id;
            testingModel.data_member_id = memberTesting.data_member_id;
            testingModel.data_member_name = memberTesting.data_member.data_member_name;
            testingModel.data_questions_testing_id = memberTesting.data_questions_testing_id;
            testingModel.data_questions_testing_title = memberTesting.data_questions_testing.data_questions_testing_title;
            testingModel.data_questions_testing_remark = memberTesting.data_questions_testing_remark;
            testingModel.Questions = JsonConvert.DeserializeObject<IList<TestingItemModel>>(memberTesting.data_questions_testing_result_json);

            return PartialView("Testing", testingModel);
        }

        /// <summary>
        /// 获得调查问卷，
        /// </summary>
        /// <returns></returns>
        public string GetTesting(int member_id)
        {
            int testingId = 1;
            var findtest = _service.GetTesting(member_id, testingId);

            IList<TestingItemModel> questions = new List<TestingItemModel>();
            if (findtest == null)
            {
                //这是一个新的调查问卷
                var testing = _service.GetTesting(testingId);

                foreach (var item in testing.data_questions_testing_detail)
                {
                    questions.Add(new TestingItemModel()
                    {
                        data_questions_id = item.data_questions.data_questions_id,
                        data_questions_text = item.data_questions.data_questions_text
                    });
                }

            }
            else
            {
                //将保存的数据反序列化，传给客户端
                questions = JsonConvert.DeserializeObject<IList<TestingItemModel>>(findtest.data_questions_testing_result_json);
            }

            //return Json(questions, JsonRequestBehavior.AllowGet);

            return JsonConvert.SerializeObject(questions).ToLower();

        }

        public JsonResult UpdateTesting(int member_id, string result)
        {
            var res = new Result<bool>();
            res.IsSuccessful = false;
            res.Message = string.Empty;

            if (string.IsNullOrEmpty(result))
            {
                res.Message = "调查结果为空";
                return Json(res, JsonRequestBehavior.AllowGet);
            }

            var _data = new Dictionary<int, bool?>();

            //_data.Add(1, true);
            //_data.Add(2, false);

            //var ttt = JsonConvert.SerializeObject(_data);

            try
            {
                _data = JsonConvert.DeserializeObject<Dictionary<int, bool?>>(result);
            }
            catch (Exception exception)
            {
                res.Message = "数据结果解析错误.";

                return Json(res, JsonRequestBehavior.AllowGet);
            }

            IList<TestingItemModel> questions = new List<TestingItemModel>();
            var testing = _service.GetTesting(1);
            foreach (var item in testing.data_questions_testing_detail)
            {



                questions.Add(new TestingItemModel()
                {
                    data_questions_id = item.data_questions.data_questions_id,
                    data_questions_text = item.data_questions.data_questions_text,
                    Answer = _data.ContainsKey(item.data_questions.data_questions_id)? _data[item.data_questions.data_questions_id].ToString():"False",
                    Comments =string.Empty
                    
                });
            }
            //将结果存入
            var findres = _service.GetTesting(member_id, 1);
            if (findres == null)
            {
                //这是一个新的调查问卷
                var newtesting = new data_member_testing();
                newtesting.data_member_id = member_id;
                newtesting.data_questions_testing_id = 1;
                newtesting.data_questions_testing_result_json = JsonConvert.SerializeObject(questions);
                _service.Add(newtesting);

            }
            else
            {
                //更新一个调查问卷
                findres.data_questions_testing_result_json = JsonConvert.SerializeObject(questions);
                _service.Update(findres);
            }

            res.IsSuccessful = true;
            res.Parameter = true;
            return Json(res, JsonRequestBehavior.AllowGet);

        }




        public ActionResult Testing(int member_id, int taskId, int? backKind)
        {

            int testingId = 1; //默认就是第一份试卷

            if (member_id == 0 || testingId == 0)
            {
                throw new Exception("error");
            }

            var member = _memberService.Get(member_id);
            var testing = _service.GetTesting(testingId);

            if (member==null || testing==null)
            {
                throw new Exception("error");
            }

            var testingModel = new TestingModel();

            testingModel.data_member_id = member_id;
            testingModel.data_member_name = member.data_member_name;
            testingModel.data_questions_testing_id = testingId;
            testingModel.data_questions_testing_title = testing.data_questions_testing_title;

            var task = _codeServices.GetTaskById(taskId);
            //Tsing
            //Added 
            //在调查问卷里面添加任务的相关信息.
            testingModel.TaskId = taskId;
            testingModel.TaskComments = task.data_code_service_remark;
            testingModel.IsFinished = task.data_code_services_tasks_status.HasValue?task.data_code_services_tasks_status.Value:28; //28 的意思就是未执行.

            var testId = task.data_record_Id.HasValue ? task.data_record_Id.Value : 0;


            if (testId == 0)
            {
                //如果testID，意思就是这份试卷没有存档，重新开始。重新load试题.
                testingModel.Questions = new List<TestingItemModel>();
                foreach (var item in testing.data_questions_testing_detail)
                {
                    testingModel.Questions.Add(new TestingItemModel()
                    {
                        data_questions_id = item.data_questions.data_questions_id,
                        data_questions_text = item.data_questions.data_questions_text
                    });
                }
            }
            else
            {
                //根据ID从数据库里面load以前保存的数据.
                var memberTesting = _service.Get(testId);
                //load remark
                testingModel.data_questions_testing_remark = memberTesting.data_questions_testing_remark;
                //load 以前的保存结果
                testingModel.Questions = JsonConvert.DeserializeObject<IList<TestingItemModel>>(memberTesting.data_questions_testing_result_json);
                testingModel.data_member_testing_id = testId;
            }


            //任务状态
            ViewBag.TaskStatus = _service.ListOptions("data_code_services_tasks");
            ViewBag.backKind = backKind;
           
            return PartialView(testingModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Testing(TestingModel model, int? backKind)
        {
            foreach (var item in model.Questions)
            {
                if (string.IsNullOrEmpty(item.Answer))
                {
                    ModelState.AddModelError("", "请完成所有的问题");
                    break;
                }
            }

            if (ModelState.IsValid)
            {
                var entity = new data_member_testing();
                entity.data_member_testing_id = model.data_member_testing_id;
                entity.data_member_id = model.data_member_id;
                entity.data_questions_testing_id = model.data_questions_testing_id;
                entity.data_questions_testing_remark = model.data_questions_testing_remark;

                entity.data_questions_testing_result_json = JsonConvert.SerializeObject(model.Questions);

                if (entity.data_member_testing_id==0)
                {
                    entity=await _service.Add(entity);
                }
                else
                {
                   entity= await _service.Update(entity);
                }
                

                //Tsing这里添加对当前任务的一些更新.
                if (model.TaskId != 0)
                {
                    //tsing updated on the 8/25/2015
                    //
                    _codeServices.UpdateTaskStatus(model.TaskId, model.IsFinished, model.TaskComments, entity.data_member_testing_id);
                    //这个调差问卷时从我的任务那里条转过来的，因此当数据保存了以后，应该回到我的任务
                    if (backKind == 1)
                    {
                        return RedirectToAction("Index", "DataCodeServicesTasks");
                    }
                    else if (backKind == 2)
                    {
                        return RedirectToAction("TasksToday", "DataCodeServicesTasks");
                    }
                }

                else
                {
                    //对于直接编辑的调查问卷，直接返回到index 即可。
                    return RedirectToAction("Index");
                }


              
            }

            return PartialView(model);
        }
    }
}