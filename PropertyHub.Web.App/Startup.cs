﻿using Microsoft.Owin;
using Owin;
using PropertyHub.Web.App.App_Start;

[assembly: OwinStartupAttribute(typeof(PropertyHub.Web.App.Startup))]
namespace PropertyHub.Web.App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
