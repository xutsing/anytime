﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.IO;

namespace PropertyHub.Web.App.App_Start
{
    //added by adam 2015-08-11
    //for pdf unicode character rendering
    //refernce to http://www.cnblogs.com/lonelyxmas/p/3619072.html?utm_source=tuicool
    public class UnicodeFontFactory: FontFactoryImp
    {

        private static readonly string arialFontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
            "arialuni.ttf");//arial unicode, MS unicode character。
        private static readonly string simsunPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
          "simsun.ttc,1");//simsun font

        public override Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color,
            bool cached)
        {
            //select a font to use
            BaseFont baseFont = BaseFont.CreateFont(simsunPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            return new Font(baseFont, size, style, color);
        }
    }
}