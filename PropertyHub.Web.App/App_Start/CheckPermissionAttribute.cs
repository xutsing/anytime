﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using PropertyHub.Services;
using Microsoft.Practices.Unity;

namespace PropertyHub.Web.App
{
    //added by adam 2015-08-15 for permission checking
    class CheckPermissionAttribute : ActionFilterAttribute
    {
        [Dependency]
        public MembershipService MService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var actionName = filterContext.ActionDescriptor.ActionName;

            int i = 0;
            foreach (var parameter in filterContext.ActionParameters)
            {
                if (i == 0)
                {
                    actionName = actionName +"?"+ parameter.Key + "=" + parameter.Value;
                }
                else
                {
                    actionName = actionName + "&" + parameter.Key + "=" + parameter.Value;
                }

            }

            var query = MService.GetPermissionsByControllerAndAction(controllerName, actionName);
            if (query != null)
            {
                var list = MService.GetPermissionsByControllerAndAction(controllerName, actionName).ToList();
                filterContext.Controller.TempData["actionList"] = list;
            }   

 	        base.OnActionExecuting(filterContext);
        }
    }
}
