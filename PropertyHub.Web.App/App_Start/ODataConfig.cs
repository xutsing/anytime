﻿using PropertyHub.Web.App.Models;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using PropertyHub.Data;

namespace PropertyHub.Web.App.App_Start
{
    public static class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            
            ODataModelBuilder builder = new ODataConventionModelBuilder();

            builder.EntitySet<SubscriptionTestDto>("SubscriptionTests");
            builder.EntitySet<MemberDTO>("Members");
            builder.EntitySet<MemberCheckDTO>("MemberCheckDTO");
            builder.EntitySet<MemberCheckDataDTO>("MemberCheckDataDTO");
            builder.EntitySet<ServiceDTO>("Service");
            builder.EntitySet<CodeRecordDTO>("CodeRecordDTO");
            builder.EntitySet<CodeDTO>("CodeDTO");
            builder.EntitySet<CodeServiceDTO>("CodeServiceDTO");
            builder.EntitySet<UserDTO>("UserDTO");
            builder.EntitySet<BaseTaskDTO>("BaseTask");
            builder.EntitySet<ServiceTaskDTO>("ServiceTask");
            builder.EntitySet<MemberTestingDTO>("MemberTesting");
            //

            builder.EntitySet<RoleDTO>("RoleDTO");
            builder.EntitySet<BaseIndexDTO>("BaseIndexDTO");


            builder.EntitySet<DepartDTO>("ODepartDTO");
            builder.EntitySet<PermissionNode>("ORolePermission");

            //added by adam, for machine use 2015-08-01
            builder.EntitySet<MachineDTO>("MachineDTO");
            //added by adam, for machine type use 2015-08-04
            builder.EntitySet<MachineTypeDTO>("MachineTypeDTO");
            //added by adam, for machine action use 2015-08-04
            builder.EntitySet<MachineActionDTO>("MachineActionDTO");
            //added by adam, for member prescription use 2015-08-10
            builder.EntitySet<PrescriptionDTO>("PrescriptionDTO");

            //added by wxw, for age use 2015-08-03
            builder.EntitySet<AgeDTO>("AgeDTO");

            //added by wxw, for body label use 2015-08-04
            builder.EntitySet<BodyLabelDTO>("BodyLabelDTO");

            //added by wxw, for index data use 2015-08-05
            builder.EntitySet<IndexDataDTO>("IndexDataDTO");


            //added by tsing for message use 2015-08-05
            builder.EntitySet<MessageDTO>("MessageDTO");

            //added by wxw for supplier use 2015-08-05
            builder.EntitySet<SupplierDTO>("SupplierDTO");

            //added by wxw for data knowledge use 2015-08-06
            builder.EntitySet<DataKnowledgeDTO>("DataKnowledgeDTO");

            //added by tsing
            builder.EntitySet<DataCodeServicesTasksDTO>("DataCodeServicesTasksDTO");

            //add by wxw
            //modified by adam 2015-09-09
            builder.EntitySet<DataCodeServicesTasksDTO>("DataCodeServicesTasksTodayDTO");
            //add by wxw
            builder.EntitySet<IndexGradeDTO>("IndexGradeDTO");
            //added by Tsing
            builder.EntitySet<DataMemberFileDTO>("DataMemberFileDTO");
            //add by wxw
            builder.EntitySet<DataDictionaryDTO>("DataDictionaryDTO");
            //add by wxw
            builder.EntitySet<DataCoachDTO>("DataCoachDTO");
            //added by tsing
            builder.EntitySet<DataMemberAdviceDTO>("DataMemberAdviceDTO");

            //added by adam, for member message use 2015-10-13
            builder.EntitySet<MemberMessageDTO>("MemberMessageDTO");

            //added by wxw
            builder.EntitySet<DataMemberFilesDTO>("DataMemberFilesDTO");


            //var model = builder.GetEdmModel();
            config.EnableCaseInsensitive(caseInsensitive: true);
            config.MapODataServiceRoute("ODataRoute", "odata", builder.GetEdmModel());
            // config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
        
        }

    }
}