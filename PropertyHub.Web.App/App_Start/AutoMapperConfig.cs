using AutoMapper;
using PropertyHub.Data;
using PropertyHub.Web.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PropertyHub.Web.App.App_Start
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper()
        {
            #region config
            Mapper.CreateMap<SubscriptionTest, SubscriptionTestDto>()
                .ForMember(d => d.CompanyName, o => o.MapFrom(s => s.Subscription.CompanyName));
            Mapper.CreateMap<data_member, MemberDTO>()
                .ForMember(d => d.data_member_sex, o => o.MapFrom(s => s.data_sex.data_sex_title));
            Mapper.CreateMap<data_member_checkrecord, MemberCheckDTO>()
                .ForMember(d => d.data_member_name, o => o.MapFrom(s => s.data_member.data_member_name
                ));
            Mapper.CreateMap<data_member_checkdata, MemberCheckDataDTO>().ForMember(d => d.data_base_index_id, o => o.MapFrom(s => s.data_base_index.data_base_index_title));
            Mapper.CreateMap<data_service, ServiceDTO>().ForMember(d => d.data_service_status, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text));
            Mapper.CreateMap<data_basetask, BaseTaskDTO>().ForMember(d => d.data_basetask_type, o => o.MapFrom(s => s.data_basetask_type.data_basetask_type_title));
            Mapper.CreateMap<data_service_tasks, ServiceTaskDTO>().ForMember(d => d.data_basetask_name, o => o.MapFrom(s => s.data_basetask.data_basetask_name));
            Mapper.CreateMap<data_member_testing, MemberTestingDTO>().ForMember(d => d.data_member_name, o => o.MapFrom(s => s.data_member.data_member_name)).ForMember(d => d.data_questions_testing_title, o => o.MapFrom(s => s.data_questions_testing.data_questions_testing_title));

            //激活码生成记录
            Mapper.CreateMap<data_code_record, CodeRecordDTO>().ForMember(d => d.data_service_id, o => o.MapFrom(s => s.data_service.data_service_name));
            Mapper.CreateMap<data_code_record, CodeRecordDTO>().ForMember(d => d.data_supplier_id, o => o.MapFrom(s => s.data_supplier.data_supplier_name));
            Mapper.CreateMap<data_code_record, CodeRecordDTO>().ForMember(d => d.data_code_record_status, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text));


            //CodeServiceDTO
            Mapper.CreateMap<data_code_services, CodeServiceDTO>().ForMember(d => d.data_member_id, o => o.MapFrom(s => s.data_member.data_member_name)); //激活会员
            Mapper.CreateMap<data_code_services, CodeServiceDTO>().ForMember(d => d.data_service_id, o => o.MapFrom(s => s.data_service.data_service_name));//所激活的课程
            Mapper.CreateMap<data_code_services, CodeServiceDTO>().ForMember(d => d.data_code_id, o => o.MapFrom(s => s.data_code.data_code_text));//所激活的激活码


            Mapper.CreateMap<data_code, CodeDTO>();
      

            //User
            Mapper.CreateMap<ApplicationUser, UserDTO>();

            //Role
            Mapper.CreateMap<IdentityRole, RoleDTO>();


            Mapper.CreateMap<Department, DepartDTO>();

            //added by adam, for machine use 2015-08-01
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_machine, MachineDTO>()
                .ForMember(d => d.data_machine_status_text, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text))
                .ForMember(d => d.data_machine_type_title, o => o.MapFrom(s => s.data_machine_type.data_machine_type_title));
            //bidirection mapping
            Mapper.CreateMap<MachineDTO, data_machine>()
                .ForMember(d => d.data_machine_action, o => o.Ignore())
                .ForMember(d => d.data_machine_type, o => o.Ignore())
                .ForMember(d => d.data_dictionary, o => o.Ignore());
            //------------------------------------------------------------------    
            //added by adam, for machine type use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_machine_type, MachineTypeDTO>();
            //bidirection mapping
            Mapper.CreateMap<MachineTypeDTO, data_machine_type>()
                .ForMember(d => d.data_machine, o => o.Ignore());
            //------------------------------------------------------------------ 
            //added by adam, for machine action use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_machine_action, MachineActionDTO>()
                .ForMember(d => d.data_machine_action_status_text, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text))
                .ForMember(d => d.data_machine_name, o => o.MapFrom(s => s.data_machine.data_machine_name));
            //bidirection mapping
            Mapper.CreateMap<MachineActionDTO, data_machine_action>()
                .ForMember(d => d.data_machine, o => o.Ignore())
                .ForMember(d => d.data_dictionary, o => o.Ignore())
                .ForMember(d => d.data_member_prescription_detail, o => o.Ignore());
            //------------------------------------------------------------------ 
            //added by adam, for member prescription use 2015-08-10
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_member_prescription, PrescriptionDTO>()
                .ForMember(d => d.data_member_prescription_status_text, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text))
                .ForMember(d => d.data_member_name, o => o.MapFrom(s => s.data_member.data_member_name));
            //bidirection mapping
            Mapper.CreateMap<PrescriptionDTO, data_member_prescription>()
                .ForMember(d => d.data_dictionary, o => o.Ignore())
                .ForMember(d => d.data_member, o => o.Ignore())
                .ForMember(d => d.data_member_prescription_detail, o => o.Ignore());
            //detail 
            Mapper.CreateMap<PrescriptionDetailDTO, data_member_prescription_detail>();
            Mapper.CreateMap<data_member_prescription_detail, PrescriptionDetailDTO>()
                .ForMember(d => d.machine_action_text, o => o.MapFrom(s => s.data_machine_action.data_machine_action_title));
            //------------------------------------------------------------------ 



            //------------------------------------------------------------------ 
            //added by wxw, for age use 2015-08-04
            //------------------------------------------------------------------ y
            Mapper.CreateMap<data_age_range, AgeDTO>();
            Mapper.CreateMap<AgeDTO, data_age_range>().ForMember(d => d.data_index_data, o => o.Ignore());

            //------------------------------------------------------------------ 
            //added by wxw, for base index use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_base_index, BaseIndexDTO>();
            Mapper.CreateMap<BaseIndexDTO, data_base_index>().ForMember(d => d.data_index_data, o => o.Ignore())
                .ForMember(d => d.data_member_checkdata, o => o.Ignore());
            //------------------------------------------------------------------ 
            //added by wxw, for base label use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_body_label, BodyLabelDTO>();
            Mapper.CreateMap<BodyLabelDTO, data_body_label>().ForMember(d => d.data_index_data, o => o.Ignore());
            //------------------------------------------------------------------ 
            //added by wxw, for index data use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_index_data, IndexDataDTO>().ForMember(d => d.data_base_index_title, o => o.MapFrom(s => s.data_base_index.data_base_index_title));
            Mapper.CreateMap<data_index_data, IndexDataDTO>().ForMember(d => d.data_age_range_title, o => o.MapFrom(s => s.data_age_range.data_age_title));
            Mapper.CreateMap<data_index_data, IndexDataDTO>().ForMember(d => d.data_body_label_title, o => o.MapFrom(s => s.data_body_label.data_body_label_text));
            Mapper.CreateMap<data_index_data, IndexDataDTO>().ForMember(d => d.data_sex_title, o => o.MapFrom(s => s.data_sex.data_sex_title));
            
            Mapper.CreateMap<IndexDataDTO, data_index_data>()
               // .ForMember(d => d.data_base_index_id, o => o.Ignore())
                .ForMember(d => d.data_base_index, o => o.Ignore())
                .ForMember(d => d.data_body_label, o => o.Ignore())
                .ForMember(d => d.data_sex, o => o.Ignore())
                .ForMember(d=>d.data_age_range,o=>o.Ignore());
             

            //added by tsing for message map added by tsing
            Mapper.CreateMap<data_message, MessageDTO>()
                .ForMember(d=>d.message_member_name,o=>o.MapFrom(s=>s.data_member.data_member_name))
                .ForMember(d=>d.message_type_title,o=>o.MapFrom(s=>s.data_dictionary.data_dictionary_text));

            Mapper.CreateMap<MessageDTO, data_message>()
                .ForMember(d=>d.data_member,o=>o.Ignore())
                .ForMember(d=>d.data_dictionary,o=>o.Ignore());


            //added by wood

            Mapper.CreateMap<data_supplier, SupplierDTO>();
            Mapper.CreateMap<SupplierDTO, data_supplier>()
                .ForMember(d => d.data_code_record, o => o.Ignore())
                .ForMember(d => d.data_supplier_device,o=>o.Ignore());

            //------------------------------------------------------------------ 
            //added by wxw, for data knowledge use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_knowledge, DataKnowledgeDTO>().ForMember(d => d.data_knowledge_type_title, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text));
            Mapper.CreateMap<DataKnowledgeDTO, data_knowledge>().ForMember(d => d.data_comments, o => o.Ignore());
            Mapper.CreateMap<DataKnowledgeDTO, data_knowledge>().ForMember(d => d.data_physical_files, o => o.Ignore());
            Mapper.CreateMap<DataKnowledgeDTO, data_knowledge>().ForMember(d => d.data_dictionary, o => o.Ignore());

            //------------------------------------------------------------------ 
            //added by wxw, for data knowledge use 2015-08-04
            //------------------------------------------------------------------ 
            Mapper.CreateMap<sp_getmytasks_Result, DataCodeServicesTasksDTO>();
            //--------------
            //------------------------------------------------------------------ 
            //added by wxw, for data knowledge use 2015-08-12
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_index_grade, IndexGradeDTO>().ForMember(d => d.data_index_grade1_title, o => o.MapFrom(s => s.data_dictionary.data_dictionary_text));
            Mapper.CreateMap<IndexGradeDTO, data_index_grade>().ForMember(d => d.data_dictionary, o => o.Ignore());

            
            //Added by tsing. for data_member_information
            Mapper.CreateMap<data_member_information, MemberFileDTO>();

            Mapper.CreateMap<MemberFileDTO, data_member_information>() 
                .ForMember(d=>d.data_member,o=>o.Ignore())
                .ForMember(d=>d.data_dictionary,o=>o.Ignore())
                 .ForMember(d => d.data_dictionary1, o => o.Ignore())
                  .ForMember(d => d.data_dictionary2, o => o.Ignore())            ;

            Mapper.CreateMap<data_code_services_tasks, TaskDTO>();


            //added by tsing
            Mapper.CreateMap<data_member_information, DataMemberFileDTO>()
                .ForMember(d=>d.data_member_name,o=>o.MapFrom(s=>s.data_member.data_member_name))
                ;

            //added by adam
            Mapper.CreateMap<ServiceTaskDTO, data_service_tasks>().ForMember(d => d.data_service, o => o.Ignore())
                .ForMember(d => d.data_basetask, o => o.Ignore());

            //------------------------------------------------------------------ 
            //added by wxw, for data dictionary use 2015-09-12
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_dictionary, DataDictionaryDTO>();
            Mapper.CreateMap<DataDictionaryDTO, data_dictionary>()
                .ForMember(d => d.data_code_record, o => o.Ignore())
         .ForMember(d => d.data_knowledge, o => o.Ignore())
         .ForMember(d => d.data_machine_action, o => o.Ignore())
         .ForMember(d => d.data_machine, o => o.Ignore())
         .ForMember(d => d.data_member_prescription, o => o.Ignore())
         .ForMember(d => d.data_message, o => o.Ignore())
         .ForMember(d => d.data_service, o => o.Ignore())
         .ForMember(d => d.data_index_grade, o => o.Ignore())
         .ForMember(d => d.data_member_information, o => o.Ignore())
         .ForMember(d => d.data_member_information1, o => o.Ignore())
         .ForMember(d => d.data_member_information2, o => o.Ignore())
         .ForMember(d=>d.data_member_message,o=>o.Ignore())
          .ForMember(d => d.data_code, o => o.Ignore());

            //add by wxw
            Mapper.CreateMap<View_Coach, DataCoachDTO>();
            Mapper.CreateMap<DataCoachDTO, View_Coach>();


            //Add by Tsing
            //on 2015/9/145
            Mapper.CreateMap<data_member_advice, DataMemberAdviceDTO>()
                .ForMember(a=>a.data_member_name,o=>o.MapFrom(s=>s.data_member.data_member_name))
                .ForMember(a=>a.taskId,o=>o.Ignore())
                .ForMember(a=>a.IsFinished,o=>o.Ignore())
                .ForMember(a=>a.TaskComments,o=>o.Ignore());
                
            Mapper.CreateMap<DataMemberAdviceDTO, data_member_advice>()
                .ForMember(d => d.data_member, o => o.Ignore())
                ;

            //add by wxw
            Mapper.CreateMap<sp_getcodestatus_Result, CodeStatusDTO>().ForMember(d => d.data_member_actived_text, o => o.Ignore()); 
            Mapper.CreateMap<CodeStatusDTO, sp_getcodestatus_Result>();

            //------------------------------------------------------------------    
            //added by adam, for member message use 2015-10-13
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_member_message, MemberMessageDTO>();
            //bidirection mapping
            Mapper.CreateMap<MemberMessageDTO, data_member_message>()
                .ForMember(d=>d.data_dictionary,o=>o.Ignore());

            //------------------------------------------------------------------    
            //added by wxw, for data member file use 2015-10-26
            //------------------------------------------------------------------ 
            Mapper.CreateMap<data_member_file, DataMemberFilesDTO>().ForMember(a => a.data_member_id_text, o => o.MapFrom(s => s.data_member.data_member_name));
            //bidirection mapping
            Mapper.CreateMap<DataMemberFilesDTO, data_member_file>()
                .ForMember(d => d.data_member, o => o.Ignore());
            
            #endregion



            Mapper.AssertConfigurationIsValid();

            
        }
    }
}