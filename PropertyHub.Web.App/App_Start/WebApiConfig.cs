﻿using PropertyHub.Web.Common.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using PropertyHub.Data;
using System.Web.Http.OData.Routing;
using System.Web.Http.OData.Routing.Conventions;

namespace PropertyHub.Web.App
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Services.Replace(typeof(System.Web.Http.Dispatcher.IHttpControllerSelector),
            //    new PropertyHub.Web.App.Infrastructure.ODataHttpControllerSelector(config));
          //  config.Filters.Add(new WebApiSubscriptionFilterAttribute());


         



            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }

            );
        }
    }
}
