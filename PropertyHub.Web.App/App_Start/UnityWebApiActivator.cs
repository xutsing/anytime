using System.Web.Http;
using Microsoft.Practices.Unity.WebApi;
using System.Web.Http.Filters;
using System.Linq;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(PropertyHub.Web.App.App_Start.UnityWebApiActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(PropertyHub.Web.App.App_Start.UnityWebApiActivator), "Shutdown")]

namespace PropertyHub.Web.App.App_Start
{
    /// <summary>Provides the bootstrapping for integrating Unity with WebApi when it is hosted in ASP.NET</summary>
    public static class UnityWebApiActivator
    {
        /// <summary>Integrates Unity when the application starts.</summary>
        public static void Start() 
        {
            // Use UnityHierarchicalDependencyResolver if you want to use a new child container for each IHttpController resolution.
            var resolver = new UnityHierarchicalDependencyResolver(UnityConfig.GetConfiguredContainer());
            //var resolver = new UnityDependencyResolver(UnityConfig.GetConfiguredContainer());

            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            //RegisterFilterProviders(GlobalConfiguration.Configuration);
        }

        /// <summary>Disposes the Unity container when the application is shut down.</summary>
        public static void Shutdown()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }

        //private static void RegisterFilterProviders(HttpConfiguration config)
        //{
        //    var providers = config.Services.GetFilterProviders().ToList();
        //    config.Services.Add(typeof(System.Web.Http.Filters.IFilterProvider), new WebApiUnityActionFilterProvider(UnityConfig.GetConfiguredContainer()));
        //    var defaultprovider = providers.First(p => p is ActionDescriptorFilterProvider);
        //    config.Services.Remove(typeof(System.Web.Http.Filters.IFilterProvider), defaultprovider);
        //}
    }
}
