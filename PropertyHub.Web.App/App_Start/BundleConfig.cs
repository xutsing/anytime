﻿using System.Web;
using System.Web.Optimization;

namespace PropertyHub.Web.App
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/css").Include(
                      "~/content/css/bootstrap.min.css",
                      "~/content/font-awesome/css/font-awesome.min.css",
                //<!-- Toastr style -->
                      "~/content/css/plugins/toastr/toastr.min.css",
                //<!-- Gritter -->
                      "~/content/js/plugins/gritter/jquery.gritter.css",
                      "~/content/css/plugins/iCheck/custom.css",


                //steps
                      "~/content/css/plugins/steps/jquery.steps.css",
                //
                      "~/content/css/animate.css",
                //<!-- Gritter -->
                        "~/styles/kendo.common.min.css",
                        "~/styles/kendo.material.min.css",

                              "~/content/font-awesome.min.css",

                //datepicker
                 "~/content/css/plugins/datapicker/datepicker3.css",

                      "~/content/css/style.min.css"));

            bundles.Add(new ScriptBundle("~/content/js").Include(
                //<!-- Mainly scripts -->
                        "~/content/js/jquery-2.1.1.js",
                        "~/content/js/bootstrap.min.js",
                        "~/content/js/plugins/metisMenu/jquery.metisMenu.js",
                        "~/content/js/plugins/slimscroll/jquery.slimscroll.min.js",
                //<!-- Peity -->
                        "~/content/js/plugins/peity/jquery.peity.min.js",
                        "~/content/js/demo/peity-demo.js",
                //<!-- Custom and plugin javascript -->
                        "~/content/js/inspinia.js",
                        "~/content/js/plugins/pace/pace.min.js",
                //<!-- jQuery UI -->
                        "~/content/js/plugins/jquery-ui/jquery-ui.min.js",
                //<!-- GITTER -->
                        "~/content/js/plugins/gritter/jquery.gritter.min.js",
                //<!-- Sparkline -->
                        "~/content/js/plugins/sparkline/jquery.sparkline.min.js",
                // <!-- Toastr -->
                        "~/content/js/plugins/toastr/toastr.min.js",
                // <!-- kendo -->
                        "~/scripts/kendo.all.min.js",

                        "~/scripts/cultures/kendo.zh-CN.js",

                        // 
                        "~/content/js/custom.js",
                         "~/content/js/grid.js",


                 //datepicker
                  "~/content/js/plugins/datapicker/bootstrap-datepicker.js",

                        "~/content/js/plugins/chartJs/Chart.min.js",
                          "~/content/js/plugins/staps/jquery.steps.min.js",
                           "~/content/js/plugins/slimscroll/jquery.slimscroll.min.js",
                //adam modified 2015-08-04 add reference to jquery.form.js
                //use it to upload file
                        "~/content/js/jquery.form.js",

                //added by Tsing on 8/24 jquery timer.
                        "~/content/js/plugins/jquery-timer/timer.jquery.js",

     "~/content/js/plugins/idle-timer/idle-timer.min.js",


                        // <!-- Pages -->
                        "~/content/js/pages/*.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive-ajax*",
                        "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));
        }
    }
}
