﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Models
{
    public class MemberBodyReport
    {
        public MemberBodyReport()
        {
            this.Results = new List<sp_bodyreport_Result>();
        }

        public int data_member_id { get; set; }

        public string data_member_name { get; set; }

        public string data_sex_name { get; set; }

        public string age { get; set; }


        public List<sp_bodyreport_Result> Results { get; set; }

        public int sum { get; set; }

    }
    }