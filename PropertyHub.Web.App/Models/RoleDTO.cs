﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class RoleDTO
    {

        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

     //   public string Remark { get; set; }



    }
}