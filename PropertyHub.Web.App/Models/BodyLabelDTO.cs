﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class BodyLabelDTO
    {
        [Key]
        public int data_body_label_id { get; set; }

        [Required]
        public string data_body_label_text { get; set; }
    
    }
}