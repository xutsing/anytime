﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by adam, prescription DTO model 2015-08-10
    /// </summary>
    public class PrescriptionDTO
    {
        [Key]
        public int data_member_prescription_id { get; set; }
        public Nullable<int> data_member_id { get; set; }
        [Required(ErrorMessage = "标题不能为空")]
        public string data_member_prescription_title { get; set; }
        public string data_member_prescription_path { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby_id { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_id { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_id { get; set; }
        public string removedby_name { get; set; }
        public Nullable<int> data_member_prescription_status { get; set; }

        public string data_member_prescription_status_text { get; set; }
        public string data_member_name { get; set; }
    }
}