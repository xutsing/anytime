﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class MemberTestingDTO
    {
        [Key]
        public int data_member_testing_id { get; set; }

        public Nullable<int> data_member_id { get; set; }
        public string data_member_name { get; set; }
        public Nullable<int> data_questions_testing_id { get; set; }
        public string data_questions_testing_title { get; set; }
        public Nullable<int> data_questions_testing_point { get; set; }
        public Nullable<bool> data_questions_testing_result { get; set; }
        public string data_questions_testing_result_json { get; set; }
        public string data_questions_testing_remark { get; set; }

        public Nullable<DateTime> dateupdated { get; set; }
    }
}