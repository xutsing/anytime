﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// created by tsing
    /// 这个是用来传递打印的内容。
    /// </summary>
    public class PrintModel
    {

        [AllowHtml]
        public string Queryhtml { get; set; }

          [AllowHtml]
        public string BodyHtml { get; set; }
          [AllowHtml]
        public string CommentsHtml { get; set; }

          [AllowHtml]

        public string TotalHtml { get; set; }


        // public string html { get; set; }

        public string begindate { get; set; }
        public string enddate { get; set; }
    }
}