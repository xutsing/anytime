﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class RolePersmissionDTO
    {
        public string PermissionIds { get; set; }
        public string RoleId { get; set; }
    }
}