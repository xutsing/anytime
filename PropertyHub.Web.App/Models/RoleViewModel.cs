﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class RoleViewModel
    {

        public string Id { get; set; }
        public string UserIds { get; set; }

        public string UserName { get; set; }
    }
}