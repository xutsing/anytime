﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class DataCodeServicesTasksDTO
    {
        [Key]
        public int data_code_services_tasks_id { get; set; }
        public Nullable<int> data_code_services_id { get; set; }
        public Nullable<int> data_basetask_id { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby_code { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_code { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_code { get; set; }
        public string removedby_name { get; set; }
        public Nullable<System.DateTime> data_code_service_date { get; set; }
        public string data_code_service_remark { get; set; }
        public Nullable<int> data_code_services_tasks_status { get; set; }
        public Nullable<int> data_record_Id { get; set; }
        public string data_member_name { get; set; }
        public string data_basetask_name { get; set; }
        public string data_basetask_url { get; set; }
        public string data_dictionary_text { get; set; }

        public Nullable<int> data_member_id { get; set; }

    }

    public class TaskDTO
    {
        [Key]
        public int data_code_services_tasks_id { get; set; }
        public Nullable<int> data_code_services_id { get; set; }
        public Nullable<int> data_basetask_id { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby_code { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_code { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_code { get; set; }
        public string removedby_name { get; set; }
        public Nullable<System.DateTime> data_code_service_date { get; set; }
        public string data_code_service_remark { get; set; }
        public Nullable<int> data_code_services_tasks_status { get; set; }
        public Nullable<int> data_record_Id { get; set; }

       // public virtual data_code_services data_code_services { get; set; }
       // public virtual data_basetask data_basetask { get; set; }
    }
}