﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Models
{
    public class ReportBySecModel
    {


        public ReportBySecModel()
        {
            this.Results = new List<sp_reportmemberBySec_Result>();
        }

        public string  begin { get; set; }
        public string  end { get; set; }


        public List<sp_reportmemberBySec_Result> Results { get; set; }

        /// <summary>
        /// 总共有多少个秘书
        /// </summary>
        public int TotalSecCount
        {
            get { return this.Results.Select(a => a.UserName).Distinct().Count(); }
        }

        /// <summary>
        /// 总会员个数
        /// </summary>
        public int TotalMemberCount
        {
            get
            {
                return this.Results.Sum(a => a.member_count).HasValue ? this.Results.Sum(a => a.member_count).Value : 0;
            }
        }

        /// <summary>
        /// 总共激活的会员
        /// </summary>
        public int TotalActivedMemberCount
        {
            get
            {
                return this.Results.Sum(a => a.member_actived_count).Value;
            }
        }
    }
}