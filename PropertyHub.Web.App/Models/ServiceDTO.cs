﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class ServiceDTO
    {
        [Key]
        public int data_service_id { get; set; }
        public string data_service_name { get; set; }
        public Nullable<int> data_service_validate_months { get; set; }
        public Nullable<decimal> data_service_price { get; set; }
        public string data_service_status { get; set; }
    }
}