﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class DataCoachDTO
    {
        [Key]
        //  [DefaultValue(0)]

        public int data_anytime_friend_id { get; set; }
        public Nullable<int> data_province_Id { get; set; }
        public Nullable<int> data_city_Id { get; set; }
        public Nullable<int> data_area_Id { get; set; }
        public string data_name { get; set; }
        public string data_profession { get; set; }
        public string data_comments { get; set; }
        public string data_Ip { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> data_age { get; set; }
        public Nullable<int> data_sex_Id { get; set; }
        public string ProvinceName { get; set; }
        public string CityName { get; set; }
        public string AreaName { get; set; }
        public string data_sex_title { get; set; }
    }
}