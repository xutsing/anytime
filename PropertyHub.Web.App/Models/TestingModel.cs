﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class TestingModel
    {
        public int data_member_testing_id { get; set; }

        public Nullable<int> data_member_id { get; set; }

        public string data_member_name { get; set; }

        public Nullable<int> data_questions_testing_id { get; set; }

        public string data_questions_testing_title { get; set; }

        public IList<TestingItemModel> Questions { get; set; }

        public string data_questions_testing_remark { get; set; }


        public int TaskId { get; set; }
        public int IsFinished { get; set; }

        public string TaskComments { get; set; }
    }

    public class TestingItemModel
    {
        public int data_questions_id { get; set; }

        public string data_questions_text { get; set; }

        public string Answer { get; set; }

        public string Comments { get; set; }
    }
}