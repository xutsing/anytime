﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class DataKnowledgeDTO
    {
        [Key]
        public int data_knowledge_id { get; set; }
        public Nullable<int> data_knowledge_type { get; set; }

        public string data_knowledge_type_title { get; set; }
        [Required(ErrorMessage="标题不能为空")]
        public string data_knowledge_title { get; set; }
        public string data_knowledge_summary { get; set; }
        public string data_knowledge_remark { get; set; }
        public string data_knowledge_path { get; set; }
        public Nullable<int> data_knowledge_author_type { get; set; }
        public Nullable<int> data_knowledge_author_id { get; set; }
        public Nullable<int> data_knowledge_status { get; set; }
        public string data_knowledge_keyword { get; set; }
        public string createdby_id { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_id { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_id { get; set; }
        public string removedby_name { get; set; }
        public Nullable<bool> data_knowledge_ischecked { get; set; }
        public string checkedby_id { get; set; }
        public string checkedby_name { get; set; }
    }
}