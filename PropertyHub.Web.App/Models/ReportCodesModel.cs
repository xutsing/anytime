﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by tsing on 2015/8/28
    /// </summary>
    public class ReportCodesModel
    {
        public ReportCodesModel()
        {
            this.Results = new List<sp_reportcodes_Result>();
        }

        public string begin { get; set; }
        public string end { get; set; }

        public List<sp_reportcodes_Result> Results { get; set; }


        public int TotalSupplier
        {
            get
            {
                return this.Results.Count;
            }
        }

        public int TotalServices
        {
            get
            {
                return this.Results.Sum(a => a.total_codes).Value;
            }
        }

        public int TotalActivedCodes
        {
            get
            {
                return this.Results.Sum(a => a.total_actived_codes).Value;
            }
        }

    }
}