﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class ServiceTaskDTO
    {
        [Key]
        public int data_service_tasks_id { get; set; }
        public Nullable<int> data_service_id { get; set; }
        public Nullable<int> data_basetask_id { get; set; }
        public string data_basetask_name { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<int> data_nexttask_interval { get; set; }
        public Nullable<int> data_service_tasks_order { get; set; }
        public Nullable<bool> Iscycle { get; set; }
        public Nullable<int> cycleDays { get; set; }

        public Nullable<int> cyclemax { get; set; }
    }

}