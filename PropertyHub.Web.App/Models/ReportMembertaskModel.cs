﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Models
{
    public class ReportMembertaskModel
    {
        public ReportMembertaskModel()
        {
            this.Results = new List<sp_membertasks_Result>();
        }
        public string begin { get; set; }
        public string end { get; set; }

        public List<sp_membertasks_Result> Results { get; set; }

        public int TotalMemberCount
        {
            get { return this.Results.Sum(a => a.会员数); }
        }

        public int TotalTaskCount
        {
            get
            {
                return this.Results.Sum(a => a.全部工作);
            }
        }

        public int TotalFinishedTaskCount
        {
            get
            {
                return this.Results.Sum(a => a.完成工作);
            }
        }


       
    }
}