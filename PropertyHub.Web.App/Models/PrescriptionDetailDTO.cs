﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by adam, prescription detail DTO model 2015-08-10
    /// </summary>
    public class PrescriptionDetailDTO
    {
        [Key]
        public int data_member_prescription_detail_id { get; set; }
        public Nullable<int> data_member_prescription_id { get; set; }
        public Nullable<int> data_machine_action_id { get; set; }
        public Nullable<int> data_machine_action_times { get; set; }
        public Nullable<int> data_machine_action_number { get; set; }
        public string data_machine_action_remark { get; set; }
        public string createdby_id { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_id { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_id { get; set; }
        public string removedby_name { get; set; }

        public string machine_action_text { get; set; }
        public virtual data_machine_action data_machine_action { get; set; }
        public virtual data_member_prescription data_member_prescription { get; set; }
    }
}