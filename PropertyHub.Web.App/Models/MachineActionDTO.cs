﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by adam, machine action DTO model 2015-08-04
    /// </summary>
    public class MachineActionDTO
    {
        [Key]
        public int data_machine_action_id { get; set; }
        public Nullable<int> data_machine_id { get; set; }

        [Required(ErrorMessage = "动作名称不能为空")]
        public string data_machine_action_title { get; set; }
        public string data_machine_action_path { get; set; }
        public string data_machine_action_remark { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<int> data_machine_action_status { get; set; }

        public string data_machine_name { get; set; }
        public string data_machine_action_status_text { get; set; }
    }
}