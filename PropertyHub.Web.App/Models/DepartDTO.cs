﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class DepartDTO
    {
        
        public int Id { get; set; }


        [StringLength(50)]
        [Required]
        [DisplayName("部门名称")]
        public string DepartName { get; set; }

        [StringLength(50)]
        [Required]
        [DisplayName("备注")]
        public string Reamrk { get; set; }


    }



    public class DepartDTOModel
    {

        public int Id { get; set; }


        [StringLength(50)]
        [Required]
        [DisplayName("部门名称")]
        public string DepartName { get; set; }

        [StringLength(50)]
        [Required]
        [DisplayName("备注")]
        public string Reamrk { get; set; }

        [Required]
        [DisplayName("部门领导")]
        public string leaders { get; set; }


        [Required]
        [DisplayName("分管秘书")]
        public string members { get; set; }


    }


}