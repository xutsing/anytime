﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class MemberCheckDTO
    {
        public Nullable<int> data_member_id { get; set; }
        public string data_member_name { get; set; }
        [Key]
        public int data_member_checkrecord_id { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }    
        public string data_member_checkrecord_remark { get; set; }


        public Nullable<bool> data_IsCreated { get; set; }
        public Nullable<bool> data_Isfinished { get; set; }
        public Nullable<System.DateTime> data_create_date { get; set; }
        public string data_checkreport { get; set; }
        public string data_create_code { get; set; }
    }


    /// <summary>
    /// 会员运动健身建议
    /// </summary>
    public class MemberCheckMVCModel
    {
        public bool IsFinished { get; set; }
        public string CheckReport { get; set; }

        public int check_id { get; set; }

    }
}