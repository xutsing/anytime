﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace PropertyHub.Web.App.Models
{
    public class SupplierDTO
    {
        [Key]
        public int data_supplier_id { get; set; }
          [Required(ErrorMessage = "公司名称不能为空")]
        public string data_supplier_name { get; set; }
        public string data_supplier_address { get; set; }
        public string data_supplier_phone { get; set; }
        public string data_suplier_contracts { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<int> data_record_status { get; set; }
    
    }
}