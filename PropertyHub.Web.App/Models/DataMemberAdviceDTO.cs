﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class DataMemberAdviceDTO
    {
        [Key]
        public int data_member_advice_id { get; set; }
        public Nullable<int> data_member_id { get; set; }

        //会员
        public string data_member_name { get; set; }

        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby { get; set; }
        public string data_member_advice_summary { get; set; }

        public bool data_member_advice_isfinished { get; set; }


        //the Properties for task.
        public int taskId { get; set; }
        public int IsFinished { get; set; }
        public string TaskComments { get; set; }

    }
}