﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PropertyHub.Data;
using PropertyHub.Services;


namespace PropertyHub.Web.App.Models
{
    public class MemberCheckDataDTO
    {
        
        [Key]
        public int data_member_checkdata_id { get; set; }

        public Nullable<int> data_member_checkrecord_id { get; set; }
        public string  data_base_index_id { get; set; }
        public string data_member_value { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }

       // public string data_base_index { get; set; }
    }


    public class MemberCheckDataReport
    {
        public MemberCheckDataReport()
        {
            this.Results = new List<CheckDataModel>();
        }

        public int data_member_id { get; set; }

        public string data_member_name { get; set; }

        public string data_sex_name { get; set; }

        public string age { get; set; }


        public List<CheckDataModel> Results { get; set; }


        public Dictionary<string, string> Data { get; set; }

        public int sum { get; set; }

    }
}