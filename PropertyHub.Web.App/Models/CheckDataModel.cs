﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class CheckDataModel
    {

        public string data_base_index_title { get; set; }
        public string data_base_index_unit { get; set; }

        public string data_base_index_index { get; set; }

        public string data_member_value { get; set; }

        public string data_body_label_text { get; set; }


    }

}
}