﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace PropertyHub.Web.App.Models
{
    public class DataMemberFileDTO
    {
        [Key]
        public int data_member_information_Id { get; set; }
        public Nullable<int> data_member_id { get; set; }

        public string data_member_name { get; set; }
        public string data_member_identity { get; set; }
        public string data_member_country { get; set; }

        public Nullable<System.DateTime> dateupdated { get; set; }
    }
}