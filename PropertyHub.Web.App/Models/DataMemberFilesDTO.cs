﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class DataMemberFilesDTO
    {
        [Key]
        public int dat_member_file_id { get; set; }
        public Nullable<int> data_member_id { get; set; }
        public string data_member_id_text { get; set; }
        public string data_member_file_path { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby { get; set; }
    }
}