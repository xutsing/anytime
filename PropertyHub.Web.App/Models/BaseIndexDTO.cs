﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

//333333333333333


//wood. this file is updated 6:49 pm
//i will go home ,once you resolve this confiict.
//
//

namespace PropertyHub.Web.App.Models
{
    public class BaseIndexDTO
    {
           [Key]
           public int data_base_index_id { get; set; }

          [Required(ErrorMessage = "中文名称不能为空")]
           public string data_base_index_title { get; set; }
           public string data_base_index_unit { get; set; }
           public Nullable<System.DateTime> datecreated { get; set; }
           public Nullable<int> createdby { get; set; }
           public Nullable<System.DateTime> dateupdated { get; set; }
           public Nullable<int> updatedby { get; set; }
           public Nullable<System.DateTime> dateremoved { get; set; }
           public Nullable<int> removedby { get; set; }
           public string data_base_index_title_en { get; set; }
           public string data_base_key { get; set; }
    
    }
}