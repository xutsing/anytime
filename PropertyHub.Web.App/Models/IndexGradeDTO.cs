﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace PropertyHub.Web.App.Models
{
    public class IndexGradeDTO
    {
        [Key]
        public int data_index_grade_id { get; set; }
        public Nullable<int> data_index_grade1 { get; set; }

        public string data_index_grade1_title { get; set; }
        [Required(ErrorMessage = "最小年龄不能为空")]
        public Nullable<int> data_index_grade_age_min { get; set; }

        [Required(ErrorMessage = "最大年龄不能为空")]
        public Nullable<int> data_index_grade_age_max { get; set; }
        [Required(ErrorMessage = "最低成绩不能为空")]
        public Nullable<int> data_index_grade_point_min { get; set; }
        [Required(ErrorMessage = "最高成绩不能为空")]
        public Nullable<int> data_index_grade_point_max { get; set; }
        
        [StringLength(50)]
        public string data_index_grade_comments { get; set; }
    
    }
}