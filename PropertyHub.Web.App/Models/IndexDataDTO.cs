﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class IndexDataDTO
    {

        public Nullable<int> data_base_index_id { get; set; }

        public string data_base_index_title { get; set; }
        public Nullable<int> data_body_label_id { get; set; }

        public string data_body_label_title { get; set; }
        public Nullable<int> data_sex_id { get; set; }

        public string data_sex_title { get; set; }
        public Nullable<int> data_age_range_id { get; set; }

        public string data_age_range_title { get; set; }

         [Key]
        public int data_index_data_id { get; set; }
        [Required(ErrorMessage = "最小值不能为空")]
        public Nullable<decimal> data_index_data_min { get; set; }
        [Required(ErrorMessage = "最大值不能为空")]
        public Nullable<decimal> data_index_data_max { get; set; }
        public Nullable<int> data_index_data_point { get; set; }
        public string data_index_data_label { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
    }
}