﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    public class BaseTaskDTO
    {
        [Key]
        public int data_basetask_id { get; set; }
        public string data_basetask_type { get; set; }
        public string data_basetask_name { get; set; }
        public string data_basetask_remark { get; set; }
        public string data_basetask_url { get; set; }
    }
}