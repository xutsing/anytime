﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class CodeStatusDTO
    {       
        public Nullable<int> data_supplier_id { get; set; }
        public string data_supplier_name { get; set; }
        public Nullable<int> data_service_id { get; set; }
        public string data_service_name { get; set; }
        public Nullable<decimal> data_code_record_money { get; set; }
        public Nullable<int> data_code_record_status { get; set; }
        public string data_code_record_status_text { get; set; }
        public Nullable<int> data_code_status { get; set; }
        public string data_code_status_text { get; set; }
        [Key]
        [Required(ErrorMessage = "激活码不能为空")]
        public string data_code_text { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> data_member_sales { get; set; }
        public Nullable<int> data_member_actived { get; set; }
        //add by wxw
        public string data_member_actived_text { get; set;  }
        public Nullable<int> data_code_services_status { get; set; }
        public string data_code_services_status_text { get; set; }
        public Nullable<System.DateTime> dateStarted { get; set; }
        public Nullable<System.DateTime> dateEnd { get; set; }
    }
}