﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class CodeDTO
    {

        [Key]
        public int data_code_id { get; set; }
        public Nullable<int> data_code_record_id { get; set; }
        public Nullable<int> data_code_status { get; set; }
        public string data_code_text { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
     
        public Nullable<int> data_member_id { get; set; }

    }
}