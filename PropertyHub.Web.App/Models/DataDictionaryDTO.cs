﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace PropertyHub.Web.App.Models
{
    public class DataDictionaryDTO
    {
        [Key]
        public int data_dictionary_id { get; set; }
        [Required(ErrorMessage = "数据类型不能为空")]
        public string data_dictionary_type { get; set; }
        [Required(ErrorMessage = "数据文本不能为空")]
        public string data_dictionary_text { get; set; }
        public Nullable<int> data_dictionary_value { get; set; }
    }
}