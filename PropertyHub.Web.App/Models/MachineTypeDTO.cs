﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by adam, machine_type DTO model 2015-08-04
    /// </summary>
    public class MachineTypeDTO
    {
        [Key] 
        public int data_machine_type_id { get; set; }
        [Required(ErrorMessage = "器械类型标题不能为空")]
        public string data_machine_type_title { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
    }
}