﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class AgeDTO
    {
        [Key]

        public int data_age_range_id { get; set; }
        [Required(ErrorMessage = "最小值不能为空")]
        public Nullable<int> data_age_min { get; set; }
        [Required(ErrorMessage = "最大值不能为空")]
        public Nullable<int> data_age_max { get; set; }
        public string data_age_title { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<bool> data_age_all { get; set; }

    
    }
}