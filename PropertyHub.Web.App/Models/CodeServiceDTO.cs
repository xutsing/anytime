﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class CodeServiceDTO
    {

        [Key]
        public int data_code_services_id { get; set; }
        public string data_code_id { get; set; }
        public string  data_service_id { get; set; }
        public string data_member_id { get; set; }
        public Nullable<System.DateTime> data_code_services_date { get; set; }
        public Nullable<int> data_code_services_status { get; set; }
        public string data_code_services_remark { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<System.DateTime> dateStarted { get; set; }
        public Nullable<System.DateTime> dateEnd { get; set; }
        public Nullable<System.DateTime> dateActualStarted { get; set; }
        public Nullable<System.DateTime> dateActualEnd { get; set; }


    }
}