﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyHub.Data;

namespace PropertyHub.Web.App.Models
{
    public class ModuleViewModels
    {

        public List<Modules> Modules { get; set; }
    }
}