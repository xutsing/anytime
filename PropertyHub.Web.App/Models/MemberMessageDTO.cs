﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by adam, machine DTO model 2015-08-01
    /// </summary>
    public class MemberMessageDTO
    {
        [Key]
        public int data_member_message_id { get; set; }
        public string data_member_ids { get; set; }
        public string data_member_names { get; set; }
        public string data_member_message_title { get; set; }
        public string data_member_message_text { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby { get; set; }
        public Nullable<int> record_status { get; set; }

        public Nullable<int> data_member_message_type { get; set; }
    }
}