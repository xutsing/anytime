﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyHub.Data;
using System.ComponentModel.DataAnnotations;

namespace PropertyHub.Web.App.Models
{
    public class ReportMemberModel
    {
        public string begin { get; set; }
        public string end { get; set; }

        public List<sp_members_Result> Results { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy年MM月dd日}")]
        public virtual System.DateTime PostTime
        {
            get;
            set;
        }

        public ReportMemberModel()
        {
            this.Results = new List<sp_members_Result>();
        }

        public int TotalValidMemberCount
        {
            get { return this.Results.Count(a => a.status.Equals("true")); }
        }


        public int TotalInvalidMemberCount
        {
            get
            {
                return this.Results.Count(a => a.status.Equals("false"));
            }
        }
    }
}