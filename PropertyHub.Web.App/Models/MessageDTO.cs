﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Services.Description;

namespace PropertyHub.Web.App
{
    public class MessageDTO
    {

        [Key]
        public int data_message_id { get; set; }
        public Nullable<int> message_type_id { get; set; }
        public string message_type_title { get; set; }
        public Nullable<int> message_member_id { get; set; }
        public string message_member_name { get; set; }
        public string message_service_id { get; set; }
        public string message_service_name { get; set; }
        public string message_text { get; set; }
        public bool message_isread { get; set; }
        public Nullable<System.DateTime> message_date { get; set; }
        public Nullable<int> message_status { get; set; }
        public bool message_from_member { get; set; }
        public bool message_Isreply { get; set; }
        public string message_comments { get; set; }
        public Nullable<System.DateTime> message_comment_date { get; set; }


       



    }


}