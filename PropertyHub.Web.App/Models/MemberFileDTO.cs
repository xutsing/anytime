﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    /// <summary>
    /// added by tsing 
    /// 会员家庭健康档案 
    /// </summary>
    public class MemberFileDTO
    {
        public int data_member_information_Id { get; set; }
        public Nullable<int> data_member_id { get; set; }
        public string data_member_identity { get; set; }
        public string data_member_country { get; set; }
        public string data_member_family { get; set; }
        public Nullable<int> data_member_nationality { get; set; }
        public string data_member_nationality_comments { get; set; }
        public Nullable<int> data_member_blood { get; set; }
        public Nullable<int> data_member_rh { get; set; }
        public Nullable<int> data_member_profession { get; set; }
        public string data_member_disease { get; set; }
        public string data_member_disease_c { get; set; }
        public string data_member_disease_other { get; set; }
        public string data_member_disease_1 { get; set; }
        public Nullable<System.DateTime> data_member_disease_1_date { get; set; }
        public string data_member_disease_2 { get; set; }
        public Nullable<System.DateTime> data_member_disease_2_date { get; set; }
        public string data_member_disease_3 { get; set; }
        public Nullable<System.DateTime> data_member_disease_3_date { get; set; }
        public string data_member_disease_4 { get; set; }
        public Nullable<System.DateTime> data_member_disease_4_date { get; set; }
        public string data_member_disease_5 { get; set; }
        public Nullable<System.DateTime> data_member_disease_5_date { get; set; }
        public string data_member_disease_6 { get; set; }
        public Nullable<System.DateTime> data_member_disease_6_date { get; set; }
        public bool data_member_surgery { get; set; }
        public string data_member_surgery_1 { get; set; }
        public Nullable<System.DateTime> data_member_surgery_1_date { get; set; }
        public string data_member_surgery_2 { get; set; }
        public Nullable<System.DateTime> data_member_surgery_2_date { get; set; }
        public bool data_member_injury { get; set; }
        public string data_member_injury_1 { get; set; }
        public Nullable<System.DateTime> data_member_injury_1_date { get; set; }
        public string data_member_injury_2 { get; set; }
        public Nullable<System.DateTime> data_member_injury_2_date { get; set; }
        public string data_member_injury_3 { get; set; }
        public Nullable<System.DateTime> data_member_injury_3_date { get; set; }
        public bool data_member_blood_transfer { get; set; }
        public string data_member_blood_transfer_1_reason { get; set; }
        public Nullable<System.DateTime> data_member_blood_transfer_1_date { get; set; }
        public string data_member_blood_transfer_2_reason { get; set; }
        public Nullable<System.DateTime> data_member_blood_transfer_2_date { get; set; }
        public string data_member_family_father { get; set; }
        public string data_member_family_mother { get; set; }
        public string data_member_family_brothers { get; set; }
        public string data_member_family_children { get; set; }
        public string data_member_family_disease { get; set; }
        public bool data_member_inherited { get; set; }
        public string data_member_inherited_disease { get; set; }
        public Nullable<decimal> data_member_height { get; set; }
        public Nullable<decimal> data_member_hear_rate { get; set; }
        public Nullable<decimal> data_member_waist_line { get; set; }
        public Nullable<decimal> data_member_blood_pressure { get; set; }
        public Nullable<decimal> data_member_hips { get; set; }
        public Nullable<decimal> data_member_bmi { get; set; }
        public Nullable<decimal> data_member_muscle { get; set; }
        public Nullable<decimal> data_member_balance { get; set; }
        public Nullable<decimal> data_member_myodynamic { get; set; }
        public Nullable<decimal> data_member_staying { get; set; }
        public Nullable<decimal> data_member_whr { get; set; }
        public Nullable<decimal> data_member_flexible { get; set; }
        public Nullable<decimal> data_member_reflection { get; set; }

        public Nullable<decimal> data_member_lung { get; set; }
        public Nullable<decimal> data_member_weight { get; set; }

        public Nullable<int> data_physical_rate { get; set; }
        public Nullable<int> data_physical_time { get; set; }
        public Nullable<decimal> data_physical_total_time { get; set; }
        public Nullable<int> data_physical_place { get; set; }
        public Nullable<int> data_physical_schedule { get; set; }
        public string data_physical_item { get; set; }
        public string data_food { get; set; }
        public Nullable<int> data_smoke { get; set; }
        public Nullable<decimal> data_smoke_day { get; set; }
        public Nullable<int> data_smoke_begin { get; set; }
        public Nullable<int> data_smoke_end { get; set; }


        public Nullable<decimal> data_drink_average1 { get; set; }
        public Nullable<decimal> data_drink_average2 { get; set; }



        public Nullable<DateTime> data_drink_Isdry_date { get; set; }

        public Nullable<int> data_smoke_age { get; set; }
        public Nullable<int> data_drink { get; set; }
        public Nullable<decimal> data_drink_day { get; set; }
        public bool data_drink_Isdry { get; set; }
        public Nullable<int> data_drink_Isdry_age { get; set; }
        public Nullable<int> data_drink_start { get; set; }
        public bool data_drink_Isdrink { get; set; }
        public string data_drink_wine_options { get; set; }
        public string data_drink_wine_options_others { get; set; }
        public string data_member_brain { get; set; }
        public string data_member_brain_others { get; set; }
        public string data_member_kidney { get; set; }
        public string data_member_kidney_others { get; set; }
        public string data_member_heart { get; set; }
        public string data_member_heart_others { get; set; }
        public string data_member_artery { get; set; }
        public string data_member_artery_others { get; set; }
        public string data_member_eye { get; set; }
        public string data_member_eye_others { get; set; }
        public bool data_member_nerve { get; set; }
        public string data_member_nerve_others { get; set; }
        public bool data_member_other_disease { get; set; }
        public string data_member_other_disease_others { get; set; }
        public string data_member_address { get; set; }
        public string data_member_winxin { get; set; }
        public string data_member_qq { get; set; }
        public string data_member_email { get; set; }

     //   public virtual data_member data_member { get; set; }
        public string data_member_rh_comments { get; set; }


        public string createdby_code { get; set; }
        public string createdby_name { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public string updatedby_code { get; set; }
        public string updatedby_name { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public string removedby_code { get; set; }
        public string removedby_name { get; set; }



        public string data_member_ethnic { get; set; }
        public string data_member_diseases_comments { get; set; }
        public string data_member_operations_comments { get; set; }
        public string data_member_wound_comments { get; set; }
        public string data_member_blood_comments { get; set; }
        public bool data_member_diseases_1 { get; set; }
        public string data_member_diseases_1_text { get; set; }
        public string data_member_diseases_1_relationship { get; set; }
        public bool data_member_diseases_2 { get; set; }
        public string data_member_diseases_2_text { get; set; }
        public string data_member_diseases_2_relationship { get; set; }
        public bool data_member_diseases_3 { get; set; }
        public string data_member_diseases_3_text { get; set; }
        public string data_member_diseases_3_relationship { get; set; }


        public bool data_member_diseases_bit { get; set; }
        public bool data_member_operations_bit { get; set; }
        public bool data_member_wound_bit { get; set; }
        public bool  data_member_blood_bit { get; set; }
    }
}