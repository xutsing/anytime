﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyHub.Web.App.Models
{
    public class MemberDTO
    {
       // public int Id { get; set; }

        [Key]
        public int data_member_id { get; set; }
     
        public string data_member_name { get; set; }
        public string data_member_loginname { get; set; }
        public string data_member_pw { get; set; }
        public string data_member_mobile { get; set; }
        public string data_member_sex { get; set; }
     
        public string data_member_group { get; set; }
        
        public DateTime?  data_member_birthday { get; set; }

        //modified by adam 2015-08-06
        //add fields
        public string data_member_email { get; set; }
        public string data_member_service_id { get; set; }
        public string data_member_service_name { get; set; }
        public Nullable<bool> data_member_has_service { get; set; }

        public Nullable<System.DateTime> datecreated { get; set; }



        //added by tsing on 2015/10/21
        public string data_member_height { get; set; }
        public string data_member_type_text { get; set; }

    }
}