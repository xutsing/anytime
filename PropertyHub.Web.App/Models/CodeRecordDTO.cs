﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace PropertyHub.Web.App.Models
{
    public class CodeRecordDTO
    {

        //testing
        [Key]
        public int data_code_record_id { get; set; }
        public string  data_supplier_id { get; set; }
        public string  data_service_id { get; set; }
        public Nullable<int> data_code_record_type { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
       // public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
      //  public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
       // public Nullable<int> removedby { get; set; }
        public Nullable<bool> data_code_record_isPay { get; set; }
        public Nullable<decimal> data_code_record_money { get; set; }
        public Nullable<int> data_code_record_Paytype { get; set; }
        public string  data_code_record_status { get; set; }

        public Nullable<int> data_code_record_count { get; set; }


        public string data_member_service_id { get; set; }
        public string data_member_service_name { get; set; }


    }
}