﻿using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Security.Claims;
using System.Threading;

namespace PropertyHub.Data.Infrastructure
{
    /// <summary>
    /// Custom implementation of <see cref="IDbCommandInterceptor"/>.
    /// In this class we set the actual value of the tenantId when querying the database as the command tree is cached  
    /// </summary>
    public class TenantCommandInterceptor : IDbCommandInterceptor
    {
        public virtual int? SubscriptionId
        {
            get
            {
                var value = System.Web.HttpContext.Current == null ? null : System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values["Subscription"];
                if (value != null)
                {
                    return (int?)value;
                }
                return null;
            }
        }
        public virtual bool HasSubscriptionId
        {
            get { return SubscriptionId > 0; }
        }

        public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
        }

        public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
        }

        public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
        }

        private void SetTenantParameterValue(DbCommand command)
        {
            if (HasSubscriptionId)
            {
                // Enumerate all command parameters and assign the correct value in the one we added inside query visitor
                foreach (DbParameter param in command.Parameters)
                {
                    if (param.ParameterName != TenantAwareAttribute.TenantIdFilterParameterName)
                        continue;
                    param.Value = SubscriptionId;
                }
            }
        }

    }
}