﻿using System;
using System.Data.Entity;

namespace PropertyHub.Data.Infrastructure
{
    public class EntityFrameworkConfiguration : DbConfiguration
    {
        public EntityFrameworkConfiguration()
        {
            AddInterceptor(new TenantCommandInterceptor());
            AddInterceptor(new TenantCommandTreeInterceptor());
        }

    }
}