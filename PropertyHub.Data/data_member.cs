//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PropertyHub.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class data_member
    {
        public data_member()
        {
            this.data_code_services = new HashSet<data_code_services>();
            this.data_comments = new HashSet<data_comments>();
            this.data_member_device = new HashSet<data_member_device>();
            this.data_member_secretary = new HashSet<data_member_secretary>();
            this.data_member_prescription = new HashSet<data_member_prescription>();
            this.data_member_checkrecord = new HashSet<data_member_checkrecord>();
            this.data_member_testing = new HashSet<data_member_testing>();
            this.data_member_advice = new HashSet<data_member_advice>();
            this.data_message = new HashSet<data_message>();
            this.data_service_comments = new HashSet<data_service_comments>();
            this.data_member_information = new HashSet<data_member_information>();
            this.data_member_file = new HashSet<data_member_file>();
        }
    
        public int data_member_id { get; set; }
        public Nullable<int> data_member_type_id { get; set; }
        public string data_member_name { get; set; }
        public string data_member_loginname { get; set; }
        public string data_member_pw { get; set; }
        public string data_member_mobile { get; set; }
        public Nullable<int> data_member_sex { get; set; }
        public Nullable<System.DateTime> data_member_birthday { get; set; }
        public string data_member_group { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> dateupdated { get; set; }
        public Nullable<int> updatedby { get; set; }
        public Nullable<System.DateTime> dateremoved { get; set; }
        public Nullable<int> removedby { get; set; }
        public Nullable<int> data_member_status { get; set; }
        public string data_member_email { get; set; }
        public string data_member_service_id { get; set; }
        public string data_member_service_name { get; set; }
        public Nullable<bool> data_member_has_service { get; set; }
        public string data_member_height { get; set; }
        public string data_member_type_text { get; set; }
        public string data_member_source { get; set; }
    
        public virtual ICollection<data_code_services> data_code_services { get; set; }
        public virtual ICollection<data_comments> data_comments { get; set; }
        public virtual ICollection<data_member_device> data_member_device { get; set; }
        public virtual data_member_type data_member_type { get; set; }
        public virtual ICollection<data_member_secretary> data_member_secretary { get; set; }
        public virtual ICollection<data_member_prescription> data_member_prescription { get; set; }
        public virtual ICollection<data_member_checkrecord> data_member_checkrecord { get; set; }
        public virtual ICollection<data_member_testing> data_member_testing { get; set; }
        public virtual ICollection<data_member_advice> data_member_advice { get; set; }
        public virtual data_sex data_sex { get; set; }
        public virtual ICollection<data_message> data_message { get; set; }
        public virtual ICollection<data_service_comments> data_service_comments { get; set; }
        public virtual ICollection<data_member_information> data_member_information { get; set; }
        public virtual ICollection<data_member_file> data_member_file { get; set; }
    }
}
