﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{


    public abstract class EntityOriginal
    {
        [StringLength(255)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        [StringLength(255)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public int RecordStatus { get; set; }
    }


    public abstract class EntityBase:EntityOriginal
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
      
    }

    [TenantAware("SubscriptionId")]
    public abstract class SubscriptionEntityBase : EntityBase
    {
        public virtual int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
    }

    public abstract class PropertyEntityBase : SubscriptionEntityBase
    {
        public virtual int PropertyId { get; set; }
        public virtual Property Property { get; set; }
    }
}
