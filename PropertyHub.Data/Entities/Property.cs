﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    public partial class Property : SubscriptionEntityBase
    {
        [StringLength(255)]
        [Required]
        public virtual string Street1 { get; set; }
        [StringLength(255)]
        public virtual string Street2 { get; set; }
        [StringLength(255)]
        [Required]
        public virtual string City { get; set; }
        [StringLength(255)]
        [Required]
        public virtual string State { get; set; }
        [StringLength(4)]
        public virtual string Postcode { get; set; }
        public virtual File Photo { get; set; }
        [StringLength(4000)]
        public virtual string Summary { get; set; }
        public virtual ICollection<Tenant> Tenants { get; set; }
        public virtual ICollection<PropertyNote> Nodes { get; set; }

        [NotMapped]
        public virtual Tenant CurrentTenant
        {
            get
            {
                return Tenants.FirstOrDefault(x => x.IsCurrent == true && x.IsPrimary == true);
            }
        }
        [NotMapped]
        public virtual IEnumerable<Tenant> PreviousTenants
        {
            get
            {
                return Tenants.Where(x => x.IsCurrent == false && x.IsPrimary == true);
            }
        }

        public Property()
        {
            Tenants = new HashSet<Tenant>();
            Nodes = new HashSet<PropertyNote>();
        }
    }
}
