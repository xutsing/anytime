﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PropertyHub.Data
{
   public  class Department:SubscriptionEntityBase
    {

       public Department()
       {
           this.Members = new HashSet<DepartmentMember>();
       }

       [Required]
       [StringLength(255)]
       public virtual string DepartName { get; set; }


       [Required]
       [StringLength(255)]
       public virtual string Reamrk { get; set; }

       /// <summary>
       /// the leaders of this department
       /// </summary>
     //  public virtual ICollection<ApplicationUser> Leaders { get; set; }

       /// <summary>
       /// the member of this depart.
       /// </summary>
       public virtual ICollection<DepartmentMember> Members { get; set; }

    }


   public class DepartmentMember: SubscriptionEntityBase
   {
       public string UserId { get; set; }

       public bool IsLeader { get; set; }


   }



}
