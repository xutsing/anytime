﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    /// <summary>
    /// created by tsing
    /// </summary>
    public partial class Modules : SubscriptionEntityBase
    {

        [StringLength(255)]
        [Required]
        [DisplayName("Module Name")]
        public virtual string ModuleName { get; set; }



        [StringLength(255)]
        // [Required]
        [DisplayName("Module Icon")]
        public virtual string ModuleIcon { get; set; }


        [StringLength(255)]
        //[Required]
        [DisplayName("Module Url")]
        public virtual string ModuleUrl { get; set; }


        public virtual int? ParentId { get; set; }


        public virtual ICollection<Modules> Children { get; set; }

        //模块权限
        public virtual ICollection<ModulePermission> Permissions { get; set; }


    }
}
