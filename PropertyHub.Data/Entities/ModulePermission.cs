﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
   public  class ModulePermission:SubscriptionEntityBase
    {
          [StringLength(50)]
       //权限名称
       public string PermissionName { get; set; }
       //具体个某个权限点.
       public int PermissionId { get; set; }

    //  public int Modules_Id { get; set; } 
    }
}
