﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    //[TenantAware("Id")]
    public partial class Subscription : EntityBase
    {
        public Subscription()
        {
            Users = new HashSet<ApplicationUser>();
            Properties = new HashSet<Property>();
            Departs = new HashSet<Department>();
        }

        [StringLength(255)]
        [Required]
        [DisplayName("Company Name")]
        public virtual string CompanyName { get; set; }
        [Index("IX_Subdomain", 1, IsUnique = true)]
        [StringLength(20)]
        [Required]
        [DisplayName("Web Address")]
        public virtual string Subdomain { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }

      //  public virtual ICollection<ApplicationRole> Roles { get; set; }
        public virtual ICollection<Property> Properties { get; set; }

        public virtual ICollection<SubscriptionTest> Tests { get; set; }

        //包含的部门
        public virtual ICollection<Department> Departs { get; set; }
        
    }

    public partial class SubscriptionTest : SubscriptionEntityBase
    {
        [StringLength(255)]
        [Required]
        public virtual string Name { get; set; }
    }
}
