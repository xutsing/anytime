﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    public class ModuleRolePermissions : SubscriptionEntityBase
    {
        [StringLength(50)]
        //角色ID 
        public string RoleId { get; set; }
        //当前节点是个模块
        public bool IsModule { get; set; }
        //具体的功能点.
        public int PermissionId { get; set; }
    }
}
