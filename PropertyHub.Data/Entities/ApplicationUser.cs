﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    [TenantAware("SubscriptionId")]
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [StringLength(50)]
        [Required]
        [DisplayName("邮箱")]
        [EmailAddress]
        public override string Email
        {
            get
            {
                return base.Email;
            }
            set
            {
                base.Email = value;
            }
        }

        [StringLength(50)]
        [Required]
        [DisplayName("姓")]
        public string FirstName { get; set; }
        [StringLength(50)]
        [Required]
        [DisplayName("名")]
        public string LastName { get; set; }
        public virtual int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
    }
}
