﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    public partial class PropertyNote : PropertyEntityBase
    {
        [Required]
        [StringLength(4000)]
        public virtual string Notes { get; set; }
    }
}
