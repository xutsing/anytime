﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    public partial class File : SubscriptionEntityBase
    {
        [Required]
        [StringLength(255)]
        public virtual string Name { get; set; }
        [Required]
        public virtual long Size { get; set; }
        [Required]
        [StringLength(50)]
        public virtual string DisplaySize { get; set; }
        [Required]
        [StringLength(50)]
        public virtual string Extension { get; set; }
        [Required]
        [StringLength(255)]
        public virtual string Url { get; set; }
    }
}
