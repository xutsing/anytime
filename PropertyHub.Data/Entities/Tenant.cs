﻿using PropertyHub.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data
{
    public partial class Tenant : PropertyEntityBase
    {
        [Required]
        [StringLength(255)]
        public virtual string Name { get; set; }
        [Required]
        [StringLength(50)]
        public virtual string PhoneNumber { get; set; }
        [StringLength(50)]
        public virtual string MobileNumber { get; set; }
        [StringLength(50)]
        public virtual string EmailAddress { get; set; }
        public virtual ICollection<File> Files { get; set; }
        [StringLength(4000)]
        public virtual string Notes { get; set; }
        public virtual bool IsCurrent { get; set; }
        public virtual bool IsPrimary { get; set; }
        public virtual Tenant PrimaryTenant { get; set; }
        public virtual ICollection<Tenant> References { get; set; }

        public Tenant()
        {
            Files = new HashSet<File>();
            References = new HashSet<Tenant>();
        }
    }
}
