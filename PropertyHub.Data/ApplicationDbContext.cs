namespace PropertyHub.Data
{
    using PropertyHub.Data.Maps;
    using PropertyHub.Data.Infrastructure;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    //[DbConfigurationType(typeof(EntityFrameworkConfiguration))]
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("name=PropertyHub", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        #region Db Sets
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<SubscriptionTest> Tests { get; set; }
        public virtual DbSet<Modules> Modules { get; set; }


        public virtual DbSet<Department> Departs { get; set; }

        public virtual DbSet<DepartmentMember> DepartMembers { get; set; }





        public virtual DbSet<ModuleRolePermissions> RolePermissions { get; set; }
        public virtual DbSet<ModulePermission> ModulePermissions { get; set; }

      //  public virtual DbSet<>

       // public virtual DbSet<data_sex> data_sex { get; set; }
        #endregion

        #region Model Creating
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Map
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
               .Where(type => !String.IsNullOrEmpty(type.Namespace))
               .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                    && type.IsAbstract == false
                    && (type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>)
                        || type.BaseType.GetGenericTypeDefinition() == typeof(SubscriptionMapBase<>)));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            #endregion

            base.OnModelCreating(modelBuilder);

            var conv = new AttributeToTableAnnotationConvention<TenantAwareAttribute, string>(
                    TenantAwareAttribute.TenantAnnotation, (type, attributes) => attributes.Single().ColumnName);

            modelBuilder.Conventions.Add(conv);
        }
        #endregion

        #region Audit Entity
        public override int SaveChanges()
        {
            try
            {
                this.AuditEntity();

                return base.SaveChanges();
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public override Task<int> SaveChangesAsync()
        {
            this.AuditEntity();

            return base.SaveChangesAsync();
        }

        public override Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            this.AuditEntity();

            return base.SaveChangesAsync(cancellationToken);
        }

        protected void AuditEntity()
        {
            ObjectContext context = ((IObjectContextAdapter)this).ObjectContext;

            var currentTime = DateTime.Now;

            var identityName = (System.Web.HttpContext.Current == null
                || System.Web.HttpContext.Current.User == null
                || System.Web.HttpContext.Current.User.Identity == null) ? null : System.Web.HttpContext.Current.User.Identity.Name;

            IEnumerable<ObjectStateEntry> objectStateEntries =
                from e in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified)
                where
                    e.IsRelationship == false &&
                    e.Entity is EntityBase
                select e;

            foreach (var entry in objectStateEntries)
            {
                var entityBase = entry.Entity as EntityBase;
                if (entityBase != null)
                {
                    if (entry.State == EntityState.Added)
                    {
                        entityBase.CreatedAt = currentTime;
                        entityBase.CreatedBy = identityName;
                    }

                    entityBase.UpdatedAt = currentTime;
                    entityBase.UpdatedBy = identityName;
                }
            }
        }
        #endregion
    }

    public sealed class ApplicationDbContextConfiguration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public ApplicationDbContextConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            
        }

        protected override void Seed(ApplicationDbContext context)
        {

        }
    }
}