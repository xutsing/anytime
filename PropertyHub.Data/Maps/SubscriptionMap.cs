﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class SubscriptionMap : EntityTypeConfiguration<Subscription>
    {
        public SubscriptionMap()
        {
        }
    }

    public abstract class SubscriptionMapBase<T> : EntityTypeConfiguration<T> where T : SubscriptionEntityBase
    {
        public SubscriptionMapBase()
        {
            HasRequired(t => t.Subscription)
                .WithMany()
                .HasForeignKey(t => t.SubscriptionId)
                .WillCascadeOnDelete();
        }
    }

    public abstract class BaseDataMap<T> : EntityTypeConfiguration<T> where T : EntityOriginal
    {
        public BaseDataMap()
        { 
        }
    }
}
