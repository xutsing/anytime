﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class SubscriptionTestMap : SubscriptionMapBase<SubscriptionTest>
    {
        public SubscriptionTestMap()
        {
            HasRequired(t => t.Subscription)
            .WithMany(s => s.Tests)
            .HasForeignKey(t => t.SubscriptionId)
            .WillCascadeOnDelete();
        }
    }
}
