﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class TenantMap : SubscriptionMapBase<Tenant>
    {
        public TenantMap()
            : base()
        {
            HasRequired(t => t.Property)
                .WithMany(s => s.Tenants)
                .HasForeignKey(t => t.PropertyId)
                .WillCascadeOnDelete(false);

            HasMany(t => t.Files)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("TenantId");
                })
                .WillCascadeOnDelete(false);

            HasOptional(t => t.PrimaryTenant)
                .WithMany(s=>s.References)
                .Map(m =>
                {
                    m.MapKey("PrimaryTenantId");
                })
                .WillCascadeOnDelete(false);
        }
    }
}
