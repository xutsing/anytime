﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class ApplicationUserMap : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserMap()
        {
            HasRequired(t => t.Subscription)
                .WithMany(s => s.Users)
                .HasForeignKey(t => t.SubscriptionId)
                .WillCascadeOnDelete();

           
        }
    }
}
