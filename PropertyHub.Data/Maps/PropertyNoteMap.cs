﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class PropertyNoteMap : SubscriptionMapBase<PropertyNote>
    {
        public PropertyNoteMap()
            : base()
        {
            HasRequired(t => t.Property)
                .WithMany(s => s.Nodes)
                .HasForeignKey(t => t.PropertyId)
                .WillCascadeOnDelete(false);
        }
    }
}
