﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Data.Maps
{
    public class PropertyMap : SubscriptionMapBase<Property>
    {
        public PropertyMap()
        {
            HasRequired(t => t.Subscription)
                .WithMany(s => s.Properties)
                .HasForeignKey(t => t.SubscriptionId)
                .WillCascadeOnDelete();

            HasOptional(t => t.Photo)
                .WithOptionalDependent()
                .Map(m =>
                {
                    m.MapKey("PhotoId");
                })
                .WillCascadeOnDelete(false);

            //HasMany(t => t.Nodes)
            //    .WithRequired(s => s.Property)
            //    .HasForeignKey(s => s.PropertyId)
            //    .WillCascadeOnDelete(false);

            //HasMany(t => t.Tenants)
            //     .WithRequired(s => s.Property)
            //     .HasForeignKey(s => s.PropertyId)
            //     .WillCascadeOnDelete(false);
        }
    }
}
