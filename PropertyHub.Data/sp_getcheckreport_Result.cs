//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PropertyHub.Data
{
    using System;
    
    public partial class sp_getcheckreport_Result
    {
        public string data_member_value { get; set; }
        public Nullable<int> data_base_index_id { get; set; }
        public Nullable<int> data_body_label_id { get; set; }
        public Nullable<int> data_sex_id { get; set; }
        public Nullable<int> data_age_range_id { get; set; }
        public Nullable<decimal> data_index_data_min { get; set; }
        public Nullable<decimal> data_index_data_max { get; set; }
        public string data_sex_title { get; set; }
        public string data_base_index_title { get; set; }
        public string data_base_index_title_en { get; set; }
        public string data_base_key { get; set; }
        public string data_base_index_unit { get; set; }
        public Nullable<int> data_age_min { get; set; }
        public Nullable<int> data_age_max { get; set; }
        public string data_age_title { get; set; }
        public string data_body_label_text { get; set; }
    }
}
