//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PropertyHub.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Module
    {
        public int Id { get; set; }
        public string ModuleName { get; set; }
        public string ModuleIcon { get; set; }
        public string ModuleUrl { get; set; }
        public Nullable<int> ParentId { get; set; }
        public int SubscriptionId { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public Nullable<int> Modules_Id { get; set; }
        public int RecordStatus { get; set; }
    
        public virtual Subscription Subscription { get; set; }
    }
}
