﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace PropertyHub.Data
{
    public class PermissionNode
    {

        public PermissionNode()
        {
            this.Children = new List<PermissionNode>();//Children
            this.permissiontype = PermissionType.Module;//默认情况下是 模块
            this.expanded = true;
            this.Checked = true;
        }
        /// <summary>
        /// 这个就是ModuleId 或者permission Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 节点显示名称
        /// </summary>
        public string  Text { get; set; }

        public int IsChecked { get; set; }

        public PermissionType permissiontype { get; set; }

        public List<PermissionNode> Children { get; set; }

        public bool expanded { get; set; }

        [JsonProperty(PropertyName = "checked")]
        public bool Checked { get; set; }
        

    }

    public enum PermissionType
    {
        Module,
        Permission,
        Temp
    }
}
