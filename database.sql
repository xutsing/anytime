/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     7/17/2015 8:44:18 AM                         */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_basetask') and o.name = 'FK_DATA_BAS_REFERENCE_DATA_BAS')
alter table data_basetask
   drop constraint FK_DATA_BAS_REFERENCE_DATA_BAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code') and o.name = 'FK_DATA_COD_REFERENCE_DATA_MEM_11')
alter table data_code
   drop constraint FK_DATA_COD_REFERENCE_DATA_MEM_11
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code') and o.name = 'FK_DATA_COD_REFERENCE_DATA_COD_03')
alter table data_code
   drop constraint FK_DATA_COD_REFERENCE_DATA_COD_03
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_record') and o.name = 'FK_DATA_COD_REFERENCE_DATA_SER_30')
alter table data_code_record
   drop constraint FK_DATA_COD_REFERENCE_DATA_SER_30
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_record') and o.name = 'FK_DATA_COD_REFERENCE_DATA_SUP')
alter table data_code_record
   drop constraint FK_DATA_COD_REFERENCE_DATA_SUP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_services') and o.name = 'FK_DATA_COD_REFERENCE_DATA_MEM_19')
alter table data_code_services
   drop constraint FK_DATA_COD_REFERENCE_DATA_MEM_19
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_services') and o.name = 'FK_DATA_COD_REFERENCE_DATA_COD_04')
alter table data_code_services
   drop constraint FK_DATA_COD_REFERENCE_DATA_COD_04
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_services') and o.name = 'FK_DATA_COD_REFERENCE_DATA_SER_35')
alter table data_code_services
   drop constraint FK_DATA_COD_REFERENCE_DATA_SER_35
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_code_services_tasks') and o.name = 'FK_DATA_COD_REFERENCE_DATA_COD_05')
alter table data_code_services_tasks
   drop constraint FK_DATA_COD_REFERENCE_DATA_COD_05
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_comments') and o.name = 'FK_DATA_COM_REFERENCE_DATA_MEM')
alter table data_comments
   drop constraint FK_DATA_COM_REFERENCE_DATA_MEM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_comments') and o.name = 'FK_DATA_COM_REFERENCE_DATA_KNO')
alter table data_comments
   drop constraint FK_DATA_COM_REFERENCE_DATA_KNO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_index_data') and o.name = 'FK_DATA_IND_REFERENCE_DATA_AGE_2000')
alter table data_index_data
   drop constraint FK_DATA_IND_REFERENCE_DATA_AGE_2000
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_index_data') and o.name = 'FK_DATA_IND_REFERENCE_DATA_BAS')
alter table data_index_data
   drop constraint FK_DATA_IND_REFERENCE_DATA_BAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_index_data') and o.name = 'FK_DATA_IND_REFERENCE_DATA_SEX_210000')
alter table data_index_data
   drop constraint FK_DATA_IND_REFERENCE_DATA_SEX_210000
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_index_data') and o.name = 'FK_DATA_IND_REFERENCE_DATA_BOD')
alter table data_index_data
   drop constraint FK_DATA_IND_REFERENCE_DATA_BOD
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_machine') and o.name = 'FK_DATA_MAC_REFERENCE_DATA_MAC_08')
alter table data_machine
   drop constraint FK_DATA_MAC_REFERENCE_DATA_MAC_08
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_machine_action') and o.name = 'FK_DATA_MAC_REFERENCE_DATA_MAC_07')
alter table data_machine_action
   drop constraint FK_DATA_MAC_REFERENCE_DATA_MAC_07
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_02')
alter table data_member
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_02
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_checkdata') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_14')
alter table data_member_checkdata
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_14
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_checkdata') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_BAS')
alter table data_member_checkdata
   drop constraint FK_DATA_MEM_REFERENCE_DATA_BAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_checkrecord') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_13')
alter table data_member_checkrecord
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_13
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_device') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_01')
alter table data_member_device
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_01
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_device') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_SUP')
alter table data_member_device
   drop constraint FK_DATA_MEM_REFERENCE_DATA_SUP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_prescription') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_09')
alter table data_member_prescription
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_09
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_prescription_detail') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_10')
alter table data_member_prescription_detail
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_10
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_prescription_detail') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MAC')
alter table data_member_prescription_detail
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MAC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_secretary') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_06')
alter table data_member_secretary
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_06
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_secretary') and o.name = 'FK_DATA_MEM_REFERENCE_SYS_USER')
alter table data_member_secretary
   drop constraint FK_DATA_MEM_REFERENCE_SYS_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_testing') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_MEM_18')
alter table data_member_testing
   drop constraint FK_DATA_MEM_REFERENCE_DATA_MEM_18
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_member_testing') and o.name = 'FK_DATA_MEM_REFERENCE_DATA_QUE')
alter table data_member_testing
   drop constraint FK_DATA_MEM_REFERENCE_DATA_QUE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_physical_files') and o.name = 'FK_DATA_PHY_REFERENCE_DATA_KNO')
alter table data_physical_files
   drop constraint FK_DATA_PHY_REFERENCE_DATA_KNO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_questions_testing_detail') and o.name = 'FK_DATA_QUE_REFERENCE_DATA_QUE_15')
alter table data_questions_testing_detail
   drop constraint FK_DATA_QUE_REFERENCE_DATA_QUE_15
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_questions_testing_detail') and o.name = 'FK_DATA_QUE_REFERENCE_DATA_QUE_17')
alter table data_questions_testing_detail
   drop constraint FK_DATA_QUE_REFERENCE_DATA_QUE_17
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_service_comments') and o.name = 'FK_DATA_SER_REFERENCE_DATA_SER_60')
alter table data_service_comments
   drop constraint FK_DATA_SER_REFERENCE_DATA_SER_60
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_service_comments') and o.name = 'FK_DATA_SER_REFERENCE_DATA_MEM')
alter table data_service_comments
   drop constraint FK_DATA_SER_REFERENCE_DATA_MEM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_service_tasks') and o.name = 'FK_DATA_SER_REFERENCE_DATA_BAS')
alter table data_service_tasks
   drop constraint FK_DATA_SER_REFERENCE_DATA_BAS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_service_tasks') and o.name = 'FK_DATA_SER_REFERENCE_DATA_SER_50')
alter table data_service_tasks
   drop constraint FK_DATA_SER_REFERENCE_DATA_SER_50
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('data_supplier_device') and o.name = 'FK_DATA_SUP_REFERENCE_DATA_SUP')
alter table data_supplier_device
   drop constraint FK_DATA_SUP_REFERENCE_DATA_SUP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('basetable')
            and   type = 'U')
   drop table basetable
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_age_range')
            and   type = 'U')
   drop table data_age_range
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_base_index')
            and   type = 'U')
   drop table data_base_index
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_basetask')
            and   type = 'U')
   drop table data_basetask
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_basetask_type')
            and   type = 'U')
   drop table data_basetask_type
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_body_label')
            and   type = 'U')
   drop table data_body_label
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_code')
            and   type = 'U')
   drop table data_code
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_code_record')
            and   type = 'U')
   drop table data_code_record
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_code_services')
            and   type = 'U')
   drop table data_code_services
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_code_services_tasks')
            and   type = 'U')
   drop table data_code_services_tasks
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_comments')
            and   type = 'U')
   drop table data_comments
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_dictionary')
            and   type = 'U')
   drop table data_dictionary
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_index_data')
            and   type = 'U')
   drop table data_index_data
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_knowledge')
            and   type = 'U')
   drop table data_knowledge
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_machine')
            and   type = 'U')
   drop table data_machine
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_machine_action')
            and   type = 'U')
   drop table data_machine_action
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_machine_type')
            and   type = 'U')
   drop table data_machine_type
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member')
            and   type = 'U')
   drop table data_member
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_checkdata')
            and   type = 'U')
   drop table data_member_checkdata
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_checkrecord')
            and   type = 'U')
   drop table data_member_checkrecord
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_device')
            and   type = 'U')
   drop table data_member_device
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_prescription')
            and   type = 'U')
   drop table data_member_prescription
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_prescription_detail')
            and   type = 'U')
   drop table data_member_prescription_detail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_secretary')
            and   type = 'U')
   drop table data_member_secretary
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_testing')
            and   type = 'U')
   drop table data_member_testing
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_member_type')
            and   type = 'U')
   drop table data_member_type
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_message')
            and   type = 'U')
   drop table data_message
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_physical_files')
            and   type = 'U')
   drop table data_physical_files
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_questions')
            and   type = 'U')
   drop table data_questions
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_questions_testing')
            and   type = 'U')
   drop table data_questions_testing
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_questions_testing_detail')
            and   type = 'U')
   drop table data_questions_testing_detail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_service')
            and   type = 'U')
   drop table data_service
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_service_comments')
            and   type = 'U')
   drop table data_service_comments
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_service_tasks')
            and   type = 'U')
   drop table data_service_tasks
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_sex')
            and   type = 'U')
   drop table data_sex
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_supplier')
            and   type = 'U')
   drop table data_supplier
go

if exists (select 1
            from  sysobjects
           where  id = object_id('data_supplier_device')
            and   type = 'U')
   drop table data_supplier_device
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sys_user')
            and   type = 'U')
   drop table sys_user
go

/*==============================================================*/
/* Table: basetable                                             */
/*==============================================================*/
create table basetable (
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null
)
go

/*==============================================================*/
/* Table: data_age_range                                        */
/*==============================================================*/
create table data_age_range (
   data_age_range_id    int                  identity,
   data_age_min         int                  null,
   data_age_max         int                  null,
   data_age_title       nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_age_all         bit                  null,
   constraint PK_DATA_AGE_RANGE primary key (data_age_range_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '0-100
   
   如果是0-100的意思就是不依靠年龄.',
   'user', @CurrentUser, 'table', 'data_age_range'
go

/*==============================================================*/
/* Table: data_base_index                                       */
/*==============================================================*/
create table data_base_index (
   data_base_index_id   int                  identity,
   data_base_index_title nvarchar(50)         null,
   data_base_index_unit nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_BASE_INDEX primary key (data_base_index_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '各种分析指标
   例如，体重，单位
   血压。。',
   'user', @CurrentUser, 'table', 'data_base_index'
go

/*==============================================================*/
/* Table: data_basetask                                         */
/*==============================================================*/
create table data_basetask (
   data_basetask_id     int                  identity,
   data_basetask_type_id int                  null,
   data_basetask_name   nvarchar(50)         null,
   data_basetask_remark nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_status          int                  null,
   constraint PK_DATA_BASETASK primary key (data_basetask_id)
)
go

/*==============================================================*/
/* Table: data_basetask_type                                    */
/*==============================================================*/
create table data_basetask_type (
   data_basetask_type_id int                  identity,
   data_basetask_type_title nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_status          int                  null,
   constraint PK_DATA_BASETASK_TYPE primary key (data_basetask_type_id)
)
go

/*==============================================================*/
/* Table: data_body_label                                       */
/*==============================================================*/
create table data_body_label (
   data_body_label_id   int                  identity,
   data_body_label_text nvarchar(50)         null,
   constraint PK_DATA_BODY_LABEL primary key (data_body_label_id)
)
go

/*==============================================================*/
/* Table: data_code                                             */
/*==============================================================*/
create table data_code (
   data_code_id         int                  identity,
   data_code_record_id  int                  null,
   data_member_id       int                  null,
   data_code_status     int                  null,
   data_code_text       nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_CODE primary key (data_code_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '那个会员买的？',
   'user', @CurrentUser, 'table', 'data_code'
go

/*==============================================================*/
/* Table: data_code_record                                      */
/*==============================================================*/
create table data_code_record (
   data_code_record_id  int                  not null,
   data_supplier_id     int                  null,
   data_service_id      int                  null,
   data_code_record_type int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_code_record_isPay bit                  null,
   data_code_record_money money                null,
   data_code_record_Paytype int                  null,
   constraint PK_DATA_CODE_RECORD primary key (data_code_record_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '激活码生成记录
   
   全时是默认的供应商
   
   销售记录',
   'user', @CurrentUser, 'table', 'data_code_record'
go

/*==============================================================*/
/* Table: data_code_services                                    */
/*==============================================================*/
create table data_code_services (
   data_code_services_id int                  identity,
   data_code_id         int                  null,
   data_service_id      int                  null,
   data_member_id       int                  null,
   data_code_services_date datetime             null,
   data_code_services_status int                  null,
   data_code_services_remark nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   dateStarted          datetime             null,
   dateEnd              datetime             null,
   dateActualStarted    datetime             null,
   dateActualEnd        datetime             null,
   constraint PK_DATA_CODE_SERVICES primary key (data_code_services_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '激活记录
   激活码对应着一个服务，服务被激活   有可能被不同的会员所激活',
   'user', @CurrentUser, 'table', 'data_code_services'
go

/*==============================================================*/
/* Table: data_code_services_tasks                              */
/*==============================================================*/
create table data_code_services_tasks (
   data_code_services_tasks_id int                  identity,
   data_code_services_id int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_code_service_date date                 null,
   data_code_service_remark nvarchar(500)        null,
   constraint PK_DATA_CODE_SERVICES_TASKS primary key (data_code_services_tasks_id)
)
go

/*==============================================================*/
/* Table: data_comments                                         */
/*==============================================================*/
create table data_comments (
   data_comments_comments_id int                  identity,
   data_member_id       int                  null,
   data_knowledge_id    int                  null,
   data_comments_remark nvarchar(200)        null,
   data_comments_date   datetime             null,
   constraint PK_DATA_COMMENTS primary key (data_comments_comments_id)
)
go

/*==============================================================*/
/* Table: data_dictionary                                       */
/*==============================================================*/
create table data_dictionary (
   data_dictionary_id   int                  identity,
   data_dictionary_enum int                  null,
   data_dictionary_text nvarchar(50)         null,
   data_dictionary_value nvarchar(50)         null
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '数据字典',
   'user', @CurrentUser, 'table', 'data_dictionary'
go

/*==============================================================*/
/* Table: data_index_data                                       */
/*==============================================================*/
create table data_index_data (
   data_base_index_id   int                  null,
   data_body_label_id   int                  null,
   data_sex_id          int                  null,
   data_age_range_id    int                  null,
   data_index_data_id   int                  identity,
   data_index_data_min  decimal(18,2)        null,
   data_index_data_max  decimal(18,2)        null,
   data_index_data_point int                  null,
   data_index_data_label nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null
)
go

/*==============================================================*/
/* Table: data_knowledge                                        */
/*==============================================================*/
create table data_knowledge (
   data_knowledge_id    int                  identity,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_knowledge_title nvarchar(50)         null,
   data_knowledge_summary nvarchar(500)        null,
   data_knowledge_remark nvarchar(500)        null,
   data_knowledge_path  nvarchar(500)        null,
   data_knowledge_author_type int                  null,
   data_knowledge_author_id int                  null,
   data_knowledge_status int                  null,
   constraint PK_DATA_KNOWLEDGE primary key (data_knowledge_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '知识库',
   'user', @CurrentUser, 'table', 'data_knowledge'
go

/*==============================================================*/
/* Table: data_machine                                          */
/*==============================================================*/
create table data_machine (
   data_machine_id      int                  not null,
   data_machine_type_id int                  null,
   data_machine_name    char(10)             null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MACHINE primary key (data_machine_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '健身器材',
   'user', @CurrentUser, 'table', 'data_machine'
go

/*==============================================================*/
/* Table: data_machine_action                                   */
/*==============================================================*/
create table data_machine_action (
   data_machine_action_id int                  identity,
   data_machine_id      int                  null,
   data_machine_action_title nvarchar(50)         null,
   data_machine_action_path nvarchar(500)        null,
   data_machine_action_remark nvarchar(500)        null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MACHINE_ACTION primary key (data_machine_action_id)
)
go

/*==============================================================*/
/* Table: data_machine_type                                     */
/*==============================================================*/
create table data_machine_type (
   data_machine_type_id int                  identity,
   data_machine_type_title nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MACHINE_TYPE primary key (data_machine_type_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '设备类别',
   'user', @CurrentUser, 'table', 'data_machine_type'
go

/*==============================================================*/
/* Table: data_member                                           */
/*==============================================================*/
create table data_member (
   data_member_id       int                  identity,
   data_member_type_id  int                  null,
   data_member_name     nvarchar(50)         null,
   data_member_loginname nvarchar(50)         null,
   data_member_pw       nvarchar(50)         null,
   data_member_mobile   nvarchar(50)         null,
   data_member_sex      nvarchar(50)         null,
   data_member_birthday date                 null,
   data_member_group    nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_member_status   int                  null,
   data_member_email    nvarchar(50)         null,
   constraint PK_DATA_MEMBER primary key (data_member_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '会员表',
   'user', @CurrentUser, 'table', 'data_member'
go

/*==============================================================*/
/* Table: data_member_checkdata                                 */
/*==============================================================*/
create table data_member_checkdata (
   data_member_checkrecord_id int                  null,
   data_member_checkdata_id int                  identity,
   data_base_index_id   int                  null,
   data_member_value    decimal(18,2)        null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MEMBER_CHECKDATA primary key (data_member_checkdata_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '会员的检测数据',
   'user', @CurrentUser, 'table', 'data_member_checkdata'
go

/*==============================================================*/
/* Table: data_member_checkrecord                               */
/*==============================================================*/
create table data_member_checkrecord (
   data_member_id       int                  null,
   data_member_checkrecord_id int                  identity,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_member_checkrecord_remark nvarchar(500)        null,
   constraint PK_DATA_MEMBER_CHECKRECORD primary key (data_member_checkrecord_id)
)
go

/*==============================================================*/
/* Table: data_member_device                                    */
/*==============================================================*/
create table data_member_device (
   data_member_device_id int                  not null,
   data_member_id       int                  null,
   data_supplier_device_id int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_supplier_device_price money                null,
   data_supplier_device_remark nvarchar(50)         null,
   constraint PK_DATA_MEMBER_DEVICE primary key (data_member_device_id)
)
go

/*==============================================================*/
/* Table: data_member_prescription                              */
/*==============================================================*/
create table data_member_prescription (
   data_member_prescription_id int                  identity,
   data_member_id       int                  null,
   data_member_prescription_title nvarchar(50)         null,
   data_member_prescription_path nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MEMBER_PRESCRIPTION primary key (data_member_prescription_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '会员处方',
   'user', @CurrentUser, 'table', 'data_member_prescription'
go

/*==============================================================*/
/* Table: data_member_prescription_detail                       */
/*==============================================================*/
create table data_member_prescription_detail (
   data_member_prescription_detail_id int                  identity,
   data_member_prescription_id int                  null,
   data_machine_action_id int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_MEMBER_PRESCRIPTION_DE primary key (data_member_prescription_detail_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '处方详细',
   'user', @CurrentUser, 'table', 'data_member_prescription_detail'
go

/*==============================================================*/
/* Table: data_member_secretary                                 */
/*==============================================================*/
create table data_member_secretary (
   data_member_secretary_id int                  not null,
   data_member_id       int                  null,
   sys_user_id          int                  null,
   constraint PK_DATA_MEMBER_SECRETARY primary key (data_member_secretary_id)
)
go

/*==============================================================*/
/* Table: data_member_testing                                   */
/*==============================================================*/
create table data_member_testing (
   data_member_testing_id int                  identity,
   data_member_id       int                  null,
   data_questions_testing_detail_id int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_questions_testing_result bit                  null,
   data_questions_testing_remark nvarchar(50)         null,
   constraint PK_DATA_MEMBER_TESTING primary key (data_member_testing_id)
)
go

/*==============================================================*/
/* Table: data_member_type                                      */
/*==============================================================*/
create table data_member_type (
   data_member_type_id  int                  not null,
   data_member_type_name char(10)             null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   constraint PK_DATA_MEMBER_TYPE primary key (data_member_type_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '会员分类',
   'user', @CurrentUser, 'table', 'data_member_type'
go

/*==============================================================*/
/* Table: data_message                                          */
/*==============================================================*/
create table data_message (
   data_message_id      int                  not null,
   message_from_type    int                  null,
   message_from_id      int                  null,
   message_to_type      int                  null,
   message_to_id        int                  null,
   message_text         nvarchar(200)        null,
   message_isread       bit                  null,
   constraint PK_DATA_MESSAGE primary key (data_message_id)
)
go

/*==============================================================*/
/* Table: data_physical_files                                   */
/*==============================================================*/
create table data_physical_files (
   data_physical_files_id int                  identity,
   data_knowledge_id    int                  null,
   data_physical_files_title nvarchar(50)         null,
   data_physical_files_path nvarchar(200)        null,
   data_physical_files_status int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_PHYSICAL_FILES primary key (data_physical_files_id)
)
go

/*==============================================================*/
/* Table: data_questions                                        */
/*==============================================================*/
create table data_questions (
   data_questions_id    int                  identity,
   data_questions_text  nvarchar(50)         null,
   data_questions_answer_1 int                  null,
   data_questions_answer_1_point int                  null,
   data_questions_answer_0 int                  null,
   data_questions_answer_0_point int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_QUESTIONS primary key (data_questions_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '调查问卷，问题数据库',
   'user', @CurrentUser, 'table', 'data_questions'
go

/*==============================================================*/
/* Table: data_questions_testing                                */
/*==============================================================*/
create table data_questions_testing (
   data_questions_testing_id int                  identity,
   data_questions_testing_title nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_QUESTIONS_TESTING primary key (data_questions_testing_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '调查问卷',
   'user', @CurrentUser, 'table', 'data_questions_testing'
go

/*==============================================================*/
/* Table: data_questions_testing_detail                         */
/*==============================================================*/
create table data_questions_testing_detail (
   data_questions_testing_detail_id int                  identity,
   data_questions_testing_id int                  null,
   data_questions_id    int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_QUESTIONS_TESTING_DETA primary key (data_questions_testing_detail_id)
)
go

/*==============================================================*/
/* Table: data_service                                          */
/*==============================================================*/
create table data_service (
   data_service_id      int                  identity,
   data_service_name    nvarchar(50)         null,
   data_service_validate_months int                  null,
   data_service_price   money                null,
   data_service_status  int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_SERVICE primary key (data_service_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '服务',
   'user', @CurrentUser, 'table', 'data_service'
go

/*==============================================================*/
/* Table: data_service_comments                                 */
/*==============================================================*/
create table data_service_comments (
   data_service_comments_id int                  not null,
   data_service_id      int                  null,
   data_member_id       int                  null,
   data_service_comments_remark nvarchar(20)         null,
   data_service_comments_date datetime             null,
   constraint PK_DATA_SERVICE_COMMENTS primary key (data_service_comments_id)
)
go

/*==============================================================*/
/* Table: data_service_tasks                                    */
/*==============================================================*/
create table data_service_tasks (
   data_service_tasks_id int                  identity,
   data_service_id      int                  null,
   data_basetask_id     int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_nexttask_interval int                  null,
   constraint PK_DATA_SERVICE_TASKS primary key (data_service_tasks_id)
)
go

/*==============================================================*/
/* Table: data_sex                                              */
/*==============================================================*/
create table data_sex (
   data_sex_id          int                  identity,
   data_sex_title       nvarchar(50)         null,
   data_sex_index       int                  null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_SEX primary key (data_sex_id)
)
go

/*==============================================================*/
/* Table: data_supplier                                         */
/*==============================================================*/
create table data_supplier (
   data_supplier_id     int                  identity,
   data_supplier_name   nvarchar(50)         null,
   data_supplier_address nvarchar(50)         null,
   data_supplier_phone  nvarchar(50)         null,
   data_suplier_contracts nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   data_record_status   int                  null,
   constraint PK_DATA_SUPPLIER primary key (data_supplier_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '厂商供应商
   
   全时公司是默认的供应商',
   'user', @CurrentUser, 'table', 'data_supplier'
go

/*==============================================================*/
/* Table: data_supplier_device                                  */
/*==============================================================*/
create table data_supplier_device (
   data_supplier_device_id int                  identity,
   data_supplier_id     int                  null,
   data_suplier_device_name nvarchar(50)         null,
   data_suplier_device_remark nvarchar(50)         null,
   datecreated          datetime             null,
   createdby            int                  null,
   dateupdated          datetime             null,
   updatedby            int                  null,
   dateremoved          datetime             null,
   removedby            int                  null,
   constraint PK_DATA_SUPPLIER_DEVICE primary key (data_supplier_device_id)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '供应商所提供的设备',
   'user', @CurrentUser, 'table', 'data_supplier_device'
go

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user (
   sys_user_id          int                  not null,
   constraint PK_SYS_USER primary key (sys_user_id)
)
go

alter table data_basetask
   add constraint FK_DATA_BAS_REFERENCE_DATA_BAS foreign key (data_basetask_type_id)
      references data_basetask_type (data_basetask_type_id)
go

alter table data_code
   add constraint FK_DATA_COD_REFERENCE_DATA_MEM_11 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_code
   add constraint FK_DATA_COD_REFERENCE_DATA_COD_03 foreign key (data_code_record_id)
      references data_code_record (data_code_record_id)
go

alter table data_code_record
   add constraint FK_DATA_COD_REFERENCE_DATA_SER_30 foreign key (data_service_id)
      references data_service (data_service_id)
go

alter table data_code_record
   add constraint FK_DATA_COD_REFERENCE_DATA_SUP foreign key (data_supplier_id)
      references data_supplier (data_supplier_id)
go

alter table data_code_services
   add constraint FK_DATA_COD_REFERENCE_DATA_MEM_19 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_code_services
   add constraint FK_DATA_COD_REFERENCE_DATA_COD_04 foreign key (data_code_id)
      references data_code (data_code_id)
go

alter table data_code_services
   add constraint FK_DATA_COD_REFERENCE_DATA_SER_35 foreign key (data_service_id)
      references data_service (data_service_id)
go

alter table data_code_services_tasks
   add constraint FK_DATA_COD_REFERENCE_DATA_COD_05 foreign key (data_code_services_id)
      references data_code_services (data_code_services_id)
go

alter table data_comments
   add constraint FK_DATA_COM_REFERENCE_DATA_MEM foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_comments
   add constraint FK_DATA_COM_REFERENCE_DATA_KNO foreign key (data_knowledge_id)
      references data_knowledge (data_knowledge_id)
go

alter table data_index_data
   add constraint FK_DATA_IND_REFERENCE_DATA_AGE_2000 foreign key (data_age_range_id)
      references data_age_range (data_age_range_id)
go

alter table data_index_data
   add constraint FK_DATA_IND_REFERENCE_DATA_BAS foreign key (data_base_index_id)
      references data_base_index (data_base_index_id)
go

alter table data_index_data
   add constraint FK_DATA_IND_REFERENCE_DATA_SEX_210000 foreign key (data_sex_id)
      references data_sex (data_sex_id)
go

alter table data_index_data
   add constraint FK_DATA_IND_REFERENCE_DATA_BOD foreign key (data_body_label_id)
      references data_body_label (data_body_label_id)
go

alter table data_machine
   add constraint FK_DATA_MAC_REFERENCE_DATA_MAC_08 foreign key (data_machine_type_id)
      references data_machine_type (data_machine_type_id)
go

alter table data_machine_action
   add constraint FK_DATA_MAC_REFERENCE_DATA_MAC_07 foreign key (data_machine_id)
      references data_machine (data_machine_id)
go

alter table data_member
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_02 foreign key (data_member_type_id)
      references data_member_type (data_member_type_id)
go

alter table data_member_checkdata
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_14 foreign key (data_member_checkrecord_id)
      references data_member_checkrecord (data_member_checkrecord_id)
go

alter table data_member_checkdata
   add constraint FK_DATA_MEM_REFERENCE_DATA_BAS foreign key (data_base_index_id)
      references data_base_index (data_base_index_id)
go

alter table data_member_checkrecord
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_13 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_member_device
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_01 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_member_device
   add constraint FK_DATA_MEM_REFERENCE_DATA_SUP foreign key (data_supplier_device_id)
      references data_supplier_device (data_supplier_device_id)
go

alter table data_member_prescription
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_09 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_member_prescription_detail
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_10 foreign key (data_member_prescription_id)
      references data_member_prescription (data_member_prescription_id)
go

alter table data_member_prescription_detail
   add constraint FK_DATA_MEM_REFERENCE_DATA_MAC foreign key (data_machine_action_id)
      references data_machine_action (data_machine_action_id)
go

alter table data_member_secretary
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_06 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_member_secretary
   add constraint FK_DATA_MEM_REFERENCE_SYS_USER foreign key (sys_user_id)
      references sys_user (sys_user_id)
go

alter table data_member_testing
   add constraint FK_DATA_MEM_REFERENCE_DATA_MEM_18 foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_member_testing
   add constraint FK_DATA_MEM_REFERENCE_DATA_QUE foreign key (data_questions_testing_detail_id)
      references data_questions_testing_detail (data_questions_testing_detail_id)
go

alter table data_physical_files
   add constraint FK_DATA_PHY_REFERENCE_DATA_KNO foreign key (data_knowledge_id)
      references data_knowledge (data_knowledge_id)
go

alter table data_questions_testing_detail
   add constraint FK_DATA_QUE_REFERENCE_DATA_QUE_15 foreign key (data_questions_testing_id)
      references data_questions_testing (data_questions_testing_id)
go

alter table data_questions_testing_detail
   add constraint FK_DATA_QUE_REFERENCE_DATA_QUE_17 foreign key (data_questions_id)
      references data_questions (data_questions_id)
go

alter table data_service_comments
   add constraint FK_DATA_SER_REFERENCE_DATA_SER_60 foreign key (data_service_id)
      references data_service (data_service_id)
go

alter table data_service_comments
   add constraint FK_DATA_SER_REFERENCE_DATA_MEM foreign key (data_member_id)
      references data_member (data_member_id)
go

alter table data_service_tasks
   add constraint FK_DATA_SER_REFERENCE_DATA_BAS foreign key (data_basetask_id)
      references data_basetask (data_basetask_id)
go

alter table data_service_tasks
   add constraint FK_DATA_SER_REFERENCE_DATA_SER_50 foreign key (data_service_id)
      references data_service (data_service_id)
go

alter table data_supplier_device
   add constraint FK_DATA_SUP_REFERENCE_DATA_SUP foreign key (data_supplier_id)
      references data_supplier (data_supplier_id)
go

