﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    /// <summary>
    /// created by adam 2015-08-10 the service of member prescription module
    /// database table: data_member_prescription 
    /// associate tables: data_member_prescription_detail, data_member, data_member_prescription_action, data_dictionary
    /// </summary>
    public class PrescriptionService : BusinessService
    {
        public IQueryable<data_member_prescription> ListPrescriptions()
        {
            return from item in DataContext.data_member_prescription
                   select item;
        }

        public async Task<data_member_prescription> Add(data_member_prescription entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_member_prescription.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_member_prescription> Update(data_member_prescription entity)
        {
            var service = DataContext.data_member_prescription.FirstOrDefault(x => x.data_member_prescription_id == entity.data_member_prescription_id);

            DataContext.data_member_prescription_detail.RemoveRange(service.data_member_prescription_detail);

            if (service != null)
            {
                service.data_member_prescription_title = entity.data_member_prescription_title;   
                service.data_member_prescription_status = entity.data_member_prescription_status;
                service.data_member_id = entity.data_member_id;
                service.data_member_prescription_detail = entity.data_member_prescription_detail;

                service.dateupdated = entity.dateupdated;
                service.updatedby_id = entity.updatedby_id;
                
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_member_prescription Get(int id)
        {
            return DataContext.data_member_prescription.SingleOrDefault(x => x.data_member_prescription_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_member_prescription.SingleOrDefaultAsync(x => x.data_member_prescription_id == id);
            
            DataContext.data_member_prescription_detail.RemoveRange(entity.data_member_prescription_detail);
            DataContext.data_member_prescription.Remove(entity);

            await DataContext.SaveChangesAsync();
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_prescription"
                   select new SelectListItem() {Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListMachineActions()
        {
            return from item in DataContext.data_machine_action
                   from status in DataContext.data_dictionary 
                   where item.data_machine_action_status == status.data_dictionary_id
                        && status.data_dictionary_text == "可用"
                   select new SelectListItem() { Text = item.data_machine_action_title, Value = item.data_machine_action_id.ToString() };
        }
    }
}
