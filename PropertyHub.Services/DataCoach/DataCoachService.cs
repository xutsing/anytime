﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using PropertyHub.Data;


namespace PropertyHub.Services
{
    public class DataCoachService : BusinessService
    {
        public View_Coach Get(int id)
        {
            return DataContext.View_Coach.FirstOrDefault(a => a.data_anytime_friend_id == id);
           
        }

        public IQueryable<View_Coach> ListDataCoach()
        {
            return from item in DataContext.View_Coach
                   select item;
        }
    }
}
