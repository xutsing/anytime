﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services
{
    public class Result<T>
    {
        public Result()
        {
            this.ErrorType = ExceptionType.Business;
       
        }

        public T Parameter { get; set; }
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }

        public ExceptionType ErrorType;

        public long recordcount { get; set; }

        public long pagesize { get; set; }

        public long pagenumber { get; set; }
    }

    public class Result
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }


    public enum ExceptionType
    {
        Service,
        DataBase,
        Business,
        Unkown,
        Cancel,
        Background

    }


}
