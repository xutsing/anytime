﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public interface IValidationDictionaryFactory
    {
        void AddError(string key, string errorMessage);
        void AddError(string key, Exception exception);
        bool IsValid { get; }
    }

    public class MvcModelStateValidationDictionaryFactory : IValidationDictionaryFactory
    {
        private ModelStateDictionary _modelState = null;
        public virtual System.Web.Mvc.ModelStateDictionary ModelState
        {
            get
            {
                if (_modelState == null)
                {
                    _modelState = System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values["ValidationModelState"] as ModelStateDictionary;
                }
                return _modelState;
            }
        }

        public virtual void AddError(string key, string errorMessage)
        {
            ModelState.AddModelError(key, errorMessage);
        }

        public virtual void AddError(string key, Exception exception)
        {
            ModelState.AddModelError(key, exception);
        }

        public virtual bool IsValid
        {
            get { return ModelState.IsValid; }
        }
    }
}
