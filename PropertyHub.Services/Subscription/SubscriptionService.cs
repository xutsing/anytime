﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PropertyHub.Services
{
    public partial class SubscriptionService : ServiceBase
    {
        public async Task<Subscription> Add(Subscription entity)
        {
            if (Validate(entity))
            {
                DataContext.Subscriptions.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public bool Validate(Subscription entity)
        {
            if (string.IsNullOrWhiteSpace(entity.CompanyName))
            {
                Validator.AddError("CompanyName", "The field is required.");
            }
            if (string.IsNullOrWhiteSpace(entity.Subdomain))
            {
                Validator.AddError("Subdomain", "The field is required.");
            }

            return Validator.IsValid;
        }

        #region Modules
        public IQueryable<Modules> GetAllModules()
        {
            var results= (
                from item in DataContext.Modules
                where item.ParentId.HasValue==false
                       select item);

            foreach (var result in results)
            {
                result.Children = new List<Modules>();

                

                var children = (from item in DataContext.Modules
                                where item.ParentId.HasValue  && item.ParentId.Value == result.Id
                                select item
                                  );

                foreach (var child in children)
                {
                    result.Children.Add(child);
                }
            }

            return results;
          
        }

        //added by adam 2015-08-15 filter module list by roleid
        public IQueryable<Modules> GetMenuModulesByRole(string roleIDs)
        {
            var results = (
                from item in DataContext.Modules
                from roleM in DataContext.RolePermissions
                where item.ParentId.HasValue == false && roleM.PermissionId == item.Id && roleIDs.Contains("," + roleM.RoleId + ",")
                select item).Distinct();

            foreach (var result in results)
            {
                result.Children = new List<Modules>();

                var children = (from item in DataContext.Modules
                                from roleM in DataContext.RolePermissions
                                where item.ParentId.HasValue && item.ParentId.Value == result.Id && roleM.PermissionId == item.Id && roleIDs.Contains("," + roleM.RoleId + ",")
                                select item
                                  ).Distinct();

                foreach (var child in children)
                {
                    result.Children.Add(child);
                }
            }

            return results;

        }
        #endregion



        #region Users
        public IQueryable<ApplicationUser> GetUsers()
        {
            return DataContext.Users.AsQueryable();   
        }

        //public IQueryable<ApplicationUser> GetLeaders()
        //{
            
        //}


        #endregion

        #region Roles
        public IQueryable<IdentityRole> GetRoles()
        {

            int count = DataContext.Roles.Count();

            return DataContext.Roles.AsQueryable();
        }


        #endregion


        public IQueryable<Department> GetDeparts()
        {
            return DataContext.Departs.AsQueryable();
        }

        public Department GetDepartById(int Id)
        {
            var depart = DataContext.Departs.FirstOrDefault(a => a.Id == Id);

            if (depart == null)
                return null;

            return depart;



        }


        public IQueryable<DepartmentMember> GetDepartMembers(int DepId)
        {

            var depart = DataContext.Departs.FirstOrDefault(a => a.Id == DepId);
            if (depart == null)
                return null;

            return depart.Members.AsQueryable();
        }



        #region Test
        public IQueryable<SubscriptionTest> ListTest()
        {
            return from item in DataContext.Tests
                   //orderby item.CreatedAt descending
                   //where item.SubscriptionId == SubscriptionId
                   select item;
        }

        public async Task<SubscriptionTest> AddTest()
        {
            var entity = new SubscriptionTest
            {
                Name = Guid.NewGuid().ToString(),
            };
            DataContext.Tests.Add(entity);
            await DataContext.SaveChangesAsync();

            return entity;
        }

        public Department UpdateDepart(int Id, string Name, string remark, string leaderIds, string memberIds)
        {
            var finddepart = DataContext.Departs.FirstOrDefault(a => a.Id == Id);
            if (finddepart == null)
            {
                return this.AddDepart(Name, remark, leaderIds, memberIds);
            }

            finddepart.DepartName = Name;
            finddepart.Reamrk = remark;
            //clear all the members under this role.


            //while (finddepart.Members.Count() > 0)
            //{
            //    finddepart.Members.Remove(finddepart.Members.FirstOrDefault());
            //}

            finddepart.Members.Clear();

            if (leaderIds != string.Empty)
            {
                var leaders = leaderIds.Split(',');

                foreach (string leader in leaders)
                {
                    var newleader = new DepartmentMember();
                    newleader.IsLeader = true;
                    newleader.UserId = leader;
                    newleader.SubscriptionId = 1;

                    finddepart.Members.Add(newleader);
                }

            }

            if (memberIds != string.Empty)
            {
                var members = memberIds.Split(',');
                foreach (string member in members)
                {
                    var newmember = new DepartmentMember();
                    newmember.IsLeader = false;
                    newmember.UserId = member;
                    newmember.SubscriptionId = 1;
                    finddepart.Members.Add(newmember);
                }
            }


            DataContext.SaveChanges();

            return finddepart;

        }


        public bool DeletePart(int Id)
        {
            var finddepart = DataContext.Departs.FirstOrDefault(a => a.Id == Id);
            if (finddepart == null)
                return false;
            finddepart.Members.Clear();

            DataContext.Departs.Remove(finddepart);
            DataContext.SaveChanges();

            return true;

        }



        public Department AddDepart(string Name,string remark,string leaderIds,string memberIds)
        {
            var newdepart = new Department();
            newdepart.DepartName = Name;
            newdepart.Reamrk = remark;
            newdepart.SubscriptionId = 1;
            DataContext.Departs.Add(newdepart);

            //newdepart.Members =new  List

            if (leaderIds != string.Empty)
            {
                var leaders = leaderIds.Split(',');

                foreach (string leader in leaders)
                {
                    var newleader = new DepartmentMember();
                    newleader.IsLeader = true;
                    newleader.UserId = leader;
                    newleader.SubscriptionId = 1;

                    newdepart.Members.Add(newleader);
                }

            }

            if (memberIds != string.Empty)
            {
                var members = memberIds.Split(',');
                foreach (string  member in members)
                {
                    var newmember = new DepartmentMember();
                    newmember.IsLeader = false;
                    newmember.UserId = member;
                    newmember.SubscriptionId = 1;
                    newdepart.Members.Add(newmember);
                }
            }



            DataContext.SaveChanges();

            return newdepart;

        }



        public async Task<SubscriptionTest> AddTest(SubscriptionTest entity)
        {
            if (string.IsNullOrWhiteSpace(entity.Name))
            {
                Validator.AddError("Name", "The field is required.");
            }

            if (Validator.IsValid)
            {
                DataContext.Tests.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task DeleteTest(int id)
        {
            var entity = await DataContext.Tests.SingleOrDefaultAsync(x => x.Id == id);
            DataContext.Tests.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
        #endregion


       
    }
}
