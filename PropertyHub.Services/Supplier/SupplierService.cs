﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class SupplierService : BusinessService
    {
        public data_supplier GetSupplier(int id)
        {
            return DataContext.data_supplier.SingleOrDefault(x => x.data_supplier_id== id);
        }

        public IQueryable<data_supplier> ListSupplier()
        {
            return from item in DataContext.data_supplier
                   select item;
        }

        public async Task<data_supplier> Add(string data_supplier_name, string data_supplier_address, string data_supplier_phone, string data_suplier_contracts)
        {
            data_supplier entity = new data_supplier();
            entity.data_supplier_name = data_supplier_name;
            entity.data_supplier_address = data_supplier_address;
            entity.data_supplier_phone = data_supplier_phone;
            entity.data_suplier_contracts = data_suplier_contracts;

            if (Validator.IsValid)
            {
                DataContext.data_supplier.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_supplier"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }


        public async Task<data_supplier> Update(data_supplier entity)
        {
            var service = DataContext.data_supplier.FirstOrDefault(x => x.data_supplier_id == entity.data_supplier_id);

            if (service != null)
            {
                service.data_supplier_name = entity.data_supplier_name;
                service.data_supplier_address = entity.data_supplier_address;
                service.data_supplier_phone = entity.data_supplier_phone;
                service.data_suplier_contracts = entity.data_suplier_contracts;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_supplier.SingleOrDefaultAsync(x => x.data_supplier_id == id);
            DataContext.data_supplier.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
