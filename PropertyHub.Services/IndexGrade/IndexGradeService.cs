﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class IndexGradeService : BusinessService
    {
     public data_index_grade GetIndexGrade(int id)
        {
            return DataContext.data_index_grade.SingleOrDefault(x => x.data_index_grade_id == id);
        }

        public IQueryable<data_index_grade> ListIndexGrade()
        {
            return from item in DataContext.data_index_grade
                   select item;
        }

        public IQueryable<SelectListItem> ListIndexDictionary()
        {
            return from item in DataContext.data_dictionary
                   //where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }


        public async Task<data_index_grade> Add(Nullable<int> data_index_grade1, Nullable<int> data_index_grade_age_min, Nullable<int> data_index_grade_age_max,
           Nullable<int> data_index_grade_point_min, Nullable<int> data_index_grade_point_max, string data_index_grade_comments)
        {
            data_index_grade entity = new data_index_grade();
            entity.data_index_grade1 = data_index_grade1;
            entity.data_index_grade_age_min = data_index_grade_age_min;
            entity.data_index_grade_age_max = data_index_grade_age_max;
            entity.data_index_grade_point_min =  data_index_grade_point_min;
            entity.data_index_grade_point_max = data_index_grade_point_max;
            entity.data_index_grade_comments = data_index_grade_comments;

            if (Validator.IsValid)
            {
                DataContext.data_index_grade.Add(entity);
                await DataContext.SaveChangesAsync();

                //try
                //{
                //    DataContext.SaveChanges();
                //}
                //catch (Exception ex)
                //{
 
                //}
              
            }

            return entity;
        }


        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_index_grade"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public async Task<data_index_grade> Update(data_index_grade entity)
        {
            var service = DataContext.data_index_grade.FirstOrDefault(x => x.data_index_grade_id == entity.data_index_grade_id);

            if (service != null)
            {
                service.data_index_grade1 = entity.data_index_grade1;
                service.data_index_grade_age_min = entity.data_index_grade_age_min;
                service.data_index_grade_age_max = entity.data_index_grade_age_max;
                service.data_index_grade_point_min = entity.data_index_grade_point_min;
                service.data_index_grade_point_max = entity.data_index_grade_point_max;
                service.data_index_grade_comments = entity.data_index_grade_comments;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_index_grade.SingleOrDefaultAsync(x => x.data_index_grade_id == id);
            DataContext.data_index_grade.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
