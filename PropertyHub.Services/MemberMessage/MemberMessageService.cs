﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services.MemberMessage;

namespace PropertyHub.Services
{
    /// <summary>
    /// created by adam 2015-10-13 the service of member message
    /// database table: data_member_message
    /// associate tables: data_member
    /// </summary>
    public class MemberMessageService : BusinessService
    {
        public IQueryable<data_member_message> List()
        {
            return from item in DataContext.data_member_message
                   select item;
        }

        public IQueryable<data_member_message> GetPersonalMemberMessageList(int data_member_id)
        {
            return from item in DataContext.data_member_message
                   where ((item.data_member_ids == null) || (item.data_member_ids.Contains("," + data_member_id.ToString() + ","))) && item.record_status == 1
                   select item;
        }

        public IEnumerable<SelectListItem> ListStatus()
        {
            return new List<SelectListItem>() { 
                new SelectListItem { Text = "草稿", Value = "0"},
                new SelectListItem { Text = "已审核", Value = "1"}
            };
        }

        public async Task<data_member_message> Add(data_member_message entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_member_message.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_member_message> Update(data_member_message entity)
        {
            var service = DataContext.data_member_message.FirstOrDefault(x => x.data_member_message_id == entity.data_member_message_id);

            if (service != null)
            {
                service.data_member_message_title = entity.data_member_message_title;
                service.data_member_message_text = entity.data_member_message_text;
                service.data_member_ids = entity.data_member_ids;
                service.data_member_names = entity.data_member_names;

                service.data_member_message_type = entity.data_member_message_type;

                service.record_status = entity.record_status;
                service.dateupdated = entity.dateupdated;
                service.updatedby = entity.updatedby;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_member_message Get(int id)
        {
            return DataContext.data_member_message.SingleOrDefault(x => x.data_member_message_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_member_message.SingleOrDefaultAsync(x => x.data_member_message_id == id);
            DataContext.data_member_message.Remove(entity);
            await DataContext.SaveChangesAsync();
        }


        /// <summary>
        /// 获得全时消息列表
        /// </summary>
        /// <returns></returns>
        public Result<List<MessageModel>> GetMessages(int data_member_id, int? toprows, int pagesize, int pagenumber)
        {

            var result = new Result<List<MessageModel>>();

            try
            {

          

            var member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);

                if (member == null)
                {
                    result.IsSuccessful = false;
                    result.Parameter = new List<MessageModel>();
                    result.Message = "无法找到此会员";

                    return result;
                }

                var registerdate = member.datecreated;


            var res = (from item in DataContext.data_member_message
                       where
                       ((item.data_member_ids == null) || (item.data_member_ids.Contains("," + data_member_id.ToString() + ",")))
                       && item.record_status == 1
                       && item.data_member_message_type == 143 //全时消息

                       && item.datecreated > registerdate
                       select new MessageModel
                       {
                           messageId = item.data_member_message_id,
                           title = item.data_member_message_title,
                           text = item.data_member_message_text,
                           datecreated = item.datecreated.HasValue ? item.datecreated.Value : DateTime.Now,
                           type = 99
                       }).ToList();



            //  res = (from item in res
            //   //当前这个会员的阅读日志。
            //   join log in (from _log in DataContext.data_member_message_log  where _log.data_member_id==data_member_id select _log)

            //   on item.messageId equals log.data_message_id.Value && item.type == log.data_message_type   

            //   into temp

            //   from tt in temp.DefaultIfEmpty()

            //   select new MessageModel()
            //   {
            //       messageId = item.messageId,
            //       type = item.type,
            //       title = item.title,
            //       text = item.text,
            //       datecreated = item.datecreated,
            //       hasPDF = item.hasPDF,
            //       PDFPath = item.PDFPath,
            //       isread =tt.data_member_message_log_id==null?false:true 
            //   }

            //).ToList();

            var logs = DataContext.data_member_message_log.Where(a => a.data_member_id == data_member_id);
            foreach (var log in logs)
            {
                var findmessage =
                    res.FirstOrDefault(a => a.messageId == log.data_message_id && a.type == log.data_message_type);
                if (findmessage != null)
                    findmessage.isread = true;
                //else
                //{
                //    findmessage.isread = false;
                //}
            }




            // if (toprows.HasValue)
                result.recordcount = res.Count;
            result.Parameter = res.OrderByDescending(d => d.datecreated)
                //.Take(toprows.HasValue ? toprows.Value : 20)
                .Skip((pagenumber - 1) * pagesize)
                .Take(pagesize)

                .ToList();

                result.IsSuccessful = true;
                // else
                // {

                // }
            }
            catch (Exception exception)
            {

                result.IsSuccessful = false;
                result.Message = exception.Message;
                result.Parameter = new List<MessageModel>();
            }

            return result;

        }

        public Result<long> GetUnReads(int data_member_id)
        {
            var res = new Result<long>();
            try
            {

                var member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);

                if (member == null)
                {
                    res.Parameter = 0;
                    res.IsSuccessful = true;
                    return res;
                }

                var registerdate = member.datecreated;


                var readlist = (from item in DataContext.data_member_message_log
                                where item.data_member_id == data_member_id
                                && item.data_message_type == 99 //消息
                                select item.data_message_id).ToList();


                var unread =
                    DataContext.data_member_message.Count(item =>

                           ((item.data_member_ids == null) || (item.data_member_ids.Contains("," + data_member_id.ToString() + ",")))
                       && item.record_status == 1
                       && item.data_member_message_type == 143 //全时消息
                       && item.datecreated > registerdate
                        &&
                        readlist.Contains(item.data_member_message_id) == false);

                res.IsSuccessful = true;
                res.Parameter = unread;

            }
            catch (Exception exception)
            {
                res.Message = exception.Message;
                res.IsSuccessful = false;
                res.Parameter = 0;

            }
            return res;
        }



        public Result<long> GetUnReadsAdvices(int data_member_id)
        {
            var res = new Result<long>();

            try
            {

                var member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);

                if (member == null)
                {
                    res.Parameter = 0;
                    res.IsSuccessful = true;
                    return res;
                }

                var registerdate = member.datecreated;


                var read0 = (from item in DataContext.data_member_message_log
                             where item.data_member_id == data_member_id
                             && item.data_message_type == 0 //消息
                             select item.data_message_id).ToList();

                var unread0 = DataContext.data_member_checkrecord.Count(
                    a => a.data_Isfinished == true
                        && a.data_member_id == data_member_id
                        && read0.Contains(a.data_member_checkrecord_id) == false
                    );


                var read1 = (from item in DataContext.data_member_message_log
                             where item.data_member_id == data_member_id
                             && item.data_message_type == 1 //消息
                             select item.data_message_id).ToList();

                var unread1 = DataContext.data_member_advice.Count(a => a.data_member_advice_isfinished == true
                    &&
                    a.data_member_id == data_member_id
                    &&
                    read1.Contains(a.data_member_advice_id) == false
                    );




                var read2 = (from item in DataContext.data_member_message_log
                             where item.data_member_id == data_member_id
                             && item.data_message_type == 2//消息
                             select item.data_message_id).ToList();

                var unread2 = DataContext.data_member_message.Count(item =>
                    ((item.data_member_ids == null) ||
                     (item.data_member_ids.Contains("," + data_member_id.ToString() + ",")))
                    && item.record_status == 1
                    && item.datecreated > registerdate
                    && item.data_member_message_type == 144
                    && read2.Contains(item.data_member_message_id)==false
                    );


                var read3 = (from item in DataContext.data_member_message_log
                             where item.data_member_id == data_member_id
                             && item.data_message_type == 3 //消息
                             select item.data_message_id).ToList();

                var unread4 = DataContext.data_member_prescription
                    .Count(item => item.data_member_id == data_member_id
                          && item.data_member_prescription_status == 3
                          && read3.Contains(item.data_member_prescription_id)==false
                          );


                res.IsSuccessful = true;
                res.Parameter = unread0 + unread1 + unread2 + unread4;



            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = 0;

                //   throw;
            }
            return res;
        }



        public Result<MessageModel> GetAdviceDetails(int data_member_id, int id, int type)
        {

            var res = new Result<MessageModel>();

            //Tsing
            //2015/11/15
            //创建一条获得日志。

            this.AddMemberMessageLog(data_member_id, id, type);



            if (type == 0)
            {

                var record = DataContext.data_member_checkrecord.FirstOrDefault(a => a.data_member_checkrecord_id == id);
                if (record == null)
                    return res;

                var message = new MessageModel
                {
                    messageId = record.data_member_checkrecord_id,
                    type = 0,
                    title = "健身建议",
                    datecreated = record.datecreated.HasValue ? record.datecreated.Value : DateTime.Now,
                    text = record.data_checkreport
                };
                res.IsSuccessful = true;
                res.Parameter = message;

                return res;
            }
            else if (type == 1)
            {
                var record = DataContext.data_member_advice.FirstOrDefault(a => a.data_member_advice_id == id);
                if (record == null)
                    return res;
                var message = new MessageModel
                {
                    messageId = record.data_member_advice_id,
                    type = 0,
                    title = "健身建议",
                    datecreated = record.datecreated.HasValue ? record.datecreated.Value : DateTime.Now,
                    text = record.data_member_advice_summary
                };
                res.IsSuccessful = true;
                res.Parameter = message;

                return res;

            }


            else
            //if (type == 2)
            {

                var record = DataContext.data_member_message.FirstOrDefault(a => a.data_member_message_id == id);

                if (record == null)
                    return res;

                var message = new MessageModel
                {
                    messageId = record.data_member_message_id,
                    type = 0,
                    title = "健身建议",
                    datecreated = record.datecreated.HasValue ? record.datecreated.Value : DateTime.Now,
                    text = record.data_member_message_text
                };
                res.IsSuccessful = true;
                res.Parameter = message;

                return res;

            }







            res.IsSuccessful = false;
            res.Message = "";
            return res;

        }



        public void AddMemberMessageLog(int data_member_id, int data_message_id, int data_message_type)
        {

            var findres = DataContext.data_member_message_log.FirstOrDefault(a => a.data_member_id == data_member_id
                                                                                  &&
                                                                                  a.data_message_id == data_message_id
                                                                                  &&
                                                                                  a.data_message_type ==
                                                                                  data_message_type

                );




            if (findres == null)
            {
                var newlog = new data_member_message_log();
                newlog.data_member_message_log_date = DateTime.Now;
                newlog.data_member_id = data_member_id;
                newlog.data_message_id = data_message_id;
                newlog.data_message_type = data_message_type;
                DataContext.data_member_message_log.Add(newlog);
                DataContext.SaveChanges();
            }


        }




        public Result<List<MessageModel>> GetMemberAdvices(int data_member_id, int? toprows, int pagesize, int pagenumber)
        {
            //这块内容是推送的健身建议
            var result = new Result<List<MessageModel>>();

            try
            {


                var res = new List<MessageModel>();


                //这个地方首先判断一下当前会员是否已经激活，
                //只有激活的才返回这些信息.

                var member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);

                if (member == null)
                {
                    result.IsSuccessful = false;
                    result.Message = "无法找到会员";
                    result.Parameter = new List<MessageModel>();


                    return result;
                }


                var registerdate = member.datecreated;




                var messagelist1 = (from item in DataContext.data_member_message
                                    where
                                        ((item.data_member_ids == null) ||
                                         (item.data_member_ids.Contains("," + data_member_id.ToString() + ",")))
                                        && item.record_status == 1
                                        && item.data_member_message_type == 144
                                    //全时消息

                                    where item.datecreated > registerdate

                                    select new MessageModel
                                    {
                                        messageId = item.data_member_message_id,
                                        title = item.data_member_message_title,
                                        text = item.data_member_message_text,
                                        datecreated = item.datecreated.HasValue ? item.datecreated.Value : DateTime.Now,
                                        type = 2
                                    }).ToList();











                //这块健身建议来自于每一次的数据上传记录
                var messagelist2 = (from item in DataContext.data_member_checkrecord
                                    where item.data_member_id == data_member_id
                                          && item.data_Isfinished == true
                                    select new MessageModel()
                                    {
                                        messageId = item.data_member_checkrecord_id,
                                        title = "健身建议",
                                        text = item.data_checkreport,
                                        datecreated = item.datecreated.HasValue ? item.datecreated.Value : DateTime.Now,
                                        type = 0
                                    }
                    ).ToList();
                //通过全时消息发送的.
                var messagelist3 = (
                    from item in DataContext.data_member_advice
                    where item.data_member_id == data_member_id
                          && item.data_member_advice_isfinished == true

                    select new MessageModel()
                    {
                        messageId = item.data_member_advice_id,
                        type = 1,
                        title = "定时健身建议",
                        text = item.data_member_advice_summary,
                        datecreated = item.datecreated.HasValue ? item.datecreated.Value : DateTime.Now
                    }
                    ).ToList();

                //会员的

                var pdflist4 =
                    (
                        from item in DataContext.data_member_prescription
                        where item.data_member_id == data_member_id
                              && item.data_member_prescription_status == 3

                        select new MessageModel()
                        {
                            messageId = item.data_member_prescription_id,
                            type = 3,
                            title = item.data_member_prescription_title,
                            text = item.data_member_prescription_title,
                            datecreated = item.datecreated.HasValue ? item.datecreated.Value : DateTime.Now,
                            hasPDF = true,
                            PDFPath = item.data_member_prescription_path
                        }

                        );






                res.AddRange(messagelist1);
                res.AddRange(messagelist2);
                res.AddRange(messagelist3);
                res.AddRange(pdflist4);



                //res = (from item in res
                //       //当前这个会员的阅读日志。
                //       join log in (from _log in DataContext.data_member_message_log  where _log.data_member_id==data_member_id select _log)  

                //       on item.messageId equals log.data_message_id.Value

                //       select new MessageModel()
                //       {
                //           messageId = item.messageId,
                //           type = item.type,
                //           title = item.title,
                //           text = item.text,
                //           datecreated = item.datecreated,
                //           hasPDF = item.hasPDF,
                //           PDFPath = item.PDFPath,
                //           isread =log.data_member_message_log_id==null?false:true 
                //       }

                //    ).ToList();

                var logs = DataContext.data_member_message_log.Where(a => a.data_member_id == data_member_id);
                foreach (var log in logs)
                {
                    var findmessage =
                        res.FirstOrDefault(a => a.messageId == log.data_message_id && a.type == log.data_message_type);
                    if (findmessage != null)
                        findmessage.isread = true;
                    //else
                    //{
                    //    findmessage.isread = false;
                    //}
                }




                result.Parameter = res.OrderByDescending(a => a.datecreated)
                    .Skip((pagenumber - 1) * pagesize)
                    //.Take(toprows.HasValue ? toprows.Value : 20)
                    .Take(pagesize).ToList();
                result.IsSuccessful = true;
                result.recordcount = res.Count();


            }
            catch (Exception exception)
            {

                result.IsSuccessful = false;
                result.Parameter = new List<MessageModel>();
                result.Message = exception.Message;

            }

            return result;
        }







    }
}
