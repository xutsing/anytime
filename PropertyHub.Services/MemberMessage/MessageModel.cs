﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services.MemberMessage
{
    /// <summary>
    /// 用于消息返回值
    /// </summary>
    public class MessageModel
    {

        public MessageModel()
        {
            hasPDF = false;;
            PDFPath = string.Empty;
        }

        public int messageId { get; set; }

        /// <summary>
        /// 0 每次的健身建议
        /// 1 推送的健身建议
        /// 2 定时的健身建议
        /// </summary>
        public int type { get; set; }

        public string title { get; set; }

        public string text { get; set; }

        public DateTime datecreated { get; set; }

        public bool hasPDF { get; set; }

        public string PDFPath { get; set; }

        public bool isread { get; set; }

    }
}
