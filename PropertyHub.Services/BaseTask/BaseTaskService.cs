﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{

    public class BaseTaskService : BusinessService
    {
        public IQueryable<data_basetask> ListBaseTask()
        {
            return from item in DataContext.data_basetask
                   where item.data_status == 1
                   select item;
        }

        public async Task<data_basetask> Add(data_basetask entity)
        {
            if (Validator.IsValid)
            {
                entity.data_status = 1;
                DataContext.data_basetask.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_basetask> Update(data_basetask entity)
        {
            var service = DataContext.data_basetask.FirstOrDefault(x => x.data_basetask_id == entity.data_basetask_id);

            if (service != null)
            {
                service.data_basetask_name = entity.data_basetask_name;
                service.data_basetask_type = entity.data_basetask_type;
                service.data_basetask_remark = entity.data_basetask_remark;
                service.data_basetask_url = entity.data_basetask_url;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_basetask Get(int id)
        {
            return DataContext.data_basetask.SingleOrDefault(x => x.data_basetask_id == id);
        }

        public async Task Delete(int id)
        {

            var entity = await DataContext.data_basetask.SingleOrDefaultAsync(x => x.data_basetask_id == id);
            if (entity != null && (entity.data_status == 1))
            {
                entity.data_status = 0;
                //DataContext.data_basetask.Remove(entity);
                await DataContext.SaveChangesAsync();
            }
        }
        public IQueryable<SelectListItem> ListBaseTaskType()
        {
            return from item in DataContext.data_basetask_type
                   select new SelectListItem() { Text = item.data_basetask_type_title, Value = item.data_basetask_type_id.ToString() };
        }

    }
}
