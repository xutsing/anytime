﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Security;
using PropertyHub.Security;

namespace PropertyHub.Services
{
    public class DataDictionaryService : BusinessService
    {
        public data_dictionary GetDataDictionary(int? id)
        {
            return DataContext.data_dictionary.SingleOrDefault(x => x.data_dictionary_id == id);
        }

        public IQueryable<data_dictionary> ListDataDictionary()
        {
            return from item in DataContext.data_dictionary

                   select item;
        }

        public IQueryable<SelectListItem> ListSmokingType()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_smoke"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListExerciseFrequency()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_physical"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListExerciseTime()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_physical_schedule"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListDrinkingType()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_drink"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_dictionary.SingleOrDefaultAsync(x => x.data_dictionary_id == id);
            DataContext.data_dictionary.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task<data_dictionary> Add(string data_dictionary_type, string data_dictionary_text, Nullable<int> data_dictionary_value)
        {
            data_dictionary entity = new data_dictionary();
            entity.data_dictionary_type = data_dictionary_type;
            entity.data_dictionary_text = data_dictionary_text;
            entity.data_dictionary_value = data_dictionary_value;

            if (Validator.IsValid)
            {
                DataContext.data_dictionary.Add(entity);
                await DataContext.SaveChangesAsync();
            }
            return entity;
        }

        public async Task<data_dictionary> Update(data_dictionary entity)
        {
            var service = DataContext.data_dictionary.FirstOrDefault(x => x.data_dictionary_id == entity.data_dictionary_id);

            if (service != null)
            {
                service.data_dictionary_type = entity.data_dictionary_type;
                service.data_dictionary_text = entity.data_dictionary_text;
                service.data_dictionary_value = entity.data_dictionary_value;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }
    }
}
