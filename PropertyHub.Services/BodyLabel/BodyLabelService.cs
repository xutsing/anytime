﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class BodyLabelService : BusinessService
    {
        public data_body_label GetBaseIndex(int id)
        {
            return DataContext.data_body_label.SingleOrDefault(x => x.data_body_label_id == id);
        }

        public int? GetBodyLabel(int grade)
        {
            var entity = DataContext.data_body_label.SingleOrDefault(x => x.data_body_label_text == grade.ToString());
            if (entity != null)
            {
                return entity.data_body_label_id;
            }
            else
            {
                return null;
            }
        }

        public IQueryable<data_body_label> ListBaseIndex()
        {
            return from item in DataContext.data_body_label
                   select item;
        }

        public async Task<data_body_label> Add(string data_body_label_text)
        {
            data_body_label entity = new data_body_label();
            entity.data_body_label_text = data_body_label_text;
      
            if (Validator.IsValid)
            {
                DataContext.data_body_label.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_body_label"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }


        public async Task<data_body_label> Update(data_body_label entity)
        {
            var service = DataContext.data_body_label.FirstOrDefault(x => x.data_body_label_id == entity.data_body_label_id);

            if (service != null)
            {
                service.data_body_label_text = entity.data_body_label_text;
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }


        public async Task Delete(int id)
        {
            var entity = await DataContext.data_body_label.SingleOrDefaultAsync(x => x.data_body_label_id == id);
            DataContext.data_body_label.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
