﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class BaseIndexService : BusinessService
    {
        public data_base_index GetBaseIndex(int id)
        {
            return DataContext.data_base_index.SingleOrDefault(x => x.data_base_index_id == id);
        }

        public int? GetBaseIndexIdByName(string baseindex_name)
        {
            var entity = DataContext.data_base_index.SingleOrDefault(x => x.data_base_index_title == baseindex_name);
            if (entity != null)
            {
                return entity.data_base_index_id;
            }
            else
            {
                return null;
            }
        }

        public IQueryable<data_base_index> ListBaseIndex()
        {
            return from item in DataContext.data_base_index
                   select item;
        }

        public async Task<data_base_index> Add(string data_base_index_title, string data_base_index_title_en, string data_base_index_unit, string data_base_key)
        {
            data_base_index entity = new data_base_index();
            entity.data_base_index_title = data_base_index_title;
            entity.data_base_index_title_en = data_base_index_title_en;
            entity.data_base_index_unit = data_base_index_unit;
            entity.data_base_key = data_base_key;

            if (Validator.IsValid)
            {
                DataContext.data_base_index.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_base_index"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }


        public async Task<data_base_index> Update(data_base_index entity)
        {
            var service = DataContext.data_base_index.FirstOrDefault(x => x.data_base_index_id == entity.data_base_index_id);

            if (service != null)
            {
                service.data_base_index_title = entity.data_base_index_title;
                service.data_base_index_title_en = entity.data_base_index_title_en;
                service.data_base_index_unit= entity.data_base_index_unit;
                service.data_base_key = entity.data_base_key;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_base_index.SingleOrDefaultAsync(x => x.data_base_index_id == id);
            DataContext.data_base_index.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
