﻿using PropertyHub.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PropertyHub.Services
{
    public abstract class ServiceBase : ServiceBase<ApplicationDbContext>
    {
        [Dependency]
        public override ApplicationDbContext DataContext
        {

            get
            {
                return base.DataContext;
            }
            set
            {
                base.DataContext = value;
            }
        }

        private int? _subscriptionId = -1;
        public virtual int? SubscriptionId
        {
            get
            {
                if (_subscriptionId == -1)
                {
                    var value = WebContext == null ? null : WebContext.Request.RequestContext.RouteData.Values["Subscription"];
                    if (value != null)
                    {
                        _subscriptionId = (int?)value;
                    }
                }
                return _subscriptionId;
            }
        }

        private Subscription _subscription;
        public virtual Subscription Subscription
        {
            get
            {
                if (SubscriptionId > 0 && _subscription == null)
                {
                    _subscription = DataContext.Subscriptions.FirstOrDefault(x => x.Id == _subscriptionId);
                }
                return _subscription;
            }
        }

        public static void RegisterAllServices(IUnityContainer container)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
               .Where(type => !String.IsNullOrEmpty(type.Namespace))
               .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                && type.BaseType.GetGenericTypeDefinition() == typeof(ServiceBase));
            foreach (var type in typesToRegister)
            {
                container.RegisterType(type);
            }
        }
    }

    public abstract class ServiceBase<T> where T : System.Data.Entity.DbContext
    {
        public virtual string UserName
        {
            get { return WebContext.User.Identity.Name; }
        }
        public virtual T DataContext { get; set; }
        public virtual bool IsAuthenticated
        {
            get { return WebContext.User.Identity.IsAuthenticated; }
        }
        public virtual HttpContext WebContext
        {
            get { return HttpContext.Current; }
        }
        [Dependency]
        public IValidationDictionaryFactory Validator { get; set; }
    }
}
