﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class IndexDataService : BusinessService
    {
        public data_index_data GetIndexData(int id)
        {
            return DataContext.data_index_data.SingleOrDefault(x => x.data_index_data_id == id);
        }

        public string GetIndexDataMin(int? base_index_id, int? body_label_id, int? sex_id, int? age_range_id)
        {
            var model = DataContext.data_index_data.SingleOrDefault(x => (x.data_base_index_id == base_index_id) && (x.data_body_label_id == body_label_id) && (x.data_sex_id == sex_id) && (x.data_age_range_id == age_range_id));
            if (model != null)
            {
                return model.data_index_data_min.ToString();
            }
            else
            {
                return null;
            }
        }

        public string GetIndexDataMax(int base_index_id, int body_label_id, int sex_id, int age_range_id)
        {
            var model = DataContext.data_index_data.SingleOrDefault(x => (x.data_base_index_id == base_index_id) && (x.data_body_label_id == body_label_id) && (x.data_sex_id == sex_id) && (x.data_age_range_id == age_range_id));
            if (model != null)
            {
                return model.data_index_data_max.ToString();
            }
            else
            {
                return null;
            }
        }

        public IQueryable<data_index_data> ListIndexData()
        {
            return from item in DataContext.data_index_data
                   select item;
        }

        public IQueryable<SelectListItem> ListBaseIndex()
        {
            return from item in DataContext.data_base_index
                   where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_base_index_title, Value = item.data_base_index_id.ToString() };
        }

        public IQueryable<SelectListItem> ListBodyLabel()
        {
            return from item in DataContext.data_body_label
                   //where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_body_label_text, Value = item.data_body_label_id.ToString() };
        }

        public IQueryable<SelectListItem> ListSex()
        {
            return from item in DataContext.data_sex
                   where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_sex_title, Value = item.data_sex_id.ToString() };
        }

        public IQueryable<SelectListItem> ListAgeRange()
        {
            return from item in DataContext.data_age_range
                   where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_age_title, Value = item.data_age_range_id.ToString() };
        }

        public async Task<data_index_data> Add(Nullable<int> data_base_index_id, Nullable<int> data_body_label_id, Nullable<int> data_sex_id,
           Nullable<int> data_age_range_id, Nullable<decimal> data_index_data_min, Nullable<decimal> data_index_data_max, Nullable<int> data_index_data_point, string data_index_data_label)
        {
            data_index_data entity = new data_index_data();
            entity.data_base_index_id = data_base_index_id;
            entity.data_body_label_id = data_body_label_id;
            entity.data_sex_id = data_sex_id;
            entity.data_age_range_id = data_age_range_id;
            entity.data_index_data_min = data_index_data_min;
            entity.data_index_data_max = data_index_data_max;
            entity.data_index_data_point = data_index_data_point;
            entity.data_index_data_label = data_index_data_label;

            if (Validator.IsValid)
            {
                DataContext.data_index_data.Add(entity);
                await DataContext.SaveChangesAsync();

                //try
                //{
                //    DataContext.SaveChanges();
                //}
                //catch (Exception ex)
                //{
 
                //}
              
            }

            return entity;
        }


        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_index_data"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public async Task<data_index_data> Update(data_index_data entity)
        {
            var service = DataContext.data_index_data.FirstOrDefault(x => x.data_index_data_id == entity.data_index_data_id);

            if (service != null)
            {
                service.data_base_index_id = entity.data_base_index_id;
                service.data_body_label_id = entity.data_body_label_id;
                service.data_sex_id = entity.data_sex_id;
                service.data_age_range_id = entity.data_age_range_id;
                service.data_index_data_min = entity.data_index_data_min;
                service.data_index_data_max = entity.data_index_data_max;
                service.data_index_data_point = entity.data_index_data_point;
                service.data_index_data_label = entity.data_index_data_label;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_index_data.SingleOrDefaultAsync(x => x.data_index_data_id == id);
            DataContext.data_index_data.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
