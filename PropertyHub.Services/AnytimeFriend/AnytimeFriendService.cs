﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
     public class AnytimeFriendService : BusinessService
    {
        public async Task<data_anytime_friends> Add(Nullable<int> data_age, Nullable<int> data_province_Id, Nullable<int> data_city_Id, Nullable<int> data_area_Id, string data_name,
            Nullable<int> data_sex_Id, string data_profession, string data_comments, string data_IP)
        {
            data_anytime_friends entity = new data_anytime_friends();
            entity.data_age = data_age;
            entity.data_province_Id = data_province_Id;
            entity.data_city_Id = data_city_Id;
            entity.data_area_Id = data_area_Id;
            entity.data_name = data_name;
            entity.data_sex_Id = data_sex_Id;
            entity.data_profession = data_profession;
            entity.data_comments = data_comments;
            entity.data_Ip = data_IP;
            entity.datecreated = DateTime.Now;

        //    if (Validator.IsValid)
          //  {
                DataContext.data_anytime_friends.Add(entity);
                 DataContext.SaveChanges();
          //  }

            return entity;
        }

        public IQueryable<data_anytime_friends> ListDataCoach()
        {
            return from item in DataContext.data_anytime_friends
                   select item;
        }

        public IQueryable<SelectListItem> ListSex()
        {
            return from item in DataContext.data_sex
                   //where item.data_dictionary_type == "data_base_index"
                   select new SelectListItem() { Text = item.data_sex_title, Value = item.data_sex_id.ToString() };
        }

        public IQueryable<SelectListItem> ListProvice()
        {
            return from item in DataContext.Data_Province
                   select new SelectListItem() { Text = item.ProvinceName, Value = item.ProvinceCode.ToString() };
        }

        public IQueryable<SelectListItem> ListCity(String ID)
        {
            
            return from item in DataContext.Data_City
                   where item.ProvinceCode == ID
                   select new SelectListItem() { Text = item.CityName, Value = item.CityCode.ToString()};
        }

        public IQueryable<SelectListItem> ListArea(String ID)
        {
            return from item in DataContext.Data_Area
                   where item.CityCode == ID
                   select new SelectListItem() { Text = item.AreaName, Value = item.AreaId.ToString() };
        }

        public IQueryable<SelectListItem> ListBeiJingProvice()
        {
            return from item in DataContext.Data_City
                   where item.ProvinceCode == "110000"
                   select new SelectListItem() { Text = item.CityName, Value = item.CityCode.ToString() };
        }

        public IQueryable<SelectListItem> ListBeiJingArea()
        {
            return from item in DataContext.Data_Area
                   where item.CityCode == "110100"
                   select new SelectListItem() { Text = item.AreaName, Value = item.AreaId.ToString() };
        }
    }
}
