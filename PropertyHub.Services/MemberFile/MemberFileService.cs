﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Logging;
using Newtonsoft.Json;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services.MemberFile;

namespace PropertyHub.Services
{
    /// <summary>
    /// Added by Tsing,家庭化健康档案.
    /// </summary>
    public class MemberFileService : BusinessService
    {

        public IQueryable<data_member_information> GetData()
        {
            return DataContext.data_member_information.OrderByDescending(a => a.dateupdated).AsQueryable();
        }


        public data_member GetMember(int data_member_id)
        {
            return DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);
        }

        public data_member_information GetMemberInfor(int data_member_id)
        {
            var file = DataContext.data_member_information.FirstOrDefault(a => a.data_member_id == data_member_id);

            //如果没有找到这份档案，意味着这个会员目前还没有健康档案
            if (file == null)
            {
                file = new data_member_information();
                file.data_member_id = data_member_id;
                file.data_member_injury = false;
                file.data_member_blood_transfer = false;
                file.data_member_inherited = false;
                file.data_drink_Isdrink = false;
                file.data_member_nerve = false;
                file.data_member_other_disease = false;

                file.createdby_code = WebContext.User.Identity.GetUserId();
                file.createdby_name = WebContext.User.Identity.GetUserName();
                file.dateupdated = DateTime.Now;



                DataContext.data_member_information.Add(file);
                DataContext.SaveChanges();
            }

            //一个会员只有一份健康
            return file;
        }

        public void UpdateMemberInfo(data_member_information member)
        {
            var info = DataContext.data_member_information.FirstOrDefault(a => a.data_member_information_Id == member.data_member_information_Id);
            if (info == null)
            {
                DataContext.data_member_information.Add(member);
            }

            info.data_member_identity = member.data_member_identity;
            info.data_member_country = member.data_member_country;
            info.data_member_family = member.data_member_family;
            info.data_member_nationality = member.data_member_nationality;
            info.data_member_nationality_comments = member.data_member_nationality_comments;
            info.data_member_blood = member.data_member_blood;
            info.data_member_rh = member.data_member_rh;
            info.data_member_rh_comments = member.data_member_rh_comments;
            info.data_member_profession = member.data_member_profession;
            info.data_member_disease = member.data_member_disease;
            info.data_member_disease_c = member.data_member_disease_c;
            info.data_member_disease_other = member.data_member_disease_other;

            info.data_member_disease_1 = member.data_member_disease_1;
            info.data_member_disease_1_date = member.data_member_disease_1_date;

            info.data_member_disease_2 = member.data_member_disease_2;
            info.data_member_disease_2_date = member.data_member_disease_2_date;
            info.data_member_disease_3 = member.data_member_disease_3;
            info.data_member_disease_3_date = member.data_member_disease_3_date;

            info.data_member_disease_4 = member.data_member_disease_4;
            info.data_member_disease_4_date = member.data_member_disease_4_date;

            info.data_member_disease_5 = member.data_member_disease_5;
            info.data_member_disease_5_date = member.data_member_disease_5_date;


            info.data_member_disease_6 = member.data_member_disease_6;
            info.data_member_disease_6_date = member.data_member_disease_6_date;

            info.data_member_surgery = member.data_member_surgery;
            info.data_member_surgery_1 = member.data_member_surgery_1;
            info.data_member_surgery_1_date = member.data_member_surgery_1_date;


            info.data_member_surgery_2 = member.data_member_surgery_2;
            info.data_member_surgery_2_date = member.data_member_surgery_2_date;



            info.data_member_injury = member.data_member_injury;
            info.data_member_injury_1 = member.data_member_injury_1;
            info.data_member_injury_1_date = member.data_member_injury_1_date;

            info.data_member_injury_2 = member.data_member_injury_2;
            info.data_member_injury_2_date = member.data_member_injury_2_date;

            info.data_member_injury_3 = member.data_member_injury_3;
            info.data_member_injury_3_date = member.data_member_injury_3_date;


            info.data_member_blood_transfer = member.data_member_blood_transfer;

            info.data_member_blood_transfer_1_date = member.data_member_blood_transfer_1_date;
            info.data_member_blood_transfer_1_reason = member.data_member_blood_transfer_1_reason;

            info.data_member_blood_transfer_2_date = member.data_member_blood_transfer_2_date;
            info.data_member_blood_transfer_2_reason = member.data_member_blood_transfer_2_reason;


            info.data_member_family_father = member.data_member_family_father;
            info.data_member_family_mother = member.data_member_family_mother;
            info.data_member_family_brothers = member.data_member_family_brothers;
            info.data_member_family_children = member.data_member_family_children;


            info.data_member_family_disease = member.data_member_family_disease;
            info.data_member_inherited = member.data_member_inherited;
            info.data_member_inherited_disease = member.data_member_inherited_disease;


            info.data_member_height = member.data_member_height;
            info.data_member_hear_rate = member.data_member_hear_rate;
            info.data_member_waist_line = member.data_member_waist_line;
            info.data_member_blood_pressure = member.data_member_blood_pressure;
            info.data_member_hips = member.data_member_hips;
            info.data_member_lung = member.data_member_lung;
            info.data_member_weight = member.data_member_weight;
            info.data_member_bmi = member.data_member_bmi;

            info.data_member_muscle = member.data_member_muscle;
            info.data_member_balance = member.data_member_balance;
            info.data_member_myodynamic = member.data_member_myodynamic;
            info.data_member_staying = member.data_member_staying;
            info.data_member_whr = member.data_member_whr;
            info.data_member_flexible = member.data_member_flexible;

            info.data_physical_rate = member.data_physical_rate;
            info.data_physical_time = member.data_physical_time;
            info.data_physical_total_time = member.data_physical_total_time;
            info.data_physical_place = member.data_physical_place;
            info.data_physical_schedule = member.data_physical_schedule;
            info.data_physical_item = member.data_physical_item;
            info.data_food = member.data_food;
            info.data_smoke = member.data_smoke;
            info.data_smoke_day = member.data_smoke_day;
            info.data_smoke_begin = member.data_smoke_begin;
            info.data_smoke_end = member.data_smoke_end;

            info.data_drink = member.data_drink;
            info.data_drink_day = member.data_drink_day;
            info.data_drink_Isdry = member.data_drink_Isdry;
            info.data_drink_Isdry_age = member.data_drink_Isdry_age;
            info.data_drink_start = member.data_drink_start;
            info.data_smoke_end = member.data_smoke_end;
            info.data_drink_Isdrink = member.data_drink_Isdrink;

            info.data_drink_wine_options = member.data_drink_wine_options;
            info.data_drink_wine_options_others = member.data_drink_wine_options_others;



            info.data_member_brain = member.data_member_brain;
            info.data_member_brain_others = member.data_member_brain_others;

            info.data_member_kidney = member.data_member_kidney;
            info.data_member_kidney_others = member.data_member_kidney_others;

            info.data_member_artery = member.data_member_artery;
            info.data_member_artery_others = member.data_member_artery_others;

            info.data_member_eye = member.data_member_eye;
            info.data_member_eye_others = member.data_member_eye_others;

            info.data_member_nerve = member.data_member_nerve;
            info.data_member_nerve_others = member.data_member_nerve_others;

            info.data_member_other_disease = member.data_member_other_disease;
            info.data_member_other_disease_others = member.data_member_other_disease_others;

            info.data_member_address = member.data_member_address;
            info.data_member_winxin = member.data_member_winxin;
            info.data_member_qq = member.data_member_qq;
            info.data_member_email = member.data_member_email;


            info.updatedby_code = WebContext.User.Identity.GetUserId();
            info.updatedby_name = WebContext.User.Identity.GetUserName();
            info.dateupdated = DateTime.Now;




            info.data_member_blood_comments = member.data_member_blood_comments;
            info.data_member_diseases_comments = member.data_member_diseases_comments;
            info.data_member_operations_comments = member.data_member_operations_comments;
            info.data_member_wound_comments = member.data_member_wound_comments;



            info.data_member_diseases_1 = member.data_member_diseases_1;
            info.data_member_diseases_1_relationship = member.data_member_diseases_1_relationship;
            info.data_member_diseases_1_text = member.data_member_diseases_1_text;


            info.data_member_diseases_2 = member.data_member_diseases_2;
            info.data_member_diseases_2_relationship = member.data_member_diseases_2_relationship;
            info.data_member_diseases_2_text = member.data_member_diseases_2_text;


            info.data_member_diseases_3= member.data_member_diseases_3;
            info.data_member_diseases_3_relationship = member.data_member_diseases_3_relationship;
            info.data_member_diseases_3_text = member.data_member_diseases_3_text;


            info.data_member_blood_bit = member.data_member_blood_bit;
            info.data_member_diseases_bit = member.data_member_diseases_bit;
            info.data_member_operations_bit = member.data_member_operations_bit;
            info.data_member_wound_bit = member.data_member_wound_bit;

            //Tsinghua

            info.data_drink_average1 = member.data_drink_average1; //每次平均多少两
            info.data_drink_average2 = member.data_drink_average2; //每次平均多少瓶

            //民族

            info.data_member_ethnic = member.data_member_ethnic;//民族信息


            DataContext.SaveChanges();

        }


        /// <summary>
        /// 更新锻炼情况
        /// </summary>
        /// <param name="habbit"></param>
        public Result<bool> UpdateMemberPhysicalInfo(SportHabbit habbit)
        {

            var log = new data_exception();
            log.data_exception_parameters = JsonConvert.SerializeObject(habbit);
            log.data_exception_date = DateTime.Now;

            DataContext.data_exception.Add(log);
            DataContext.SaveChanges();



            var res = new Result<bool>();

            try
            {
                data_member_information file = DataContext.data_member_information.FirstOrDefault(a => a.data_member_id == habbit.data_member_id);

                if (file == null)
                {
                    file = new data_member_information();
                    file.data_member_id = habbit.data_member_id;
                    DataContext.data_member_information.Add(file);
                }
                file.data_physical_rate = habbit.sportrate;
                file.data_physical_time = habbit.eachtime;
                file.data_physical_total_time = habbit.totaltime;
                file.data_physical_place = habbit.sportlocation;
                file.data_physical_schedule = habbit.sportschedule;
                file.data_physical_item = habbit.favoritesport;

                DataContext.SaveChanges();

                res.IsSuccessful = true;
                res.Parameter = true;
                res.Message = string.Empty;
            }
            catch (Exception exception)
            {

                res.Message = exception.Message;
                //  if (exception.InnerException != null)
                res.Message = res.Message + exception.Source;
                res.Message = res.Message + exception.StackTrace;
            }
            return res;


        }

        /// <summary>
        /// 更新生活习惯
        /// </summary>
        /// <param name="habbit"></param>
        public Result<bool> UpdateMemberLiveHabbit(LiveHabbit habbit)
        {

            var log = new data_exception();
            log.data_exception_parameters = JsonConvert.SerializeObject(habbit);
            log.data_exception_date = DateTime.Now;

            DataContext.data_exception.Add(log);
            DataContext.SaveChanges();




            var res = new Result<bool>();

            try
            {


                data_member_information file = DataContext.data_member_information.FirstOrDefault(a => a.data_member_id == habbit.data_member_id);

                if (file == null)
                {
                    file = new data_member_information();
                    file.data_member_id = habbit.data_member_id;
                    DataContext.data_member_information.Add(file);
                }


                file.data_smoke = habbit.smoke;
                file.data_smoke_age = habbit.smokeage;
                file.data_smoke_day = habbit.smokeday;
                file.data_drink = habbit.drinkrate;//
                file.data_drink_average1 = habbit.average1;
                file.data_drink_average2 = habbit.average2;
                file.data_drink_Isdry = habbit.isdry;
              
                
                if(habbit.isdry ==true)
                    file.data_drink_Isdry_date = habbit.drinkdrydate;



                file.data_drink_wine_options = string.IsNullOrEmpty(habbit.drinkoptions)?string.Empty:habbit.drinkoptions;
                file.data_drink_wine_options_others = string.IsNullOrEmpty(habbit.drinkoptionscomments) ? string.Empty : habbit.drinkoptionscomments;

                file.data_food = habbit.food;

                DataContext.SaveChanges();

                res.IsSuccessful = true;


            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                //  if (exception.InnerException != null)
                res.Message = res.Message + exception.Source;
                res.Message = res.Message + exception.StackTrace;

                res.Parameter = false;
            }
            return res;

        }

        public Result<bool> UpdateMemberInformation(MemberInfo info)
        {


            var log = new data_exception();
            log.data_exception_parameters = JsonConvert.SerializeObject(info);
            log.data_exception_date = DateTime.Now;

            DataContext.data_exception.Add(log);
            DataContext.SaveChanges();



            var res = new Result<bool>();
            try
            {



                data_member_information file = DataContext.data_member_information.FirstOrDefault(a => a.data_member_id == info.data_member_id);

                if (file == null)
                {
                    file = new data_member_information {data_member_id = info.data_member_id};
                    DataContext.data_member_information.Add(file);
                }

                file.data_member_identity = info.identifytext;
                file.data_member_country = info.nationality;
                file.data_member_ethnic = info.data_member_ethnic;

                if(info.bloodtype!=0)
                    file.data_member_blood = info.bloodtype;
                
                if(info.rhproperty!=0)
                file.data_member_rh = info.rhproperty;
                if(info.job !=0)
                file.data_member_profession = info.job;

                file.data_member_disease = info.diseases;//发现的疾病
                file.data_member_disease_c = info.cancer;//恶性肿瘤
                file.data_member_other_disease_others = info.otherdiseases;

                //file.data_member_disease_1 = info.disease1;

                //if(!string.IsNullOrEmpty(info.disease1))
                //    file.data_member_disease_1_date = info.disease1_date;

                //file.data_member_disease_2 = info.disease2;
                //if (!string.IsNullOrEmpty(info.disease2))
                //file.data_member_disease_2_date = info.disease2_date;

                //file.data_member_disease_3 = info.disease3;
                //if (!string.IsNullOrEmpty(info.disease3))
                //file.data_member_disease_3_date = info.disease3_date;


                //file.data_member_disease_4 = info.disease4;
                //if (!string.IsNullOrEmpty(info.disease4))
                //file.data_member_disease_4_date = info.disease4_date;

                //file.data_member_disease_5 = info.disease5;
                //if (!string.IsNullOrEmpty(info.disease5))
                //file.data_member_disease_5_date = info.disease5_date;

                //file.data_member_disease_6 = info.disease6;
                //if (!string.IsNullOrEmpty(info.disease6))
                //file.data_member_disease_6_date = info.disease6_date;


                //file.data_member_surgery = info.issurgery;
                //file.data_member_surgery_1 = info.surgery1;
                //if(!string.IsNullOrEmpty(info.surgery1))
                //    file.data_member_surgery_1_date = info.surgery1_date;

                //file.data_member_surgery_2 = info.surgery2;

                //if(!string.IsNullOrEmpty(info.surgery2))
                //    file.data_member_surgery_2_date = info.surgery2_date;



                //file.data_member_injury = info.iswound;
                //file.data_member_injury_1 = info.wound1;

                //if(!string.IsNullOrEmpty(info.wound1))
                //file.data_member_injury_1_date = info.wound1_date;

                //file.data_member_injury_2 = info.wound2;

                //if(!string.IsNullOrEmpty(info.wound2))
                //file.data_member_injury_2_date = info.wound2_date;

                //file.data_member_blood_transfer = info.isblood;
                //file.data_member_blood_transfer_1_reason = info.blood1;

                //if(!string.IsNullOrEmpty(info.blood1))
                //file.data_member_blood_transfer_1_date = info.blood1_date;

                //file.data_member_blood_transfer_2_reason = info.blood2;

                //if(!string.IsNullOrEmpty(info.blood2))
                //file.data_member_blood_transfer_2_date = info.blood2_date;


                //file.data_member_family_father = info.father_disease;
                //file.data_member_family_mother = info.mother_disease;
                //file.data_member_family_brothers = info.sister_disease;
                //file.data_member_family_children = info.children_disease;
                //file.data_member_inherited = info.isinherited;
                //file.data_member_inherited_disease = info.inherited_disease;


                //
                file.data_member_ethnic = info.data_member_ethnic;
                file.data_member_diseases_comments = info.diseases_comments;
                file.data_member_blood_comments = info.blood_comments;
                file.data_member_wound_comments = info.wound_comments;
                file.data_member_operations_comments = info.operstions_comments;

                file.data_member_diseases_1 = info.disease_1;
                file.data_member_diseases_1_text = info.disease_1_text;
                file.data_member_diseases_1_relationship = info.disease_1_relationship;


                file.data_member_diseases_2 = info.disease_2;
                file.data_member_diseases_2_text = info.disease_2_text;
                file.data_member_diseases_2_relationship = info.disease_2_relationship;

                file.data_member_diseases_3 = info.disease_3;
                file.data_member_diseases_3_text = info.disease_3_text;
                file.data_member_diseases_3_relationship = info.disease_3_relationship;



                file.data_member_blood_bit = info.data_member_blood_bit;
                file.data_member_diseases_bit = info.data_member_diseases_bit;
                file.data_member_operations_bit = info.data_member_operations_bit;
                file.data_member_wound_bit = info.data_member_wound_bit;


                DataContext.SaveChanges();

                res.IsSuccessful = true;
                res.Parameter = true;

            }
            catch (Exception exception)
            {

                res.Message = exception.Message;
                //  if (exception.InnerException != null)
                res.Message = res.Message + exception.Source;
                res.Message = res.Message + exception.StackTrace;

                res.IsSuccessful = false;
                res.Parameter = false;
            }
            return res;
        }




        public data_code_services_tasks GetTask(int taskId)
        {
            if (taskId == 0)
                return null;
            else
            {
                var findresult = DataContext.data_code_services_tasks.FirstOrDefault(a => a.data_code_services_tasks_id == taskId);
                if (findresult == null)
                    return null;

                return findresult;

            }
        }


        /// <summary>
        /// 获得血型
        /// </summary>
        /// <returns></returns>
        public IQueryable<SelectListItem> ListBloods()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_blood"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        /// <summary>
        /// RH
        /// </summary>
        /// <returns></returns>
        public IQueryable<SelectListItem> ListRH()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_rh"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        /// <summary>
        /// 职业
        /// </summary>
        /// <returns></returns>
        public IQueryable<SelectListItem> ListProfession()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_profession"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        /// <summary>
        /// 疾病
        /// </summary>
        /// <returns></returns>
        public IQueryable<SelectListItem> ListDisease()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_disease"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListPhysicalRate()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_member_physical"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        //锻炼场所
        //data_physical_place
        public IQueryable<SelectListItem> ListPhysicalPlace()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_physical_place"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        //运动时间段
        //data_physical_place
        public IQueryable<SelectListItem> ListPhysicalSchedule()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_physical_schedule"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        //吸烟状况
        //data_smoke
        public IQueryable<SelectListItem> ListSmoke()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_smoke"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        //饮酒情况
        //data_drink
        public IQueryable<SelectListItem> ListDrink()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_drink"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }
        //饮酒种类
        //data_drink_wine_options







    }
}
