﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services.MemberFile
{
    /// <summary>
    /// 生活方式
    /// </summary>
    public class LiveHabbit
    {

        /// <summary>
        /// 会员编号
        /// </summary>
        public int data_member_id { get; set; }

        public string food { get; set; }

        public int smoke { get; set; }
        /// <summary>
        /// 烟龄 
        /// </summary>
        public int smokeage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal smokeday { get; set; }

        public int drinkrate { get; set; }

        /// <summary>
        /// 每次平均XX两
        /// </summary>
        public decimal average1 { get; set; }

        /// <summary>
        /// 每次平均XX瓶
        /// </summary>
        public decimal average2 { get; set; }

        /// <summary>
        /// 是否戒酒
        /// </summary>
        public bool isdry { get; set; }
        /// <summary>
        /// 戒酒时间
        /// </summary>
        public DateTime drinkdrydate { get; set; }
        /// <summary>
        /// 饮酒种类而
        /// </summary>
        public string drinkoptions { get; set; }
        /// <summary>
        /// 其他饮酒种类
        /// </summary>
        public string drinkoptionscomments { get; set; }


    }
}
