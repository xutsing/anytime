﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services.MemberFile
{

    public class SportHabbit
    {
        /// <summary>
        /// 会员编号
        /// </summary>
        public int data_member_id { get; set; }
        /// <summary>
        /// 坚持锻炼时间
        /// </summary>
        public decimal totaltime { get; set; }
        /// <summary>
        /// 每次锻炼时间
        /// </summary>
        public int eachtime { get; set; }
        /// <summary>
        /// 锻炼频率
        /// </summary>
        public int sportrate { get; set; }
        /// <summary>
        /// 运动时间段
        /// </summary>
        public int sportschedule { get; set; }
        /// <summary>
        /// 锻炼位置
        /// </summary>
        public int sportlocation { get; set; }
        /// <summary>
        /// 最喜爱的运动
        /// </summary>
        public string favoritesport { get; set; }
    }
}
