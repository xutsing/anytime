﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services.MemberFile
{
    /// <summary>
    /// 会员档案中的基本信息，疾病史，手术外伤输血
    /// </summary>
    public class MemberInfo
    {

        /// <summary>
        /// 会员编号
        /// </summary>
        public int data_member_id { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string identifytext { get; set; }

        /// <summary>
        /// 国家
        /// </summary>
        public string nationality { get; set; }

        /// <summary>
        /// 血型
        /// </summary>
        public int bloodtype { get; set; }

        /// <summary>
        /// rh property
        /// </summary>
        public int rhproperty { get; set; }

        /// <summary>
        /// 职业
        /// </summary>
        public int job { get; set; }

        /// <summary>
        /// 疾病
        /// </summary>
        public string diseases { get; set; }

        /// <summary>
        /// 恶性肿瘤
        /// </summary>
        public string cancer { get; set; }

        /// <summary>
        /// 其他疾病
        /// </summary>
        public string otherdiseases { get; set; }

        /// <summary>
        /// 疾病1
        /// </summary>
       // public string disease1 { get; set; }
        /// <summary>
        /// 疾病1发现时间
        /// </summary>
      //  public DateTime disease1_date { get; set; }

        /// <summary>
        /// 疾病2
        /// </summary>
       // public string disease2 { get; set; }
        /// <summary>
        /// 疾病2发现时间
        /// </summary>
       // public DateTime disease2_date { get; set; }

        /// <summary>
        /// 疾病3
        /// </summary>
       // public string disease3 { get; set; }
        /// <summary>
        /// 疾病3发现时间
        /// </summary>
        //public DateTime disease3_date { get; set; }

        /// <summary>
        /// 疾病4
        /// </summary>
       // public string disease4 { get; set; }
        /// <summary>
        /// 疾病4发现时间
        /// </summary>
       // public DateTime disease4_date { get; set; }

        /// <summary>
        /// 疾病5
        /// </summary>
       // public string disease5 { get; set; }
        /// <summary>
        /// 疾病5发现时间
        /// </summary>
       // public DateTime disease5_date { get; set; }

        /// <summary>
        /// 疾病6
        /// </summary>
       // public string disease6 { get; set; }
        /// <summary>
        /// 疾病6发现时间
        /// </summary>
       // public DateTime disease6_date { get; set; }

        /// <summary>
        /// 是否手术
        /// </summary>
       // public bool issurgery { get; set; }
        /// <summary>
        /// 手术1
        /// </summary>
       // public string surgery1 { get; set; }
        /// <summary>
        /// 手术1时间
        /// </summary>
      //  public DateTime surgery1_date { get; set; }

        /// <summary>
        /// 手术2
        /// </summary>
       // public string surgery2 { get; set; }
        /// <summary>
        /// 手术2时间
        /// </summary>
       // public DateTime surgery2_date { get; set; }

        /// <summary>
        /// 是否受过外伤
        /// </summary>
        //public bool iswound { get; set; }
        /// <summary>
        /// 外伤1
        /// </summary>
       // public string wound1 { get; set; }
        /// <summary>
        /// 外伤1时间
        /// </summary>
       // public DateTime wound1_date { get;set;}

        /// <summary>
        /// 外伤2
        /// </summary>
       // public string wound2 { get; set; }
        /// <summary>
        /// 外伤2时间
        /// </summary>
     //   public DateTime wound2_date { get; set; }
//
        /// <summary>
        /// 是否输血
        /// </summary>
       // public bool isblood { get; set; }
        /// <summary>
        /// 第一次输血
        /// </summary>
       // public string blood1 { get;set;}
      //  public DateTime blood1_date { get; set; }

        /// <summary>
        /// 第二次输血
        /// </summary>
       // public string blood2 { get; set; }
      //  public DateTime blood2_date { get; set; }

        /// <summary>
        /// 父亲
        /// </summary>
       // public string father_disease { get; set; }

        /// <summary>
        /// 母亲
        /// </summary>
       // public string mother_disease { get; set; }

        /// <summary>
        /// 兄弟姐妹
        /// </summary>
       // public string sister_disease { get; set; }

        /// <summary>
        /// 子女疾病
        /// </summary>
       // public string children_disease { get; set; }

        /// <summary>
        /// 是否有遗传病史
        /// </summary>
       // public bool isinherited { get; set; }

        /// <summary>
        /// 遗传病
        /// </summary>
       // public string inherited_disease { get; set; }



        //2015/10/12补充
        /// <summary>
        /// 会员民族
        /// </summary>
        public string data_member_ethnic { get; set; }

        /// <summary>
        /// 会员疾病简介
        /// </summary>
        public string diseases_comments { get; set; }
        /// <summary>
        /// 会员手术简介
        /// </summary>
        public string operstions_comments { get; set; }
        /// <summary>
        /// 会员外伤简介
        /// </summary>
        public string wound_comments { get;set;}
        /// <summary>
        /// 会员输血简介
        /// </summary>
        public string blood_comments { get; set; }

        /// <summary>
        /// 会员疾病1
        /// </summary>
        public bool disease_1 { get; set; }
        /// <summary>
        /// 会员疾病1说明
        /// </summary>
        public string disease_1_text { get;set;}
        /// <summary>
        /// 会员疾病1 关系
        /// </summary>
        public string disease_1_relationship { get; set; }

        public bool disease_2 { get; set; }
        public string disease_2_text { get; set; }
        public string disease_2_relationship { get; set; }

        public bool disease_3 { get; set; }
        public string disease_3_text { get; set; }
        public string disease_3_relationship { get; set; }


        /// <summary>
        /// 是否有过疾病
        /// </summary>
        public bool data_member_diseases_bit { get; set; }
        /// <summary>
        /// 是否动过手术
        /// </summary>
        public bool data_member_operations_bit { get; set; }
        /// <summary>
        /// 是否动过外伤
        /// </summary>
        public bool data_member_wound_bit { get; set; }
        /// <summary>
        /// 是否输过血
        /// </summary>
        public bool data_member_blood_bit { get; set; }



    }
}
