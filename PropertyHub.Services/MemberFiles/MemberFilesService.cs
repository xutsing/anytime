﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Logging;
using Newtonsoft.Json;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;


namespace PropertyHub.Services
{
    public class MemberFilesService : BusinessService
    {
        public IQueryable<data_member_file> ListMemberFiles()
        {
            return from item in DataContext.data_member_file
                   select item;
        }

        //public IQueryable<SelectListItem> ListMembers()
        //{
        //    return from item in DataContext.data_member
        //           select new SelectListItem() { Text = item.data_member_name, Value = item.data_member_id.ToString() };
        //}

        public async Task<data_member_file> Add(Nullable<int> data_member_id, string data_member_file_path)
        {
            data_member_file entity = new data_member_file();
            entity.data_member_id = data_member_id;
            entity.data_member_file_path = data_member_file_path;

            if (Validator.IsValid)
            {
                DataContext.data_member_file.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_member_file> Update(data_member_file entity)
        {
            var service = DataContext.data_member_file.FirstOrDefault(x => x.dat_member_file_id == entity.dat_member_file_id);

            if (service != null)
            {
                service.data_member_id = entity.data_member_id;
                service.data_member_file_path = entity.data_member_file_path;


                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_member_file GetDataMemberFiles(int id)
        {
            return DataContext.data_member_file.SingleOrDefault(x => x.dat_member_file_id == id);
        }

        public string GetMemberName(int id)
        {
            data_member model = DataContext.data_member.SingleOrDefault(x => x.data_member_id == id);
            return model.data_member_name;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_member_file.SingleOrDefaultAsync(x => x.dat_member_file_id == id);
            DataContext.data_member_file.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
