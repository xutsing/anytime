﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class Service : BusinessService
    {
        public IQueryable<data_service> ListService()
        {
            return from item in DataContext.data_service
                   select item;
        }

        public async Task<data_service> Add(data_service entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_service.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_service> Update(data_service entity)
        {
            var service = DataContext.data_service.FirstOrDefault(x => x.data_service_id == entity.data_service_id);

            if (service != null)
            {
                service.data_service_name = entity.data_service_name;
                service.data_service_validate_months = entity.data_service_validate_months;
                service.data_service_price = entity.data_service_price;
                service.data_service_status = entity.data_service_status;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_service_tasks> AddTask(data_service_tasks entity)
        {
            if (entity.data_basetask_id == null || entity.data_basetask_id == 0)
            {
                Validator.AddError("data_basetask_id", "The field is required.");
            }

            entity.data_service_tasks_order = ListServiceTasks(entity.data_service_id ?? 0).Count() + 1;

            if (Validator.IsValid)
            {
                DataContext.data_service_tasks.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        //public async Task<data_service_tasks> UpdateTask(data_service_tasks entity)
        //{
        //    var service = DataContext.data_service_tasks.FirstOrDefault(x => x.data_service_tasks_id == entity.data_service_tasks_id);

        //    if (service != null)
        //    {
        //        service.data_service_id = entity.data_service_id;
        //        service.data_basetask_id = entity.data_basetask_id;
        //        service.data_nexttask_interval = entity.data_nexttask_interval;
        //        service.data_service_tasks_order = entity.data_service_tasks_order;

        //        await DataContext.SaveChangesAsync();
        //    }

        //    return entity;
        //}

        public bool UpdateTask(data_service_tasks entity)
        {
            try
            {
                var service = DataContext.data_service_tasks.FirstOrDefault(x => x.data_service_tasks_id == entity.data_service_tasks_id);
                if (service == null)
                    return false;
                
                service.data_basetask_id = entity.data_basetask_id;
                service.data_nexttask_interval = entity.data_nexttask_interval;
                
                //added by adam 
                service.Iscycle = entity.Iscycle;
                service.cycleDays = entity.cycleDays;
                service.cyclemax = entity.cyclemax;

                DataContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public data_service Get(int id)
        {
            return DataContext.data_service.SingleOrDefault(x => x.data_service_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_service.SingleOrDefaultAsync(x => x.data_service_id == id);
            DataContext.data_service.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task DeleteTask(int id)
        {
            var entity = await DataContext.data_service_tasks.SingleOrDefaultAsync(x => x.data_service_tasks_id == id);

            var currentOrder = entity.data_service_tasks_order;

            var list = ListServiceTasks(entity.data_service_id ?? 0);

            foreach (var item in list)
            {
                if (item.data_service_tasks_order > currentOrder)
                {
                    item.data_service_tasks_order -= 1;
                }
            }

            DataContext.data_service_tasks.Remove(entity);

            await DataContext.SaveChangesAsync();
        }

        public async Task ChangeOrder(int id, int direction)
        {
            var entity = await DataContext.data_service_tasks.SingleOrDefaultAsync(x => x.data_service_tasks_id == id);
            var currentOrder = entity.data_service_tasks_order;

            var targetOrder = direction == 1 ? (currentOrder - 1) : (currentOrder + 1);

            var targetEntity = await DataContext.data_service_tasks.SingleOrDefaultAsync(x => x.data_service_id == entity.data_service_id && x.data_service_tasks_order == targetOrder);

            if (targetEntity != null)
            {
                entity.data_service_tasks_order = targetOrder;
                targetEntity.data_service_tasks_order = currentOrder;

                await DataContext.SaveChangesAsync();
            }
        }

        public data_service_tasks GetServiceTask(int id)
        {
            return DataContext.data_service_tasks.SingleOrDefault(x => x.data_service_tasks_id == id);
        }

        public string GetServiceTaskBaseIndexName(int id)
        {
            var tasks = DataContext.data_service_tasks.SingleOrDefault(x => x.data_service_tasks_id == id);
            
            var basetaskid = tasks.data_basetask_id;
            
            var basetask = DataContext.data_basetask.SingleOrDefault(x => x.data_basetask_id == basetaskid);
            
            string basetaskname = basetask.data_basetask_name;
            
            return basetaskname;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_service"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListBaseTasks()
        {
            return from item in DataContext.data_basetask
                   where item.data_status == 1
                   select new SelectListItem() { Text = item.data_basetask_name, Value = item.data_basetask_id.ToString() };
        }

        public IQueryable<data_service_tasks> ListServiceTasks(int serviceId)
        {
            return from item in DataContext.data_service_tasks
                   where item.data_service_id == serviceId
                   orderby item.data_service_tasks_order
                   select item;
        }
    }
}
