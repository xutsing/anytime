﻿using System.Data.Entity;
using System.Data.Entity.Migrations.History;
using Microsoft.AspNet.Identity.EntityFramework;
using PropertyHub.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using PropertyHub.Data;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public partial class MembershipService : ServiceBase
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        //    private ApplicationRoleManager _roleManager;
        private SubscriptionService _subscriptionService;

        public MembershipService(SubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                if (_signInManager == null)
                {
                    _signInManager = WebContext.GetOwinContext().Get<ApplicationSignInManager>();
                }
                return _signInManager;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    _userManager = WebContext.GetOwinContext().Get<ApplicationUserManager>();
                }
                return _userManager;
            }
        }

        //public ApplicationRoleManager RoleManager
        //{
        //    get
        //    {
        //        if (_roleManager == null)
        //        {
        //            _roleManager = WebContext.GetOwinContext().Get<ApplicationRoleManager>();
        //        }
        //        return _roleManager;
        //    }
        //}


        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return WebContext.GetOwinContext().Authentication;
            }
        }

        public async Task<Result<bool>> ChangePassword(string userId, string newpassword)
        {

            var res = new Result<bool>();

            var token = UserManager.GeneratePasswordResetToken(userId);
            var result = await UserManager.ResetPasswordAsync(userId, token, newpassword);





            // result
            //  var rolemanager = new ();

            if (result.Succeeded)
            {
                res.IsSuccessful = true;
                res.Parameter = false;
                res.Message = string.Empty;
                return res;
            }
            else
            {
                res.IsSuccessful = false;
                res.Parameter = false;
                res.Message = "更新密码失败";
            }

            return res;

        }


        public bool UpdateRoleUsers(string roleId, string UserIds)
        {

            var role = DataContext.Roles.FirstOrDefault(a => a.Id == roleId);

            if (role == null)
                return false;

            var rolename = role.Name;

            //需要首先把所有的人员清空。

            foreach (var user in role.Users)
            {
                UserManager.RemoveFromRoles(user.UserId, rolename);

               
            }



            string[] users = UserIds.Split(',');
            foreach (var user in users)
            {
                UserManager.AddToRole(user, rolename);
            }

            return true;
        }



        public async Task<bool> LockUser(string Id)
        {
            //  var result = await UserManager.SetLockoutEnabledAsync(Id, true);

            var user = UserManager.Users.FirstOrDefault(a => a.Id == Id);

            var deleteresult = await UserManager.DeleteAsync(user);

            //  UserManager.()
            if (deleteresult.Succeeded)
                return true;
            else
            {
                return false;
            }

        }

        public async Task<bool> UpdateRoe(string Id, string rolename)
        {
            var role = DataContext.Roles.FirstOrDefault(a => a.Id == Id);
            if (role != null)
            {
                role.Name = rolename;
                await this.DataContext.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> CreateRole(string rolename)
        {
            var newrole = new IdentityRole();
            newrole.Id = Guid.NewGuid().ToString();
            newrole.Name = rolename;
            this.DataContext.Roles.Add(newrole);
            await this.DataContext.SaveChangesAsync();
            return true;
        }


        public async Task<bool> RegisterAsSubscription(ApplicationUser user, Subscription subscription,
            string password, string confirmPassword)
        {
            if (_subscriptionService.Validate(subscription))
            {
                var userValidator = UserManager.UserValidator as UserValidator<ApplicationUser>;
                userValidator.RequireUniqueEmail = false;
                var validateResult = await userValidator.ValidateAsync(user);
                var passwordResult = await UserManager.PasswordValidator.ValidateAsync(password);
                if (validateResult.Succeeded && passwordResult.Succeeded)
                {

                    //var subscription2 = await _subscriptionService.Add(subscription);
                    user.SubscriptionId = 1;// subscription2.Id;
                    user.UserName = string.Format("{0}.{1}", -1, user.Email);

                    var result = await UserManager.CreateAsync(user, password);
                    AddErrors(result);
                }
                else
                {
                    AddErrors(validateResult);
                    AddErrors(passwordResult);
                }
            }

            return Validator.IsValid;
        }

        public async Task<bool> UpdateAsSubscription(ApplicationUser user, Subscription subscription)
        {
            //   if (_subscriptionService.Validate(subscription))
            //   {
            var userValidator = UserManager.UserValidator as UserValidator<ApplicationUser>;
            userValidator.RequireUniqueEmail = false;
            var validateResult = await userValidator.ValidateAsync(user);
            if (validateResult.Succeeded)
            {
                var currentuser = UserManager.Users.FirstOrDefault(a => a.Id == user.Id);
                currentuser.FirstName = user.FirstName;
                currentuser.LastName = user.LastName;
                currentuser.Email = user.Email;
                currentuser.UserName = string.Format("{0}.{1}", -1, user.Email);



                user.SubscriptionId = 1;// subscription2.Id;




                try
                {
                    var result = await UserManager.UpdateAsync(currentuser);

                    //  await this.ChangePassword(user.Id, password);

                    //UserManager.RemovePassword(currentuser.Id);
                    //  DataContext.Entry(ApplicationUser).State=EntityState.Modified;
                    AddErrors(result);
                }
                catch (Exception exception)
                {


                    throw;
                }





            }
            else
            {
                AddErrors(validateResult);
            }
            //  }

            return Validator.IsValid;

        }




        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                Validator.AddError("", error);
            }
        }

        public async Task<SignInStatus> Login(string username, string password, bool rememberMe)
        {
            username = string.Format("{0}", username).Trim();
            if (SubscriptionId != null)
            {
                username = string.Format("{0}.{1}", SubscriptionId, username);
            }
            var result = await SignInManager.PasswordSignInAsync(username, password, rememberMe, shouldLockout: false);
            return result;
        }

        public void SignOut()
        {
            AuthenticationManager.SignOut();
        }

        public IdentityRole GetRoleById(string id)
        {
            return DataContext.Roles.FirstOrDefault(a => a.Id == id);
        }
        public bool DeleteRoleById(string Id)
        {
            var findrole = DataContext.Roles.FirstOrDefault(a => a.Id == Id);
            if (findrole != null)
            {
                DataContext.Roles.Remove(findrole);
                DataContext.SaveChanges();
                return true;
            }
            return false;
        }


        public IQueryable<ApplicationUser> GetUsers()
        {
            return DataContext.Users.AsQueryable();
        }


        public ApplicationUser GetUserbyId(string Id)
        {
            var user = DataContext.Users.FirstOrDefault(a => a.Id == Id);



            //if(user==null)
            return user;
        }

        //added by adam 2015-08-07
        //get user by username
        public ApplicationUser GetUserbyUserName(string userName)
        {
            return DataContext.Users.FirstOrDefault(a => a.UserName == userName);
        }
        //added by adam 2015-08-07
        //get current log in user
        //only export basic info to use for security reason
        public ApplicationUser GetCurrentLogonUser()
        {
            return GetUserbyUserName(this.WebContext.User.Identity.Name);
        }

        public IQueryable<SelectListItem> GetUserItems()
        {
            return from item in DataContext.Users
                   select new SelectListItem() { Text = item.FirstName + " " + item.LastName, Value = item.Id.ToString() };
        }


        public IQueryable<string> GetUsersByRole(string Id)
        {

            var role = DataContext.Roles.FirstOrDefault(a => a.Id == Id);
            return role.Users.Select(a => a.UserId).AsQueryable();

        }

        public void  UpdateRolePermissions(string roleId, string permissionIds)
        {
            //首先把现在角色的权限数据清除掉.
            var sql = "delete from ModuleRolePermissions where RoleId='{0}'";
            sql = string.Format(sql,roleId);

            var executeres=DataContext.Database.ExecuteSqlCommand(sql);

            string[] permissions = permissionIds.Split(',');
            foreach (var permission in permissions)
            {
                var newpermission = new ModuleRolePermissions();
                newpermission.RoleId = roleId;
                newpermission.IsModule = true;
                newpermission.PermissionId = int.Parse(permission);
                newpermission.SubscriptionId = 1;
                newpermission.RecordStatus = 0;

                DataContext.RolePermissions.Add(newpermission);
            }
            DataContext.SaveChanges();


 
        }

        public string GetRolePermissionids(string roleId)
        {
            string Ids = string.Empty;
            var rolePermissions = DataContext.RolePermissions.Where(a => a.RoleId == roleId);

            int i = 0;
            foreach (var permission in rolePermissions)
            {

                if (i == 0)
                {
                    Ids = permission.PermissionId.ToString();
                }
                else
                {
                    Ids = Ids + "," + permission.PermissionId.ToString();
                }

                i++; 
            }

            return Ids;
        }

        public IQueryable<PermissionNode> GetRolePermission(string RoleId, int? moduleId)
        {

            var permissions = new List<PermissionNode>();

            //这说明这是一个Top tree node.
            if (moduleId == null)
            {
                var modules = DataContext.Modules.Where(a => a.ParentId.HasValue == false).ToList();
                foreach (var module in modules)
                {
                    var node = new PermissionNode();

                    node.Id = module.Id;
                    node.Text = module.ModuleName;
                    node.permissiontype = PermissionType.Module;
                    permissions.Add(node);
                }

              //  return permissions.AsQueryable();
            }
            else
            {
                //查找当前进来的Module
                var childrenmodules = DataContext.Modules.Where(a => a.ParentId.Value == moduleId).ToList();
                foreach (var child in childrenmodules)
                {
                    var childnode = new PermissionNode();
                    childnode.Id = child.Id;
                    childnode.Text = child.ModuleName;
                    childnode.permissiontype = PermissionType.Module;

                    permissions.Add(childnode);
                }

                //如果当前没有children.
                //说明当前节点可能是
                if (childrenmodules.Count() == 0)
                {
                 //   var childrenpersmissions=DataContext.ModulePermissions.Where(a=>a.)


                    var currentmodel = DataContext.Modules.FirstOrDefault(a => a.Id == moduleId);
                    if (currentmodel != null)
                    {
                        //查找一下
                        foreach (var modulepermission in currentmodel.Permissions)
                        {
                            var childnode = new PermissionNode();
                            childnode.Id = modulepermission.PermissionId;
                            childnode.Text = modulepermission.PermissionName;
                            childnode.permissiontype = PermissionType.Permission;

                            permissions.Add(childnode);
                        }                    
                    }               
                }


            }

    

            //检查一下当前权限点，当前角色的权限里面是否有，如果有的话,则Checked.
            foreach (var permission in permissions)
            {

                var findpermission = DataContext.RolePermissions.FirstOrDefault(a =>
                    a.RoleId == RoleId && a.PermissionId == permission.Id);

                if (findpermission != null)
                    permission.Checked = true;
                else
                    permission.Checked = false;            
            }

            //返回权限点
            return permissions.AsQueryable();


        }

        public IQueryable<PermissionNode> GetRolePermission(string RoleId)
        {
            //返回值.
            var permissions = new List<PermissionNode>();

            //找到第一级别的节点.
            var modules = DataContext.Modules.Where(a => a.ParentId.HasValue == false).ToList();


            foreach (var module in modules)
            {
                var node = new PermissionNode();

                node.Id = module.Id;
                node.Text = module.ModuleName;
                node.permissiontype = PermissionType.Module;


                var childrenmodules = DataContext.Modules.Where(a => a.ParentId.Value == module.Id).ToList();
                permissions.Add(node);



                foreach (var child in childrenmodules)
                {
                    var childnode = new PermissionNode();
                    childnode.Id = child.Id;
                    childnode.Text = child.ModuleName;
                    childnode.permissiontype = PermissionType.Module;

                    node.Children.Add(childnode);


                    // DataContext.Roles

                    // var permissions=this.DataContext.ModulePermissions.Where(a=>a.m)


                }



            }

            return permissions.AsQueryable();
        }

        //  added by adam 2015-08-06
        public IQueryable<ApplicationUser> GetUsersByRoleName(string roleName)
        {
            var roleID = DataContext.Roles.FirstOrDefault(a => a.Name == roleName).Id;
            return from user in DataContext.Users
                   where user.Roles.Select(r => r.RoleId).Contains(roleID)
                   select user;
        }
        public IQueryable<SelectListItem> ListSecratries()
        {           
            return GetUsersByRoleName("健身秘书").Select<ApplicationUser, SelectListItem>(u => new SelectListItem() {Text = u.FirstName + u.LastName, Value = u.Id} );
        }

        //added by adam 2015-08-22
        //get action permission of the login user
        public IQueryable<PermissonInfo> GetPermissionsByControllerAndAction(string controllerName, string actionName)
        {
            if (actionName.ToLower() == "index")
                actionName = "";

            var url = "/" + controllerName.ToLower() + "/" + actionName.ToLower();
            if (actionName == "")
            {
                url = "/" + controllerName.ToLower();
            }          

            int mid = (from module in DataContext.Modules
                       where module.ModuleUrl == url
                       select module.Id).FirstOrDefault();

            var user = GetCurrentLogonUser();

            if (user == null)
                return null;

            var roles = "";
            foreach (var u_r in user.Roles)
            {
                roles += "," + u_r.RoleId + ",";
            }

            var permissions = (from mrp in DataContext.RolePermissions
                               from r in DataContext.Roles
                               from mp in DataContext.ModulePermissions
                               from m in (from module in DataContext.Modules where module.Id == mid select module)
                               where mrp.RoleId == r.Id && mrp.PermissionId == mp.PermissionId && roles.Contains(r.Id) && m.Permissions.Contains(mp)
                               select new PermissonInfo
                               {
                                   ModuleID = mid,
                                   PermissionID = mrp.PermissionId,
                                   PermissionName = mp.PermissionName
                               }).Distinct();

            return permissions;
        }

        public class PermissonInfo 
        {
            public int ModuleID { get; set; }
            public int PermissionID { get; set; }
            public string PermissionName { get; set; }
        }
    }
}
