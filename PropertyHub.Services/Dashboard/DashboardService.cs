﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PropertyHub.Services
{
    public class DashboardService : BusinessService
    {

        public int GetMemberCount()
        {
            return DataContext.data_member.Count();
        }
        public int GetTodayMemberCount()
        {
            DateTime today=DateTime.Parse(DateTime.Now.ToString());
            return DataContext.data_member.Where(a => a.datecreated.Value > today).Count();
        }

        public int GetServicesCount()
        {
            return DataContext.data_base_index.Count();
        }
        public int GetTodayServicesCount()
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString());
            return DataContext.data_base_index.Where(a => a.datecreated.Value > today).Count();
        }

        public int GetCoursesCount()
        {
            return DataContext.data_code_services.Count();
        }

        public int GetTodayCoursesCount()
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString());
            return DataContext.data_code_services.Where(a => a.datecreated.Value > today).Count();
        }

        public int GetTasks()
        {
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            return DataContext.sp_getmytasks(userId).Count(); ;
        }

        public int GetUnProcessedTasks()
        {
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();

            var currentMax = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString()).AddSeconds(-1);

            //tsing 
            //added on 2015/08/25
            //
            return DataContext.sp_getmytasks(userId)
                .Where(a=>
                    a.data_code_services_tasks_status!=30
                && a.data_code_service_date <= currentMax
                ).Count(); 
        }


        public int GetTodayTasks()
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString());
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            return DataContext.sp_getmytasks(userId).Where(a => a.datecreated.Value > today).Count(); 
        }

        public int GetMessages()
        {
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            return DataContext.data_message.Count(a => a.message_service_id == userId);
        }

        public int GetUnProcessedMessages()
        {
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            return DataContext.data_message.Where(a => a.message_service_id == userId &&a.message_Isreply==false).Count();
        }


        public int GetTodayMessages()
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString());
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            return DataContext.data_message.Where(a => (a.message_service_id == userId && a.message_date.Value > today)).Count();
            //return 0;
        }

        public int GetMyChecksCount()
        {
            return 0;
        }

        public int GetTodayMyChecksCount()
        {
            DateTime today = DateTime.Parse(DateTime.Now.ToString());
            return 0;
        }
    }
}
