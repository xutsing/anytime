﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    /// <summary>
    /// created by adam 2015-08-04 the service of machine type module
    /// database table: data_machine_type 
    /// </summary>
    public class MachineTypeService : BusinessService
    {
        public IQueryable<data_machine_type> ListMachineTypes()
        {
            return from item in DataContext.data_machine_type
                   select item;
        }

        public async Task<data_machine_type> Add(data_machine_type entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_machine_type.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_machine_type> Update(data_machine_type entity)
        {
            var service = DataContext.data_machine_type.FirstOrDefault(x => x.data_machine_type_id == entity.data_machine_type_id);

            if (service != null)
            {
                service.data_machine_type_id = entity.data_machine_type_id;
                service.data_machine_type_title = entity.data_machine_type_title;         
                service.dateupdated = entity.dateupdated;
                service.updatedby = entity.updatedby;
                
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_machine_type Get(int id)
        {
            return DataContext.data_machine_type.SingleOrDefault(x => x.data_machine_type_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_machine_type.SingleOrDefaultAsync(x => x.data_machine_type_id == id);
            DataContext.data_machine_type.Remove(entity);
            await DataContext.SaveChangesAsync();
        }
    }
}
