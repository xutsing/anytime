﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    /// <summary>
    /// created by adam 2015-08-04 the service of machine module
    /// database table: data_machine_action 
    /// associate tables: data_dictionary, data_machine_action_type
    /// </summary>
    public class MachineActionService : BusinessService
    {
        public IQueryable<data_machine_action> ListMachineActions()
        {
            return from item in DataContext.data_machine_action
                   select item;
        }

        public async Task<data_machine_action> Add(data_machine_action entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_machine_action.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_machine_action> Update(data_machine_action entity)
        {
            var service = DataContext.data_machine_action.FirstOrDefault(x => x.data_machine_action_id == entity.data_machine_action_id);

            if (service != null)
            {
                service.data_machine_action_id = entity.data_machine_action_id;
                service.data_machine_action_title = entity.data_machine_action_title;              
                service.data_machine_action_path = entity.data_machine_action_path;
                service.data_machine_action_remark = entity.data_machine_action_remark;
                service.dateupdated = entity.dateupdated;
                service.updatedby = entity.updatedby;
                service.data_machine_id = entity.data_machine_id;
                service.data_machine_action_status = entity.data_machine_action_status;
                
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_machine_action Get(int id)
        {
            return DataContext.data_machine_action.SingleOrDefault(x => x.data_machine_action_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_machine_action.SingleOrDefaultAsync(x => x.data_machine_action_id == id);
            DataContext.data_machine_action.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_machine_action"
                   select new SelectListItem() {Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListMachines()
        {
            return from item in DataContext.data_machine
                   where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_machine_name, Value = item.data_machine_id.ToString() };
        }
    }
}
