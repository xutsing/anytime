﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PropertyHub.Data;

namespace PropertyHub.Services
{

    /// <summary>
    /// 这个Service是专门用来操作会员的.
    /// </summary>
    public class MemberService : BusinessService
    {

        public Result<int> RegisterUser(int? member_id, string name, string loginname, string pw, string mobile, string sex, string birthday, string height = "182", string type = "业余", string source = "1")
        {
            var result = new Result<int>();
            result.Parameter = -1;

            try
            {
                if (member_id.HasValue)
                {
                    //这是一个编辑过程
                    try
                    {



                        var findresult = DataContext.data_member.FirstOrDefault(a => a.data_member_id == member_id);
                        if (findresult == null)
                        {
                            throw new Exception("无法找到会员.");
                        }

                        var checkloginname = DataContext.data_member.FirstOrDefault(a =>
                            a.data_member_loginname == loginname
                            &&
                            a.data_member_id != member_id
                            );
                        if (checkloginname != null)
                        {
                            throw new Exception("登录名重复!");
                        }


                        //

                        findresult.data_member_name = name;
                        findresult.data_member_loginname = loginname;
                        findresult.data_member_pw = pw;
                        findresult.data_member_mobile = mobile;
                        findresult.data_member_sex = int.Parse(sex);
                        findresult.datecreated = DateTime.Now;
                        findresult.dateupdated = DateTime.Now;
                        findresult.data_member_birthday = DateTime.Parse(birthday);

                        findresult.data_member_height = height;//身高
                        findresult.data_member_type_text = type;
                        findresult.data_member_source = source;


                        DataContext.SaveChanges();

                        result.IsSuccessful = true;
                        result.Parameter = findresult.data_member_id;
                        result.Message = string.Empty;

                    }
                    catch (Exception exception)
                    {

                        result.IsSuccessful = false;
                        result.Message = exception.Message;
                        result.Parameter = -1;
                    }

                    return result;
                }

                if (string.IsNullOrEmpty(pw))
                {
                    result.Message = "密码为空";
                    return result;
                }

                if (string.IsNullOrEmpty(mobile))
                {
                    result.Message = "手机号码为空";
                    return result;
                }

                if (string.IsNullOrEmpty(name))
                {
                    result.Message = "姓名不能为空";
                    return result;
                }
                //
                if (string.IsNullOrEmpty(birthday))
                {
                    result.Message = "生日不能为空";
                    return result;
                }


                var checkresult = this.DataContext.data_member.FirstOrDefault(a => a.data_member_loginname == loginname);
                if (checkresult != null)
                {
                    result.Message = "用户名重复..";
                    return result;
                }

                var findloginname = this.DataContext.data_member.FirstOrDefault(a => a.data_member_mobile == mobile);
                if (findloginname != null)
                {
                    result.Message = "手机已经被注册.";
                    return result;
                }

                var newmember = new data_member();
                newmember.data_member_type_id = 3;// general member.
                newmember.data_member_name = name;
                newmember.data_member_loginname = loginname;
                newmember.data_member_pw = pw;
                newmember.data_member_mobile = mobile;
                newmember.data_member_sex = int.Parse(sex);
                newmember.datecreated = DateTime.Now;
                newmember.dateupdated = DateTime.Now;
                newmember.data_member_birthday = DateTime.Parse(birthday);

                newmember.data_member_height = height;//身高
                newmember.data_member_type_text = type;
                newmember.data_member_source = source;

                //Save the new member into the database.
                this.DataContext.data_member.Add(newmember);
                this.DataContext.SaveChanges();


                result.Message = string.Empty;
                result.IsSuccessful = true;
                result.Parameter = newmember.data_member_id;


            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Parameter = -1;
                result.IsSuccessful = false;
            }

            return result;
        }

        /// <summary>
        /// name可以是loginname以及mobile
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pw"></param>
        /// <returns></returns>
        public Result<int> ValidateUser(string name, string pw)
        {

            var log = new data_exception();
            log.data_exception_parameters = name + pw;
            log.data_exception_date = DateTime.Now;

            DataContext.data_exception.Add(log);
            DataContext.SaveChanges();

            var result = new Result<int>();
            result.Parameter = -1;
            try
            {
                var findresult = this.DataContext.data_member.FirstOrDefault(
                a =>

                    (
                    a.data_member_mobile == name
                    ||
                    a.data_member_loginname == name
                     )
                    &&

                    a.data_member_pw == pw
                    );

                if (findresult == null)
                {
                    result.IsSuccessful = false;
                    result.Message = "用户名或者密码不正确.";
                }
                else
                {
                    result.IsSuccessful = true;
                    result.Parameter = findresult.data_member_id;
                }


            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;
                result.Message = ex.Message;
            }
            return result;
        }

        //add by wxw
        //2015-11-4
        public Result<int> ConfirmUser(string loginname, string name, string mobile, string birthday, string height)
        {
            var result = new Result<int>();
            try
            {

                var checkresult = this.DataContext.data_member.FirstOrDefault(a => a.data_member_loginname == loginname);

                if (checkresult == null)
                {
                    result.IsSuccessful = false;
                    result.Message = "用户名不存在.";
                }
                else
                {
                    int num = 0;
                    if (name == checkresult.data_member_name)
                    {
                        num++;
                    }
                    if (mobile == checkresult.data_member_mobile)
                    {
                        num++;
                    }          
                    if (checkresult.data_member_birthday.HasValue && DateTime.Parse(birthday).ToShortDateString()==checkresult.data_member_birthday.Value.ToShortDateString())
                    {
                        num++;
                    }
                    if (height == checkresult.data_member_height)
                    {
                        num++;
                    }

                    if (num < 2)
                    {
                        result.IsSuccessful = false;
                        result.Message = "验证信息不正确.";
                    }
                    else
                    {
                        result.IsSuccessful = true;
                        result.Parameter = checkresult.data_member_id;
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;
                result.Message = ex.Message;
            }

            return result;
        }
        //add by wxw
        //2015-11-04
        public Result<int> ModifyPassword(int? member_id, string password, string confirm_password)
        {
            var result = new Result<int>();
            try
            {
                if (member_id == null)
                {
                    result.IsSuccessful = false;
                    result.Message = "会员号不能为空.";
                }
                else
                {
                    if (password != confirm_password)
                    {
                        result.IsSuccessful = false;
                        result.Message = "密码与验证密码不一致.";
                    }
                    else
                    {
                        var checkresult = this.DataContext.data_member.FirstOrDefault(a => a.data_member_id == member_id);

                        if (checkresult == null)
                        {
                            result.IsSuccessful = false;
                            result.Message = "用户不存在.";
                        }
                        else
                        {
                            checkresult.data_member_pw = password;
                            DataContext.SaveChanges();
                            result.IsSuccessful = true;
                            result.Message = "修改成功.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;
                result.Message = ex.Message;
            }
            return result;

        }


        public IQueryable<data_member> List()
        {
            return from item in DataContext.data_member
                   select item;
        }

        public IQueryable<data_member_checkdata> ListCheckData()
        {
            return from item in DataContext.data_member_checkdata
                   select item;
        }

        public IQueryable<data_member_checkrecord> ListMemberRecords(int? data_member_id)
        {

            if (data_member_id.HasValue == false)
                return DataContext.data_member_checkrecord.AsQueryable();


            var data_member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id
                );
            if (data_member != null)
                return data_member.data_member_checkrecord.AsQueryable();
            else
                return (new List<data_member_checkrecord>()).AsQueryable();
        }








        /// <summary>
        /// 保存监测数据
        /// </summary>
        /// <param name="member_id">会员编号</param>
        /// <param name="checkdate">检测日期</param>
        /// <param name="weight">体重</param>
        /// <param name="water_content">水分含量</param>
        /// <param name="body_content">身体脂肪</param>
        /// <param name="skeleton">骨骼</param>
        /// <param name="bmi">体质指数</param>
        /// <param name="fat">内脏脂肪</param>
        /// <param name="basic_reate">基础代谢率</param>
        /// <param name="muscle_content">肌肉含量</param>
        /// <param name="bmi_value">体质指数值</param>
        /// <returns></returns>
        public Result<int> SaveCheckData(int member_id, string checkdate, string data)
        {

            var res = new Result<int>();

            try
            {

                var currentmember = DataContext.data_member.FirstOrDefault(a => a.data_member_id == member_id);
                if (currentmember == null)
                {
                    res.Message = "会员编号无效.";
                    res.IsSuccessful = false;
                    return res;
                }


                if (string.IsNullOrEmpty(data))
                {
                    res.Message = "没有上传数据.";
                    res.IsSuccessful = false;
                    return res;
                }

                //生成一条上传记录。
                var newrecord = new data_member_checkrecord();
                newrecord.data_member_id = member_id;
                newrecord.data_member_checkrecord_date = DateTime.Parse(checkdate);
                newrecord.datecreated = DateTime.Now;
                newrecord.data_member_checkrecord_remark = "";
                DataContext.data_member_checkrecord.Add(newrecord);


                //string checkdate, string weight, string water_content,
                //string body_content,string skeleton,string bmi,string fat,
                //string basic_reate,string muscle_content,string bmi_value

                //{"weight":"80","water_content","50"}

                var test = new Dictionary<string, string>();
                test.Add("1", "20");
                test.Add("2", "30");

                var teststring = JsonConvert.SerializeObject(test);


                var dataArrays = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                foreach (var _data in dataArrays)
                {
                    var findindex = DataContext.data_base_index.FirstOrDefault(a => a.data_base_key == _data.Key);
                    if (findindex == null)
                    {
                        var newindex = new data_base_index();
                        newindex.data_base_index_title = _data.Key;
                        newindex.data_base_index_title_en = _data.Key;
                        newindex.data_base_key = _data.Key;

                        DataContext.data_base_index.Add(newindex);
                        DataContext.SaveChanges();
                    }

                    var newvalue = new data_member_checkdata();

                    newvalue.data_base_index_id = findindex.data_base_index_id;

                    if (string.IsNullOrEmpty(_data.Value.Trim()))
                    {
                        newvalue.data_member_value = "0";

                    }
                    else
                    {
                        newvalue.data_member_value = _data.Value;

                    }


                    newvalue.datecreated = DateTime.Now;

                    newrecord.data_member_checkdata.Add(newvalue);

                }

                DataContext.SaveChanges();

                res.IsSuccessful = true;
                res.Message = string.Empty;
                res.Parameter = 1;




            }
            catch (Exception ex)
            {
                res.IsSuccessful = false;
                res.Message = ex.Message;
            }

            return res;



        }

        public data_member Update(data_member entity)
        {
            var service = DataContext.data_member.FirstOrDefault(x => x.data_member_id == entity.data_member_id);

            if (service != null)
            {
                service.data_member_type_id = entity.data_member_type_id;
                service.data_member_name = entity.data_member_name;
                service.data_member_mobile = entity.data_member_mobile;
                service.data_member_sex = entity.data_member_sex;
                service.data_member_birthday = entity.data_member_birthday;
                service.data_member_group = entity.data_member_group;
                service.data_member_status = entity.data_member_status;
                service.data_member_email = entity.data_member_email;
                service.data_member_service_id = entity.data_member_service_id;
                service.data_member_service_name = entity.data_member_service_name;
                service.data_member_has_service = entity.data_member_has_service;

                service.data_member_secretary = entity.data_member_secretary;

                service.dateupdated = entity.dateupdated;
                service.updatedby = entity.updatedby;

                DataContext.SaveChanges();
            }

            return entity;
        }

        public data_member Get(int id)
        {
            return DataContext.data_member.Single(x => x.data_member_id == id);
        }



        /// <summary>
        /// 创建一个用户的
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public Result<data_member_advice> CreateMemberAdvice(int data_member_id, string comments, bool isfinished, int data_member_advice_id = 0)
        {
            var res = new Result<data_member_advice>();

            try
            {
                data_member_advice newadvice = null;

                if (data_member_advice_id != 0)
                {
                    newadvice =
                        DataContext.data_member_advice.FirstOrDefault(
                            a => a.data_member_advice_id == data_member_advice_id);

                    if (newadvice == null)
                        newadvice = new data_member_advice();
                }
                else
                {
                    newadvice = new data_member_advice();
                }


                newadvice.data_member_id = data_member_id;
                newadvice.data_member_advice_summary = comments;
                newadvice.data_member_advice_isfinished = isfinished;



                newadvice.createdby = WebContext.User.Identity.GetUserId();
                newadvice.datecreated = DateTime.Now;


                DataContext.data_member_advice.Add(newadvice);
                DataContext.SaveChanges();

                res.IsSuccessful = true;
                res.Message = string.Empty;
                res.Parameter = newadvice;
            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
            }
            return res;
        }


        public Result<bool> UpdateMemberAdvice(int data_member_advice_id, int data_member_id, string comments)
        {
            var res = new Result<bool>();
            try
            {
                var findres =
                    DataContext.data_member_advice.FirstOrDefault(a => a.data_member_advice_id == data_member_advice_id);
                if (findres == null)
                {
                    res.IsSuccessful = false;
                    res.Message = "该数据无法找到.!";
                    return res;
                }

                findres.data_member_advice_summary = comments;


                findres.updatedby = WebContext.User.Identity.GetUserId();
                findres.dateupdated = DateTime.Now;


                DataContext.SaveChanges();
                res.IsSuccessful = true;
                res.Message = string.Empty;
            }
            catch (Exception exception)
            {
                res.Message = exception.Message;
                res.IsSuccessful = false;

            }
            return res;

        }



        public Result<data_member_advice> GetMemberAdvice(int data_member_advice_id)
        {
            var res = new Result<data_member_advice>();
            try
            {

                var findres =
                    DataContext.data_member_advice.FirstOrDefault(a => a.data_member_advice_id == data_member_advice_id);

                if (findres == null)
                {
                    res.IsSuccessful = false;
                    res.Message = "无法找到数据";
                    return res;
                }

                res.Parameter = findres;
                res.IsSuccessful = true;
                res.Message = string.Empty;
            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
            }
            return res;
        }



        public IQueryable<data_member_advice> GetMemberAdvices()
        {
            return DataContext.data_member_advice.AsQueryable();
        }


        public Result<data_member_advice> GetMemberAdvicesByTaskid(int taskId)
        {
            var res = new Result<data_member_advice>();

            try
            {
                var task = DataContext.data_code_services_tasks.FirstOrDefault(a => a.data_code_services_tasks_id == taskId);

                if (task == null)
                {
                    res.IsSuccessful = false;
                    res.Message = "任务没有找到.";
                    return res;
                }

                if (!task.data_record_Id.HasValue)
                {
                    res.IsSuccessful = false;
                    res.Message = "没有存储Id";
                    return res;
                }
                var adviceId = task.data_record_Id.Value;


                return GetMemberAdvice(adviceId);

            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
            }

            return res;
        }

    }
}
