﻿using PropertyHub.Data;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace PropertyHub.Services
{
    public class BusinessService : ServiceBase<AnyTimeEntities>
    {
      [Dependency]
        public override AnyTimeEntities DataContext
      {

          get
          {
              return base.DataContext;
          }
          set
          {
              base.DataContext = value;
          }
      }

      private int? _subscriptionId = -1;
      public virtual int? SubscriptionId
      {
          get
          {
              if (_subscriptionId == -1)
              {
                  var value = WebContext == null ? null : WebContext.Request.RequestContext.RouteData.Values["Subscription"];
                  if (value != null)
                  {
                      _subscriptionId = (int?)value;
                  }
              }
              return _subscriptionId;
          }
      }



      public static void RegisterAllServices(IUnityContainer container)
      {
          var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
             .Where(type => !String.IsNullOrEmpty(type.Namespace))
             .Where(type => type.BaseType != null && type.BaseType.IsGenericType
              && type.BaseType.GetGenericTypeDefinition() == typeof(ServiceBase));
          foreach (var type in typesToRegister)
          {
              container.RegisterType(type);
          }
      }

         public IQueryable<SelectListItem> ListSuppliers()
            {
           return from item in DataContext.data_supplier
                  select new SelectListItem (){Text=item.data_supplier_name,Value=item.data_supplier_id.ToString()};
             }

         public IQueryable<SelectListItem> ListClasses()
         {
             return from item in DataContext.data_service
                    select new SelectListItem()
                    {
                        Text = item.data_service_name,
                        Value = item.data_service_id.ToString()
                    };
         }
         public IQueryable<SelectListItem> ListOptions(string key)
         {
             return from item in DataContext.data_dictionary
                    where item.data_dictionary_type == key
                    select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
         }
         public IQueryable<SelectListItem> ListMembers()
         {
             return from item in DataContext.data_member
                    select new SelectListItem() { Text = item.data_member_name, Value = item.data_member_id.ToString() };
         }


        public IQueryable<SelectListItem> ListKnowledge(int type_Id)
        {
            return from item in DataContext.data_knowledge
                select new SelectListItem()
                {
                    Text=item.data_knowledge_keyword + " "+item.data_knowledge_title ,
                    Value =item.data_knowledge_id.ToString()
                };
        }

        public IQueryable<data_knowledge> ListKnowledge(int? data_knowledge_type, string data_knowledge_keyword)
        {
            return from item in DataContext.data_knowledge
                   where (data_knowledge_type != null ? item.data_knowledge_type == data_knowledge_type : true)
                        && item.data_knowledge_keyword.Contains(data_knowledge_keyword)
                   select item;
                   //{
                   //    Text = item.data_knowledge_keyword + " " + item.data_knowledge_title,
                   //    Value = item.data_knowledge_id.ToString()
                   //};
        }

        public IQueryable<SelectListItem> ListKnowledgeType()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_knowledge"
                   select new SelectListItem()
                   {
                       Text = item.data_dictionary_text,
                       Value = item.data_dictionary_id.ToString()
                   };
        }
    }
}
