﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class DataKnowledgeService : BusinessService
    {
        public data_knowledge GetDataKnowledge(int id)
        {
            return DataContext.data_knowledge.SingleOrDefault(x => x.data_knowledge_id == id);
        }

        public IQueryable<data_knowledge> ListDataKnowledge()
        {
            return from item in DataContext.data_knowledge
                   select item;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_knowledge_status"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListDataKnowledgeType()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_knowledge"
                  // where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_knowledge.SingleOrDefaultAsync(x => x.data_knowledge_id == id);
            DataContext.data_knowledge.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task Delete(int[] ids)
        {
            var entities = DataContext.data_knowledge.Where(x => ids.Contains(x.data_knowledge_id));

            foreach (var item in entities)
            {
                DataContext.data_knowledge.Remove(item);
            }
            
            await DataContext.SaveChangesAsync();
        }

        //added by adam check multiple record
        public async Task Checks(int[] ids)
        {
            var entities = DataContext.data_knowledge.Where(x => ids.Contains(x.data_knowledge_id));

            foreach (var item in entities)
            {
                item.data_knowledge_status = 35;
            }

            await DataContext.SaveChangesAsync();
        }

        public async Task<data_knowledge> Add(Nullable<int> data_knowledge_type, string data_knowledge_title,
            string data_knowledge_summary, string data_knowledge_remark, string data_knowledge_path, Nullable<int> data_knowledge_author_type,
            Nullable<int> data_knowledge_author_id, string data_knowledge_keyword, string createdby_id, string createdby_name)
        {
            data_knowledge entity = new data_knowledge();
            entity.data_knowledge_type = data_knowledge_type;
            entity.data_knowledge_title = data_knowledge_title;
            entity.data_knowledge_summary = data_knowledge_summary;
            entity.data_knowledge_remark = data_knowledge_remark;
            entity.data_knowledge_path = data_knowledge_path;
            entity.data_knowledge_author_type = data_knowledge_author_type;
            entity.data_knowledge_author_id = data_knowledge_author_id;
            entity.data_knowledge_status = 34;
            entity.data_knowledge_keyword = data_knowledge_keyword;
            entity.createdby_id = createdby_id;
            entity.createdby_name = createdby_name;


            if (Validator.IsValid)
            {
                DataContext.data_knowledge.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_knowledge> Update(data_knowledge entity)
        {
            var service = DataContext.data_knowledge.FirstOrDefault(x => x.data_knowledge_id == entity.data_knowledge_id);

            if (service != null)
            {
                service.data_knowledge_type = entity.data_knowledge_type;
                service.data_knowledge_title = entity.data_knowledge_title;
                service.data_knowledge_summary = entity.data_knowledge_summary;
                service.data_knowledge_remark = entity.data_knowledge_remark;
                service.data_knowledge_path = entity.data_knowledge_path;
                service.data_knowledge_author_type = entity.data_knowledge_author_type;
                service.data_knowledge_author_id = entity.data_knowledge_author_id;
                service.data_knowledge_keyword = entity.data_knowledge_keyword;
                service.createdby_id = entity.createdby_id;
                service.createdby_name = entity.createdby_name;
             
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_knowledge>UpdateState(data_knowledge entity)
        {
            var service = DataContext.data_knowledge.FirstOrDefault(x => x.data_knowledge_id == entity.data_knowledge_id);

            if (service != null)
            {
                service.data_knowledge_status = 35;//生效
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }
    }
}
