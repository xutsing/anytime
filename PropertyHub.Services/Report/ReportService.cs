﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;

namespace PropertyHub.Services.Report
{

    /// <summary>
    /// created by tsing on 2015/8/27
    /// 这个报表Service,是用来查询各种报表提供数据源
    /// </summary>
    public class ReportService : BusinessService
    {

        public List<sp_reportmemberBySec_Result> GetReportMemberBySec(DateTime? begin, DateTime? end)
        {
            try
            {
                return DataContext.sp_reportmemberBySec(begin, end).ToList();
            }
            catch (Exception exception)
            {

                return new List<sp_reportmemberBySec_Result>();
            }

        }

        public List<sp_reportcodes_Result> GetReportCodes(DateTime? begin, DateTime? end)
        {
            try
            {
                return DataContext.sp_reportcodes(begin, end).ToList();

            }
            catch (Exception exception)
            {

                return new List<sp_reportcodes_Result>();
            }
        }

        public List<sp_membertasks_Result> GetMemberTasks(DateTime? begin, DateTime? end)
        {
            try
            {
                return DataContext.sp_membertasks(begin, end).ToList();
            }
            catch (Exception exception)
            {
                return new List<sp_membertasks_Result>();
            }
        }

        public List<sp_members_Result> GetMembers(DateTime? begin, DateTime? end)
        {
            try
            {
                return DataContext.sp_members(begin, end).ToList();
            }
            catch (Exception  exception)
            {
                return new List<sp_members_Result>();
            }
        }
    }
}
