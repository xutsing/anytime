﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyHub.Services
{
    public class CheckCodeModel
    {

        public int Id { get; set; }
        public DateTime UploadDate { get; set; }

        public Dictionary<string, string> data { get; set; }

    }
}
