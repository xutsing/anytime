using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Provider;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Security;
using PropertyHub.Security;

namespace PropertyHub.Services
{
    /// <summary>
    /// 这个Service是专门用来操作激活码的
    /// </summary>
    public class CodeServices : BusinessService
    {

        public IQueryable<data_code_record> ListRecords()
        {

            return from item in DataContext.data_code_record

                select item;
        }

        public List<sp_codesdetails_Result> ListCodedetails(int? data_code_record_id)
        {

            return DataContext.sp_codesdetails(data_code_record_id).ToList();
        }

        public string GetMemberName(int? Id)
        {
            var findres = DataContext.data_member.FirstOrDefault(a => a.data_member_id == Id);
            return findres.data_member_name;
        }

        public string GetSecretaryName(int Id)
        {
            var findres = DataContext.data_member_secretary.FirstOrDefault(a => a.data_member_secretary_id == Id);
            return findres.data_member_service_name;
        }


        public Result<List<CheckCodeModel>> ListCheckData(int data_member_id,int toprows=20)
        {
            var res = new Result<List<CheckCodeModel>>();
            res.Parameter = new List<CheckCodeModel>();
            try
            {
                var records = DataContext.data_member_checkrecord
                    .OrderByDescending(a=>a.datecreated)
                    .Where(a => a.data_member_id == data_member_id).Take(toprows);

                foreach (var record in records)
                {
                    var newmodel = new CheckCodeModel();
                    newmodel.Id = record.data_member_checkrecord_id;
                    newmodel.UploadDate = record.datecreated.HasValue?record.datecreated.Value:DateTime.Now;
                    newmodel.data = new Dictionary<string, string>();
                    foreach (var data in record.data_member_checkdata)
                    {
                        newmodel.data.Add(data.data_base_index.data_base_key, data.data_member_value);
                    }

                    res.Parameter.Add(newmodel);
                }


                res.IsSuccessful = true;
                res.Message = string.Empty;

            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;

            }

            return res;
        }



        public data_code_services_tasks GetTaskById(int Id)
        {
            return DataContext.data_code_services_tasks.FirstOrDefault(a => a.data_code_services_tasks_id == Id);

        }

        public bool UpdateTaskStatus(int Id, int? status, string comments, int recordId = 0)
        {
            var findres = DataContext.data_code_services_tasks.FirstOrDefault(a => a.data_code_services_tasks_id == Id);
            if (findres == null)
            {
                return false;
            }

            findres.data_code_services_tasks_status = status;
            findres.data_code_service_remark = comments;

            //UPDATED BY TSING ON 8/19/2015
            findres.dateupdated = DateTime.Now;
            findres.updatedby_code = WebContext.User.Identity.GetUserId();
            findres.updatedby_name = WebContext.User.Identity.GetUserName();
            findres.data_record_Id = recordId != 0 ? recordId : 0; //这个地方是用来和TaksId对应起来，哪个任务，对应哪个具体的问题表单.

            DataContext.SaveChanges();
            return true;
        }


        public IQueryable<data_code> ListRecordCodes()
        {
            return from item in DataContext.data_code
                select item;
        }

        public IQueryable<data_code_services> ListCodeServices()
        {
            return from item in DataContext.data_code_services
                select item;
        }



        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                where item.data_dictionary_type == "data_code_record"
                select
                    new SelectListItem() {Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString()};
        }

        public IQueryable<SelectListItem> ListTaskStatus()
        {
            return from item in DataContext.data_dictionary
                where item.data_dictionary_type == "data_code_services_tasks"
                select
                    new SelectListItem() {Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString()};
        }

        public IQueryable<SelectListItem> ListSecretary()
        {
            return from item in DataContext.data_member_secretary
                   select
                       new SelectListItem() { Text = item.data_member_service_name, Value = item.data_member_secretary_id.ToString() };
        }

        public async Task<Result<bool>> CreateCodes(int supplyId, int serviceId, int codetype, int ispay, decimal money,
            int paytype, int? coderecordstatus, int count, string secretaryid, string secretaryname)
        {
            var result = new Result<bool>();

            try
            {
                this.DataContext.sp_createcodesnew(supplyId, serviceId, codetype, ispay, money, paytype, coderecordstatus, count,
                    WebContext.User.Identity.GetUserId(), secretaryid, secretaryname);


                result.IsSuccessful = true;
                result.Message = string.Empty;
            }
            catch (Exception ex)
            {
                result.IsSuccessful = false;
                result.Message = ex.Message;
            }
            return result;

        }

        /// <summary>
        /// 更新一个激活码生成记录
        /// </summary>
        /// <param name="record_id"></param>
        /// <param name="supplyId"></param>
        /// <param name="serviceId"></param>
        /// <param name="codetype"></param>
        /// <param name="ispay"></param>
        /// <param name="money"></param>
        /// <param name="paytype"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Result<bool> UpdateCodes(int record_id,int supplyId, int serviceId, int codetype, int ispay, decimal money,
            int paytype, int count, int status, string service_id, string service_name)
        {

            var result = new Result<bool>();

            try
            {
                var record = DataContext.data_code_record.FirstOrDefault(a => a.data_code_record_id == record_id);
                if (record == null)
                {
                    result.IsSuccessful = false;
                    result.Message = "无法找到此激活码生成记录";
                    result.Parameter = false;
                    return result;
                }
                //生效
                if (record.data_code_record_status == 13)
                {
                    result.IsSuccessful = false;
                    result.Message = "已经生效的激活码无法进行编辑";
                    result.Parameter = false;
                    return result;
                }

                bool iscountchanged = count == record.data_code_record_count;

                record.data_service_id = serviceId;
                record.data_supplier_id = supplyId;
                record.data_code_record_money = money;
                record.data_code_record_count = count;
                record.data_code_record_status = status;
                record.data_member_service_id = service_id;
                record.data_member_service_name = service_name;

                DataContext.SaveChanges();


                if (!iscountchanged)
                {
                    //数目发生了改变了.
                    DataContext.sp_updatecodes(record_id, count, WebContext.User.Identity.GetUserId());
                }


                if (status == 13)
                {
                    string sql = "update data_code set data_code_status=15 where data_code_record_id=" + record_id;
                    DataContext.Database.ExecuteSqlCommand(sql);
                }

                result.IsSuccessful = true;
                result.Message = string.Empty;
                result.Parameter = false;

            }
            catch (Exception exception)
            {
                result.IsSuccessful = true;
                result.Message = exception.Message;
                result.Parameter = false;

            }

            return result;

        }





        /// <summary>
        /// Added by Tsing
        /// 获得某人的任务列表
        /// </summary>
        /// <param name="service_id"></param>
        /// <returns></returns>
        public IQueryable<sp_getmytasks_Result> GetMyTasks(string userId)
        {
            var tasks = DataContext.sp_getmytasks(userId).ToList();
            return tasks.AsQueryable();
        }

        /// <summary>
        /// Added by adam
        /// 获得某人的当日任务列表
        /// </summary>
        /// <param name="userId">user's id</param>
        /// <returns></returns>
        public IQueryable<sp_getmytasks_Result> GetMyTodayTasks(string userId)
        {
            var tasks = DataContext.sp_getmytasks(userId)
                .Where(
                    m =>
                        (m.data_code_service_date < DateTime.Now.Date && m.data_code_services_tasks_status < 30) ||
                        (m.data_code_service_date == DateTime.Now.Date))
                .ToList();
            return tasks.AsQueryable();
        }


        public IQueryable<data_code> ListCodes()
        {
            return from item in DataContext.data_code
                select item;
        }

        public data_code_record Get(int id)
        {
            var result = DataContext.data_code_record.FirstOrDefault(a => a.data_code_record_id == id);
            return result;
        }


        /// <summary>
        /// 注册一个激活码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Result<bool> RegisterCode(string code, int data_member_id)
        {
            var res = new Result<bool>();
            try
            {
                //检查这个激活码是否存在。
                var checkresult = DataContext.data_code
                    .FirstOrDefault(a => a.data_code_text == code);

                if (checkresult == null)
                {
                    res.IsSuccessful = false;
                    res.Message = "激活码输入不正确";

                    return res;
                }

                //16是已经激活的。
                if (checkresult.data_code_status == 14)
                {
                    res.IsSuccessful = false;
                    res.Message = "激活码目前没有发送给供应商";
                    return res;
                }


                if (checkresult.data_code_status == 16)
                {
                    res.IsSuccessful = false;
                    res.Message = "激活码已经被注册";
                    return res;
                }

                //更新这个激活码的状态
                checkresult.data_code_status = 16;
                checkresult.dateupdated = DateTime.Now;
                checkresult.updatedby = WebContext.User.Identity.GetUserId();

                //生成这个激活码所带的服务的基本信息

                var newservice = new data_code_services();
                newservice.data_code_id = checkresult.data_code_id;
                newservice.data_service_id = checkresult.data_code_record.data_service_id;
                newservice.data_member_id = data_member_id;
                newservice.data_code_services_date = DateTime.Now;

                newservice.data_code_services_status = 17; //课程生效

                newservice.datecreated = DateTime.Now;
                newservice.dateupdated = DateTime.Now;

                newservice.dateStarted = DateTime.Now;
                newservice.dateEnd =
                    DateTime.Now.AddMonths(checkresult.data_code_record.data_service.data_service_validate_months.Value);


                newservice.dateActualStarted = newservice.dateStarted;
                newservice.dateActualEnd = newservice.dateEnd;

                DataContext.data_code_services.Add(newservice);



                //Tsing
                //

                if (!string.IsNullOrEmpty(checkresult.data_code_record.data_member_service_id))
                {
                    //这说明这张卡目前有默认的健康秘书。

                    var serviceId = checkresult.data_code_record.data_member_service_id;
                    var servicename = checkresult.data_code_record.data_member_service_name;


                    //u.data_member_service_id = selected_secratry;
                    //u.data_member_service_name = selected_secratry_name;
                    //u.data_member_has_service = true;
                    //u.data_member_secretary.Add(new data_member_secretary
                    //{
                    //    data_member_id = u.data_member_id,
                    //    data_member_service_id = selected_secratry,
                    //    data_member_service_name = selected_secratry_name,
                    //    datecreated = DateTime.Now
                    //});
                    //service.Update(u);

                    var findmember = DataContext.data_member.FirstOrDefault(a => a.data_member_id == data_member_id);
                    if (findmember != null)
                    {
                        findmember.data_member_service_id = serviceId;
                        findmember.data_member_service_name = servicename;
                        findmember.data_member_has_service = true;

                        findmember.data_member_secretary.Add(new data_member_secretary
                        {
                            data_member_id = data_member_id,
                            data_member_service_id = serviceId,
                            data_member_service_name = servicename,
                            datecreated = DateTime.Now
                        });
                    }

                }



                //针对这个服务，生成任务列表

                DataContext.SaveChanges();

                var taskdate = DateTime.Now;

                foreach (var task in checkresult.data_code_record.data_service.data_service_tasks)
                {
                    taskdate = taskdate.AddDays(task.data_nexttask_interval.Value);

                    var newtask = new data_code_services_tasks();

                    //newtask.data_code_services_id = checkresult.data_code_record.data_service_id;
                    newtask.data_code_services_id = newservice.data_code_services_id;


                    newtask.data_basetask_id = task.data_basetask_id;
                    newtask.datecreated = DateTime.Now;
                    newtask.data_code_service_date = taskdate;
                    newtask.data_code_services_tasks_status = 28; //没有执行.

                    DataContext.data_code_services_tasks.Add(newtask);

                    //added by adam add cycle tasks
                    if (task.Iscycle != null && task.Iscycle.Value
                        && task.cycleDays != null && task.cycleDays.Value > 0)
                    {
                        var i = 1;
                        // modified by adam 2015-11-12, add the "cyclemax" limitation
                        while (taskdate.AddDays(task.cycleDays.Value*i) <= newservice.dateEnd && task.cyclemax > i )
                        {
                            newtask = new data_code_services_tasks();
                         //   newtask.data_code_services_id = checkresult.data_code_record.data_service_id;


                            newtask.data_code_services_id = newservice.data_code_services_id;
                            newtask.data_basetask_id = task.data_basetask_id;
                            newtask.datecreated = DateTime.Now;
                            newtask.data_code_service_date = taskdate.AddDays(task.cycleDays.Value*i);
                            newtask.data_code_services_tasks_status = 28; //没有执行.

                            DataContext.data_code_services_tasks.Add(newtask);
                            i++;
                        }
                    }
                }

                var saveresult=DataContext.SaveChanges();
                res.IsSuccessful = true;
                res.Parameter = true;
                res.Message = string.Empty;


            }
            catch (Exception ex)
            {
                res.IsSuccessful = false;
                res.Parameter = false;
                res.Message = ex.Message;

            }
            return res;
        }

        public async Task execute(int id)
        {
            var service = DataContext.data_code_services_tasks.FirstOrDefault(x => x.data_code_services_tasks_id == id);
            if (service != null)
            {
                service.data_code_services_tasks_status = 29; //生效
                await DataContext.SaveChangesAsync();
            }
        }

        //added by adam 2015-08-12
        //for updating the status of service tasks
        public int CompleteServiceTasks(int taskID, int recID, int statusCode)
        {
            var sql =
                String.Format(
                    "update data_code_services_tasks set data_code_services_tasks_status={2},  data_record_id={0} where data_code_services_tasks_id={1}",
                    new object[3] {recID, taskID, statusCode});

            return DataContext.Database.ExecuteSqlCommand(sql);
        }


        //public Result<bool> CreateCodes(int supplyId, int serviceId, int codetype, bool ispay, decimal money, int paytype, int count)
        //{
        //    return null;
        //}
        // public Dictionary<string,> 

        public data_member_checkrecord GetCheckrecord(int check_record_id)
        {
            return DataContext.data_member_checkrecord
                .FirstOrDefault(a => a.data_member_checkrecord_id == check_record_id);
        }


        /// <summary>
        /// 获得
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <param name="data_check_record_id"></param>
        /// <returns></returns>
        public Result<List<CheckDataModel>> GetReport(
            int data_check_record_id)
        {

            var res = new Result<List<CheckDataModel>>();

            try
            {
                var findcheckrecord = DataContext.data_member_checkrecord
                    .FirstOrDefault(a => a.data_member_checkrecord_id == data_check_record_id);

                if (findcheckrecord == null)
                {
                    throw new Exception("无法找到检测记录");
                }



                var data_member_id = findcheckrecord.data_member_id;


                //查找会员
                var findmember = DataContext.data_member
                    .FirstOrDefault(a => a.data_member_id == data_member_id);

                if (findmember == null)
                {
                    throw new Exception("无法找到当前会员");
                }
                //当前会员的性别
                var data_sex_id = findmember.data_sex.data_sex_id;

                var birthday = findmember.data_member_birthday.HasValue
                    ? findmember.data_member_birthday.Value
                    : DateTime.Now;
                var age = (DateTime.Now.Year - birthday.Year); //年龄

                //这个地方仅仅就是获得有标准数据的那些统计
                //

                var data = new List<CheckDataModel>();
                var result = DataContext.sp_getcheckreport(age, data_sex_id, data_check_record_id);
                foreach (var record in result)
                {
                    var newrecord = new CheckDataModel();
                    newrecord.data_base_index_id = record.data_base_index_id;
                    newrecord.data_base_index_title = record.data_base_index_title;
                    newrecord.data_base_index_unit = record.data_base_index_unit;
                    newrecord.data_body_label_text = record.data_body_label_text;
                    newrecord.data_member_value = record.data_member_value;

                    newrecord.data_base_index_key = record.data_base_key;

                    data.Add(newrecord);
                }

                var _data =
                    DataContext.data_member_checkdata.Where(a => a.data_member_checkrecord_id == data_check_record_id)
                        .ToList();

                foreach (var record in _data)
                {
                    var findres = data.FirstOrDefault(a => a.data_base_index_id == record.data_base_index_id);
                    if (findres == null)
                    {
                        //没有找到的。这些数据一般就是没有录入标准值的
                        var index =
                            DataContext.data_base_index.FirstOrDefault(
                                a => a.data_base_index_id == record.data_base_index_id);

                        if (index != null)
                        {
                            var newrecord = new CheckDataModel();
                            newrecord.data_base_index_id = record.data_base_index_id;
                            newrecord.data_base_index_title = index.data_base_index_title;
                            newrecord.data_base_index_unit = index.data_base_index_unit;
                            newrecord.data_body_label_text = "0";
                            newrecord.data_member_value = record.data_member_value;

                            newrecord.data_base_index_key = record.data_base_index.data_base_key;
                            data.Add(newrecord);


                        }
                    }


                }





                res.Message = string.Empty;
                res.Parameter = data;

            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = null;
            }

            return res;

        }

        public Result<List<sp_bodyreport_Result>> GetBodyReport(
            int data_member_id)
        {
            var res = new Result<List<sp_bodyreport_Result>>();

            try
            {
                var firstmemberrecord = DataContext.data_member
                    .FirstOrDefault(a => a.data_member_id == data_member_id);

                if (firstmemberrecord == null)
                {
                    throw new Exception("无法找到检测人员");
                }
                var member_id = data_member_id.ToString();


                var results = DataContext.sp_bodyreport(member_id);

                var tmp = results.ToList();

                foreach (var result in tmp)
                {
                    if (result.data_body_label_text == null)
                    {
                        //result.data_body_label_text = "没有设置标准值";
                        result.data_body_label_text = "0";
                    }
                }


                res.Message = string.Empty;
                res.Parameter = tmp;

            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = null;
            }

            return res;

        }

        public Result<bool> CreateCheckReport(int check_Id, bool isfinished, string comments)
        {
            var res = new Result<bool>();



            try
            {
                var findres =DataContext.data_member_checkrecord.FirstOrDefault(a => a.data_member_checkrecord_id == check_Id);
                if (findres == null)
                {
                    res.IsSuccessful = false;
                    res.Message = "无法找到检测记录";
                    res.Parameter = false;

                    return res;
                }

                findres.data_Isfinished = isfinished;
                findres.data_checkreport = comments;
                findres.data_create_code = WebContext.User.Identity.GetUserName();
                findres.data_create_date = DateTime.Now;

                DataContext.SaveChanges();

                res.IsSuccessful = true;
                res.Parameter = true;
                res.Message = string.Empty;

            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = false;

            }

            return res;
        }

        /// <summary>
        /// 获得会员的服务信息。
        /// </summary>
        /// <param name="member_id"></param>
        /// <returns></returns>
        public Result<string> GetMemberServices(int member_id)
        {
            var res = new Result<string>();

            try
            {

                var member = DataContext.data_member.FirstOrDefault(a => a.data_member_id == member_id);
                if (member == null)
                {
                    res.IsSuccessful = false;
                    res.Parameter = string.Empty;
                    res.Message = "无法找到这个会员";

                    return res;
                }

                //if (string.IsNullOrEmpty(member.data_member_service_id))
                //{
                //    res.IsSuccessful = false;
                //    res.Parameter = string.Empty;
                //    res.Message = "暂时没有分配健康秘书！";

                //    return res;
                //}



                var services = DataContext.sp_getmemberservices(member_id).ToList();
                res.IsSuccessful = true;
                res.Message = string.Empty;

                IsoDateTimeConverter timeFormat = new IsoDateTimeConverter();
                timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";


                res.Parameter = JsonConvert.SerializeObject(services, Newtonsoft.Json.Formatting.Indented, timeFormat);
            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = string.Empty;

            }
            return res;
        }


        public Result<string> GetMemberAdvice(int member_id, int? data_member_checkrecord_id)
        {
            var res = new Result<string>();
            try
            {


                if (data_member_checkrecord_id.HasValue)
                {

                    var advice = from item in DataContext.data_member_checkrecord
                        where item.data_member_checkrecord_id == data_member_checkrecord_id.Value
                        select new
                        {
                            item.data_member_id,
                            item.data_member_checkrecord_id,
                            item.datecreated,
                            item.data_create_date,
                            item.data_checkreport
                        };

                    res.IsSuccessful = true;



                    var timeFormat = new IsoDateTimeConverter();
                    timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

                    res.Parameter = JsonConvert.SerializeObject(advice, Formatting.Indented, timeFormat);
                    res.Message = string.Empty;

                }
                else
                {
                    var advices = DataContext.sp_getmemberadvice(member_id).ToList();
                    res.IsSuccessful = true;

                                        var timeFormat = new IsoDateTimeConverter();
                    timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";


                    res.Parameter = JsonConvert.SerializeObject(advices,Formatting.Indented,timeFormat);
                    res.Message = string.Empty;
                }






            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = string.Empty;
            }

            return res;
        }

        public Result<string> GetMemberAdvices(int data_member_id)
        {
            var res = new Result<string>();
            try
            {

                var advices = DataContext.data_member_advice.Where(a => a.data_member_id == data_member_id
                                                                        && a.data_member_advice_isfinished
                    ).ToList();

                res.IsSuccessful = true;

                var items = from item in advices
                    select
                        new
                        {
                            Id = item.data_member_advice_id,
                            Text = item.data_member_advice_summary,
                            Date = item.datecreated
                        };



                var timeFormat = new IsoDateTimeConverter();
                timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

                res.Parameter = JsonConvert.SerializeObject(items,Formatting.Indented,timeFormat);


            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = string.Empty;
            }
            return res;
        }




        /// <summary>
        /// added by Tsing.这个用来根据输入的ids返回这些知识库的内容
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        public Result<string> GetKnowledgeComments(string Ids)
        {
            var res = new Result<string>();
            
            try
            {
                string[] kids = Ids.Split(',');
                string comments = string.Empty;

                foreach (var id in kids)
                {

                    var kid = int.Parse(id);
                    var knowledge = DataContext.data_knowledge.FirstOrDefault(a => a.data_knowledge_id == kid);

                    if (comments == string.Empty)
                    {
                        comments = knowledge.data_knowledge_summary;
                    }
                    else
                    {
                        comments = comments + "<p></p>" + knowledge.data_knowledge_summary;
                    }
                }
                res.Parameter = comments;
                res.IsSuccessful = true;
                res.Message = string.Empty;
            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
            }

            return res;
        }



        /// <summary>
        /// 获得一个会员的健身处方
        /// </summary>
        /// <param name="data_member_id"></param>
        /// <returns></returns>
        public Result<string> GetMemberPrescriptions(int data_member_id, int toprows=20)
        {
            var res = new Result<string>();
            try
            {
                var orders =
                    DataContext.data_member_prescription.
                    OrderByDescending(a=>a.datecreated)
                    .Where(a => a.data_member_id == data_member_id)
                    .Take(toprows)
                    .ToList();

                var detials = from item in orders
                    select
                        new
                        {
                            id = item.data_member_prescription_id,
                            title = item.data_member_prescription_title,
                            url = item.data_member_prescription_path
                        };


                res.IsSuccessful = true;
                res.Parameter = JsonConvert.SerializeObject(detials);
                res.Message = string.Empty;


            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = string.Empty;

            }

            return res;

        }

        public sp_getcodestatus_Result GetCodeStatus(string code)
        {
            var result = DataContext.sp_getcodestatus(code).FirstOrDefault();
            return result;
        }

    }
}
