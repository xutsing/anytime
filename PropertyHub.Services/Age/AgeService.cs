﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class AgeService : BusinessService
    {
        public data_age_range GetAge(int id)
        {
            return DataContext.data_age_range.SingleOrDefault(x => x.data_age_range_id == id);
        }

        public int? GetAgeId(int age)
        {
            var entity = DataContext.data_age_range.SingleOrDefault(x => x.data_age_max <= age && x.data_age_min >= age);
            if (entity != null)
            {
                return entity.data_age_range_id;
            }
            else
            {
                return null;
            }
        }


        public IQueryable<data_age_range> ListAge()
        {
            return from item in DataContext.data_age_range
                   select item;
        }

        public async Task<data_age_range> Add(Nullable<int> data_age_min, Nullable<int> data_age_max, string data_age_title)
        {
            data_age_range entity = new data_age_range();
            entity.data_age_min = data_age_min;
            entity.data_age_max = data_age_max;
            entity.data_age_title = data_age_title;

            if (Validator.IsValid)
            {
                DataContext.data_age_range.Add(entity);
                await DataContext.SaveChangesAsync();
            }
            return entity;
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_age_range"
                   select new SelectListItem() { Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }


        public async Task<data_age_range> Update(data_age_range entity)
        {
            var service = DataContext.data_age_range.FirstOrDefault(x => x.data_age_range_id == entity.data_age_range_id);

            if (service != null)
            {
                service.data_age_max = entity.data_age_max;
                service.data_age_min = entity.data_age_min;
                service.data_age_title = entity.data_age_title;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_age_range.SingleOrDefaultAsync(x => x.data_age_range_id == id);
            DataContext.data_age_range.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

    }
}
