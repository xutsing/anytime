﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyHub.Services
{
    //added by adam 2015-10-05 体质检测报告
    public class CheckReportModel
    {
        //public CheckReportModel()
        //{
        //    this.baseInfor = new BaseInfor();
        //    this.illHistory = new IllHistory();
        //    this.habit = new LivingHabit();
        //    this.exeRisk = new ExeciseRisk();
        //    this.exeHabit = new ExeciseHabit();
        //    this.checkReport = new CheckReport();
        //    this.dietAdvice = new DietAdvice();
        //    this.prescription = new Prescription();
        //}

        public int memberId { get; set; }
        //public BaseInfor baseInfor { get; set; }
        //public IllHistory illHistory { get; set; }
        //public LivingHabit habit { get; set; }
        //public ExeciseRisk exeRisk { get; set; }
        //public ExeciseHabit exeHabit { get; set; }
        //public CheckReport checkReport { get; set; }
        //public DietAdvice dietAdvice { get; set; }
        //public Prescription prescription { get; set; }

        //public class BaseInfor {
            public string Name { get; set; }
            public string Sex { get; set; }
            public string Age { get; set; }
            public string Phone { get; set; }
            public string Nation { get; set; }
            public string Job { get; set; }

            public string LoginName{ get; set; }

        //}

        //public class IllHistory
        //{
            public bool isIll { get; set; }
            public string isIll_text { get; set; }
            public string illName { get; set; }
            public bool isSurgery { get; set; }
            public string isSurgery_text { get; set; }
            public string surgeryName { get; set; }
            public string surgeryDate { get; set; }
            public bool isWound { get; set; }
            public string isWound_text { get; set; }
            public string woundName { get; set; }
            public string woundDate { get; set; }
            public bool isBlood { get; set; }
            public string isBlood_text { get; set; }
            public string bloodName { get; set; }
            public string bloodDate { get; set; }
            //home
            public bool isIll_H1 { get; set; }
            public string ill_H1Name { get; set; }
            public string ill_H1Relation { get; set; }
            public bool isIll_H2 { get; set; }
            public string ill_H2Name { get; set; }
            public string ill_H2Relation { get; set; }
            public bool isIll_H3 { get; set; }
            public string ill_H3Name { get; set; }
            public string ill_H3Relation { get; set; }
        //}

        //public class LivingHabit
        //{
            public bool isDiet1 { get; set; }
            public bool isDiet2 { get; set; }
            public bool isDiet3 { get; set; }
            public bool isDiet4 { get; set; }
            public bool isDiet5 { get; set; }
            public bool isDiet6 { get; set; }
            public string isDiet_text { get; set; }
            //吸烟情况
            public int isSmoking { get; set; }
            public bool isSmoking1 { get; set; }
            public bool isSmoking2 { get; set; }
            public bool isSmoking3 { get; set; }
            public string isSmoking_text { get; set; }

            public string smokeAge { get; set; }
            public string smokeAmount { get; set; }
            //饮酒情况
            public int isDrinking { get; set;  }

            public string isDrinking_text { get; set; }
            public bool isDrinking1 { get; set; }
            public bool isDrinking2 { get; set; }
            public bool isDrinking3 { get; set; }
            public bool isDrinking4 { get; set; }
            public string drinkingAmount1 { get; set; }
            public string drinkingAmount2 { get; set; }
            //是否戒酒
            public bool isDryOut { get; set; }
            public bool isDryOut1 { get; set; }
            public bool isDryOut2 { get; set; }
            public string isDryOut_text { get; set; }
            public string dryOutDate { get; set; }
            //是否饮酒
            public bool isWine1 { get; set; }
            public bool isWine2 { get; set; }
            public bool isWine3 { get; set; }
            public bool isWine4 { get; set; }
            public string otherWine { get; set; }
            public string isWine_text { get; set; }
        //}

        //public class ExeciseRisk 
        //{
            public bool isQ1 { get; set; }
            public string isQ1_text { get; set; }
            public bool isQ2 { get; set; }
            public string isQ2_text { get; set; }
            public bool isQ3 { get; set; }
            public string isQ3_text { get; set; }
            public bool isQ4 { get; set; }
            public string isQ4_text { get; set; }
            public bool isQ5 { get; set; }
            public string isQ5_text { get; set; }
            public bool isQ6 { get; set; }
            public string isQ6_text { get; set; }
            public bool isQ7 { get; set; }
            public string isQ7_text { get; set; }
            public bool isQ8 { get; set; }
            public string isQ8_text { get; set; } 
            public string riskEstimate { get; set; }
        //}
        //运动习惯
        //public class ExeciseHabit
        //{

            public string exeYears { get; set; }
            public string exeMinites { get; set; }
            //public bool isRate1 { get; set; }
            //public bool isRate2 { get; set; }
            //public bool isRate3 { get; set; }
            //public bool isRate4 { get; set; }
            public int? isRate { get; set; }
            public string isRate_text { get; set; }
            public bool isTime1 { get; set; }
            public bool isTime2 { get; set; }
            public bool isTime3 { get; set; }
            public bool isTime4 { get; set; }
            public bool isTime5 { get; set; }
            public string isTime_text { get; set; }
            public bool isPlace1 { get; set; }
            public bool isPlace2 { get; set; }
            public bool isPlace3 { get; set; }
            public bool isPlace4 { get; set; }
            public string isPlace_text { get; set; }
            public string favSports { get; set; }
        //}
        //体测报告
        //public class CheckReport
        //{
            //public string Name { get; set; }
            //public string Sex { get; set; }

            //public string Age { get; set; }
            //public string Date { get; set; }
            public string Score_1 { get; set; }
            public string Level_1 { get; set; }
            public string Score_2 { get; set; }
            public string Level_2 { get; set; }
            public string Score_3 { get; set; }
            public string Level_3 { get; set; }
            public string Score_4 { get; set; }
            public string Level_4 { get; set; }
            public string Score_5 { get; set; }
            public string Level_5 { get; set; }
            public string Score_6 { get; set; }
            public string Level_6 { get; set; }
            public string Score_7 { get; set; }
            public string Level_7 { get; set; }
            public string Score_8 { get; set; }
            public string Level_8 { get; set; }
            public string Score_9 { get; set; }
            public string Level_9 { get; set; }
            public string Score_10 { get; set; }
            public string Level_10 { get; set; }
            public string Level_11 { get; set; }
            public string Score_11 { get; set; }
            public string Level_12 { get; set; }
            public string Score_12 { get; set; }
            public string BMI { get; set; }

            public string BMI_Score { get; set; }
            public string BloodPress_1 { get; set; }
            public string BloodPress_2 { get; set; }
            public string Rate { get; set; }
            public string TotalRate { get; set; }
            public string CheckReport_Content_1 { get; set; }
            public string CheckReport_Content_2 { get; set; }
            public string CheckReport_Content_3 { get; set; }
        //}
        //饮食建议
        //public class DietAdvice
        //{
            public string DietAdvice_Content_1 { get; set; }
            public string DietAdvice_Content_2 { get; set; }
        //}
        //运动处方
        //public class Prescription
        //{
            public string Height { get; set; }
            public string Weight { get; set; }
            public string Muscle { get; set; }
            public string BodyFat { get; set; }
            

            public string BMI_Min { get; set; }
            public string BMI_Max { get; set; }
            public string Weight_Min { get; set; }
            public string Weight_Max { get; set; }
            public string Muscle_Min{ get; set; }
            public string Muscle_Max { get; set; }
            public string BodyFat_Min { get; set; }
            public string BodyFat_Max { get; set; }

            public string Ill_History { get; set; }

            public string Prescription_Content_1 { get; set; }
            public string Prescription_Content_2 { get; set; }
            public string Doc_Template { get; set; }
        //}
    }
}