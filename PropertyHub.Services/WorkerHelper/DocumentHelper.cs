﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using WordDocumentGenerator.Library;
using PropertyHub.Services;
using System.Web.Mvc;


namespace PropertyHub.Services.WorkerHelper
{
  public class DocumentHelper
    {

      private static DocumentGenerationInfo GetDocumentGenerationInfo(string docType, string docVersion, object dataContext, string fileName, bool useDataBoundControls)
      {
          DocumentGenerationInfo generationInfo = new DocumentGenerationInfo();
          generationInfo.Metadata = new DocumentMetadata() { DocumentType = docType, DocumentVersion = docVersion };
          generationInfo.DataContext = dataContext;
          generationInfo.TemplateData = File.ReadAllBytes(fileName);
          generationInfo.IsDataBoundControls = useDataBoundControls;

          return generationInfo;
      }



      public static void GenerateDocumentUsingSampleDocGenerator(CheckReportModel model, MemberFilesService mfservice)
      {
          // Test document generation from template("Test_Template - 1.docx")

          //var model = new MemberFileModel();
          //var newmodel = new CheckReportModel();

          //model.age = 32;
          //model.ethnic = "汉族";
          //model.mobile = "13506483744";
          //model.name = "徐青";
          //model.name2 = true;
          //model.sex = "女";
          //model.test2 = "y系列第四代产品格里芬战靴:JordanBrand打造的JordanSuper.Fly系列凭借其出众实战性能,备受格里芬和众多球员的喜爱,在取得全面成功的情况.";
          string temppath;
          if (model.Doc_Template == "标准模板")
          {
              temppath = System.Web.HttpContext.Current.Server.MapPath("~/uploads/templates/member_template.docx");
          }
          else
          {
              temppath = System.Web.HttpContext.Current.Server.MapPath("~/uploads/templates/member_template.docx");
          }

          DocumentGenerationInfo generationInfo = GetDocumentGenerationInfo("SampleDocumentGenerator", "1.0", model,
                                                  temppath, false);

          MemberDocumentGenerator sampleDocumentGenerator = new MemberDocumentGenerator(generationInfo);
          byte[] result = result = sampleDocumentGenerator.GenerateDocument();
          //string NowDate = String.Format("yyyyMMdd",  DateTime.Now);
          string NowDate = DateTime.Now.Date.ToString("yyyyMMdd");
          string str_outpath;

              str_outpath = "~/uploads/memberfiles/" + model.Name + model.LoginName + NowDate + model.Doc_Template + ".docx";
 
        //  var temppath = System.Web.HttpContext.Current.Server.MapPath("uploads/tempates/member_template.docx");
        //  var outputpath = System.Web.HttpContext.Current.Server.MapPath("~/uploads/memberfiles/member_template.docx");
            var outputpath = System.Web.HttpContext.Current.Server.MapPath(str_outpath);

            WriteOutputToFile(outputpath, temppath, result);
            string new_outpath = str_outpath.Replace("~/", "");
            mfservice.Add(model.memberId, new_outpath);
       
           
      }
      private static void WriteOutputToFile(string fileName, string templateName, byte[] fileContents)
      {
          ConsoleColor consoleColor = Console.ForegroundColor;

          if (fileContents != null)
          {
              File.WriteAllBytes(Path.Combine("Sample Templates", fileName), fileContents);
             // Console.ForegroundColor = ConsoleColor.Green;
            //  Console.WriteLine(string.Format("Generation succeeded for template({0}) --> {1}", templateName, fileName));
            //  Console.WriteLine();
          }
          else
          {
            //  Console.ForegroundColor = ConsoleColor.Red;
            //  Console.WriteLine(string.Format("Generation failed for template({0}) --> {1}", templateName, fileName));
          }

        //  Console.ForegroundColor = consoleColor;
      }

    }
}
