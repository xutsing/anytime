﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using PropertyHub.Data;

namespace PropertyHub.Services
{
    public class CSVFileHelper
    {
        /// <summary>
        /// 将DataTable中数据写入到CSV文件中
        /// </summary>
        /// <param name="dt">提供保存数据的DataTable</param>
        /// <param name="fileName">CSV的文件路径</param>
        public string CreateFile(string folder, string fileName, string fileExtension)
        {
            FileStream fs = null;
            string filePath = folder + fileName + "." + fileExtension;
            try
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                fs = System.IO.File.Create(filePath);
            }
            catch (Exception ex)
            { }
            finally
            {
                if (fs != null)
                {
                    fs.Dispose();
                }
            }
            return filePath;
        }

        public PropertyInfo[] GetPropertyInfoArray()
        {
            PropertyInfo[] props = null;
            try
            {
                Type type = typeof(PropertyHub.Data.sp_codesdetails_Result);
                object obj = Activator.CreateInstance(type);
                props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            }
            catch (Exception ex)
            { }
            return props;
        }

        /// <summary>
        /// Save the List data to CSV file
        /// </summary>
        /// <param name="studentList">data source</param>
        /// <param name="filePath">file path</param>
        /// <returns>success flag</returns>
        public bool SaveDataToCSVFile(List<PropertyHub.Data.sp_codesdetails_Result> studentList, string filePath)
        {
            bool successFlag = true;

            StringBuilder strColumn = new StringBuilder();
            StringBuilder strValue = new StringBuilder();
            StreamWriter sw = null;
            PropertyInfo[] props = GetPropertyInfoArray();

            try
            {
                sw = new StreamWriter(filePath, false, UnicodeEncoding.GetEncoding("GB2312"));
                //sw = new StreamWriter(filePath);
                for (int i = 0; i < props.Length; i++)
                {
                    strColumn.Append(props[i].Name);
                    strColumn.Append(",");
                }
                strColumn.Remove(strColumn.Length - 1, 1);
                sw.WriteLine(strColumn);    //write the column name

                for (int i = 0; i < studentList.Count; i++)
                {
                    strValue.Remove(0, strValue.Length); //clear the temp row value
                    strValue.Append(studentList[i].data_code_id);
                    strValue.Append(",");
                    strValue.Append(studentList[i].data_code_text);
                    strValue.Append(",");
                    strValue.Append(studentList[i].data_dictionary_text);
                    strValue.Append(",");
                    strValue.Append(studentList[i].data_member_id);
                    strValue.Append(",");
                    strValue.Append(studentList[i].data_member_name);
                    strValue.Append(",");
                    strValue.Append(studentList[i].data_member_mobile);
                    sw.WriteLine(strValue); //write the row value
                }
            }
            catch (Exception ex)
            {
                successFlag = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                }
            }

            return successFlag;
        }
    }
}
