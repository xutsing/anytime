﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using DocumentFormat.OpenXml.Wordprocessing;
using PropertyHub.Services.WorkerHelper;
using WordDocumentGenerator.Library;

namespace PropertyHub.Services
{
    /// <summary>
    /// 用这个文档来生成文档。
    /// </summary>
    public class MemberDocumentGenerator:DocumentGenerator
    {

        #region 构造函数

        public MemberDocumentGenerator(DocumentGenerationInfo generationInfo)
            : base(generationInfo)
        {

        }



        #endregion

        protected override Dictionary<string, PlaceHolderType> GetPlaceHolderTagToTypeCollection()
        {
            Dictionary<string, PlaceHolderType> placeHolderTagToTypeCollection = new Dictionary<string, PlaceHolderType>();
            
            //会员基础信息
            //姓名
            placeHolderTagToTypeCollection.Add("Name",PlaceHolderType.NonRecursive);
            //性别
            placeHolderTagToTypeCollection.Add("Sex", PlaceHolderType.NonRecursive);
            //年龄
            placeHolderTagToTypeCollection.Add("Age", PlaceHolderType.NonRecursive);
            //电话
            placeHolderTagToTypeCollection.Add("Phone", PlaceHolderType.NonRecursive);
            //民族
            placeHolderTagToTypeCollection.Add("Nation", PlaceHolderType.NonRecursive);
            //工作
            placeHolderTagToTypeCollection.Add("Job", PlaceHolderType.NonRecursive);

            //既往史
            //疾病
            placeHolderTagToTypeCollection.Add("isIll_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("illName", PlaceHolderType.NonRecursive);
            //手术
            placeHolderTagToTypeCollection.Add("isSurgery_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("surgeryName", PlaceHolderType.NonRecursive);
            //外伤
            placeHolderTagToTypeCollection.Add("isWound_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("woundName", PlaceHolderType.NonRecursive);
            //输血
            placeHolderTagToTypeCollection.Add("isBlood_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("bloodName", PlaceHolderType.NonRecursive);

            //家族史
            placeHolderTagToTypeCollection.Add("isIll_H1", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H1Name", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H1Relation", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isIll_H2", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H2Name", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H2Relation", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isIll_H3", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H3Name", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("ill_H3Relation", PlaceHolderType.NonRecursive);

            //生活方式
            //饮食情况
            placeHolderTagToTypeCollection.Add("isDiet_text", PlaceHolderType.NonRecursive);

            //吸烟情况
            placeHolderTagToTypeCollection.Add("isSmoking_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("smokeAge", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("smokeAmount", PlaceHolderType.NonRecursive);
            //饮酒情况
            placeHolderTagToTypeCollection.Add("isDrinking_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("drinkingAmount1", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("drinkingAmount2", PlaceHolderType.NonRecursive);
            //戒酒
            placeHolderTagToTypeCollection.Add("isDryOut_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("dryOutDate", PlaceHolderType.NonRecursive);
            //饮酒种类  
            placeHolderTagToTypeCollection.Add("isWine_text", PlaceHolderType.NonRecursive);
          //运动状况调查
            placeHolderTagToTypeCollection.Add("isQ1_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ2_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ3_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ4_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ5_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ6_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ7_text", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("isQ8_text", PlaceHolderType.NonRecursive);
            //运动习惯调查
            //运动历程
            placeHolderTagToTypeCollection.Add("exeYears", PlaceHolderType.NonRecursive);
            //每次运动时间
            placeHolderTagToTypeCollection.Add("exeMinites", PlaceHolderType.NonRecursive);
            //运动频率
            placeHolderTagToTypeCollection.Add("isRate_text", PlaceHolderType.NonRecursive);
            //运动时间段
            placeHolderTagToTypeCollection.Add("isTime_text", PlaceHolderType.NonRecursive);
            //运动场所
            placeHolderTagToTypeCollection.Add("isPlace_text", PlaceHolderType.NonRecursive);
           //体质测试报告
            placeHolderTagToTypeCollection.Add("Score_1", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_2", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_3", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_4", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_5", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_6", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_7", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_8", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_9", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_10", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_11", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Score_12", PlaceHolderType.NonRecursive);
            //BMI
            placeHolderTagToTypeCollection.Add("BMI", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("TotalRate", PlaceHolderType.NonRecursive);

            //运动处方
            placeHolderTagToTypeCollection.Add("Height", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Weight", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Muscle", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("BodyFat", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Ill_History", PlaceHolderType.NonRecursive);

            placeHolderTagToTypeCollection.Add("BMI_Min", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("BMI_Max", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Weight_Min", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Weight_Max", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Muscle_Min", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("Muscle_Max", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("BodyFat_Min", PlaceHolderType.NonRecursive);
            placeHolderTagToTypeCollection.Add("BodyFat_Max", PlaceHolderType.NonRecursive);

            //placeHolderTagToTypeCollection.Add("CheckReport_Content_1", PlaceHolderType.NonRecursive);
            

            return placeHolderTagToTypeCollection;
        }

        protected override void ContainerPlaceholderFound(string placeholderTag, OpenXmlElementDataContext openXmlElementDataContext)
        {
           // throw new NotImplementedException();
        }


        protected override void RecursivePlaceholderFound(string placeholderTag, OpenXmlElementDataContext openXmlElementDataContext)
        {
            //throw new NotImplementedException();
        }

        protected override void IgnorePlaceholderFound(string placeholderTag, OpenXmlElementDataContext openXmlElementDataContext)
        {
            //throw new NotImplementedException();
        }

        protected override void NonRecursivePlaceholderFound(string placeholderTag, OpenXmlElementDataContext openXmlElementDataContext)
        {
            if (openXmlElementDataContext == null || openXmlElementDataContext.Element == null || openXmlElementDataContext.DataContext == null)
            {
                return;
            }

            string tagPlaceHolderValue = string.Empty;
            string tagGuidPart = string.Empty;
            GetTagValue(openXmlElementDataContext.Element as SdtElement, out tagPlaceHolderValue, out tagGuidPart);

            string tagValue = string.Empty;
            string content = string.Empty;

            var model = (openXmlElementDataContext.DataContext) as CheckReportModel;

            switch(tagPlaceHolderValue)
            {
                //会员基本信息
                case "Name":
                    tagValue = model.Name;
                    content = model.Name;
                    break;
                case "Sex":
                    tagValue = model.Sex;
                    content = model.Sex;
                    break;
                case "Age":
                    tagValue = model.Age.ToString();
                    content = model.Age.ToString();
                    break;
                case "Phone":
                    tagValue = model.Phone;
                    content = model.Phone;
                    break;
                case "Nation":
                    tagValue = model.Nation;
                    content = model.Nation;
                    break;
                case "Job":
                    tagValue = model.Job;
                    content = model.Job;
                    break;

                //既往史
                //疾病
                case "isIll_text":
                    tagValue = model.isIll_text;
                    content = model.isIll_text;
                    break;
                case "illName":
                    tagValue = model.illName;
                    content = model.illName;
                    break;
                //手术
                case "isSurgery_text":
                    tagValue = model.isSurgery_text;
                    content = model.isSurgery_text;
                    break;
                case "surgeryName":
                    tagValue = model.surgeryName;
                    content = model.surgeryName;
                    break;
                //外伤
                case "isWound_text":
                    tagValue = model.isWound_text;
                    content = model.isWound_text;
                    break;
                case "woundName":
                    tagValue = model.woundName;
                    content = model.woundName;
                    break;
                //输血
                case "isBlood_text":
                    tagValue = model.isBlood_text;
                    content = model.isBlood_text;
                    break;
                case "bloodName":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                //家族史
                //疾病1
                case "isIll_H1":
                    tagValue = model.isBlood_text;
                    content = model.isBlood_text;
                    break;
                case "ill_H1Name":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                case "ill_H1Relation":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                //疾病2
                case "isIll_H2":
                    tagValue = model.isBlood_text;
                    content = model.isBlood_text;
                    break;
                case "ill_H2Name":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                case "ill_H2Relation":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                //疾病3
                case "isIll_H3":
                    tagValue = model.isBlood_text;
                    content = model.isBlood_text;
                    break;
                case "ill_H3Name":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                case "ill_H3Relation":
                    tagValue = model.bloodName;
                    content = model.bloodName;
                    break;
                //生活方式
                //饮食习惯
                case "isDiet_text":
                    tagValue = model.isDiet_text;
                    content = model.isDiet_text;
                    break;
                //吸烟情况
                case "isSmoking_text":
                    tagValue = model.isSmoking_text;
                    content = model.isSmoking_text;
                    break;
                case "smokeAge":
                    tagValue = model.smokeAge;
                    content = model.smokeAge;
                    break;
                case "smokeAmount":
                    tagValue = model.smokeAmount;
                    content = model.smokeAmount;
                    break;
                //饮酒情况
                case "isDrinking_text":
                    tagValue = model.isDrinking_text;
                    content = model.isDrinking_text;
                    break;
                case "drinkingAmount1":
                    tagValue = model.drinkingAmount1;
                    content = model.drinkingAmount1;
                    break;
                case "drinkingAmount2":
                    tagValue = model.drinkingAmount2;
                    content = model.drinkingAmount2;
                    break;
                //戒酒情况
                case "isDryOut_text":
                    tagValue = model.isDryOut_text;
                    content = model.isDryOut_text;
                    break;
                case "dryOutDate":
                    tagValue = model.dryOutDate;
                    content = model.dryOutDate;
                    break;
                //饮酒种类
                case "isWine_text":
                    tagValue = model.isWine_text;
                    content = model.isWine_text;
                    break;
                //运动状况调查
                case "isQ1_text":
                    tagValue = model.isQ1_text;
                    content = model.isQ1_text;
                    break;
                case "isQ2_text":
                    tagValue = model.isQ2_text;
                    content = model.isQ2_text;
                    break;
                case "isQ3_text":
                    tagValue = model.isQ3_text;
                    content = model.isQ3_text;
                    break;
                case "isQ4_text":
                    tagValue = model.isQ4_text;
                    content = model.isQ4_text;
                    break;
                case "isQ5_text":
                    tagValue = model.isQ5_text;
                    content = model.isQ5_text;
                    break;
                case "isQ6_text":
                    tagValue = model.isQ6_text;
                    content = model.isQ6_text;
                    break;
                case "isQ7_text":
                    tagValue = model.isQ7_text;
                    content = model.isQ7_text;
                    break;
                case "isQ8_text":
                    tagValue = model.isQ8_text;
                    content = model.isQ8_text;
                    break;

                //运动历程
                case "exeYears":
                    tagValue = model.exeYears;
                    content = model.exeYears;
                    break;
                //每次运动时间
                case "exeMinites":
                    tagValue = model.exeMinites;
                    content = model.exeMinites;
                    break;
                //运动频率
                case "isRate_text":
                    tagValue = model.isRate_text;
                    content = model.isRate_text;
                    break;
                //运动时间段
                case "isTime_text":
                    tagValue = model.isTime_text;
                    content = model.isTime_text;
                    break;
                //运动场所
                case "isPlace_text":
                    tagValue = model.isPlace_text;
                    content = model.isPlace_text;
                    break;
                //体质测试报告
                case "Score_1":
                    tagValue = model.Score_1;
                    content = model.Score_1;
                    break;
                case "Score_2":
                    tagValue = model.Score_2;
                    content = model.Score_2;
                    break;
                case "Score_3":
                    tagValue = model.Score_3;
                    content = model.Score_3;
                    break;
                case "Score_4":
                    tagValue = model.Score_4;
                    content = model.Score_4;
                    break;
                case "Score_5":
                    tagValue = model.Score_5;
                    content = model.Score_5;
                    break;
                case "Score_6":
                    tagValue = model.Score_6;
                    content = model.Score_6;
                    break;
                case "Score_7":
                    tagValue = model.Score_7;
                    content = model.Score_7;
                    break;
                case "Score_8":
                    tagValue = model.Score_8;
                    content = model.Score_8;
                    break;
                case "Score_9":
                    tagValue = model.Score_9;
                    content = model.Score_9;
                    break;
                case "Score_10":
                    tagValue = model.Score_10;
                    content = model.Score_10;
                    break;
                case "Score_11":
                    tagValue = model.Score_11;
                    content = model.Score_1;
                    break;
                case "Score_12":
                    tagValue = model.Score_12;
                    content = model.Score_12;
                    break;
                //
                case "BMI":
                    tagValue = model.BMI;
                    content = model.BMI;
                    break;  
                //
                case "TotalRate":
                    tagValue = model.TotalRate;
                    content = model.TotalRate;
                    break; 
                case "Height":
                    tagValue = model.Height;
                    content = model.Height;
                    break; 
                case  "Weight":
                    tagValue = model.Weight;
                    content = model.Weight;
                    break;
                case "Muscle":
                    tagValue = model.Muscle;
                    content = model.Muscle;
                    break;
                case "BodyFat":
                    tagValue = model.BodyFat;
                    content = model.BodyFat;
                    break;
                case "Ill_History":
                    tagValue = model.Ill_History;
                    content = model.Ill_History;
                    break; 
               
                case "BMI_Min":
                    tagValue = model.BMI_Min;
                    content = model.BMI_Min;
                    break; 
                case  "BMI_Max":
                    tagValue = model.BMI_Max;
                    content = model.BMI_Max;
                    break;
                case "Weight_Min":
                    tagValue = model.Weight_Min;
                    content = model.Weight_Min;
                    break;
                case "Weight_Max":
                    tagValue = model.Weight_Max;
                    content = model.Weight_Max;
                    break; 
                
                case "Muscle_Min":
                    tagValue = model.Muscle_Min;
                    content = model.Muscle_Min;
                    break; 
                case  "Muscle_Max":
                    tagValue = model.Muscle_Max;
                    content = model.Muscle_Max;
                    break;
                case "BodyFat_Min":
                    tagValue = model.BodyFat_Min;
                    content = model.BodyFat_Min;
                    break;
                case "BodyFat_Max":
                    tagValue = model.BodyFat_Max;
                    content = model.BodyFat_Max;
                    break;            

            }

            //switch (tagPlaceHolderValue)
            //{
            //    case PlaceholderNonRecursiveA:
            //        tagValue = ((openXmlElementDataContext.DataContext) as Vendor).Id.ToString();
            //        content = ((openXmlElementDataContext.DataContext) as Vendor).Name;
            //        break;
            //    case PlaceholderNonRecursiveB:
            //        tagValue = ((openXmlElementDataContext.DataContext) as Item).Id.ToString();
            //        content = ((openXmlElementDataContext.DataContext) as Item).Name;
            //        break;
            //    case PlaceholderNonRecursiveC:
            //        tagValue = ((openXmlElementDataContext.DataContext) as Order).Id.ToString();
            //        content = ((openXmlElementDataContext.DataContext) as Order).Name;
            //        break;
            //    case PlaceholderNonRecursiveD:
            //        tagValue = ((openXmlElementDataContext.DataContext) as Order).Id.ToString();
            //        content = ((openXmlElementDataContext.DataContext) as Order).Description;
            //        break;
            //}

            // Set the tag for the content control
            if (!string.IsNullOrEmpty(tagValue))
            {
                this.SetTagValue(openXmlElementDataContext.Element as SdtElement, GetFullTagValue(tagPlaceHolderValue, tagValue));
            }

            // Set text without data binding
            this.SetContentOfContentControl(openXmlElementDataContext.Element as SdtElement, content);
        }
    }
}
