﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    /// <summary>
    /// created by adam 2015-08-01 the service of machine module
    /// database table: data_machine 
    /// associate tables: data_dictionary, data_machine_type
    /// </summary>
    public class MachineService : BusinessService
    {
        public IQueryable<data_machine> ListMachines()
        {
            return from item in DataContext.data_machine
                   select item;
        }

        public async Task<data_machine> Add(data_machine entity)
        {
            if (Validator.IsValid)
            {
                DataContext.data_machine.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_machine> Update(data_machine entity)
        {
            var service = DataContext.data_machine.FirstOrDefault(x => x.data_machine_id == entity.data_machine_id);

            if (service != null)
            {
                service.data_machine_name = entity.data_machine_name;
                service.data_machine_type_id = entity.data_machine_type_id;               
                service.data_machine_status = entity.data_machine_status;
                service.dateupdated = entity.dateupdated;
                service.updatedby = entity.updatedby;
                
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_machine Get(int id)
        {
            return DataContext.data_machine.SingleOrDefault(x => x.data_machine_id == id);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_machine.SingleOrDefaultAsync(x => x.data_machine_id == id);
            DataContext.data_machine.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public IQueryable<SelectListItem> ListStatus()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_machine"
                   select new SelectListItem() {Text = item.data_dictionary_text, Value = item.data_dictionary_id.ToString() };
        }

        public IQueryable<SelectListItem> ListMachineTypes()
        {
            return from item in DataContext.data_machine_type
                   where item.dateremoved == null
                   select new SelectListItem() { Text = item.data_machine_type_title, Value = item.data_machine_type_id.ToString() };
        }
    }
}
