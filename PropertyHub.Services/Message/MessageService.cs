
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PropertyHub.Data;

namespace PropertyHub.Services
{
    /// <summary>
    /// added by Tsing on 8/5/2015
    /// </summary>
    public class MessageService : BusinessService
    {

        public data_message GetMessageById(int message_id)
        {
            var message = DataContext.data_message.FirstOrDefault(a => a.data_message_id == message_id);
            return message;

        }

        public bool UpdateMessage(int message_id, bool isfinished, string comments,string userId)
        {
            try
            {
                var message = DataContext.data_message.FirstOrDefault(a => a.data_message_id == message_id);
                if (message == null)
                    return false;

                message.message_Isreply = isfinished;
                message.message_comments = comments;
                message.message_comment_date = DateTime.Now;
                DataContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {

                return false;
            }
         



        }



        public IQueryable<SelectListItem> ListMessageTypes()
        {
            return from item in DataContext.data_dictionary
                   where item.data_dictionary_type == "data_message"
                   select new SelectListItem()
                   {
                       Text = item.data_dictionary_text,
                       Value = item.data_dictionary_id.ToString()
                   };
        }

        public Result<int> CreateMessage(int member_id, int message_type, string message_text, bool isformmember)
        {
            //首先寻找这个会员的健身秘书，
            var res = new Result<int>();

            try
            {

                //Tsing
                //检查一下当前会员是否有有效的服务，如果没有激活，或者所有的服务都过期了，则无法留言

                var findservice = DataContext.data_code_services.Where(a => a.data_member_id == member_id).ToList();
                if (findservice.Count == 0)
                {
                    res.IsSuccessful = false;
                    res.Message = "会员账户未激活，不能留言.";
                    res.Parameter = -1;
                    return res;
                }

                var allrecords = DataContext.data_code_services.Where(a => a.data_member_id == member_id);

                var findrecords = DataContext.data_code_services.Where(a => a.data_member_id == member_id
                                                                        && 
                                                                        (
                                                                        DateTime.Now < a.dateStarted
                                                                        || 
                                                                        DateTime.Now > a.dateEnd
                                                                        )
                    );

                if (findrecords.Count() == allrecords.Count())
                {
                    res.IsSuccessful = false;
                    res.Message = "所有服务已经到期,不能留言.";
                    res.Parameter = -1;
                    return res;
                }





                string  serviceId = "";

                var finduser = DataContext.data_member_secretary.OrderByDescending(a=>a.datecreated).FirstOrDefault
                    (a => a.data_member_id == member_id   //当前会员
                  //  && a.data_member_secretary_status == 1  //有效
                    );






                if (finduser != null)
                {
                    serviceId = finduser.data_member_service_id;
                }
                else
                {
                    //当前会员还没有分配健康秘书，则随便找一个。
                    //var randomuser =
                    //    DataContext.data_member_secretary
                    //    .Where(a => a.data_member_secretary_status == 1) //只有有效的
                    //    .Select(a => a.UserId)
                    //    .Distinct().FirstOrDefault();
                    //serviceId = randomuser==null?"580c37fb-a60c-4bcb-8b04-0c0543ed81bb":randomuser;

                    serviceId = "580c37fb-a60c-4bcb-8b04-0c0543ed81bb";
                }

                var newmessage=new data_message();
                newmessage.message_type_id=message_type;
                newmessage.message_member_id=member_id;
                newmessage.message_service_id =serviceId;
                newmessage.message_service_name=string.Empty;
                newmessage.message_text =message_text;
                newmessage.message_isread =false;
                newmessage.message_date =DateTime.Now;
                newmessage.message_status =0;
                newmessage.message_from_member =isformmember;
                newmessage.message_Isreply=false;
                
                DataContext.data_message.Add(newmessage);

                DataContext.SaveChanges();
                
                res.IsSuccessful =true;
                res.Parameter =newmessage.data_message_id;
                res.Message =string.Empty;



            }
            catch (Exception ex)
            {
                res.IsSuccessful =false;
                res.Parameter =-1;
                res.Message =ex.Message;
            }

            return res;
            
        }

        

        public IQueryable<data_message> GetAllMessages()
        {
            return DataContext.data_message.AsQueryable();
        }


        /// <summary>
        /// 这个地方认为是200
        /// </summary>
        /// <param name="member_id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Result<int> GetUnReadCount(int member_id, int? type)
        {
            var res = new Result<int>();
            try
            {

                //获得当前会员已经读取的消息.
                var readmessages = DataContext.data_member_message_log.Where(a => 
                    a.data_member_id == member_id
                    && a.data_message_type == type
                    ).Select(a=>a.data_message_id).ToList();


                var count = DataContext.data_message.Where(a =>
                    a.message_member_id == member_id  //当前会员的
                    && readmessages.Contains(a.data_message_id) == false //当前用户没有看到
                    && a.message_Isreply
                    ).Count();


                res.IsSuccessful = true;
                res.Parameter = count;
                res.Message = string.Empty;


            }
            catch (Exception exception)
            {
                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = 0;

            }
            return res;
        }


        public List<int> GetReadMessages(int data_member_id)
        {
            return DataContext.data_member_message_log
                .Where(a => a.data_member_id == data_member_id
                            && a.data_message_type == 200
                ).Select(a => a.data_message_id.Value).ToList();
        }


        public Result<List<data_message>> GetMessages(int member_id, int? type,int pagesize,int pagenumber)
        {

            var res = new Result<List<data_message>>();
            try
            {
                //按照ID 倒序
                var messages = DataContext.data_message
                    .Where(a => a.message_member_id == member_id).OrderByDescending(a=>a.data_message_id).ToList();

                if (type.HasValue)
                {
                    messages = messages.Where(a => a.message_type_id == type.Value).ToList();
                }

                //if (toprows.HasValue)
                //{
                //    messages = messages.ToList().Take(toprows.Value).ToList();
                //}



                res.recordcount = messages.Count;

                res.IsSuccessful = true;
                res.Parameter = messages.Skip((pagenumber -1)*pagesize).Take(pagesize).ToList();
                res.Message = string.Empty;

            }
            catch (Exception ex)
            {
                res.IsSuccessful = false;
                res.Message = ex.Message;
                res.Parameter = new List<data_message>();
            }

           
            return res;
        }


        public Result<data_message> GetMessageInformationById(int data_message_id,int data_member_id)
        {
            var res = new Result<data_message>();

            try
            {
                var message = this.GetMessageById(data_message_id);


                //增加一条读取日志。


                var findrecord = DataContext.data_member_message_log.FirstOrDefault(a =>

                    a.data_member_id == data_member_id
                    &&
                    a.data_message_id == data_message_id
                    &&
                    a.data_message_type == 200
                    );

                //只有对已经回复了的，才增加日志。
                if (findrecord == null && message.message_Isreply)
                {

                    var newlog = new data_member_message_log
                    {
                        data_message_type = 200,
                        data_message_id = data_message_id,
                        data_member_id = data_member_id,
                        data_member_message_log_date = DateTime.Now
                    };
                    DataContext.data_member_message_log.Add(newlog);
                    DataContext.SaveChanges();
                }



                res.IsSuccessful = true;
                res.Parameter = message;
                res.Message = string.Empty;

            }
            catch (Exception exception)
            {

                res.IsSuccessful = false;
                res.Message = exception.Message;
                res.Parameter = null;
            }

            return res;


        }


    }
}

