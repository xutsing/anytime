﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;

namespace PropertyHub.Services
{
    public class MemberTestingService : BusinessService
    {
        public IQueryable<data_member_testing> ListMemberTesting()
        {
            return from item in DataContext.data_member_testing
                   select item;
        }


        public data_member_testing GetTesting(int member_id, int testingId)
        {
            return
                DataContext.data_member_testing.FirstOrDefault(
                    a => a.data_member_id == member_id && a.data_questions_testing_id == testingId);
        }

        public async Task<data_member_testing> Add(data_member_testing entity)
        {
            if (Validator.IsValid)
            {
                entity.datecreated = DateTime.Now;
                entity.dateupdated = DateTime.Now;

                DataContext.data_member_testing.Add(entity);
                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public async Task<data_member_testing> Update(data_member_testing entity)
        {
            var memberTesting = DataContext.data_member_testing.FirstOrDefault(x => x.data_member_testing_id == entity.data_member_testing_id);

            if (memberTesting != null)
            {
                memberTesting.data_questions_testing_result_json = entity.data_questions_testing_result_json;
                memberTesting.data_questions_testing_remark = entity.data_questions_testing_remark;
               // entity.datecreated = DateTime.Now;
                memberTesting.dateupdated = DateTime.Now;

                await DataContext.SaveChangesAsync();
            }

            return entity;
        }

        public data_member_testing Get(int memberTestingId)
        {
            return DataContext.data_member_testing.SingleOrDefault(x => x.data_member_testing_id == memberTestingId);
        }

        public async Task Delete(int id)
        {
            var entity = await DataContext.data_member_testing.SingleOrDefaultAsync(x => x.data_member_testing_id == id);
            DataContext.data_member_testing.Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public data_questions_testing GetTesting(int testingId)
        {
            return DataContext.data_questions_testing.Single(x => x.data_questions_testing_id == testingId);
        }

    }
}
