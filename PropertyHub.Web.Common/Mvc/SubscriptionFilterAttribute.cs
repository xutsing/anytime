﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace PropertyHub.Web.Common.Mvc
{
    public class SubscriptionFilterAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var fullAddress = filterContext.HttpContext.Request.Headers["Host"].Split('.');
            if (fullAddress.Length >= 2)
            {
                var subscriptionSubdomain = fullAddress[0];

                var cacheKey = string.Format("Subscription-{0}", subscriptionSubdomain);
                var cache = filterContext.HttpContext.Cache;
                var cacheValue = cache.Get(cacheKey);
                if (cacheValue == null)
                {
                    Subscription subscription = null;
                    using (var dbContext = new ApplicationDbContext())
                    {
                        subscription = dbContext.Subscriptions.FirstOrDefault(x => x.Subdomain == subscriptionSubdomain);
                    }

                    if (subscription == null)
                    {
                        filterContext.Result = new HttpStatusCodeResult(404);
                    }
                    else
                    {
                        filterContext.RouteData.Values.Add("Subscription", subscription.Id);
                        cache.Insert(cacheKey, subscription.Id, null, DateTime.Now.AddMinutes(60), Cache.NoSlidingExpiration);
                    }
                }
                else if (!filterContext.RouteData.Values.ContainsKey("Subscription"))
                {
                    filterContext.RouteData.Values.Add("Subscription", cacheValue);
                }
            }
            else
            {
                filterContext.Result = new HttpStatusCodeResult(404);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}