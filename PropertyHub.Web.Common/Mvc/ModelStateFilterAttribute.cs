﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace PropertyHub.Web.Common.Mvc
{
    public class ModelStateFilterAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RouteData.Values.Add("ValidationModelState", filterContext.Controller.ViewData.ModelState);

            base.OnActionExecuting(filterContext);
        }
    }

}