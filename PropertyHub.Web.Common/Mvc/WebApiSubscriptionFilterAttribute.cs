﻿using PropertyHub.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Caching;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace PropertyHub.Web.Common.Mvc
{
    public class WebApiSubscriptionFilterAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var fullAddress = actionContext.Request.Headers.GetValues("Host").First().Split('.');
            if (fullAddress.Length >= 2 && actionContext.RequestContext.RouteData.Values.ContainsKey("Subscription") == false)
            {
                var subscriptionSubdomain = fullAddress[0];

                var cacheKey = string.Format("Subscription-{0}", subscriptionSubdomain);
                var cache = MemoryCache.Default;
                var cacheValue = cache.Get(cacheKey);
                if (cacheValue == null)
                {
                    Subscription subscription = null;
                    using (var dbContext = new ApplicationDbContext())
                    {
                        subscription = dbContext.Subscriptions.FirstOrDefault(x => x.Subdomain == subscriptionSubdomain);
                    }

                    if (subscription == null)
                    {
                        //actionContext.Response.Content = new HttpResponse new System.Web.Mvc.HttpStatusCodeResult(404);
                    }
                    else
                    {
                        actionContext.RequestContext.RouteData.Values.Add("Subscription", subscription.Id);
                        cache.Add(cacheKey, subscription.Id, DateTime.Now.AddMinutes(60));
                    }
                }
                else
                {
                    actionContext.RequestContext.RouteData.Values.Add("Subscription", cacheValue);
                }
            }
            else
            {
                //actionContext.Result = new HttpStatusCodeResult(404);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}