﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace PropertyHub.Web.Public.Models
{
    public class AnytimeFriendDTO
    {
      //  [Key]
      //  [DefaultValue(0)]
        public Nullable<int> data_anytime_friend_id { get; set; }

        [Required(ErrorMessage = "年龄不能为空")]

        [Range(0, 200, ErrorMessage = "年龄格式不对")]

        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "年龄只能为数字")]
      
        public int data_age { get; set; }

        [Required(ErrorMessage = "省不能为空")]
        public Nullable<int> data_province_Id { get; set; }
         
        public string data_province_title { get; set; }
        [Required(ErrorMessage = "市不能为空")]
        public Nullable<int> data_city_Id { get; set; }
     
        public string data_city_title { get; set; }
        [Required(ErrorMessage = "区域不能为空")]
        public Nullable<int> data_area_Id { get; set; }
         
        public string data_area_title { get; set; }
         [Required(ErrorMessage = "姓名不能为空")]
        public string data_name { get; set; }

        [Required(ErrorMessage = "性别不能为空")]
        public Nullable<int> data_sex_Id { get; set; }
         
        public string data_sex_title { get; set; }
         [Required(ErrorMessage = "职业不能为空")]
        public string data_profession { get; set; }
        
        public string data_comments { get; set; }
         [Required(ErrorMessage = "电话不能为空")]

        public string data_Ip { get; set; }

        
         public bool IsChecked { get; set; }
    }
}