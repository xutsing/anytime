﻿using PropertyHub.Web.Common.Mvc;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.Public
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ModelStateFilterAttribute());
        }
    }
}
