﻿using PropertyHub.Data;
using PropertyHub.Services;
using PropertyHub.Web.Public.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyHub.Web.Public.Controllers
{
    [AllowAnonymous]
    public class RegisterController : Controller
    {
        private readonly MembershipService _membershipService;
        private readonly SubscriptionService _subscriptionService;

        public RegisterController(MembershipService membershipService, SubscriptionService subscriptionService)
        {
            _membershipService = membershipService;
            _subscriptionService = subscriptionService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(RegisterAsSubscriptionModel model)
        {
            //await _subscriptionService.Add(subscription);
            model.User.UserName = model.User.Email;

            await _membershipService.RegisterAsSubscription(model.User, model.Subscription, model.Password, model.ConfirmPassword);
            if (ModelState.IsValid)
            {
                ModelState.AddModelError("", "Success!");
            }
            return View(model);
        }
    }
}