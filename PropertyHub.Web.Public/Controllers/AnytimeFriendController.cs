﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyHub.Data;
using System.Data.Entity;
using System.Web.Mvc;
using PropertyHub.Services;
using PropertyHub.Web.Public.Models;

namespace PropertyHub.Web.Public.Controllers
{
    public class AnytimeFriendController : Controller
    {
        private readonly AnytimeFriendService _service;
        public AnytimeFriendController(AnytimeFriendService service)
        {
            _service = service;
        }
        public ActionResult Index()
        {
            ViewBag.Provice = _service.ListProvice();
            ViewBag.Sex = _service.ListSex();
            ViewBag.City = _service.ListBeiJingProvice();
            ViewBag.Area = _service.ListBeiJingArea();

            return View();
        }

        public ActionResult Index2()
        {
            ViewBag.Provice = _service.ListProvice();
            ViewBag.Sex = _service.ListSex();

            return View();
        }

        public ActionResult Success()
        {
            return View();
        }
        public ActionResult Agreement()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(AnytimeFriendDTO model)
        {

            ViewBag.Provice = _service.ListProvice();
            ViewBag.Area = _service.ListBeiJingArea();
            ViewBag.Sex = _service.ListSex();
            ViewBag.City = _service.ListBeiJingProvice();

            if (model.IsChecked == false)
            {
                ModelState.AddModelError("IsChecked", "必须选中");
                return View(model);
            }

          
            if (ModelState.IsValid)
            {
                await _service.Add(model.data_age, model.data_province_Id, model.data_city_Id, model.data_area_Id, model.data_name,
          model.data_sex_Id, model.data_profession, model.data_comments, model.data_Ip);

                ModelState.AddModelError("", "Success!");
                return View("Success");
            }

            return View(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public string  GetCity(String ID)
        {
            //List<KeyValuePair<string,string>> items = new List<KeyValuePair<string, string>>();
            var citys = _service.ListCity(ID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(citys);
        }



        public string GetArea(String ID)
        {
            //List<KeyValuePair<string,string>> items = new List<KeyValuePair<string, string>>();
            var areas = _service.ListArea(ID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(areas);
        }
    }
}